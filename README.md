Branches:  
master: [![pipeline status](https://gitlab.com/jebradbury39/Jasmint-JavaHost/badges/master/pipeline.svg)](https://gitlab.com/jebradbury39/Jasmint-JavaHost/commits/master)  
dev: [![pipeline status](https://gitlab.com/jebradbury39/Jasmint-JavaHost/badges/dev/pipeline.svg)](https://gitlab.com/jebradbury39/Jasmint-JavaHost/commits/dev)  

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.jasmint%3AJasmint&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.jasmint%3AJasmint)


# Jasmint

This repository is meant to create a library, not an executable. This library provides tools to parse, interpret, and serialize/deserialize the Jasmint language in its AST form. Numerous utility functions are provided with the goal of helping others to write Jasmint modules with minimal hassle.

### Building

This and all related repositories rely on the ANT build system (they all come with build.xml).

1. Download or clone this repository
2. Run `ant` (this generates `jasmint.jar` in the `dist/` folder)
3. Before building any other Jasmint project, copy `jasmint.jar` into that project's `lib` folder
