package multifile;

import ast.SerializationError;
import audit.AuditEntry.AuditEntryType;
import audit.AuditEntryLite;
import audit.AuditRenameTypesLite;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import typecheck.ModuleType;

/*
 * A module can be loaded in RAM, or stored on disk in some build directory.
 * This interface seeks to abstract over that, so the optimization can be hidden.
 * 
 * ProjectAssemble will multi-thread parse all sources that need to be rebuilt and load them into
 * memory. It will also find all sources that do not need to be rebuilt and create StoredModules
 * for the built files.
 */
public abstract class ModuleHandle {
  public final String srcFilename; //abs path
  protected final ModuleType moduleType; //abs module path
  protected final Set<ModuleType> submods; //abs module paths (our children mods)
  public final BuildManifest manifest; //needed for load/store
  
  /*
   * True if the import is a hard dependency
   * 1) First used for determining if we need to rebuild from scratch
   * 2) Used for determining module ordering (ignore soft imports)
   * 
   * Once calculated, no need to keep recalculating
   * 
   * Same pointer to ModuleBuildObject.effectiveImports, but we can largely ignore h_imports.
   */
  protected EffectiveImports effectiveImports;
  //this becomes false after we regenerate, link headers, and determine mod order
  public boolean needRebuild;
  
  public ModuleHandle(String srcFilename, ModuleType moduleType, Set<ModuleType> submods,
      EffectiveImports effectiveImports, boolean needRebuild, BuildManifest manifest) {
    this.srcFilename = srcFilename;
    this.moduleType = moduleType;
    this.submods = submods;
    this.effectiveImports = effectiveImports;
    this.needRebuild = needRebuild;
    this.manifest = manifest;
  }
  
  //load into RAM (nop if already in RAM)
  public abstract ModuleHandle load(Project project, MsgState msgState)
      throws FatalMessageException, IOException, SerializationError;
  
  //store onto disk (nop if already on disk)
  public abstract ModuleHandle store(MsgState msgState)
      throws FatalMessageException, IOException, SerializationError;

  public static ModuleType convertModuleType(ModuleType realType, AuditTrailLite auditTrail,
      MsgState msgState)
      throws FatalMessageException {
    for (AuditEntryLite entry : auditTrail.values()) {
      if (entry.getType() == AuditEntryType.RENAME_TYPES) {
        Nullable<ModuleType> tmp = ((AuditRenameTypesLite) entry).getNewType(realType);
        if (tmp.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAME_TYPES, "Cannot find type");
        }
        realType = tmp.get();
      }
    }
    return realType;
  }
  
  public ModuleType getModuleType(AuditTrailLite auditTrail, MsgState msgState)
      throws FatalMessageException {
    return convertModuleType(moduleType, auditTrail, msgState);
  }
  
  public Set<ModuleType> getSubmods(AuditTrailLite auditTrail, MsgState msgState)
      throws FatalMessageException {
    Set<ModuleType> newSubmods = new HashSet<>();
    for (ModuleType submod : submods) {
      newSubmods.add(convertModuleType(submod, auditTrail, msgState));
    }
    return newSubmods;
  }
  
  public EffectiveImports getImports(AuditTrailLite auditTrail, MsgState msgState)
      throws FatalMessageException {
    return effectiveImports.applyTransforms(auditTrail, msgState);
  }
}
