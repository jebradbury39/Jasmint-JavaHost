package multifile;

import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

public class ProjectGraph {
  
  public static class ProjectGraphNode {
    public final Project project;
    
    Map<ProjectNameVersion, ProjectGraphNode> desiredImports = new HashMap<>();
    Set<ProjectNameVersion> exportedProjects = new HashSet<>();
    
    public ProjectGraphNode(Project project) {
      this.project = project;
    }
    
    @Override
    public String toString() {
      return desiredImports.keySet().toString();
    }

    public void indexExportedLibs() throws FatalMessageException {
      exportedProjects = project.findExportedProjects();
    }

    public boolean validateLibs(MsgState msgState) throws FatalMessageException {
      
      for (ProjectGraphNode impProj : desiredImports.values()) {
        if (!validateLibSets(desiredImports.keySet(), impProj.exportedProjects, msgState)) {
          return false;
        }
      }
      
      return true;
    }

    private static boolean validateLibSets(Set<ProjectNameVersion> desiredLibs,
        Set<ProjectNameVersion> exportedLibs, MsgState msgState) throws FatalMessageException {
      
      for (ProjectNameVersion desiredLib : desiredLibs) {
        for (ProjectNameVersion exportedLib : exportedLibs) {
          if (exportedLib.name.equals(desiredLib.name)) {
            //if same lib being imported by us and our dependency, it must be exact same version
            if (!exportedLib.equals(desiredLib)) {
              msgState.addMessage(MsgType.ERROR, MsgClass.PROJECT_ASSEMBLE,
                  "Cannot use both " + exportedLib + " and " + desiredLib);
              return false;
            }
          }
        }
      }
      return true;
    }

    public boolean checkForCycle(List<ProjectNameVersion> path) {
      boolean hitCycle = path.contains(project.projectNameVersion);
      path.add(project.projectNameVersion);
      if (hitCycle) {
        return true;
      }
      
      for (ProjectGraphNode impProj : desiredImports.values()) {
        if (impProj.checkForCycle(path)) {
          return true;
        }
      }
      
      path.remove(path.size() - 1);
      return false;
    }
  }

  Map<ProjectNameVersion, ProjectGraphNode> nodes = new HashMap<>();
  
  public ProjectGraph(Map<ProjectNameVersion, Project> projectMap) {
    for (Project proj : projectMap.values()) {
      addNode(projectMap, proj, true);
    }
  }

  private ProjectGraphNode addNode(final Map<ProjectNameVersion, Project> projectMap,
      Project project, boolean addLinks) {
    /*
     * Add us if we don't exist
     */
    ProjectGraphNode node = nodes.get(project.projectNameVersion);
    if (node == null) {
      node = new ProjectGraphNode(project);
      nodes.put(project.projectNameVersion, node);
    }
    
    if (addLinks) {
      /*
       * Add links to our desired imports. If they do not exist in the collection, then error
       */
      for (ProjectNameVersion desiredLib : project.desiredLibs) {
        addLinkTo(node, projectMap, desiredLib);
      }
    }
    
    return node;
  }

  private boolean addLinkTo(ProjectGraphNode node, Map<ProjectNameVersion, Project> projectMap,
      ProjectNameVersion desiredLib) {
    /*
     * Check if the desired lib exists among the options. If multiple candidates, then choose the
     * best match (e.g. highest version, or equals)
     */
    Project foundMatch = null;
    for (Entry<ProjectNameVersion, Project> entry : projectMap.entrySet()) {
      if (entry.getKey().compare(desiredLib)) {
        foundMatch = entry.getValue();
        break;
      }
    }
    
    if (foundMatch == null) {
      //error
      return false;
    }
    
    /*
     * Create the node if it does not exist (but don't go for libs yet)
     */
    ProjectGraphNode libNode = addNode(projectMap, foundMatch, false);
    node.desiredImports.put(libNode.project.projectNameVersion, libNode);
    return true;
  }
  
  public Optional<List<Project>> validateExportedLibs(ProjectNameVersion rootProj,
      MsgState msgState) throws FatalMessageException {
    
    
    /*
     * Go through projects in order top to bottom and ensure that if we import a lib, we can
     * handle their implicit exported projects as well
     */
    for (ProjectGraphNode node : nodes.values()) {
      node.indexExportedLibs();
    }
    
    for (ProjectGraphNode node : nodes.values()) {
      /*
       * Check all exported libs and make sure that there are no collisions
       */
      if (!node.validateLibs(msgState)) {
        return Optional.empty();
      }
    }
    
    return Optional.of(linkProjects(rootProj, msgState)); //so we have the libs properly set up
  }
  
  public boolean checkForCycle(MsgState msgState) throws FatalMessageException {
    
    List<ProjectNameVersion> path = new LinkedList<>();
    for (ProjectGraphNode node : nodes.values()) {
      
      if (!path.isEmpty()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE, "path must be empty");
      }
      
      if (node.checkForCycle(path)) {
        if (path.isEmpty()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE, "path cannot be empty");
        }
        msgState.addMessage(MsgType.ERROR, MsgClass.PROJECT_ASSEMBLE,
            "Found cycle in project dependencies: " + path);
        return true;
      }
    }
    
    if (!path.isEmpty()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE, "path must be empty");
    }
    return false;
  }
  
  /*
   * Return in BFS order (so that all projects without any dependencies go first,
   * then their children...
   */
  private List<Project> linkProjects(ProjectNameVersion rootProj, MsgState msgState)
      throws FatalMessageException {
    List<Project> rootProjLibs = new LinkedList<>();
    Set<ProjectNameVersion> foundLibs = new HashSet<>();
    
    //exclude the rootProj from the list of libs
    ProjectGraphNode rootProjNode = null;
    while (rootProjLibs.size() != nodes.values().size() - 1) {
      for (ProjectGraphNode proj : nodes.values()) {
        if (proj.project.projectNameVersion.equals(rootProj)) {
          rootProjNode = proj;
          continue;
        }
        if (foundLibs.contains(proj.project.projectNameVersion)) {
          continue;
        }
        //verify that all our imports have already been processed
        boolean foundAllImports = true;
        for (ProjectGraphNode impProj : proj.desiredImports.values()) {
          if (impProj.project.projectNameVersion.equals(rootProj)) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.PROJECT_ASSEMBLE,
                "Project " + proj.project + " is not allowed to import root project " + rootProj);
          }
          
          if (!foundLibs.contains(impProj.project.projectNameVersion)) {
            foundAllImports = false;
            break;
          }
          //add link in libs
          proj.project.addLib(impProj.project, msgState);
        }
        if (!foundAllImports) {
          continue;
        }
        
        rootProjLibs.add(proj.project);
        foundLibs.add(proj.project.projectNameVersion);
      }
      
    }
    
    //link in root proj libs (for lookups)
    for (ProjectGraphNode impProj : rootProjNode.desiredImports.values()) {
      rootProjNode.project.addLib(impProj.project, msgState);
    }
    
    return rootProjLibs;
  }
}
