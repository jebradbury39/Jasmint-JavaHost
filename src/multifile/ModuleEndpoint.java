package multifile;

import ast.SerializationError;
import audit.AuditTrailLite;
import classgraph.ClassGraph;
import classgraph.ModuleNode;
import classgraph.Node;
import classgraph.Node.ClassUsageLink;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ProgHeader;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import import_mgmt.ImportGraph;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import type_env.TypeEnvironment;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ResolvedImportType;
import util.Pair;

/*
 * Wrap around the module handle to allow for easy swapping of the handle
 */

public class ModuleEndpoint {
  private ModuleHandle handle; //can be loaded or stored
  public Project project = null; //set this immediately
  public final MsgState msgState; //from the project set of msgStates
  
  public ModuleEndpoint(ModuleHandle handle, MsgState msgState) {
    this.handle = handle;
    this.msgState = msgState;
  }
  
  private void sanityCheck() {
    if (project == null) {
      throw new IllegalArgumentException("ModuleEndpoint.project is null");
    }
  }
  
  public ModuleHandle getHandle() {
    sanityCheck();
    return handle;
  }
  
  public ModuleType getModuleType(AuditTrailLite auditTrail) throws FatalMessageException {
    sanityCheck();
    return handle.getModuleType(auditTrail, msgState);
  }
  
  public void load() throws FatalMessageException {
    sanityCheck();
    try {
      this.handle = handle.load(project, msgState);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SerializationError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  public void store() throws FatalMessageException, IOException, SerializationError {
    sanityCheck();
    this.handle = handle.store(msgState);
  }
  
  public Node populateClassGraph(Project project, ClassGraph classGraph)
      throws FatalMessageException {
    load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) handle;
    EffectiveImports effectiveImports = moduleHandle.effectiveImports.applyTransforms(
        project.getAuditTrail(), msgState);
    Node modNode = classGraph.addModuleNode(getModuleType(project.getAuditTrail()));
    
    ProgHeader newProg = moduleHandle.moduleBuildObj.progHeader.applyTransforms(project,
        project.getAuditTrail(), moduleHandle.moduleBuildObj,
        moduleHandle.registeredGenerics, msgState);
    
    for (ClassHeader ch : newProg.classes) { 
      //first add us to the graph
      Node node = classGraph.addClassNode(ch.absName);
      //if no parent, then just continue
      if (ch.getParent().isNull()) {
        continue;
      }
      //add an uplink to the parent
      node.populateUplinks(classGraph, effectiveImports, ch.getParent().get(), ImportUsage.HARD);
      if (ch.getParent().get().outerType.isNotNull()) {
        //also add a HARD uplink to parent module
        modNode.addModuleUplink(classGraph, ch.getParent().get().outerType.get(), ImportUsage.HARD);
      }
    }
    
    return modNode;
  }
  
  /*
   * Only involves the parent-child classes (since we need this order for linkImports)
   * If includeFields is true, then using it for full module dependency graph (rebuild)
   */
  public void populateClassGraphPlus(Project project, ClassGraph classGraph, boolean includeFields)
      throws FatalMessageException {
    sanityCheck();
    final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);
    
    if (!handle.needRebuild) {
      EffectiveImports effectiveImports = handle.getImports(project.getAuditTrail(),
          msgState);
      Node node = classGraph.addModuleNode(getModuleType(project.getAuditTrail()));
      //imports also contains parent and submods (to know if they are hard/soft)
      for (Entry<ImportType, EffectiveImport> impMod
          : effectiveImports.getImportMap(true, filterImpTypes).entrySet()) {
        if (impMod.getValue().getUsage() != ImportUsage.HARD && includeFields) {
          //rebuild on soft import, but do not reorder on soft import
          continue;
        }
        node.addModuleUplink(classGraph, (ResolvedImportType) impMod.getKey(),
            impMod.getValue().getUsage());
      }
      return;
    }
    
    //rebuilding, so starting from scratch (no transformations here)
    load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) handle;
    Node modNode = populateClassGraph(project, classGraph);
    
    if (includeFields) {
      //also look at functions/bodies to update usage levels
      //tenv will only have class table and import table
      //this will call Ast.addDependency and Type.addDependency
      Pair<ProgHeader, TypeEnvironment> progTenv = project.createEmptyTopLevelTenv(this,
          moduleHandle);
      TypeEnvironment tenv = progTenv.b;
      
      moduleHandle.moduleBuildObj.program.buildModuleDependencyGraph(classGraph, tenv);
    }
    
    //reset imports (imports, submods, parent mod) to usage NONE.
    //moduleHandle.moduleBuildObj.effectiveImports.setAllUsage(ImportUsage.NONE);
    
    EffectiveImports effectiveImps = moduleHandle.getImports(project.getAuditTrail(), msgState);
    List<Pair<ImportType, Nullable<ProjectNameVersion>>> importList = effectiveImps.getImportList(
        true, filterImpTypes);
    for (Pair<ImportType, Nullable<ProjectNameVersion>> imp : importList) {
      modNode.addModuleUplink(classGraph, (ResolvedImportType) imp.a, ImportUsage.NONE);
    }
    
    //now update our import usages with the correct usage based on the dependency graph
    for (ClassUsageLink uplink : modNode.getUplinks()) {
      if (uplink.node instanceof ModuleNode) {
        ModuleNode uplinkNode = (ModuleNode) uplink.node;
        moduleHandle.moduleBuildObj.effectiveImports.getImport(
            uplinkNode.moduleType.asImportType(), uplink.getUsage());
      }
    }
    
    moduleHandle = (LoadedModuleHandle) handle;
  }

  /*
   * Purely immediate imports
   */
  public void populateImportGraph(AuditTrailLite auditTrail, ImportGraph graph)
      throws FatalMessageException {
    graph.addNode(getModuleType(auditTrail), Nullable.of(project.getModProjectNameVersion()),
        handle.effectiveImports);
  }

  /*
  public ModuleEndpoint renameTypes() throws FatalMessageException {
    load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) handle;
    ModuleEndpoint newModEp = new ModuleEndpoint(moduleHandle.renameTypes(Project.moduleAppend),
        msgState);
    newModEp.project = project;
    return newModEp;
  }
  */
}
