package multifile;

import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditRenameTypes;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.ImportType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.ReferenceType;

public class JsmntGlobal {
  public static final ModuleType modType = new ModuleType(Nullable.empty(), "jsmnt_global");
  public static final ImportType importModType = new ModuleImportType(Nullable.empty(), "jsmnt_global");
  public static final String filename = "jsmnt_global.jsmnt";
  public static final ProjectNameVersion nameVersion =
      new ProjectNameVersion(false, "jsmnt_global", 0, 1);
  public static final ProjectNameVersion desiredNameVersion =
      ProjectNameVersion.Create(false, "jsmnt_global=0.1", "").a.get();
  /*
   * When printing the exception, we say the root, followed by the children:
   * 
   * Hit exception X. While handling this exception, hit exception Y. While handling Y, hit Z...
   * 
   * The MemTracker cannot define it's alloc/free functions since those can raise an exception.
   * Instead, add as sanityFunction
   */
  public static final String src = "module jsmnt_global;\n"
      + "export enum ExcType {\n"
      + "  none,\n"
      + "  nullptrDeref,\n"
      + "  outOfBounds,\n"
      + "  divByZero,\n"
      + "  timeoutExpired,\n"
      + "  recursionLimitHit,\n"
      + "  memLimitHit\n"
      + "}\n"
      + ""
      + "export enum CmpResult {\n"
      + "  none,\n"
      + "  EQ,\n"
      + "  NE,\n"
      + "  LT,\n"
      + "  GT\n"
      + "}\n"
      + ""
      + "export class Obj {\n"
      + "  public {\n"
      + "    fun int hash() {"
      + "      return 0;\n"
      + "    }"
      + ""
      + "    fun CmpResult cmp(Obj* other) {\n"
      + "      return CmpResult.NE;\n"
      + "    }\n"
      + "  }\n"
      + "}\n"
      + ""
      + "export class Exception {\n"
      + "  public {\n"
      + "    ExcType code = ExcType.none;\n"
      + "    [int] bt = [int][];\n"
      + "    init() {}\n"
      + "    init(ExcType _code) {code = _code;}\n"
      + "  }\n"
      + "}\n"
      + "export class MemTracker {\n"
      + "  public {\n"
      + "    uint32 limit = 0;\n"
      + "    uint32 current = 0;\n"
      + "    init() {}\n"
      + "    init(uint32 _limit) {limit = _limit;}\n"
      + "  }\n"
      + "}\n"
      + "export class FunctionContext {\n"
      + "  public {\n"
      + "    Exception* exc = null;\n"
      + "    [int] bt = [int][];\n"
      + "    float64 startTime = 0;\n"
      + "    float32 timeoutRemaining = 0;\n"
      + "    bool checkRecursion = false;\n"
      + "    uint32 recursionRemaining = 0;\n"
      + "    MemTracker* memtracker = null;\n"
      + "    \n"
      + "    init() {}\n"
      + "  }\n"
      + "}\n"
      + ""
      + "";
  
  //before type renames only
  /*
   * fun int fn(ExceptionRef* pre_exc) {
   *   *pre_exc = new ExceptionRef(new Exception(15, 0));
   *   return 0;
   * }
   * 
   * fun int fn2(ExceptionRef* pre_exc) {
   *   ExceptionRef tmp_exc;
   *   tmp_ret = fn(&tmp_exc);
   *   if (tmp_exc.exc != null) {
   *     pre_exc.exc = tmp_exc.exc;
   *     pre_exc.exc.bt.push(43)
   *     return 0;
   *   }
   * }
   */
  
  public static ClassType memtrackerClassType = new ClassType(Nullable.of(modType),
      "MemTracker", new LinkedList<>());
  
  public static EnumDeclType excTypeEnumDeclType = new EnumDeclType(Nullable.of(modType),
      "ExcType");
  public static int fnCtxClassTypeSizeof = 64;
  public static final EnumType excTypeEnumType = new EnumType(Nullable.of(modType), "ExcType");
  
  public static final ClassType exceptionClassType = new ClassType(Nullable.of(modType),
      "Exception", new LinkedList<>());
  public static final ReferenceType exceptionRefType = new ReferenceType(exceptionClassType);
  
  public static final ClassType fnCtxClassType = new ClassType(Nullable.of(modType),
      "FunctionContext", new LinkedList<>());
  public static final ClassDeclType fnCtxClassDeclType = new ClassDeclType(Nullable.of(modType),
      "FunctionContext");
  public static final ReferenceType fnCtxRefType = new ReferenceType(fnCtxClassType);
  public static final String fnCtxId = "fn_ctx";
  
  public static final ClassType objClassType = new ClassType(Nullable.of(modType),
      "Obj", new LinkedList<>());
  
  public static Set<String> getReservedTypeNames() {
    Set<String> invalidNames = new HashSet<>();
    invalidNames.add("Exception");
    invalidNames.add("ExcType");
    invalidNames.add("MemTracker");
    invalidNames.add("FunctionContext");
    invalidNames.add("Obj");
    invalidNames.add("CmpResult");
    return invalidNames;
  }
  
  public static ClassType getRenamedClass(Project project, ClassType originalType,
      MsgState msgState) throws FatalMessageException {
    //get type renames
    Nullable<ModuleEndpoint> globalModEp = project.lookupModule(JsmntGlobal.modType,
        new AuditTrailLite());
    if (globalModEp.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAME_TYPES,
          "Failed to find Jasmint Global lib: " + JsmntGlobal.modType);
    }
    globalModEp.get().load();
    LoadedModuleHandle globalModHandle = (LoadedModuleHandle) globalModEp.get().getHandle();
    Nullable<AuditEntry> globalRenames = globalModHandle.moduleBuildObj.auditTrail.getCombined(
        AuditEntryType.RENAME_TYPES);
    if (globalRenames.isNull()) {
      return originalType;
    }
    AuditRenameTypes globalRenaming = (AuditRenameTypes) globalRenames.get();
    
    //now run our original type through the renamings
    ClassType newType = (ClassType) originalType.replaceType(globalRenaming.typeRenamings);
    
    return newType;
  }
}
