package multifile;

import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import typecheck.ModuleType;
import util.Pair;

public class ProjectNameVersion {
  public static enum CMP {
    NONE(""),
    EQ("="),
    LT("<"),
    GT(">"),
    LTE("<="),
    GTE(">=");
    
    public final String opName;
    private CMP(String opName) {
      this.opName = opName;
    }
    
    @Override
    public String toString() {
      return opName;
    }
  }
  
  private static final Map<String, CMP> STR_TO_CMP = new HashMap<>();
  
  static {
    for (CMP b : CMP.values()) {
      STR_TO_CMP.put(b.opName, b);
    }
  }
  
  private static final Pattern pattern = Pattern.compile("(?<projectname>[\\w-]+)"
      + "(?:(?<op>=|>=|<=|>|<)(?<majorv>\\d+|\\*)(?:\\.(?<minorv>\\d+|\\*))?)?");
  private static final Pattern validProjectNamePattern = Pattern.compile("[a-zA-Z_][a-zA-Z_0-9]*");
  
  public final boolean isFfi;
  public final String name;
  public final CMP cmpOp; //only for compare (import_libs)
  public final int majorVersion;
  public final boolean majorVersionAny; //only for compare
  public final int minorVersion;
  public final boolean minorVersionAny; //only for compare
  
  private ProjectNameVersion(boolean isFfi, String name, CMP cmpOp, int majorVersion,
      boolean majorVersionAny,
      int minorVersion, boolean minorVersionAny) {
    this.isFfi = isFfi;
    this.name = name;
    this.cmpOp = cmpOp;
    this.majorVersion = majorVersion;
    this.majorVersionAny = majorVersionAny;
    this.minorVersion = minorVersion;
    this.minorVersionAny = minorVersionAny;
  }
  
  public ProjectNameVersion(boolean isFfi, String name, int majorVersion, int minorVersion) {
    this.isFfi = isFfi;
    this.name = name;
    this.cmpOp = CMP.NONE;
    this.majorVersion = majorVersion;
    this.majorVersionAny = false;
    this.minorVersion = minorVersion;
    this.minorVersionAny = false;
  }

  @Override
  public String toString() {
    String res = name + cmpOp;
    if (cmpOp == CMP.NONE) {
      res += "@";
    }
    if (majorVersionAny) {
      res += "*";
    } else {
      res += majorVersion;
    }
    res += ".";
    if (minorVersionAny) {
      res += "*";
    } else {
      res += minorVersion;
    }
    return res;
  }
  
  public String toFilePath() {
    if (majorVersionAny) {
      throw new IllegalArgumentException("this should be a provider, not requirement");
    }
    if (minorVersionAny) {
      throw new IllegalArgumentException("this should be a provider, not requirement");
    }
    
    if (isFfi) {
      return name;
    }
    return name + "_" + majorVersion + "_" + minorVersion;
  }
  
  @Override
  public int hashCode() {
    return (name + majorVersion + minorVersion).hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    //only for hashing
    if (!(other instanceof ProjectNameVersion)) {
      return false;
    }
    ProjectNameVersion otherProj = (ProjectNameVersion) other;
    return name.equals(otherProj.name)
        && majorVersion == otherProj.majorVersion
        && minorVersion == otherProj.minorVersion;
  }
  
  public static boolean validProjectName(String name) {
    return validProjectNamePattern.matcher(name).matches();
  }
  
  public static Optional<ProjectNameVersion> Create(boolean isFfi, String nameAndVersion,
      String jsonConfigFilename, MsgState msgState) throws FatalMessageException {
    Pair<Optional<ProjectNameVersion>, String> valueErr = Create(isFfi, nameAndVersion,
        jsonConfigFilename);
    // check for error
    if (!valueErr.a.isPresent()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.PROJECT_ASSEMBLE, valueErr.b);
      return Optional.empty();
    }
    return valueErr.a;
  }
  
  // return (value, errorMsg)
  public static Pair<Optional<ProjectNameVersion>, String> Create(boolean isFfi,
      String nameAndVersion,
      String jsonConfigFilename) {
    /*
     * Run through regex
     */
    Matcher match = pattern.matcher(nameAndVersion);
    if (!match.matches()) {
      String err = "invalid project format \"" + nameAndVersion + "\" in file: "
          + jsonConfigFilename;
      return new Pair<>(Optional.empty(), err);
    }
    String name = match.group("projectname");
    if (!validProjectName(name)) {
      String err = "invalid project name \"" + name + "\" in file: " + jsonConfigFilename;
      return new Pair<>(Optional.empty(), err);
    }
    
    CMP cmpOp = STR_TO_CMP.get(match.group("op"));
    
    String strMajor = match.group("majorv");
    int majorVersion = 0;
    boolean majorVersionAny = strMajor.equals("*");
    if (!majorVersionAny) {
      majorVersion = Integer.parseInt(strMajor);
    }
    
    String strMinor = match.group("minorv");
    int minorVersion = 0;
    boolean minorVersionAny = strMinor.equals("*") || majorVersionAny;
    if (!minorVersionAny) {
      minorVersion = Integer.parseInt(strMinor);
    }
    
    return new Pair<>(Optional.of(new ProjectNameVersion(isFfi, name, cmpOp, majorVersion,
        majorVersionAny,
        minorVersion, minorVersionAny)), "");
  }
  
  /*
   * 'other' has cmp (stdlib=1.0),
   * 'this' as definition stdlib [1, 0]
   * 
   * we want to know if `this CMP other` is true (this <= other)
   */
  public boolean compare(ProjectNameVersion other) {
    if (cmpOp != CMP.NONE || other.cmpOp == CMP.NONE) {
      throw new IllegalArgumentException();
    }
    
    if (!name.equals(other.name)) {
      return false;
    }
    
    //only check minor version if major version is the same
    
    if (majorVersionsEqual(other)) {
      return compareVersions(minorVersion, other.minorVersion, other.minorVersionAny,
          other.cmpOp);
    }
    return false;
  }



  private boolean compareVersions(int version, int otherVersion, boolean otherVersionAny,
      CMP otherCmpOp) {
    if (otherVersionAny) {
      return true;
    }
    
    switch (otherCmpOp) {
      case EQ:
        return version == otherVersion;
      case LT:
        return version < otherVersion;
      case GT:
        return version > otherVersion;
      case LTE:
        return version <= otherVersion;
      case GTE:
        return version >= otherVersion;
      default:
        return false;
    }
  }


  private boolean majorVersionsEqual(ProjectNameVersion other) {
    if (other.majorVersionAny) {
      return true;
    }
    return majorVersion == other.majorVersion;
  }

  public AstProto.ProjectNameVersion.Builder serialize() {
    AstProto.ProjectNameVersion.Builder document = AstProto.ProjectNameVersion.newBuilder();
    
    document.setIsFfi(isFfi);
    document.setName(name);
    document.setCmpOp(cmpOp.ordinal());
    document.setMajorVersion(majorVersion);
    document.setMajorVersionAny(majorVersionAny);
    document.setMinorVersion(minorVersion);
    document.setMinorVersionAny(minorVersionAny);
    
    return document;
  }
  
  public static ProjectNameVersion deserialize(AstProto.ProjectNameVersion document) {
    return new ProjectNameVersion(document.getIsFfi(), document.getName(),
        CMP.values()[document.getCmpOp()],
        document.getMajorVersion(), document.getMajorVersionAny(),
        document.getMinorVersion(), document.getMinorVersionAny());
  }

  public ModuleType.ProjectNameVersion toModProjectNameVersion() {
    return new ModuleType.ProjectNameVersion(isFfi, name, majorVersion, minorVersion);
  }

}
