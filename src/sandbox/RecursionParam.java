package sandbox;

import ast.AbstractExpression;
import ast.AssignmentStatement;
import ast.Ast;
import ast.Ast.AnfTune;
import ast.BoolExpression;
import ast.DeclarationStatement;
import ast.Expression;
import ast.IdentifierExpression;
import ast.LvalueDot;
import ast.Program.RenamesOfInterest;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class RecursionParam extends AbstractSandboxParam {

  public final Expression value;
  
  public RecursionParam(int lineNum, int columnNum, Expression value) {
    super(lineNum, columnNum, ParamType.RECURSION);
    this.value = value;
  }
  
  @Override
  public SandboxParam renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    return new RecursionParam(lineNum, columnNum, value.renameIds(renamings, msgState));
  }
  
  @Override
  public AstProto.SandboxParam.Builder serialize() throws SerializationError {
    AstProto.RecursionParam.Builder subDoc = AstProto.RecursionParam.newBuilder();
    
    subDoc.setValue(value.serialize());
    
    AstProto.SandboxParam.Builder document = preSerialize();
    document.setRecursionParam(subDoc);
    return document;
  }
  
  public static SandboxParam deserialize(int lineNum, int columnNum,
      AstProto.RecursionParam document)
      throws SerializationError {
    return new RecursionParam(lineNum, columnNum,
        AbstractExpression.deserialize(document.getValue()));
  }

  @Override
  public Optional<SandboxParam> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Expression>> tmp = value.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new RecursionParam(lineNum, columnNum, tmp.get().get()));
  }

  @Override
  public SandboxParam replace(Ast target, Ast with) {
    return new RecursionParam(lineNum, columnNum, (Expression) value.replace(target, with));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return value.findFreeVariables();
  }

  @Override
  public boolean compare(SandboxParam other) {
    if (!(other instanceof RecursionParam)) {
      return false;
    }
    RecursionParam ourOther = (RecursionParam) other;
    
    return value.compare(ourOther.value);
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    return value.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, SandboxParam> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> tmpPair = value.toAnf(tuning);
    return new Pair<List<DeclarationStatement>, SandboxParam>(tmpPair.a,
        new RecursionParam(lineNum, columnNum, (Expression) tmpPair.b));
  }

  @Override
  public SandboxParam replaceType(Map<Type, Type> mapFromTo) {
    return new RecursionParam(lineNum, columnNum, value.replaceType(mapFromTo));
  }

  @Override
  public Optional<SandboxParam> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmp = value.replaceVtableAccess(project, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new RecursionParam(lineNum, columnNum, (Expression) tmp.get()));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return value.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return value.containsAst(asts);
  }

  @Override
  public Optional<Type> typecheck(TypeEnvironment tenv) throws FatalMessageException {
    // now ensure that limitValue returns a uint32
    Optional<TypecheckRet> retOpt = value.typecheck(tenv, Nullable.empty());
    if (!retOpt.isPresent()) {
      return Optional.empty();
    }
    Type limitType = retOpt.get().type;
    if (!tenv.canSafeCast(limitType, IntType.Create(false, 32))) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "recursion limit must be uint32 type, not: " + limitType, lineNum, columnNum);
      return Optional.empty();
    }
    return Optional.of(limitType);
  }

  @Override
  public List<Statement> createAssignmentLines(String sandboxLambdaFnCtx,
      RenamesOfInterest jsmntGlobalRenamings) {
    
    //fn_ctx.checkRecursion = true;
    AssignmentStatement setCheckRecursion = new AssignmentStatement(
        new LvalueDot(
            new IdentifierExpression(sandboxLambdaFnCtx),
            jsmntGlobalRenamings.checkRecursion),
        new BoolExpression(true));
    
    //fn_ctx.recursionRemaining = 20;
    AssignmentStatement setRecursionRemaining = new AssignmentStatement(
        new LvalueDot(
            new IdentifierExpression(sandboxLambdaFnCtx),
            jsmntGlobalRenamings.recursionRemaining),
        value);
    
    List<Statement> statements = new LinkedList<>();
    statements.add(setCheckRecursion);
    statements.add(setRecursionRemaining);
    return statements;
  }

  @Override
  public Optional<SandboxParam> resolveUserType(MsgState msgState) throws FatalMessageException {
    return Optional.of(new RecursionParam(lineNum, columnNum, value.resolveUserTypes(msgState).get()));
  }

  @Override
  public SandboxParam addThisExpression(ClassHeader classHeader) {
    return new RecursionParam(lineNum, columnNum, value.addThisExpression(classHeader));
  }

}
