package sandbox;

import ast.AbstractExpression;
import ast.AssignmentStatement;
import ast.Ast;
import ast.Ast.AnfTune;
import ast.DeclarationStatement;
import ast.Expression;
import ast.FunctionCallExpression;
import ast.IdentifierExpression;
import ast.LvalueDot;
import ast.NewClassInstanceExpression;
import ast.NewExpression;
import ast.Program.RenamesOfInterest;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.JsmntGlobal;
import multifile.Project;
import type_env.TypeEnvironment;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class MemlimitParam extends AbstractSandboxParam {

  public final Expression value;
  
  public MemlimitParam(int lineNum, int columnNum, Expression value) {
    super(lineNum, columnNum, ParamType.MEMLIMIT);
    this.value = value;
  }

  @Override
  public SandboxParam renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    return new MemlimitParam(lineNum, columnNum, value.renameIds(renamings, msgState));
  }
  
  @Override
  public AstProto.SandboxParam.Builder serialize() throws SerializationError {
    AstProto.MemlimitParam.Builder subDoc = AstProto.MemlimitParam.newBuilder();
    
    subDoc.setValue(value.serialize());
    
    AstProto.SandboxParam.Builder document = preSerialize();
    document.setMemlimitParam(subDoc);
    return document;
  }
  
  public static SandboxParam deserialize(int lineNum, int columnNum,
      AstProto.MemlimitParam document)
      throws SerializationError {
    return new MemlimitParam(lineNum, columnNum, 
        AbstractExpression.deserialize(document.getValue()));
  }

  @Override
  public Optional<SandboxParam> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Expression>> tmp = value.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new MemlimitParam(lineNum, columnNum, tmp.get().get()));
  }

  @Override
  public SandboxParam replace(Ast target, Ast with) {
    return new MemlimitParam(lineNum, columnNum, (Expression) value.replace(target, with));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return value.findFreeVariables();
  }

  @Override
  public boolean compare(SandboxParam other) {
    if (!(other instanceof MemlimitParam)) {
      return false;
    }
    MemlimitParam ourOther = (MemlimitParam) other;
    
    return value.compare(ourOther.value);
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    return value.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, SandboxParam> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> tmpPair = value.toAnf(tuning);
    return new Pair<List<DeclarationStatement>, SandboxParam>(tmpPair.a,
        new MemlimitParam(lineNum, columnNum, (Expression) tmpPair.b));
  }

  @Override
  public SandboxParam replaceType(Map<Type, Type> mapFromTo) {
    return new MemlimitParam(lineNum, columnNum, value.replaceType(mapFromTo));
  }

  @Override
  public Optional<SandboxParam> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmp = value.replaceVtableAccess(project, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new MemlimitParam(lineNum, columnNum, (Expression) tmp.get()));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return value.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return value.containsAst(asts);
  }

  @Override
  public Optional<Type> typecheck(TypeEnvironment tenv) throws FatalMessageException {
    // now ensure that limitValue returns a uint32
    Optional<TypecheckRet> retOpt = value.typecheck(tenv, Nullable.empty());
    if (!retOpt.isPresent()) {
      return Optional.empty();
    }
    Type limitType = retOpt.get().type;
    if (!tenv.canSafeCast(limitType, IntType.Create(false, 32))) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "memlimit must be uint32 type, not: " + limitType, lineNum, columnNum);
      return Optional.empty();
    }
    return Optional.of(limitType);
  }

  @Override
  public List<Statement> createAssignmentLines(String sandboxLambdaFnCtx,
      RenamesOfInterest jsmntGlobalRenamings) {
    //fn_ctx.memtracker = new jsmnt_global.MemTracker(limit)
    LvalueDot lvalue = new LvalueDot(new IdentifierExpression(sandboxLambdaFnCtx),
        jsmntGlobalRenamings.memtracker);
    
    List<Expression> constructorArgs = new LinkedList<>();
    constructorArgs.add(value);
    FunctionCallExpression memtrackerConstructorCall = new FunctionCallExpression(Nullable.empty(),
        new IdentifierExpression(jsmntGlobalRenamings.memtrackerConstructor), constructorArgs,
        "constructor");
    NewClassInstanceExpression newMemtrackerExpr = new NewClassInstanceExpression(
        JsmntGlobal.memtrackerClassType, JsmntGlobal.memtrackerClassType.toString(),
        memtrackerConstructorCall);
    NewExpression rvalue = new NewExpression(newMemtrackerExpr);
    
    AssignmentStatement assign = new AssignmentStatement(lvalue, rvalue);
    List<Statement> statements = new LinkedList<>();
    statements.add(assign);
    return statements;
  }

  @Override
  public Optional<SandboxParam> resolveUserType(MsgState msgState) throws FatalMessageException {
    return Optional.of(new MemlimitParam(lineNum, columnNum, value.resolveUserTypes(msgState).get()));
  }

  @Override
  public SandboxParam addThisExpression(ClassHeader classHeader) {
    return new MemlimitParam(lineNum, columnNum, value.addThisExpression(classHeader));
  }
}
