package sandbox;

import ast.AbstractExpression;
import ast.AssignmentStatement;
import ast.Ast;
import ast.Ast.AnfTune;
import ast.DeclarationStatement;
import ast.Expression;
import ast.FunctionCallExpression;
import ast.IdentifierExpression;
import ast.LvalueDot;
import ast.Program.RenamesOfInterest;
import ast.SerializationError;
import ast.Statement;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;
import typecheck.Float32Type;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class TimeoutParam extends AbstractSandboxParam {

  public final Expression value;
  
  public TimeoutParam(int lineNum, int columnNum, Expression value) {
    super(lineNum, columnNum, ParamType.TIMEOUT);
    this.value = value;
  }

  @Override
  public SandboxParam renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    return new TimeoutParam(lineNum, columnNum, value.renameIds(renamings, msgState));
  }

  @Override
  public Optional<SandboxParam> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Expression>> tmp = value.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new TimeoutParam(lineNum, columnNum, tmp.get().get()));
  }

  @Override
  public AstProto.SandboxParam.Builder serialize() throws SerializationError {
    AstProto.TimeoutParam.Builder subDoc = AstProto.TimeoutParam.newBuilder();
    
    subDoc.setValue(value.serialize());
    
    AstProto.SandboxParam.Builder document = preSerialize();
    document.setTimeoutParam(subDoc);
    return document;
  }
  
  public static SandboxParam deserialize(int lineNum, int columnNum, AstProto.TimeoutParam document)
      throws SerializationError {
    return new TimeoutParam(lineNum, columnNum,
        AbstractExpression.deserialize(document.getValue()));
  }

  @Override
  public SandboxParam replace(Ast target, Ast with) {
    return new TimeoutParam(lineNum, columnNum, (Expression) value.replace(target, with));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return value.findFreeVariables();
  }

  @Override
  public boolean compare(SandboxParam other) {
    if (!(other instanceof TimeoutParam)) {
      return false;
    }
    TimeoutParam ourOther = (TimeoutParam) other;
    
    return value.compare(ourOther.value);
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    return value.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, SandboxParam> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> tmpPair = value.toAnf(tuning);
    return new Pair<List<DeclarationStatement>, SandboxParam>(tmpPair.a,
        new TimeoutParam(lineNum, columnNum, (Expression) tmpPair.b));
  }

  @Override
  public SandboxParam replaceType(Map<Type, Type> mapFromTo) {
    return new TimeoutParam(lineNum, columnNum, value.replaceType(mapFromTo));
  }

  @Override
  public Optional<SandboxParam> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmp = value.replaceVtableAccess(project, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new TimeoutParam(lineNum, columnNum, (Expression) tmp.get()));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return value.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return value.containsAst(asts);
  }

  @Override
  public Optional<Type> typecheck(TypeEnvironment tenv) throws FatalMessageException {
    // now ensure that timeoutValue returns a float32
    Optional<TypecheckRet> retOpt = value.typecheck(tenv, Nullable.empty());
    if (!retOpt.isPresent()) {
      return Optional.empty();
    }
    Type timeoutType = retOpt.get().type;
    if (!tenv.canSafeCast(timeoutType, Float32Type.Create())) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Timeout must be float32 type, not: " + timeoutType, lineNum, columnNum);
      return Optional.empty();
    }
    return Optional.of(timeoutType);
  }

  @Override
  public List<Statement> createAssignmentLines(String sandboxLambdaFnCtx,
      RenamesOfInterest jsmntGlobalRenamings) {
    
    // fn_ctx.startTime = currentUnixEpochTime();
    AssignmentStatement setStartTime = new AssignmentStatement(
        new LvalueDot(new IdentifierExpression(sandboxLambdaFnCtx),
            jsmntGlobalRenamings.startTime),
        new FunctionCallExpression(Nullable.empty(),
            new IdentifierExpression("currentUnixEpochTime"), new LinkedList<>(),
            "currentUnixEpochTime"));
    
    // fn_ctx.timeoutRemaining = 20;
    AssignmentStatement setTimeoutRemaining = new AssignmentStatement(
        new LvalueDot(new IdentifierExpression(sandboxLambdaFnCtx),
            jsmntGlobalRenamings.timeoutRemaining),
        value);
    
    List<Statement> statements = new LinkedList<>();
    statements.add(setStartTime);
    statements.add(setTimeoutRemaining);
    return statements;
  }

  @Override
  public Optional<SandboxParam> resolveUserType(MsgState msgState) throws FatalMessageException {
    return Optional.of(new TimeoutParam(lineNum, columnNum, value.resolveUserTypes(msgState).get()));
  }

  @Override
  public SandboxParam addThisExpression(ClassHeader classHeader) {
    return new TimeoutParam(lineNum, columnNum, value.addThisExpression(classHeader));
  }

}
