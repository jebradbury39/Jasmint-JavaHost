package type_env;

public abstract class AbstractTypeBox implements TypeBox {
  public final boolean export;
  public final VariableScoping scoping;
  
  protected boolean isInit = false;
  
  public AbstractTypeBox(boolean export, boolean isInit,
      VariableScoping scoping) {
    this.export = export;
    this.isInit = isInit;
    this.scoping = scoping;
  }
  
  @Override
  public boolean getIsInit() {
    return isInit;
  }
  
  @Override
  public void setIsInit(boolean val) {
    isInit = val;
  }
  
  @Override
  public VariableScoping getScoping() {
    return scoping;
  }
  
  @Override
  public boolean getExport() {
    return export;
  }
  
}
