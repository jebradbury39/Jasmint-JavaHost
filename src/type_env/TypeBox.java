package type_env;

import errors.FatalMessageException;
import java.util.Map;
import typecheck.ImportType;
import typecheck.Type;

public interface TypeBox {
  public TypeBox fullCopy();
  
  public TypeBox replaceType(TypeEnvironment tenv, Map<Type, Type> mapFromTo,
      int lineNum, int columnNum) throws FatalMessageException;

  public VariableScoping getScoping();

  boolean getIsInit();

  void setIsInit(boolean val);
  
  Type getType();

  public boolean getExport();
  
  public ImportType getImportType(ImportType importType);
}
