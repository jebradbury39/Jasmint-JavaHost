package type_env;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.Map;
import typecheck.FunctionType;
import typecheck.Type;
import util.Pair;

public abstract class IdToType {
  
  protected void sanityCheck(String name, BasicTypeBox tBox) {
    if (tBox.definedInClass.isNull()) {
      if (tBox.visibilityLevel.isNotNull()) {
        throw new IllegalArgumentException("visibility [" + tBox.visibilityLevel + "] ["
              + name + "] can only be used in class definition");
      }
    } else {
      if (tBox.visibilityLevel.isNull()) {
        throw new IllegalArgumentException("visibility must be specified [" + name
            + "] in class definition [" + tBox.definedInClass + "]");
      }
    }
  }
  
  //this actually just adds to function overload list, but does overwrite 
  public abstract IdToType write(String name, TypeBox tBox);
  
  //type is only needed when hunting for function overload. Function return type is never checked
  //Returns Pair(best match for this env, # direct matched)
  public abstract Pair<Nullable<TypeBox>, Integer> read(TypeEnvironment tenv, String name,
      FunctionType functionType, boolean ignoreExport,
      int lineNum, int columnNum) throws FatalMessageException;
  
  public abstract IdToType fullCopy(String name);
  
  public abstract IdToType replaceType(TypeEnvironment tenv, String name,
      Map<Type, Type> mapFromTo) throws FatalMessageException;
}
