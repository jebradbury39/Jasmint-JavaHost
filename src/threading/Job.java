package threading;

import errors.FatalMessageException;
import java.util.HashSet;
import java.util.Set;

public class Job {
  private static long idCounter = 0;
  
  public static enum Status {
    NONE, //not done yet
    ACTIVE, //not done, but being worked on
    SUCCESS,
    FAILURE
  }

  public final String id;
  Set<String> waitingOnJobs = new HashSet<>();

  private final JobFunction fn;
  private final Object context;


  public Job(JobFunction fn, Object context) {
    this.id = context + "_" + getCounter();
    this.fn = fn;
    this.context = context;
  }
  
  private synchronized long getCounter() {
    return idCounter++;
  }

  public synchronized void addDependency(String jobId) {
    if (jobId == null) {
      throw new IllegalArgumentException("jobId cannot be null");
    }
    if (jobId.isEmpty()) {
      throw new IllegalArgumentException("jobId cannot be empty");
    }
    waitingOnJobs.add(jobId);
  }

  public Status run() {
    try {
      return fn.function(context);
    } catch (FatalMessageException e) {
      return Status.FAILURE;
    }
  }
}
