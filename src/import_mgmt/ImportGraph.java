package import_mgmt;

import errors.Nullable;
import import_mgmt.EffectiveImport.EffectiveImportType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import typecheck.ImportType;
import typecheck.ModuleImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ResolvedImportType;
import util.DotGen;
import util.Pair;

public class ImportGraph {
  Map<ResolvedImportType, ImportNode> nodes = new HashMap<>();
  
  public DotGen toDotFormat() {
    DotGen dotGraph = new DotGen();
    for (ImportNode node : nodes.values()) {
      DotGen.Node graphNode = new DotGen.Node(node.absImportType.toString());
      graphNode.color = node.absImportType.getGraphColor();
      dotGraph.addNode(graphNode);
      node.updateDotGraph(dotGraph);
    }
    return dotGraph;
  }
  
  @Override
  public String toString() {
    return toDotFormat().toString();
  }
  
  public ImportNode addNode(ModuleType absModuleType,
      Nullable<ModuleType.ProjectNameVersion> withinProject,
      EffectiveImports effectiveImports) {
    return addNode(new ModuleImportType(absModuleType.outerType, absModuleType.name),
        withinProject, effectiveImports);
  }
  
  public ImportNode addNode(ResolvedImportType absModuleType,
      Nullable<ModuleType.ProjectNameVersion> withinProject,
      EffectiveImports effectiveImports) {
    
    ImportNode node = findNode(absModuleType, withinProject, true).get();
    
    // first find literal imports
    Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
    
    List<Pair<ImportType, Nullable<ProjectNameVersion>>> immediateAbsImports =
        effectiveImports.getImportList(true, filterImpTypes);
    
    for (Pair<ImportType, Nullable<ProjectNameVersion>> imp : immediateAbsImports) {
      node.addImport((ResolvedImportType) imp.a, EffectiveImportType.LITERAL_IMPORT, imp.b);
    }
    
    // now do implied imports
    
    filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
    
    immediateAbsImports = effectiveImports.getImportList(true, filterImpTypes);
    
    for (Pair<ImportType, Nullable<ProjectNameVersion>> imp : immediateAbsImports) {
      node.addImport((ResolvedImportType) imp.a, EffectiveImportType.IMPLIED_IMPORT, imp.b);
    }
    
    return node;
  }
  
  public Nullable<ImportNode> findNode(ResolvedImportType moduleType,
      Nullable<ProjectNameVersion> withinProject, boolean allowAdd) {
    ImportNode tmp = nodes.get(moduleType);
    if (tmp == null) {
      if (!allowAdd) {
        return Nullable.empty();
      }
      tmp = new ImportNode(this, moduleType, withinProject);
      nodes.put(moduleType, tmp);
    }
    return Nullable.of(tmp);
  }
  
  private void resetVisited() {
    for (ImportNode node : nodes.values()) {
      node.visited = false;
    }
  }

  public Nullable<Set<Pair<ImportType, Nullable<ProjectNameVersion>>>> dfs(ResolvedImportType rootModule,
      Nullable<ProjectNameVersion> withinProject) {
    Nullable<ImportNode> rootNode = findNode(rootModule, withinProject, false);
    if (rootNode.isNull()) {
      return Nullable.empty();
    }
    resetVisited();
    Set<Pair<ImportType, Nullable<ModuleType.ProjectNameVersion>>> modTypes = new HashSet<>();
    rootNode.get().dfs(modTypes);
    return Nullable.of(modTypes);
  }

}
