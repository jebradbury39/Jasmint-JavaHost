package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import ast.BlockStatement;
import ast.ClassDeclaration;
import ast.ClassDeclarationSection;
import ast.DeclarationStatement;
import ast.FfiStatement;
import ast.Function;
import errors.Nullable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import multifile.JsmntGlobal;
import org.antlr.v4.runtime.tree.TerminalNode;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.GenericType;
import typecheck.Type;
import typecheck.UndeterminedUserInstanceType;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typecheck.VoidType;

public class JasmintToAstClassDeclarationVisitor extends JasmintBaseVisitor<ClassDeclaration> {

  private final JasmintToAstTypeVisitor typeVisitor;
  private final JasmintToAstFunctionVisitor functionVisitor;
  private final JasmintToAstStatementVisitor statementVisitor;
  private final JasmintToAstProgramVisitor programVisitor;
  
  private Nullable<ClassType> withinClassParent = Nullable.empty();
  private Nullable<ClassDeclType> withinClass = Nullable.empty();
  private Set<GenericType> currentClassGenerics = new HashSet<>();
  
  public JasmintToAstClassDeclarationVisitor(JasmintToAstProgramVisitor programVisitor) {
    this.programVisitor = programVisitor;
    this.functionVisitor = new JasmintToAstFunctionVisitor(programVisitor, this);
    this.typeVisitor = new JasmintToAstTypeVisitor(programVisitor);
    this.statementVisitor = new JasmintToAstStatementVisitor(programVisitor,
        new JasmintToAstExpressionVisitor(programVisitor));
  }

  public Nullable<ClassType> getWithinClassParent() { 
    return withinClassParent;
  }
  
  public Nullable<ClassDeclType> getWithinClass() {
    return withinClass;
  }
  
  public Set<GenericType> getCurrentClassGenerics() {
    return currentClassGenerics;
  }
  
  @Override
  public ClassDeclaration visitClassDeclaration(JasmintParser.ClassDeclarationContext ctx) {
    final Set<GenericType> saveCurrentClassGenerics = currentClassGenerics;
    List<String> genericsList = gatherGenerics(ctx.parameterizedList());
    currentClassGenerics = new HashSet<>();
    for (String generic : genericsList) {
      currentClassGenerics.add(new GenericType(generic));
    }
    
    Nullable<ClassType> parent = Nullable.empty();
    if (ctx.type() != null) {
      Type tmp = typeVisitor.visit(ctx.type());
      if (tmp instanceof UndeterminedUserInstanceType) {
        UndeterminedUserInstanceType userTy = (UndeterminedUserInstanceType) tmp;
        tmp = new ClassType(userTy.outerType, userTy.name, userTy.innerTypes);
      }
      if (!(tmp instanceof ClassType)) {
        throw new IllegalArgumentException("can only extend classes, not " + tmp);
      }
      parent = Nullable.of((ClassType) tmp);
    }
    final Nullable<ClassType> saveParent = withinClassParent;
    withinClassParent = parent;
    
    String className = ctx.ID().getText();
    Set<String> invalidNames = new HashSet<>();
    if (!programVisitor.isJsmntGlobal) {
      invalidNames = JsmntGlobal.getReservedTypeNames();
      
      if (parent.isNull()) {
        parent = Nullable.of(JsmntGlobal.objClassType);
      }
    }
    if (invalidNames.contains(className)) {
      throw new IllegalArgumentException("class name cannot be '" + className + "'");
    }
    
    final Nullable<ClassDeclType> saveWithinClass = withinClass;
    withinClass = Nullable.of(new ClassDeclType(
        Nullable.of(programVisitor.getModuleType()), className)); //don't care about generics
    
    List<ClassDeclarationSection> sections = new LinkedList<>();
    gatherSections(sections, className, ctx.classDeclarationSection());
    
    ClassDeclaration val = new ClassDeclaration(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, //wrapProgramLine ctx can change this
        ctx.FFI() != null,
        ctx.STATIC() != null,
        programVisitor.getModuleType(),
        className,
        new ClassDeclType(withinClass.get().outerType, withinClass.get().name),
        genericsList,
        parent,
        sections);
    val.setOriginalLabel(val.label);
    
    withinClassParent = saveParent;
    withinClass = saveWithinClass;
    currentClassGenerics = saveCurrentClassGenerics;
    return val;
  }
  
  @Override
  public ClassDeclaration visitInnerClassDeclaration(JasmintParser.InnerClassDeclarationContext ctx) {
    final Set<GenericType> saveCurrentClassGenerics = currentClassGenerics;
    List<String> genericsList = gatherGenerics(ctx.parameterizedList());
    currentClassGenerics = new HashSet<>();
    for (String generic : genericsList) {
      currentClassGenerics.add(new GenericType(generic));
    }
    
    Nullable<ClassType> parent = Nullable.empty();
    if (ctx.type() != null) {
      Type tmp = typeVisitor.visit(ctx.type());
      if (tmp instanceof UndeterminedUserInstanceType) {
        UndeterminedUserInstanceType userTy = (UndeterminedUserInstanceType) tmp;
        tmp = new ClassType(userTy.outerType, userTy.name, userTy.innerTypes);
      }
      if (!(tmp instanceof ClassType)) {
        throw new IllegalArgumentException("can only extend classes, not " + tmp);
      }
      parent = Nullable.of((ClassType) tmp);
    }
    final Nullable<ClassType> saveParent = withinClassParent;
    withinClassParent = parent;
    
    String className = ctx.ID().getText();
    Set<String> invalidNames = new HashSet<>();
    if (!programVisitor.isJsmntGlobal) {
      invalidNames = JsmntGlobal.getReservedTypeNames();
    }
    if (invalidNames.contains(className)) {
      throw new IllegalArgumentException("class name cannot be '" + className + "'");
    }
    
    final Nullable<ClassDeclType> saveWithinClass = withinClass;
    withinClass = Nullable.of(new ClassDeclType(
        Nullable.of(programVisitor.getModuleType()), className)); //don't care about generics
    
    List<ClassDeclarationSection> sections = new LinkedList<>();
    gatherInnerSections(sections, className, ctx.innerClassDeclarationSection());
    
    ClassDeclaration val = new ClassDeclaration(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, //never exported
        false,
        false,
        programVisitor.getModuleType(),
        className,
        new ClassDeclType(withinClass.get().outerType, withinClass.get().name),
        genericsList,
        parent,
        sections);
    val.setOriginalLabel(val.label);
    
    withinClassParent = saveParent;
    withinClass = saveWithinClass;
    currentClassGenerics = saveCurrentClassGenerics;
    return val;
  }
  
  private List<String> gatherGenerics(JasmintParser.ParameterizedListContext ctx) {
    List<String> generics = new LinkedList<String>();
    
    if (ctx != null) {
      for (TerminalNode id : ctx.ID()) {
        generics.add(id.getText());
      }
    }
    
    return generics;
  }
  
  private void gatherSections(List<ClassDeclarationSection> sections, String className,
      List<JasmintParser.ClassDeclarationSectionContext> ctxLst) {

    for (JasmintParser.ClassDeclarationSectionContext sctx : ctxLst) {
      if (sctx.nl_comment() == null) {
        sections.add(handleClassDeclarationSection(sctx));
      }
    }
  }
  
  private void gatherInnerSections(List<ClassDeclarationSection> sections, String className,
      List<JasmintParser.InnerClassDeclarationSectionContext> ctxLst) {

    for (JasmintParser.InnerClassDeclarationSectionContext sctx : ctxLst) {
      if (sctx.nl_comment() == null) {
        sections.add(handleInnerClassDeclarationSection(sctx));
      }
    }
  }
  
  private ClassDeclarationSection handleClassDeclarationSection(
      JasmintParser.ClassDeclarationSectionContext ctx) {
    List<DeclarationStatement> declarations = new LinkedList<>();
    List<Function> functions = new LinkedList<>();
    List<FfiStatement> ffiBlocks = new LinkedList<>();
    List<Function> constructors = new LinkedList<>();
    List<Function> destructors = new LinkedList<>();
    List<Function> overrides = new LinkedList<>();
    int ordering = 0;
    
    for (JasmintParser.ClassDeclarationLineContext lctx : ctx.classDeclarationLine()) {
      visitClassDeclLine(lctx, declarations, functions, ffiBlocks, constructors, destructors,
          overrides, ordering);
      ordering++;
    }
    
    Nullable<Function> destructor = Nullable.empty();
    if (destructors.size() == 1) {
      destructor = Nullable.of(destructors.get(0));
    } else if (destructors.size() > 1) {
      throw new IllegalArgumentException("A class cannot have more than one destructor");
    }
    
    ClassDeclarationSection val = new ClassDeclarationSection(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        ctx.visibility().getText(),
        declarations, functions, ffiBlocks, constructors, destructor, overrides);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  private ClassDeclarationSection handleInnerClassDeclarationSection(
      JasmintParser.InnerClassDeclarationSectionContext ctx) {
    List<DeclarationStatement> declarations = new LinkedList<>();
    List<Function> functions = new LinkedList<>();
    List<Function> constructors = new LinkedList<>();
    List<Function> destructors = new LinkedList<>();
    List<Function> overrides = new LinkedList<>();
    int ordering = 0;
    
    for (JasmintParser.InnerClassDeclarationLineContext lctx : ctx.innerClassDeclarationLine()) {
      visitInnerClassDeclLine(lctx, declarations, functions, constructors, destructors,
          overrides, ordering);
      ordering++;
    }
    
    Nullable<Function> destructor = Nullable.empty();
    if (destructors.size() == 1) {
      destructor = Nullable.of(destructors.get(0));
    } else if (destructors.size() > 1) {
      throw new IllegalArgumentException("A class cannot have more than one destructor");
    }
    
    ClassDeclarationSection val = new ClassDeclarationSection(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        ctx.visibility().getText(),
        declarations, functions, new LinkedList<>(), constructors, destructor, overrides);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  private DeclarationStatement visitDeclLine(JasmintParser.DeclarationLineContext ctx) {
    if (ctx.declaration() != null) {
      DeclarationStatement val = (DeclarationStatement) statementVisitor.visit(ctx);
      if (ctx.STATIC() != null) {
        val.setIsStatic(true);
        
        DeclarationStatement newVal = new DeclarationStatement(val.lineNum, val.columnNum,
            new LinkedList<>(), new LinkedList<>(),
            val.type, val.name, val.getValue(), val.getIsStatic(), false);
        newVal.setOriginalLabel(val.getOriginalLabel());
        val = newVal;
      }
      return val;
    }
    
    //handle require case
    DeclarationStatement val = (DeclarationStatement) statementVisitor.visit(ctx.declNoExpr());
    
    val = new DeclarationStatement(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        val.type, val.name, Nullable.empty(), true, true);
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }
  
  private DeclarationStatement visitInnerDeclLine(JasmintParser.InnerDeclarationLineContext ctx) {
    DeclarationStatement val = (DeclarationStatement) statementVisitor.visit(ctx);
    if (ctx.STATIC() != null) {
      val.setIsStatic(true);
      
      DeclarationStatement newVal = new DeclarationStatement(val.lineNum, val.columnNum,
          new LinkedList<>(), new LinkedList<>(),
          val.type, val.name, val.getValue(), val.getIsStatic(), false);
      newVal.setOriginalLabel(val.getOriginalLabel());
      val = newVal;
    }
    return val;
  }
  
  private void visitClassDeclLine(JasmintParser.ClassDeclarationLineContext ctx,
      List<DeclarationStatement> declarations,
      List<Function> functions,
      List<FfiStatement> ffiBlocks,
      List<Function> constructors,
      List<Function> destructors,
      List<Function> overrides,
      int ordering) {
    if (ctx.declarationLine() != null) {
      DeclarationStatement val = visitDeclLine(ctx.declarationLine());
      val.setOrdering(ordering);
      declarations.add(val);
      return;
    }
    if (ctx.function() != null) {
      Function val = functionVisitor.visit(ctx.function());
      val.setIsStatic(ctx.STATIC() != null);
      val.setOrdering(ordering);
      functions.add(val);
      return;
    }
    if (ctx.constructorFun() != null) {
      Function val = functionVisitor.visit(ctx.constructorFun());
      val.setOrdering(ordering);
      constructors.add(val);
      return;
    }
    if (ctx.destructorFun() != null) {
      Function val = visitDestructor(ctx.destructorFun());
      val.setOrdering(ordering);
      destructors.add(val);
      return;
    }
    if (ctx.overrideFun() != null) {
      Function val = functionVisitor.visit(ctx.overrideFun());
      val.setOrdering(ordering);
      overrides.add(val);
      return;
    }
    if (ctx.ffiBlock() != null) {
      FfiStatement val = (FfiStatement) statementVisitor.visit(ctx.ffiBlock());
      val.setOrdering(ordering);
      ffiBlocks.add(val);
      return;
    }
    if (ctx.nl_comment() != null) {
      return;
    }
    throw new IllegalArgumentException("internal: Invalid item in class declaration");
  }
  
  private void visitInnerClassDeclLine(JasmintParser.InnerClassDeclarationLineContext ctx,
      List<DeclarationStatement> declarations,
      List<Function> functions,
      List<Function> constructors,
      List<Function> destructors,
      List<Function> overrides,
      int ordering) {
    if (ctx.innerDeclarationLine() != null) {
      DeclarationStatement val = visitInnerDeclLine(ctx.innerDeclarationLine());
      val.setOrdering(ordering);
      declarations.add(val);
      return;
    }
    if (ctx.regularFun() != null) {
      Function val = functionVisitor.visit(ctx.regularFun());
      val.setIsStatic(ctx.STATIC() != null);
      val.setOrdering(ordering);
      functions.add(val);
      return;
    }
    if (ctx.constructorFun() != null) {
      Function val = functionVisitor.visit(ctx.constructorFun());
      val.setOrdering(ordering);
      constructors.add(val);
      return;
    }
    if (ctx.destructorFun() != null) {
      Function val = visitDestructor(ctx.destructorFun());
      val.setOrdering(ordering);
      destructors.add(val);
      return;
    }
    if (ctx.overrideFun() != null) {
      Function val = functionVisitor.visit(ctx.overrideFun());
      val.setOrdering(ordering);
      overrides.add(val);
      return;
    }
    if (ctx.nl_comment() != null) {
      return;
    }
    throw new IllegalArgumentException("internal: Invalid item in class declaration");
  }
  
  private Function visitDestructor(JasmintParser.DestructorFunContext ctx) {
    Nullable<BlockStatement> body = Nullable.empty();
    if (ctx.block() != null) {
      body = Nullable.of((BlockStatement) statementVisitor.visitBlock(ctx.block()));
    }
    if (withinClass.isNull()) {
      //TODO better parsing error reporting
      throw new IllegalArgumentException("cannot use destructor outside of a class");
    }
    
    FunctionType fullType = new FunctionType(withinClass.get(), VoidType.Create(),
        new LinkedList<>(), LambdaStatus.NOT_LAMBDA);
    
    UserInstanceType originalName = new ClassType(Nullable.of(programVisitor.getModuleType()),
        "destructor", new LinkedList<>());
    return new Function(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, ctx.REQUIRE() != null, false,
        "destructor", (UserDeclType) fullType.within.get(),
        fullType,
        new LinkedList<>(),
        body, false, Nullable.empty(), Function.MetaType.DESTRUCTOR);
  }
}
