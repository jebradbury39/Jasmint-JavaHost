package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import ast.BlockStatement;
import ast.DeclarationStatement;
import ast.Function;
import ast.FunctionCallExpression;
import ast.ParentCallExpression;
import errors.Nullable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.Type;
import typecheck.UserDeclType;
import typecheck.VoidType;

public class JasmintToAstFunctionVisitor extends JasmintBaseVisitor<Function> {

  private final JasmintToAstTypeVisitor typeVisitor;
  private final JasmintToAstStatementVisitor statementVisitor;
  private final JasmintToAstExpressionVisitor expressionVisitor;
  
  private final JasmintToAstProgramVisitor programVisitor;
  private final JasmintToAstClassDeclarationVisitor classDeclVisitor;
  
  public JasmintToAstFunctionVisitor(JasmintToAstProgramVisitor programVisitor,
      JasmintToAstClassDeclarationVisitor classDeclVisitor) {
    this.programVisitor = programVisitor;
    this.classDeclVisitor = classDeclVisitor;
    this.typeVisitor = new JasmintToAstTypeVisitor(programVisitor);
    this.expressionVisitor = new JasmintToAstExpressionVisitor(programVisitor);
    this.statementVisitor = new JasmintToAstStatementVisitor(programVisitor, expressionVisitor);
  }
  
  @Override
  public Function visitFunction(JasmintParser.FunctionContext ctx) {
    if (ctx.regularFun() != null) {
      return visit(ctx.regularFun());
    }
    return visit(ctx.requiredFun());
  }
  
  private Function.MetaType determineFnMetaType(String name) {
    Function.MetaType metaType = Function.MetaType.NORMAL;
    
    if (name.equals("main")) {
      metaType = Function.MetaType.MAIN;
    }
    
    if (programVisitor.getWithinClass().isNotNull()) {
      if (name.equals("hash")) {
        metaType = Function.MetaType.HASH;
      } else if (name.equals("cmp")) {
        metaType = Function.MetaType.CMP;
      }
    }
    
    if (name.equals("start")) {
      throw new IllegalArgumentException("'start' is a reserved function name");
    }
    return metaType;
  }
  
  @Override
  public Function visitRegularFun(JasmintParser.RegularFunContext ctx) {
    String name = ctx.ID().getText();
    
    Function.MetaType metaType = determineFnMetaType(name);
    
    Nullable<BlockStatement> body = Nullable.empty();
    if (ctx.block() != null) {
      body = Nullable.of((BlockStatement) statementVisitor.visit(ctx.block()));
    }
    
    List<DeclarationStatement> params = gatherParams(ctx.parameters());
    
    List<Type> paramTypes = new LinkedList<>();
    for (DeclarationStatement param : params) {
      paramTypes.add(param.type);
    }
    
    LambdaStatus lambdaStatus = LambdaStatus.NOT_LAMBDA;
    if (programVisitor.getWithinSandboxExpression()) {
      lambdaStatus = LambdaStatus.SANDBOX_NOT_LAMBDA;
    }
    
    FunctionType fullType = new FunctionType((DotAccessType)
        programVisitor.getWithinModuleOrClass(),
        typeVisitor.visit(ctx.returnType()), paramTypes, lambdaStatus);
    
    Function val = new Function(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, //wrapProgramLine ctx can change this
        false,
        false,
        name,
        (UserDeclType) fullType.within.get(),
        fullType,
        params,
        body, false,
        //assume not static (checked upstream), not constructor, no parent call
        Nullable.empty(),
        metaType);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Function visitRequiredFun(JasmintParser.RequiredFunContext ctx) {
    String name = ctx.ID().getText();
    
    Function.MetaType metaType = determineFnMetaType(name);
    
    Nullable<BlockStatement> body = Nullable.empty();
    
    List<DeclarationStatement> params = gatherParams(ctx.parameters());
    
    List<Type> paramTypes = new LinkedList<>();
    for (DeclarationStatement param : params) {
      paramTypes.add(param.type);
    }
    
    FunctionType fullType = new FunctionType((DotAccessType)
        programVisitor.getWithinModuleOrClass(),
        typeVisitor.visit(ctx.returnType()), paramTypes, LambdaStatus.NOT_LAMBDA);
    
    Function val = new Function(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, //wrapProgramLine ctx can change this
        true,
        false,
        name,
        (UserDeclType) fullType.within.get(),
        fullType,
        params,
        body, false,
        //assume not static (checked upstream), not constructor, no parent call
        Nullable.empty(),
        metaType);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Function visitConstructorFun(JasmintParser.ConstructorFunContext ctx) {
    Nullable<BlockStatement> body = Nullable.empty();
    if (ctx.block() != null) {
      body = Nullable.of((BlockStatement) statementVisitor.visit(ctx.block()));
    }
    
    int line = ctx.getStart().getLine();
    int column = ctx.getStart().getCharPositionInLine();
    
    //handle optional parent call
    Nullable<FunctionCallExpression> parentCall = Nullable.empty();
    
    ParentCallExpression pname = new ParentCallExpression(line, column,
        new LinkedList<>(), new LinkedList<>(),
        classDeclVisitor.getWithinClassParent(),
        ParentCallExpression.IdType.CONSTRUCTOR);
    pname.setOriginalLabel(pname.label);
    if (ctx.parentConstructorCall() != null) {
      parentCall = Nullable.of(new FunctionCallExpression(line, column,
          new LinkedList<>(), new LinkedList<>(),
          Nullable.empty(), pname,
          expressionVisitor.gatherArguments(ctx.parentConstructorCall().arguments()),
          ""));
      parentCall.get().setOriginalLabel(parentCall.get().label);
    } else {
      if (classDeclVisitor.getWithinClassParent().isNotNull()) {
        //has parent but no explicit parent call, so add default call (may not exist,
        //typecheck error)
        parentCall = Nullable.of(new FunctionCallExpression(line, column,
            new LinkedList<>(), new LinkedList<>(),
            Nullable.empty(), pname,
            new ArrayList<>(),
            ""));
        parentCall.get().setOriginalLabel(parentCall.get().label);
      }
    }
    
    List<DeclarationStatement> params = gatherParams(ctx.parameters());
    
    List<Type> paramTypes = new LinkedList<>();
    for (DeclarationStatement param : params) {
      paramTypes.add(param.type);
    }
    
    FunctionType fullType = new FunctionType((DotAccessType)
        programVisitor.getWithinModuleOrClass(),
        VoidType.Create(), paramTypes, LambdaStatus.NOT_LAMBDA);
    
    Function val = new Function(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        false, ctx.REQUIRE() != null, false,
        //no real return type, not really applicable to constructors
        "constructor",
        (UserDeclType) fullType.within.get(),
        fullType,
        gatherParams(ctx.parameters()),
        body, false, parentCall,
        Function.MetaType.CONSTRUCTOR); //not static, is constructor
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Function visitDestructorFun(JasmintParser.DestructorFunContext ctx) {
    //no parameters, so if parent, just add default parent call
    return null;
  }
  
  @Override
  public Function visitOverrideFunShort(JasmintParser.OverrideFunShortContext ctx) {
    throw new IllegalArgumentException("override short form is not yet supported");
  }
  
  @Override
  public Function visitOverrideFunLong(JasmintParser.OverrideFunLongContext ctx) {
    Function fn = visit(ctx.function());
    long saveOL = fn.getOriginalLabel();
    
    FunctionType fullType = new FunctionType(classDeclVisitor.getWithinClass().get(),
        fn.fullType.returnType, fn.fullType.argTypes, LambdaStatus.NOT_LAMBDA);
    
    fn = new Function(fn.lineNum, fn.columnNum,
        new LinkedList<>(), new LinkedList<>(),
        fn.export, fn.required, true,
        fn.name, (UserDeclType) fullType.within.get(), 
        fullType, fn.params, fn.getBody(), fn.getIsStatic(), 
        fn.parentCall, fn.metaType);
    fn.setOriginalLabel(saveOL);
    return fn;
  }
  
  private List<DeclarationStatement> gatherParams(JasmintParser.ParametersContext ctx) {
    List<DeclarationStatement> params = new LinkedList<>();
    
    for (JasmintParser.PdeclContext pctx : ctx.pdecl()) {
      DeclarationStatement val = (DeclarationStatement) statementVisitor.visit(pctx.declNoExpr());
      
      val = new DeclarationStatement(val.lineNum, val.columnNum, val.preComments, val.postComments,
          val.type.setConst(pctx.CONST() != null),
          val.name, Nullable.empty(),
          false, false); //cannot be static or required
      val.setOriginalLabel(val.label);
      params.add(val);
    }
    
    return params;
  }
  
}
