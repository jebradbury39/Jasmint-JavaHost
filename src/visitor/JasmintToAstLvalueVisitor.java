package visitor;

import antlrgen.JasmintBaseVisitor;
import antlrgen.JasmintParser;
import ast.Lvalue;
import ast.LvalueBracket;
import ast.LvalueDereference;
import ast.LvalueDot;
import ast.LvalueFfi;
import ast.LvalueId;
import ast.MultiLvalue;
import ast.StringExpression;
import java.util.LinkedList;
import java.util.List;

public class JasmintToAstLvalueVisitor extends JasmintBaseVisitor<Lvalue> {
  private final JasmintToAstExpressionVisitor expressionVisitor;
  
  public JasmintToAstLvalueVisitor(JasmintToAstProgramVisitor programVisitor,
      JasmintToAstExpressionVisitor exprVisitor) {
    this.expressionVisitor = exprVisitor;
  }
  
  @Override
  public Lvalue visitMultiLvalue(JasmintParser.MultiLvalueContext ctx) {
    List<Lvalue> lvalues = new LinkedList<>();
    for (JasmintParser.LvalueContext lctx : ctx.lvalue()) {
      lvalues.add(visit(lctx));
    }
    if (lvalues.size() == 1) {
      return lvalues.get(0);
    }
    MultiLvalue val = new MultiLvalue(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(), lvalues);
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Lvalue visitLvalueId(JasmintParser.LvalueIdContext ctx) {
    LvalueId val = new LvalueId(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        ctx.ID().getText());
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Lvalue visitLvalueDot(JasmintParser.LvalueDotContext ctx) {
    LvalueDot val = new LvalueDot(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.expression()), ctx.ID().getText());
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Lvalue visitLvalueDereference(JasmintParser.LvalueDereferenceContext ctx) {
    LvalueDereference val = new LvalueDereference(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.expression()));
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Lvalue visitLvalueBracket(JasmintParser.LvalueBracketContext ctx) {
    LvalueBracket val = new LvalueBracket(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        expressionVisitor.visit(ctx.lft),
        expressionVisitor.visit(ctx.index));
    val.setOriginalLabel(val.label);
    return val;
  }
  
  @Override
  public Lvalue visitLvalueFfi(JasmintParser.LvalueFfiContext ctx) {
    LvalueFfi val = new LvalueFfi(ctx.getStart().getLine(),
        ctx.getStart().getCharPositionInLine(),
        new LinkedList<>(), new LinkedList<>(),
        (StringExpression) expressionVisitor.visit(ctx.ffiBlock().stringExpression()));
    val.setOriginalLabel(val.label);
    return val;
  }
}
