package typegraph;

import errors.FatalMessageException;
import java.util.Map;
import java.util.Set;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;
import util.DotGen;

public interface TypeNode {

  boolean typeEquals(Type type, boolean looseMatch);
  
  public void addLink(CastDirection linkType, TypeNode node);

  boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException;

  Type getType();

  public void updateDotGraph(DotGen dotGraph);
  
}
