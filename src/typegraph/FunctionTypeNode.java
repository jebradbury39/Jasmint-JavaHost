package typegraph;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import typecheck.AbstractType.TypeType;
import typecheck.AnyType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.NullType;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;

public class FunctionTypeNode extends AbstractTypeNode {

  public FunctionTypeNode() {
    super();
  }
  
  @Override
  public int hashCode() {
    return TypeType.FUNCTION_TYPE.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    return other instanceof FunctionTypeNode;
  }

  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    return type instanceof FunctionType || type instanceof AnyType;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    if (!typeEquals(fromTy, true)) {
      return false; //should be a redundant check
    }
    if (toTy instanceof NullType) {
      return true;
    }
    if (!typeEquals(toTy, true)) {
      return false; //not a function
    }
    if (fromTy.getInnerTypes().size() != toTy.getInnerTypes().size()) {
      return false;
    }
    
    //both are functions, now just check return type and arg types
    FunctionType fromFnTy = (FunctionType) fromTy;
    FunctionType toFnTy = (FunctionType) toTy;
    
    Iterator<Type> iterFrom = fromFnTy.argTypes.iterator();
    Iterator<Type> iterTo = toFnTy.argTypes.iterator();
    while (iterFrom.hasNext() && iterTo.hasNext()) {
      if (!graph.canCastTo(iterFrom.next(), direction, iterTo.next())) {
        return false;
      }
    }
    
    return graph.canCastTo(fromFnTy.returnType, direction, toFnTy.returnType);
  }

  @Override
  public Type getType() {
    return new FunctionType(Nullable.empty(), AnyType.Create(), new LinkedList<>(),
        LambdaStatus.UNKNOWN);
  }

}
