package typegraph;

import typegraph.TypeGraph.CastDirection;

public class TypeNodeLink {
  
  public final CastDirection linkType;
  public final TypeNode toNode;
  
  public TypeNodeLink(CastDirection linkType, TypeNode toNode) {
    this.linkType = linkType;
    this.toNode = toNode;
  }
  
  @Override
  public int hashCode() {
    return linkType.hashCode() + toNode.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof TypeNodeLink)) {
      return false;
    }
    TypeNodeLink otherLink = (TypeNodeLink) other;
    return linkType == otherLink.linkType && toNode.equals(otherLink.toNode);
  }
}
