package typegraph;

import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import typecheck.AbstractType;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.ClassType;
import typecheck.EnumType;
import typecheck.Float32Type;
import typecheck.Float64Type;
import typecheck.ImportType;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.NullType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.DotGen;
import util.Triplet;

public class TypeGraph {

  /*
   * Direction is in terms of the type pyramid 1 11111111 11111111 11111111
   * 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
   * 11111111 11111111 11111111 11111111 When you go UP the pyramid, it means you
   * are converting to a SMALLER type. Conversely, when you go DOWN the pyramid,
   * it means you are converting to a LARGER type.
   */

  public static enum CastDirection {
    UP, DOWN, SIBLING,
  }

  private NullTypeNode nullNode = new NullTypeNode();
  private List<TypeNode> referenceNodes = new ArrayList<>(); // root is null
  private List<TypeNode> userTypeNodes = new ArrayList<>(); // should all be singles
  private List<TypeNode> primitiveNodes = new ArrayList<>();

  private final Nullable<TypeGraph> parent;
  public final ModuleType currentModuleFullType;
  // only non-null if parent is null
  public final Nullable<EffectiveImports> effectiveImports;
  public final MsgState msgState;

  // Create a top-level type graph
  public TypeGraph(ModuleType currentModuleFullType, EffectiveImports effectiveImports,
      MsgState msgState) throws FatalMessageException {
    this.parent = Nullable.empty();
    this.currentModuleFullType = currentModuleFullType;
    this.effectiveImports = Nullable.of(effectiveImports);
    this.msgState = msgState;

    // add the base refs
    referenceNodes.add(new BaseRefTypeNode(AbstractType.TypeType.ARRAY_TYPE));
    referenceNodes.add(new BaseRefTypeNode(AbstractType.TypeType.MAP_TYPE));

    // add base ref linking
    nullNode.addLink(CastDirection.DOWN, referenceNodes.get(0)); // null down to array$
    nullNode.addLink(CastDirection.DOWN, referenceNodes.get(1)); // null down to map$
    referenceNodes.get(0).addLink(CastDirection.UP, nullNode); // array* up to null
    referenceNodes.get(1).addLink(CastDirection.UP, nullNode); // map* up to null
    
    FunctionTypeNode fnTypeNode = new FunctionTypeNode();
    nullNode.addLink(CastDirection.DOWN, fnTypeNode);
    fnTypeNode.addLink(CastDirection.UP, nullNode);

    // add in all the primitives
    primitiveNodes.add(new PrimitiveTypeNode(BoolType.Create()));
    primitiveNodes.add(new PrimitiveTypeNode(CharType.Create()));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(true, 8)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(true, 16)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(true, 32)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(true, 64)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(false, 8)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(false, 16)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(false, 32)));
    primitiveNodes.add(new PrimitiveTypeNode(IntType.Create(false, 64)));
    primitiveNodes.add(new PrimitiveTypeNode(Float32Type.Create()));
    primitiveNodes.add(new PrimitiveTypeNode(Float64Type.Create()));
    primitiveNodes.add(new PrimitiveTypeNode(StringType.Create()));
    primitiveNodes.add(new PrimitiveTypeNode(VoidType.Create()));
    primitiveNodes.add(fnTypeNode);
    primitiveNodes.add(new ArrayTypeNode());
    primitiveNodes.add(new MapTypeNode());
    primitiveNodes.add(new MultiTypeNode());

    // add primitive linking
    /*
     * bool -> (char | int8 | uint8) -> (int16 | uint16) -> (int32 | uint32) ->
     * (int64 | uint64) -> (float32) -> (float64)
     * 
     * char -> int8 int8 -> char
     */
    // add downlinks (and uplinks from the child to parent)
    addLinks(BoolType.Create(), CharType.Create());
    addLinks(BoolType.Create(), IntType.Create(true, 8));
    addLinks(BoolType.Create(), IntType.Create(false, 8));

    addLinks(CharType.Create(), IntType.Create(true, 8));

    addLinks(CharType.Create(), IntType.Create(true, 16));
    addLinks(CharType.Create(), IntType.Create(false, 16));
    addLinks(IntType.Create(true, 8), IntType.Create(true, 16));
    addLinks(IntType.Create(true, 8), IntType.Create(false, 16));
    addLinks(IntType.Create(false, 8), IntType.Create(true, 16));
    addLinks(IntType.Create(false, 8), IntType.Create(false, 16));
    addSiblingLinks(IntType.Create(false, 8), IntType.Create(true, 8)); //signed<->unsigned

    addLinks(IntType.Create(true, 16), IntType.Create(true, 32));
    addLinks(IntType.Create(true, 16), IntType.Create(false, 32));
    addLinks(IntType.Create(false, 16), IntType.Create(true, 32));
    addLinks(IntType.Create(false, 16), IntType.Create(false, 32));
    addSiblingLinks(IntType.Create(false, 16), IntType.Create(true, 16)); //signed<->unsigned

    addLinks(IntType.Create(true, 32), IntType.Create(true, 64));
    addLinks(IntType.Create(true, 32), IntType.Create(false, 64));
    addLinks(IntType.Create(false, 32), IntType.Create(true, 64));
    addLinks(IntType.Create(false, 32), IntType.Create(false, 64));
    addSiblingLinks(IntType.Create(false, 32), IntType.Create(true, 32)); //signed<->unsigned
    
    addSiblingLinks(IntType.Create(false, 64), IntType.Create(true, 64)); //signed<->unsigned

    addLinks(IntType.Create(true, 64), Float32Type.Create());
    addLinks(IntType.Create(false, 64), Float32Type.Create());
    addLinks(Float32Type.Create(), Float64Type.Create());
  }

  private TypeGraph(TypeGraph parent, ModuleType currentModuleFullType, MsgState msgState) {
    this.parent = Nullable.of(parent);
    this.currentModuleFullType = currentModuleFullType;
    this.effectiveImports = Nullable.empty();
    this.msgState = msgState;
  }

  public DotGen toDotFormat() {
    DotGen dotGraph = new DotGen();
    for (TypeNode refNode : referenceNodes) {
      refNode.updateDotGraph(dotGraph);
    }
    for (TypeNode userNode : userTypeNodes) {
      userNode.updateDotGraph(dotGraph);
    }
    for (TypeNode primNode : primitiveNodes) {
      primNode.updateDotGraph(dotGraph);
    }
    nullNode.updateDotGraph(dotGraph);
    return dotGraph;
  }

  @Override
  public String toString() {
    return toDotFormat().toString();
  }

  public TypeGraph extend() {
    return new TypeGraph(this, currentModuleFullType, msgState);
  }

  private void addLinks(Type parent, Type child) throws FatalMessageException {
    TypeNode parentNode = findNode(parent, false).get(0);
    TypeNode childNode = findNode(child, false).get(0);

    if (parentNode.getType().equals(childNode.getType())) {
      throw new IllegalArgumentException();
    }

    parentNode.addLink(CastDirection.DOWN, childNode);
    childNode.addLink(CastDirection.UP, parentNode);
  }
  
  private void addSiblingLinks(Type fromTy, Type toTy) throws FatalMessageException {
    TypeNode fromNode = findNode(fromTy, false).get(0);
    TypeNode toNode = findNode(toTy, false).get(0);

    if (fromNode.getType().equals(toNode.getType())) {
      throw new IllegalArgumentException();
    }

    fromNode.addLink(CastDirection.SIBLING, toNode);
    toNode.addLink(CastDirection.SIBLING, fromNode);
  }

  public Triplet<UserTypeNode, UserRefTypeNode, TypeNode> addUserNode(UserInstanceType type,
      Nullable<UserInstanceType> parent) throws FatalMessageException {
    Optional<Nullable<Type>> typeOpt = Optional.empty();

    typeOpt = type.basicNormalize(false, currentModuleFullType, getEffectiveImports(), msgState);
    if (!typeOpt.isPresent()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Failed to normalize: " + type);
    }
    type = (UserInstanceType) typeOpt.get().get();

    if (parent.isNotNull()) {
      typeOpt = parent.get().basicNormalize(false, currentModuleFullType, getEffectiveImports(),
          msgState);
      if (!typeOpt.isPresent()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to normalize parent: " + type);
      }
      parent = Nullable.of((UserInstanceType) typeOpt.get().get());
    }

    // add the solid user type
    UserTypeNode solidUserNode = new UserTypeNode(type);
    userTypeNodes.add(solidUserNode);
    
    if (type instanceof EnumType) {
      // add a link from the solid user type to int if this is an enum
      TypeNode int32Node = findNode(IntType.Create(true, 32), false).get(0);
      solidUserNode.addLink(CastDirection.UP, int32Node);
    }
    
    // add the ref with links
    UserRefTypeNode userNode = new UserRefTypeNode(type, false);
    List<TypeNode> found = findNode(new ReferenceType(type), true);
    boolean foundUserNode = false;
    for (TypeNode node : found) {
      if (!((UserRefTypeNode) node).isParent) {
        userNode = (UserRefTypeNode) node;
        foundUserNode = true;
      }
    }
    if (!foundUserNode) {
      referenceNodes.add(userNode);
      // gather and special link isParent nodes of this type
      for (TypeNode node : found) {
        if (((UserRefTypeNode) node).isParent) {
          userNode.addSameClassLink((UserRefTypeNode) node);
          ((UserRefTypeNode) node).addSameClassLink(userNode);
        } else {
          throw new IllegalArgumentException("Can only have one central user node");
        }
      }
    }

    TypeNode parentNode = nullNode;
    if (parent.isNotNull()) {
      // adjust parent to have known generics
      ArrayList<Type> newParentInnerTypes = new ArrayList<Type>(parent.get().getInnerTypes());
      for (Type userIterTy : type.getInnerTypes()) {
        int parentTyIdx = 0;
        for (Type parentIterTy : parent.get().getInnerTypes()) {
          if (parentIterTy.equals(userIterTy)) {
            // 'T' in class declaration used in extends parent{T}, so mark the parent T as
            // generic
            // newParentInnerTypes.set(parentTyIdx, ((ClassType)
            // parentIterTy).copyWithSetGeneric(true));
          }
          parentTyIdx += 1;
        }
      }
      parent = Nullable
          .of(new ClassType(parent.get().outerType, parent.get().name, newParentInnerTypes));

      // must be exact match, right down to isGenerics
      List<TypeNode> parentNodes = findNode(new ReferenceType(parent.get()), false);
      boolean foundParentNode = false; // parentNode cannot be the central node
      for (TypeNode pnode : parentNodes) {
        if (((UserRefTypeNode) pnode).isParent) {
          foundParentNode = true;
          parentNode = pnode;
          break;
        }
      }
      if (!foundParentNode) {
        TypeNode tmpParentNode = new UserRefTypeNode(parent.get(), true);
        tmpParentNode.addLink(CastDirection.UP, parentNode); // add link to null
        parentNode.addLink(CastDirection.DOWN, tmpParentNode);
        parentNode = tmpParentNode;
        referenceNodes.add(parentNode);
        // find central match and add special link (if the node exists yet)
        List<TypeNode> foundP = findNode(new ReferenceType(parent.get()), true);
        for (TypeNode node : foundP) {
          if (!((UserRefTypeNode) node).isParent) {
            ((UserRefTypeNode) parentNode).addSameClassLink((UserRefTypeNode) node);
            ((UserRefTypeNode) node).addSameClassLink((UserRefTypeNode) parentNode);
          }
        }
      }
    }
    userNode.addLink(CastDirection.UP, parentNode);
    parentNode.addLink(CastDirection.DOWN, userNode);

    return new Triplet<>(solidUserNode, userNode, parentNode);
  }

  private Map<ImportType, ImportType> getEffectiveImportsMap() {
    // get it from global
    if (parent.isNull()) {
      final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
      filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
      filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
      filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);
      return effectiveImports.get().getModuleMapping(filterImpTypes);
    }
    return parent.get().getEffectiveImportsMap();
  }

  private EffectiveImports getEffectiveImports() {
    // get it from global
    if (parent.isNull()) {
      return effectiveImports.get();
    }
    return parent.get().getEffectiveImports();
  }

  public List<TypeNode> findNode(Type type, boolean looseMatch) throws FatalMessageException {
    Optional<Nullable<Type>> typeOpt = type.basicNormalize(false, currentModuleFullType,
        getEffectiveImports(), msgState);
    if (!typeOpt.isPresent()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Failed to normalize: " + type);
    }
    type = typeOpt.get().get();

    List<TypeNode> searchList = primitiveNodes;
    List<TypeNode> found = new LinkedList<>();

    if (type instanceof ReferenceType) {
      searchList = referenceNodes;
    } else if (type instanceof UserInstanceType) {
      searchList = userTypeNodes;
    } else if (type instanceof NullType) {
      if (nullNode.typeEquals(type, looseMatch)) {
        found.add(nullNode);
      }
      return found;
    }

    for (TypeNode node : searchList) {
      if (node.typeEquals(type, looseMatch)) {
        found.add(node);
      }
    }

    if (found.isEmpty() && parent.isNotNull()) {
      return parent.get().findNode(type, looseMatch);
    }

    return found;
  }
  
  public boolean canCastTo(Type fromTy, CastDirection direction, Type toTy)
      throws FatalMessageException {
    return canCastTo(fromTy, CastFilter.Create(direction), toTy);
  }

  public boolean canCastTo(Type fromTy, CastFilter castFilter, Type toTy)
      throws FatalMessageException {
    // first normalize the types
    Optional<Nullable<Type>> normFromTy = fromTy.basicNormalize(false, this.currentModuleFullType,
        getEffectiveImports(), this.msgState);
    if (!normFromTy.isPresent()) {
      return false;
    }
    fromTy = normFromTy.get().get();

    Optional<Nullable<Type>> normToTy = toTy.basicNormalize(false, this.currentModuleFullType,
        getEffectiveImports(), this.msgState);
    if (!normToTy.isPresent()) {
      return false;
    }
    toTy = normToTy.get().get();

    if (directMatch(fromTy, castFilter, toTy)) {
      return true;
    }

    List<TypeNode> fromNodes = findNode(fromTy, true);
    List<TypeNode> toNodes = findNode(toTy, true);
    if (fromNodes.isEmpty() || toNodes.isEmpty()) {
      return false;
    }

    // check each possible starting node (only applicable for user references)
    for (TypeNode fromNode : fromNodes) {
      for (CastDirection dir : castFilter.direction) {
        Set<TypeNode> visited = new HashSet<>();
        if (fromNode.findPathTo(fromTy, dir, toTy, new HashMap<>(), this, visited)) {
          return true;
        }
      }
    }

    return false;
  }

  /*
   * Handle cases like T -> T or T$ -> T$, or null -> T$
   * 
   * Just for generics, really
   */
  private boolean directMatch(Type fromTy, CastFilter castFilter, Type toTy) {
    // handle null cases
    if (fromTy instanceof NullType) {
      if (castFilter.direction.contains(CastDirection.UP)) {
        return (toTy instanceof NullType);
      }
      return toTy instanceof ReferenceType; // null can cast down to anything
    }
    if (toTy instanceof NullType) {
      if (castFilter.direction.contains(CastDirection.DOWN)) {
        return (fromTy instanceof NullType);
      }
      return fromTy instanceof ReferenceType; // anything can cast up to null
    }

    return fromTy.equals(toTy);
  }

  public void clearGenericNodes() {
    // TODO Auto-generated method stub

  }

}
