package typegraph;

import errors.FatalMessageException;
import java.util.Map;
import java.util.Set;
import typecheck.AbstractType.TypeType;
import typecheck.AnyType;
import typecheck.MapType;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;

public class MapTypeNode extends AbstractTypeNode {

  public MapTypeNode() {
    super();
  }
  
  @Override
  public int hashCode() {
    return TypeType.MAP_TYPE.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    return other instanceof MapTypeNode;
  }

  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    return type instanceof MapType || type instanceof AnyType;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    if (!typeEquals(fromTy, true)) {
      return false; //should be a redundant check
    }
    if (!typeEquals(toTy, true)) {
      return false; //not a map
    }
    
    //both are maps, just compare key and value type
    MapType fromMapTy = (MapType) fromTy;
    MapType toMapTy = (MapType) toTy;
    
    return graph.canCastTo(fromMapTy.keyType, direction, toMapTy.keyType)
        && graph.canCastTo(fromMapTy.valueType, direction, toMapTy.valueType);
  }

  @Override
  public Type getType() {
    return new MapType(null, null);
  }

}
