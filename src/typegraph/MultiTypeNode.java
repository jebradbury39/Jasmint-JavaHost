package typegraph;

import errors.FatalMessageException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import typecheck.AbstractType.TypeType;
import typecheck.MultiType;
import typecheck.Type;
import typegraph.TypeGraph.CastDirection;

public class MultiTypeNode extends AbstractTypeNode {

  @Override
  public int hashCode() {
    return TypeType.MULTI_TYPE.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    return other instanceof MultiTypeNode;
  }
  
  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    //since this is multiple values, cannot match AnyType
    return type instanceof MultiType;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType,
      TypeGraph graph, Set<TypeNode> visited) throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (!typeEquals(fromTy, true)) {
      return false; //should be a redundant check
    }
    if (!typeEquals(toTy, true)) {
      return false; //can only cast if both are multi type, ofc
    }
    
    MultiType fromType = (MultiType) fromTy;
    MultiType toType = (MultiType) toTy;
    
    if (fromType.types.size() != toType.types.size()) {
      return false;
    }
    
    //inner types can be anything. Does not need to be walkable
    Iterator<Type> iterFrom = fromType.types.iterator();
    Iterator<Type> iterTo = toType.types.iterator();
    while (iterFrom.hasNext() && iterTo.hasNext()) {
      if (!graph.canCastTo(iterFrom.next(), direction, iterTo.next())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Type getType() {
    return new MultiType(new LinkedList<>());
  }

}
