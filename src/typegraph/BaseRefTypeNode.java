package typegraph;

import errors.FatalMessageException;
import java.util.Map;
import java.util.Set;
import typecheck.AbstractType;
import typecheck.AbstractType.TypeType;
import typecheck.AnyType;
import typecheck.ArrayType;
import typecheck.MapType;
import typecheck.NullType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.UndeterminedType;
import typegraph.TypeGraph.CastDirection;

public class BaseRefTypeNode extends AbstractTypeNode {

  //array or map
  public final AbstractType.TypeType type;
  
  public BaseRefTypeNode(AbstractType.TypeType type) {
    super();
    this.type = type;
  }
  
  @Override
  public int hashCode() {
    return TypeType.REFERENCE_TYPE.hashCode() + type.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof BaseRefTypeNode)) {
      return false;
    }
    BaseRefTypeNode node = (BaseRefTypeNode) other;
    return type == node.type;
  }

  @Override
  public boolean typeEquals(Type type, boolean looseMatch) {
    if (type instanceof AnyType) {
      return true;
    }
    if (!(type instanceof ReferenceType)) {
      return false;
    }
    Type derefTy = ((ReferenceType) type).innerType;
    return derefTy.getTypeType() == this.type;
  }

  @Override
  public boolean findPathTo(Type fromTy, CastDirection direction, Type toTy,
      Map<String, Type> mapGenericNameToRealType, TypeGraph graph, Set<TypeNode> visited)
          throws FatalMessageException {
    if (checkVisited(visited)) {
      return false;
    }
    if (fromTy instanceof AnyType || toTy instanceof AnyType) {
      return true;
    }
    if (!typeEquals(fromTy, true)) {
      return false; //should be a redundant check
    }
    
    if (toTy instanceof NullType) {
      return direction == CastDirection.UP;
    }
    if (!typeEquals(toTy, true)) {
      return false; //not a proper reference
    }
    
    Type newFrom = fromTy;
    if (fromTy instanceof ReferenceType) {
      newFrom = ((ReferenceType) fromTy).innerType;
    }
    Type newTo = toTy;
    if (toTy instanceof ReferenceType) {
      newTo = ((ReferenceType) toTy).innerType;
    }
    
    return graph.canCastTo(newFrom, direction, newTo);
  }

  @Override
  public Type getType() {
    switch (type) {
      case ARRAY_TYPE:
        return new ReferenceType(new ArrayType(null));
      case MAP_TYPE:
        return new ReferenceType(new MapType(null, null));
      default:
        break;
    }
    return UndeterminedType.Create();
  }
}
