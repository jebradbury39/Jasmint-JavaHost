package mbo;

import ast.Program;
import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntry;
import audit.AuditRenameTypes;
import audit.AuditRenameTypesLite;
import audit.AuditTrail;
import audit.AuditTrailLite;
import com.google.protobuf.util.JsonFormat;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ProgHeader;
import header.RegisteredGenerics;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import typecheck.AbstractType;
import typecheck.ClassType;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.Type;
import util.Pair;

/*
 * This datastructure can be serialized to/from a file and represents all build information
 * for one module
 */

public class ModuleBuildObj {
  /*
  public static enum AuditEntry {
    LINK_HEADERS,
    LINK_PROVIDERS,
    SHUFFLE_CLASSES,
    TYPECHECK_START,
    TYPECHECK_END,
    RENAMING_START,
    RENAMING_END,
    RESOLVE_GENERICS_START,
    RESOLVE_GENERICS_END,
    ANF,
    RENAME_TYPES_START,
    RENAME_TYPES_END
  }
  */
  
  public static enum ProgressStatus {
    NONE,
    START,
    END
  }
  
  public final ModuleType moduleType; //abs module path
  public Set<ModuleType> submods; //abs module paths
  public EffectiveImports effectiveImports; //add inherited ASAP
  public Program program; //Could be moved to another object file
  public ProgHeader progHeader;
  public final AuditTrail auditTrail;
  public ProgressStatus progressStatus;
  public Vtables vtables; //abs path, ofc, but only our module
  
  public ModuleBuildObj(ModuleType moduleType, Set<ModuleType> submods, Program program,
      ProgHeader progHeader, AuditTrail auditTrail, ProgressStatus progressStatus,
      EffectiveImports effectiveImports, Vtables vtables) {
    this.moduleType = moduleType;
    this.submods = submods;
    this.program = program;
    this.progHeader = progHeader;
    this.auditTrail = auditTrail;
    this.progressStatus = progressStatus;
    this.effectiveImports = effectiveImports;
    this.vtables = vtables;
  }
  
  public void serializeToFile(String progBuildPath) throws SerializationError, IOException {
    AstProto.ModuleBuildObj.Builder document = AstProto.ModuleBuildObj.newBuilder();
    
    document.setModuleType(moduleType.serialize());
    for (ModuleType submod : submods) {
      document.addSubmods(submod.serialize());
    }
    
    document.setProgram(program.serialize());
    document.setProgHeader(progHeader.serialize());
    document.setAuditTrail(auditTrail.serialize());
    
    document.setProgressStatus(progressStatus.ordinal());
    
    document.setEffectiveImports(effectiveImports.serialize());
    
    document.setVtables(vtables.serialize());
    
    FileOutputStream fout = Project.getOutputFile(progBuildPath);
    document.build().writeTo(fout);
    fout.close();
    
    fout = Project.getOutputFile(progBuildPath + ".json");
    JsonFormat.Printer jprinter = JsonFormat.printer();
    fout.write(jprinter.print(document.build()).getBytes());
    fout.close();
  }
  
  public static ModuleBuildObj deserializeFromFile(Project project,
      String moduleBuildFile, MsgState msgState)
      throws SerializationError, IOException, FatalMessageException {
    FileInputStream fis = new FileInputStream(moduleBuildFile);
    AstProto.ModuleBuildObj document = AstProto.ModuleBuildObj.parseFrom(fis);
    fis.close();
    
    ModuleType moduleType = (ModuleType) AbstractType.deserialize(document.getModuleType());
    
    Set<ModuleType> submods = new HashSet<>();
    for (AstProto.Type bvSubmod : document.getSubmodsList()) {
      submods.add((ModuleType) AbstractType.deserialize(bvSubmod));
    }
    
    Map<ModuleType, ImportUsage> imports = new HashMap<>();
    Iterator<AstProto.Type> iterImp = document.getImportsList().iterator();
    Iterator<Integer> iterImpUsage = document.getImportUsageList().iterator();
    while (iterImp.hasNext() && iterImpUsage.hasNext()) {
      imports.put((ModuleType) AbstractType.deserialize(iterImp.next()),
          ImportUsage.values()[iterImpUsage.next()]);
    }
    
    AuditTrail auditTrail = new AuditTrail(moduleType);//AuditTrail.deserialize(project, document.getAuditTrail());
    
    ModuleBuildObj mbo = new ModuleBuildObj(
        moduleType,
        submods, Program.deserialize(document.getProgram()),
        ProgHeader.deserialize(document.getProgHeader(), msgState),
        auditTrail,
        ProgressStatus.values()[document.getProgressStatus()],
        EffectiveImports.deserialize(document.getEffectiveImports()),
        Vtables.deserialize(document.getVtables()));
    return mbo;
  }

  /*
   * Create mapping of templated class types to real class types e.g. A<int> to A_int
   * This must come from across all effective imports
   */
  public Map<Type, Type> indexGenericToRealMapping(Project project,
      AuditTrailLite projectAuditTrailSoFar, RegisteredGenerics ourModRegisteredGenerics, MsgState msgState)
          throws FatalMessageException {
    Map<Type, Type> replacements = new HashMap<>();
    Map<Type, Type> localReplacements = new HashMap<>();
    
    //do our generics first
    ourModRegisteredGenerics.indexGenericToRealMapping(moduleType,
        effectiveImports,
        localReplacements, msgState);
    //ensure we have non-abs versions since reg generics always use abs path
    for (Entry<Type, Type> entry : localReplacements.entrySet()) {
      ClassType fromTy = (ClassType) entry.getKey();
      fromTy = new ClassType(Nullable.empty(), fromTy.name, fromTy.innerTypes);
      replacements.put(fromTy, entry.getValue());
      replacements.put(entry.getKey(), entry.getValue());
    }
    
    //then effective imports (us again)
    final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);
    
    EffectiveImports effImports = effectiveImports.applyTransforms(projectAuditTrailSoFar,
        msgState);
    for (Pair<ImportType, Nullable<ProjectNameVersion>> imp
        : effImports.getImportList(true, filterImpTypes))
    {
      Nullable<ModuleEndpoint> impModEp = project.lookupModule(imp.a, projectAuditTrailSoFar);
      if (impModEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Could not find module: " + imp);
      }
      impModEp.get().load();
      LoadedModuleHandle impModHandle = (LoadedModuleHandle) impModEp.get().getHandle();
      impModHandle.registeredGenerics.indexGenericToRealMapping(
          impModHandle.getModuleType(projectAuditTrailSoFar, msgState),
          impModHandle.moduleBuildObj.effectiveImports.applyTransforms(projectAuditTrailSoFar,
              msgState),
          replacements, msgState);
    }
    
    return replacements;
  }

  /*
  public ModuleBuildObj renameTypes(String moduleAppend) {
    Set<ModuleType> newSubmods = new HashSet<>();
    for (ModuleType submod : submods) {
      newSubmods.add((ModuleType) submod.modAppend(moduleAppend));
    }
    
    Map<Type, Type> newTypeRenamings = new HashMap<>();
    for (Entry<Type, Type> tr : typeRenamings.entrySet()) {
      newTypeRenamings.put(tr.getKey().modAppend(moduleAppend),
          tr.getValue().modAppend(moduleAppend));
    }
    
    ModuleBuildObj mbo = new ModuleBuildObj((ModuleType) moduleType.modAppend(moduleAppend),
        newSubmods, program,
        progHeader, auditTrail, progressStatus, originalEffectiveImports,
        vtables);
    
    for (Entry<ModuleType, ImportUsage> dep : dependentOn.entrySet()) {
      mbo.dependentOn.put((ModuleType) dep.getKey().modAppend(moduleAppend), dep.getValue());
    }
    
    return mbo;
  }
  */

  //include imported renamings with ours
  public Map<Type, Type> fullTypeRenamings(Project project, MsgState msgState)
      throws FatalMessageException {
    AuditEntry lastAuditEntry = auditTrail.getLast();
    if (!(lastAuditEntry instanceof AuditRenameTypes)) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAME_TYPES,
          "Last audit entry is not AuditRenameTypes: " + lastAuditEntry);
    }
    
    AuditRenameTypes mboTransform = (AuditRenameTypes) lastAuditEntry;
    mboTransform = (AuditRenameTypes) auditTrail.combineWithImported(moduleType, mboTransform,
        project, effectiveImports.applyTransforms(project.getAuditTrail(), msgState),
        project.getAuditTrail(), msgState);
    
    Map<Type, Type> fullRenamings = new HashMap<>(mboTransform.typeRenamings);
    
    //add module renamings
    fullRenamings.putAll(((AuditRenameTypesLite) project.getAuditTrail().getLast()).getMap());
    
    return fullRenamings;
  }
  
}
