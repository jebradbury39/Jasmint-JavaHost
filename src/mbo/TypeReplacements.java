package mbo;

import errors.MsgState;
import errors.Nullable;
import java.util.HashMap;
import java.util.Map;
import typecheck.Type;

public class TypeReplacements {
  public final MsgState msgState;
  private final Map<Type, Type> replacements;
  private final Nullable<TypeReplacements> parent;
  
  public TypeReplacements(MsgState msgState) {
    this.msgState = msgState;
    this.replacements = new HashMap<>();
    this.parent = Nullable.empty();
  }
  
  private TypeReplacements(TypeReplacements parent) {
    this.msgState = parent.msgState;
    this.replacements = new HashMap<>();
    this.parent = Nullable.of(parent);
  }
  
  public TypeReplacements extend() {
    return new TypeReplacements(this);
  }
  
  public Map<Type, Type> getMapping() {
    Map<Type, Type> mapping = new HashMap<>();
    if (parent.isNotNull()) {
      mapping = parent.get().getMapping();
    }
    mapping.putAll(replacements);
    return mapping;
  }

  public void put(Type oldType, Type newType) {
    replacements.put(oldType, newType);
  }
}
