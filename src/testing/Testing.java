package testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Testing {
  public static void testRegression(String workspace) {
    String var = System.getenv("RUN_JSMNT_REGRESSION");
    if (var == null || var.equals("0")) {
      System.out.println("Skip regression test");
        return;
    }
    System.out.println("Start regression test");
    
    String pythonExe = "python3";
    try {
      Runtime.getRuntime().exec("python3 --version");
    } catch (IOException e1) {
      pythonExe = "python";
    }
    
    ProcessBuilder processBuilder = new ProcessBuilder();
    processBuilder.command(pythonExe, "run_tests", "--test_files", "test_files/", "--test_projects", "test_projects/", "--ws", workspace);
    processBuilder.directory(new File("../JasmintUI"));
    processBuilder.redirectErrorStream(true);
    
    try {
      Process process = processBuilder.start();
      
      StringBuilder output = new StringBuilder();

      BufferedReader reader = new BufferedReader(
          new InputStreamReader(process.getInputStream()));

      String line;
      while ((line = reader.readLine()) != null) {
        output.append(line + "\n");
        System.out.println(line);
      }

      int exitVal = process.waitFor();
      
      assertEquals(0, exitVal);
    } catch (IOException e) {
      fail("Failed with exception: " + e);
    } catch (InterruptedException e) {
      fail("Failed with exception: " + e);
    }
  }
}
