package typecheck;

import ast.Expression;
import ast.IntegerExpression;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

//purely for utility, not exposed to the user
public class AnyType extends AbstractType {

  private static AnyType instance = null;
  private static AnyType constInstance = null;
  
  public static AnyType Create() {
    return Create(false);
  }
  
  public static AnyType Create(boolean isConst) {
    if (instance == null) {
      instance = new AnyType();
      constInstance = new AnyType();
      constInstance.isConst = true;
    }
    if (isConst) {
      return constInstance;
    }
    return instance;
  }
  
  private AnyType() {
    super(AbstractType.TypeType.ANY_TYPE);
  }
  
  @Override
  public String toString() {
    return "<any>";
  }

  @Override
  public List<Type> getInnerTypes() {
    return new ArrayList<Type>();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    return this; //never replace directly, result would be random
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();
    document.setAnyType(AstProto.AnyType.getDefaultInstance());
    return document;
  }
  
  public static AnyType deserialize(AstProto.AnyType document) {
    return Create();
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return true;
  }
  
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    return true; //never complains, this allows us to avoid having to do hacks like looseMatch
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Type setConst(boolean isConst) {
    return Create(isConst);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new IntegerExpression("0");
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(this);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    //don't know the size, but also don't throw an error
    return 0;
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }

}
