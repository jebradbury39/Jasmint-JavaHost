package typecheck;

import ast.Expression;
import ast.NullExpression;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

/* can cast to any type */
public class NullType extends AbstractType {

  private static NullType instance = null;
  private static NullType constInstance = null;
  
  public static NullType Create() {
    return Create(false);
  }
  
  public static NullType Create(boolean isConst) {
    if (instance == null) {
      instance = new NullType();
      constInstance = new NullType();
      constInstance.isConst = true;
    }
    if (isConst) {
      return constInstance;
    }
    return instance;
  }
  
  private NullType() {
    super(TypeType.NULL_TYPE);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    return other instanceof NullType;
  }

  @Override
  public List<Type> getInnerTypes() {
    return new LinkedList<>();
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }
    
    return this;
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();
    document.setNullType(AstProto.NullType.getDefaultInstance());
    return document;
  }
  
  public static NullType deserialize(AstProto.NullType document) {
    return Create();
  }
  
  @Override
  public String toString() {
    return super.toString() + "null";
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }
  
  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Type setConst(boolean isConst) {
    return Create(isConst);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new NullExpression();
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(this);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    // 8 byte ptr (assume 64-bit)
    return 8;
  }
  
  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }
}
