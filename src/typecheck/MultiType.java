package typecheck;

import ast.Expression;
import ast.MultiExpression;
import ast.SerializationError;
import astproto.AstProto;
import astproto.AstProto.Type.Builder;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

public class MultiType extends AbstractType {

  public final List<Type> types; //must be at least two
  
  public MultiType(List<Type> types) {
    super(TypeType.MULTI_TYPE);
    this.types = types;
  }

  @Override
  public int hashCode() {
    int hc = super.hashCode();
    for (Type ty : types) {
      hc += ty.hashCode();
    }
    return hc;
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof MultiType)) {
      return false;
    }
    MultiType otherTy = (MultiType)other;
    if (types.size() != otherTy.types.size()) {
      return false;
    }
    Iterator<Type> iterUs = types.iterator();
    Iterator<Type> iterOther = otherTy.types.iterator();
    while (iterUs.hasNext() && iterOther.hasNext()) {
      if (!iterUs.next().equals(iterOther.next())) {
        return false;
      }
    }
    return true;
  }
  
  @Override
  public String toString() {
    String res = super.toString() + "(";
    boolean first = true;
    for (Type type : types) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += type;
    }
    return res + ")";
  }
  
  @Override
  public List<Type> getInnerTypes() {
    return types;
  }

  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }
    List<Type> newTypes = new LinkedList<>();
    for (Type type : types) {
      newTypes.add(type.replaceType(mapFromTo));
    }
    return new MultiType(newTypes);
  }

  @Override
  public Builder serialize() {
    AstProto.Type.Builder document = preSerialize();

    AstProto.MultiType.Builder subDoc = AstProto.MultiType.newBuilder();
    
    for (Type type : types) {
      subDoc.addTypes(type.serialize());
    }
    document.setMultiType(subDoc);
    return document;
  }
  
  public static MultiType deserialize(AstProto.MultiType document)
      throws SerializationError {
    List<Type> newTypes = new LinkedList<>();
    for (AstProto.Type type : document.getTypesList()) {
      newTypes.add(AbstractType.deserialize(type));
    }
    return new MultiType(newTypes);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (types.contains(this)) {
      return true;
    }
    for (Type type : types) {
      if (type.containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    Set<UserDeclType> stackTypes = new HashSet<>();
    for (Type type : types) {
      stackTypes.addAll(type.getUserDeclTypes(stackTypesOnly));
    }
    return stackTypes;
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException {
    List<Type> newTypes = new LinkedList<>();
    for (Type type : types) {
      Optional<Type> tmp = type.normalize(tenv, lineNum, columnNum);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newTypes.add(tmp.get());
    }
    return Optional.of(new MultiType(newTypes));
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    List<Type> newTypes = new LinkedList<>();
    for (Type type : types) {
      newTypes.add(type.renameIds(renamings, msgState));
    }
    return new MultiType(newTypes);
  }

  @Override
  public Type trimRelativeType(UserInstanceType currentModuleFullType) {
    List<Type> newTypes = new LinkedList<>();
    for (Type type : types) {
      newTypes.add(type.trimRelativeType(currentModuleFullType));
    }
    return new MultiType(newTypes);
  }

  @Override
  public Type setConst(boolean isConst) {
    MultiType tmp = new MultiType(types);
    tmp.isConst = isConst;
    return tmp;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<Type> newTypes = new LinkedList<>();
    for (Type type : types) {
      Optional<Nullable<Type>> tmp = type.basicNormalize(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newTypes.add(tmp.get().get());
    }
    return Optional.of(Nullable.of(new MultiType(newTypes)));
  }

  @Override
  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState,
      Set<ModuleType> exportedTypes, boolean skipNormalize) throws FatalMessageException {
    for (Type ty : types) {
      if (!ty.getNormalizedModType(currentModule, effectiveImports, msgState, exportedTypes,
          false)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) throws FatalMessageException {
    List<Expression> expressions = new LinkedList<>();
    for (Type ty : types) {
      expressions.add(ty.getDummyExpression(project, msgState));
    }
    return new MultiExpression(expressions);
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    List<Type> newTypes = new LinkedList<>();
    for (Type type : types) {
      newTypes.add(type.prependFnRetType(prependType, false));
    }
    if (!isTop) {
      return new MultiType(newTypes);
    }
    newTypes.add(0, prependType);
    return new MultiType(newTypes);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    int size = 0;
    
    for (Type ty : types) {
      int tmp = ty.calculateSize(tenv, lineNum, columnNum);
      if (tmp == -1) {
        return -1;
      }
      size += tmp;
    }
    
    return size;
  }

  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    for (Type ty : types) {
      if (!ty.copyFromTenvIntoTenv(tenv, newTenv)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Type asClassDeclType() {
    List<Type> newTypes = new LinkedList<>();
    for (Type ty : types) {
      newTypes.add(ty.asClassDeclType());
    }
    return new MultiType(newTypes);
  }

}
