package typecheck;

public interface ImportableType extends Type {
  public ImportType asImportType();
}
