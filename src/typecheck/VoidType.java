package typecheck;

import ast.Expression;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

public class VoidType extends AbstractType {

  private static VoidType instance = null;
  private static VoidType constInstance = null;
  
  public static VoidType Create() {
    return Create(false);
  }
  
  public static VoidType Create(boolean isConst) {
    if (instance == null) {
      instance = new VoidType();
      constInstance = new VoidType();
      constInstance.isConst = true;
    }
    if (isConst) {
      return constInstance;
    }
    return instance;
  }
  
  private VoidType() {
    super(TypeType.VOID_TYPE);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    return other instanceof VoidType;
  }

  @Override
  public List<Type> getInnerTypes() {
    return new ArrayList<Type>();
  }

  @Override
  public String toString() {
    return super.toString() + "void";
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }
    
    return this;
  }

  @Override
  public AstProto.Type.Builder serialize() {
    return preSerialize();
  }
  
  public static VoidType deserialize(AstProto.VoidType document) {
    return Create();
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }
  
  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Type setConst(boolean isConst) {
    return Create(isConst);
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    throw new IllegalArgumentException("Cannot calculate dummy value for void type");
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    return prependType;
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return 0;
  }
  
  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }
}
