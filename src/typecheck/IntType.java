package typecheck;

import ast.Expression;
import ast.IntegerExpression;
import astproto.AstProto;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;

public class IntType extends AbstractType {

  private static List<Map<Integer, IntType>> signedIntInst = null; //idx0 is not const
  private static List<Map<Integer, IntType>> unsignedIntInst = null;
  
  public static IntType Create(boolean signed, int size) {
    return Create(signed, size, false);
  }
  
  public static IntType Create(boolean signed, int size, boolean isConst) {
    if (signedIntInst == null) {
      //init
      signedIntInst = new ArrayList<>();
      signedIntInst.add(new HashMap<>()); //non-const signed
      signedIntInst.add(new HashMap<>()); //const signed
      
      unsignedIntInst = new ArrayList<>();
      unsignedIntInst.add(new HashMap<>()); //non-const unsigned
      unsignedIntInst.add(new HashMap<>()); //const unsigned
    }
    if (size != 0 && size != 8 && size != 16 && size != 32 && size != 64) {
      throw new IllegalArgumentException("invalid size for integer type: " + size);
    }
    int idx = isConst ? 1 : 0;
    if (size == 0) {
      //size = Integer.SIZE; //convert arch-size to absolute size
      size = 32; //default to 4-byte int
    }
    if (signed) {
      IntType tmp = signedIntInst.get(idx).get(size);
      if (tmp != null) {
        return tmp;
      } else {
        tmp = new IntType(signed, size);
        if (isConst) {
          tmp.isConst = true;
        }
        signedIntInst.get(idx).put(size, tmp);
        return tmp;
      }
    } else {
      IntType tmp = unsignedIntInst.get(idx).get(size);
      if (tmp != null) {
        return tmp;
      } else {
        tmp = new IntType(signed, size);
        if (isConst) {
          tmp.isConst = true;
        }
        unsignedIntInst.get(idx).put(size, tmp);
        return tmp;
      }
    }
  }
  
  public final boolean signed;
  public final int size; //in bits. 0 (arch-defined), 8, 16, 32, 64, ?
  
  private IntType(boolean signed, int size) {
    super(TypeType.INT_TYPE);
    this.signed = signed;
    this.size = size;
  }
  
  @Override
  public Type setConst(boolean isConst) {
    return Create(signed, size, isConst);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof AnyType) {
      return true;
    }
    if (!(other instanceof IntType)) {
      return false;
    }
    IntType ot = (IntType) other;
    return signed == ot.signed && size == ot.size;
  }

  @Override
  public List<Type> getInnerTypes() {
    return new ArrayList<Type>();
  }
  
  @Override
  public String toString() {
    String res = super.toString();
    if (!signed) {
      res += "u";
    }
    res += "int" + size;
    return res;
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type repl = mapFromTo.get(this);
    if (repl != null) {
      return repl;
    }
    
    return this;
  }

  @Override
  public AstProto.Type.Builder serialize() {
    AstProto.Type.Builder document = preSerialize();
    
    AstProto.IntType.Builder subDoc = AstProto.IntType.newBuilder();
    subDoc.setSigned(signed);
    subDoc.setSize(size);
    
    document.setIntType(subDoc);
    return document;
  }
  
  public static IntType deserialize(AstProto.IntType document) {
    return Create(document.getSigned(), document.getSize());
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return types.contains(this);
  }
  
  @Override
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly) {
    return new HashSet<>();
  }

  @Override
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) {
    return Optional.of(this);
  }

  @Override
  public Type renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative,
      ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    return Optional.of(Nullable.of(this));
  }

  @Override
  public Expression getDummyExpression(Project project, MsgState msgState) {
    return new IntegerExpression("0");
  }

  @Override
  public Type prependFnRetType(Type prependType, boolean isTop) {
    if (!isTop) {
      return this;
    }
    List<Type> types = new LinkedList<>();
    types.add(prependType);
    types.add(this);
    return new MultiType(types);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    //8 bits in a byte. Assume division has no remainder
    return size / 8;
  }
  
  @Override
  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException {
    return true;
  }

  @Override
  public Type asClassDeclType() {
    return this;
  }
}
