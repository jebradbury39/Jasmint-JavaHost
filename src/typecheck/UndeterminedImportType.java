package typecheck;

import astproto.AstProto.Type.Builder;
import errors.Nullable;
import java.util.List;
import java.util.Map;
import util.DotGen;

public class UndeterminedImportType extends ImportType {

  public UndeterminedImportType(Nullable<ModuleType> outerType, String name) {
    super(TypeType.UNDETERMINED_IMPORT_TYPE, outerType, name);
  }

  @Override
  public UserDeclType asUserDeclType() {
    throw new UnsupportedOperationException();
  }

  @Override
  public ModuleType asModuleType() {
    throw new UnsupportedOperationException();
  }
  
  public static UndeterminedImportType staticFromList(List<String> path) {
    if (path.isEmpty()) {
      throw new IllegalArgumentException();
    }
    final String name = path.get(path.size() - 1);
    List<String> subPath = path.subList(0, path.size() - 1);
    
    Nullable<ModuleType> moduleType = Nullable.empty();
    for (String item : subPath) {
      moduleType = Nullable.of(new ModuleType(moduleType, item));
    }
    return new UndeterminedImportType(moduleType, name);
  }
  
  @Override
  public UserDeclType fromList(List<String> list) {
    return staticFromList(list);
  }

  @Override
  public ImportType onlyName() {
    return new UndeterminedImportType(Nullable.empty(), name);
  }

  @Override
  public Builder serialize() {
    throw new UnsupportedOperationException();
  }
  
  @Override
  public Type replaceType(Map<Type, Type> mapFromTo) {
    Type tmp = mapFromTo.get(this);
    if (tmp == null) {
      return this;
    }
    if (tmp instanceof UserDeclType) {
      return ((UserDeclType) tmp).asImportType();
    }
    if (tmp instanceof UndeterminedImportType) {
      return tmp;
    }
    throw new IllegalArgumentException("expected UserDeclType or UndeterminedImportType");
  }
  
  @Override
  public DotGen.Color getGraphColor() {
    return DotGen.Color.RED;
  }

}
