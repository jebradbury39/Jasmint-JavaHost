package typecheck;

import ast.Expression;
import astproto.AstProto;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import multifile.Project;
import type_env.TypeEnvironment;
import typecheck.AbstractType.TypeType;

public interface Type extends SizeofTarget {
  public String toString();

  public List<Type> getInnerTypes();

  public Type replaceType(Map<Type, Type> mapFromTo);

  public AstProto.Type.Builder serialize();

  boolean getIsConst();

  public Type setConst(boolean isConst); // returns new instance

  public TypeType getTypeType();

  public boolean containsType(Set<Type> types);

  /*
   * Find in type: - A (stack - A) - $A (heap) - $A{B} (stack - B)
   */
  public Set<UserDeclType> getUserDeclTypes(boolean stackTypesOnly);

  // same as equals except null == ref
  public boolean refEquals(Type rightType);

  // String -> std.String.String (replace shortname with abs name)
  public Optional<Type> normalize(TypeEnvironment tenv, int lineNum, int columnNum) throws FatalMessageException;

  // like replaceType, except only for UserType
  public Type renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException;

  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv,
      boolean encapTypeIsRef, int lineNum, int columnNum) throws FatalMessageException;

  /*
   * If the type is a UserType and is declared within the currentModule, then
   * strip off any abs module pathing
   */
  public Type trimRelativeType(UserInstanceType currentModuleFullType);

  /*
   * If keepCurrentModRelative, then we might return empty type
   */
  Optional<Nullable<Type>> basicNormalize(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException;

  public boolean getNormalizedModType(ModuleType currentModule, EffectiveImports effectiveImports,
      MsgState msgState, Set<ModuleType> exportedTypes, boolean skipNormalize)
      throws FatalMessageException;

  /*
   * Return the dummy/null expression/value for this type
   */
  public Expression getDummyExpression(Project project, MsgState msgState)
      throws FatalMessageException;

  public Type prependFnRetType(Type prependType, boolean isTop);

  public boolean copyFromTenvIntoTenv(TypeEnvironment tenv, TypeEnvironment newTenv)
      throws FatalMessageException;

  // recursively convert all ClassType to ClassDeclType
  public Type asClassDeclType();

}
