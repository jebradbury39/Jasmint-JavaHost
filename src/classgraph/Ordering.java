package classgraph;

import classgraph.Node.ClassUsageLink;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport.ImportUsage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import typecheck.ModuleType;
import typecheck.UserDeclType;

public class Ordering {
  private Ordering() {
    
  }
  
  /*
   * Need module-level order as well as class-level order within each module
   * 
   * modA
   *  classX
   *  classY extends classX
   * modB
   *  classZ extends modA.classX
   *  
   * Class order: modA.classX, modB.classZ, modA.classY
   * Mod order: modA, modB
   * Not a straight order, since we might want parallels
   * Mod orders:
   *  modA(classX, classY), modB(classZ)
   *  modC, modD, modI, modG
   *  modE
   *  modF, modG, modH
   *  
   *  modG relies on both modF and modI, so both of those must be done before we can process modG
   */

  public static class ModuleOrdering {
    public final ModuleType moduleType; //modA
    //(classX, classY). But abs mod path ofc
    public List<UserDeclType> classOrdering = new LinkedList<>();

    public ModuleOrdering(ModuleType moduleType) {
      this.moduleType = moduleType;
    }
  }

  public static class CompleteOrdering {
    private final MsgState msgState;
    //our direct parent mods
    public Map<UserDeclType, Set<UserDeclType>> mapDirectMods = new HashMap<>();
    public List<UserDeclType> classOrdering = new LinkedList<>(); //just the classes, as before
    public LinkedHashMap<ModuleType, ModuleOrdering> moduleOrdering = new LinkedHashMap<>();

    public CompleteOrdering(MsgState msgState) {
      this.msgState = msgState;
    }
    
    public void addModule(ModuleType moduleType, Set<ClassUsageLink> uplinks)
        throws FatalMessageException {
      ModuleOrdering tmpModOrdering = moduleOrdering.get(moduleType);
      if (tmpModOrdering == null) {
        tmpModOrdering = new ModuleOrdering(moduleType);
        moduleOrdering.put(moduleType, tmpModOrdering);
      }
      
      Set<UserDeclType> directMods = mapDirectMods.get(moduleType);
      if (directMods == null) {
        directMods = new HashSet<>();
        mapDirectMods.put(moduleType, directMods);
      }
      
      for (ClassUsageLink uplink : uplinks) {
        ModuleType uplinkModType = uplink.node.getDeclType().getModuleType();
        if (uplink.getUsage() == ImportUsage.HARD) {
          directMods.add(uplinkModType);
        }
      }
    }

    public void addClass(UserDeclType classType, Set<ClassUsageLink> uplinks)
        throws FatalMessageException {
      if (classType.outerType.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Class '" + classType
            + "' does not have abs module type");
        return;
      }
      classOrdering.add(classType);

      ModuleType moduleType = classType.outerType.get();
      ModuleOrdering tmpModOrdering = moduleOrdering.get(moduleType);
      if (tmpModOrdering == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Did not find module ordering for " + moduleType + " for class " + classType
              + " but all modules have been ordered by now");
        return;
      }
      tmpModOrdering.classOrdering.add(classType);

      if (!uplinks.isEmpty()) {
        Set<UserDeclType> directMods = mapDirectMods.get(moduleType);
        if (directMods == null) {
          directMods = new HashSet<>();
          mapDirectMods.put(moduleType, directMods);
        }

        for (ClassUsageLink uplink : uplinks) {
          if (uplink.node.getDeclType().outerType.isNull()) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
                "Uplink '" + uplink.node.getDeclType()
                + "' does not have abs module type");
            return;
          }
          if (uplink.getUsage() == ImportUsage.HARD) {
            UserDeclType uplinkMod = uplink.node.getDeclType().outerType.get();
            directMods.add(uplinkMod);
          }
        }
      }
    }

    public Nullable<List<UserDeclType>> getModClassOrder(ModuleType moduleType, MsgState msgState)
        throws FatalMessageException {
      //find the root mod
      if (moduleOrdering.isEmpty()) {
        //no classes
        return Nullable.of(new LinkedList<>());
      }
      ModuleOrdering moduleO = moduleOrdering.get(moduleType);
      if (moduleO == null) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "Module '" + moduleType
            + "' does not have a mod mapping: " + moduleOrdering.keySet());
        return Nullable.empty();
      }

      return Nullable.of(moduleO.classOrdering);
    }
  }
}
