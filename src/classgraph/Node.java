package classgraph;

import ast.ClassDeclaration;
import ast.ClassDeclarationSection;
import ast.DeclarationStatement;
import ast.Function;
import errors.FatalMessageException;
import errors.Nullable;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.EnumDeclType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.UserDeclType;
import util.DotGen;

/*
 * This stuff is done after a resolveGenerics so all types are real
 */
public abstract class Node {
  
  public static class ClassUsageLink {
    public final Node node;
    private ImportUsage usage;
    
    public ImportUsage getUsage() {
      return usage;
    }
    
    void setUsage(ImportUsage usage) {
      this.usage = usage;
    }
    
    public ClassUsageLink(Node node, ImportUsage usage) {
      this.node = node;
      this.usage = usage;
    }
    
    @Override
    public int hashCode() {
      return node.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof ClassUsageLink)) {
        return false;
      }
      ClassUsageLink otherLink = (ClassUsageLink) other;
      return node.equals(otherLink.node);
    }
  }
  
  boolean visited;

  public abstract UserDeclType getDeclType();
  
  protected abstract boolean addUplink(ClassUsageLink link);
  
  protected abstract boolean addDownlink(ClassUsageLink link);
  
  public abstract Set<ClassUsageLink> getUplinks();
  
  public abstract Set<ClassUsageLink> getDownlinks();

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Node)) {
      return false;
    }
    return getDeclType().equals(((Node) other).getDeclType());
  }

  @Override
  public int hashCode() {
    return getDeclType().toString().hashCode();
  }

  /*
   * parent must have an abs module path
   */
  public void populateUplinks(ClassGraph graph, EffectiveImports effectiveImports,
      ModuleType parent, ImportUsage usage) throws FatalMessageException {
    registerOneUplink(graph, effectiveImports, parent, usage);
  }
  
  /*
   * parent must have an abs module path
   */
  public void populateUplinks(ClassGraph graph, EffectiveImports effectiveImports,
      ClassType parent, ImportUsage usage) throws FatalMessageException {
    registerOneUplink(graph, effectiveImports, parent, usage);
  }
  
  public void populateUplinks(ClassGraph graph, EffectiveImports effectiveImports,
      ClassDeclType parent, ImportUsage usage) throws FatalMessageException {
    registerOneUplink(graph, effectiveImports, parent, usage);
  }

  //after getting all the names, now we can find everything for our uplinks
  public void populateUplinks(ClassGraph graph, EffectiveImports effectiveImports,
      ClassDeclaration declaration, ImportUsage usage) throws FatalMessageException {
    //parent is added by ModuleEndpoint.populateClassGraph

    //iterate through all the fields
    for (ClassDeclarationSection section : declaration.sections) {
      for (DeclarationStatement decl : section.declarations) {
        registerOneUplink(graph, effectiveImports, decl.type, usage);
      }
      for (Function fn : section.functions) {
        registerOneUplink(graph, effectiveImports, fn.fullType, usage);
      }
      for (Function fn : section.overrides) {
        // override cannot be static...
        registerOneUplink(graph, effectiveImports, fn.fullType, usage);
      }
    }
  }
  
  private ClassUsageLink createLinkToType(ClassGraph graph,
      EffectiveImports effectiveImports, UserDeclType depend,
      ImportUsage usage) throws FatalMessageException {
    
    Nullable<Node> linkTo = graph.findNode(depend);
    if (linkTo.isNull()) {
      //create the node
      if (depend instanceof ModuleType) {
        linkTo = Nullable.of(graph.addModuleNode((ModuleType) depend));
      } else if (depend instanceof EnumDeclType) {
        EnumDeclType declTyDepend = (EnumDeclType) depend;
        if (graph.currentModule.isNotNull()) {
          Optional<Nullable<Type>> tmp = depend.basicNormalize(false, graph.currentModule.get(),
              effectiveImports, graph.msgState);
          declTyDepend = (EnumDeclType) tmp.get().get();
        }
        linkTo = Nullable.of(graph.addClassNode(declTyDepend));
      } else {
        ClassDeclType declTyDepend = (ClassDeclType) depend;
        if (graph.currentModule.isNotNull()) {
          Optional<Nullable<Type>> tmp = depend.basicNormalize(false, graph.currentModule.get(),
              effectiveImports, graph.msgState);
          declTyDepend = (ClassDeclType) tmp.get().get();
        }
        linkTo = Nullable.of(graph.addClassNode(declTyDepend));
      }
    }
    return new ClassUsageLink(linkTo.get(), usage);
  }

  private void registerOneUplink(ClassGraph graph, EffectiveImports effectiveImports,
      Type type, ImportUsage usage) throws FatalMessageException {
    //look for stack types since these form dependencies
    Set<UserDeclType> stackTypes = new HashSet<>();
    if (type instanceof ModuleType) {
      stackTypes.add((ModuleType) type);
    } else {
      for (UserDeclType ty : type.getUserDeclTypes(true)) {
        stackTypes.add(ty);
      }
    }

    //add these types to uplinks if not already there
    for (UserDeclType depend : stackTypes) {
      // find the node for this type
      ClassUsageLink linkTo = createLinkToType(graph, effectiveImports, depend, usage);
      
      if (addUplink(linkTo)) {
        effectiveImports.getImport(linkTo.node.getDeclType().asImportType(), usage);
        effectiveImports.getImport(linkTo.node.getDeclType().getModuleType().asImportType(), usage);
        
        linkTo.node.addDownlink(new ClassUsageLink(this, ImportUsage.INVALID));
      }
    }
  }

  public List<Node> findOneCycle(Node start, List<Node> path) {
    visited = true;
    List<Node> currentPath = new LinkedList<Node>();
    currentPath.addAll(path);
    currentPath.add(this);

    for (ClassUsageLink link : getUplinks()) {
      if (link.node instanceof ModuleNode && link.usage != ImportUsage.HARD) {
        continue;
      }
      if (link.node == start) {
        return currentPath;
      }
      if (link.node.visited) {
        continue;
      }
      List<Node> tmp = link.node.findOneCycle(start, currentPath);
      if (!tmp.isEmpty()) {
        return tmp;
      }
    }
    return new LinkedList<Node>();
  }

  public List<Node> traverseDown() {
    List<Node> path = new ArrayList<Node>();
    visited = true;
    path.add(this);

    for (ClassUsageLink link : getDownlinks()) {
      if (link.node.visited) {
        continue;
      }
      path.addAll(link.node.traverseDown());
    }

    return path;
  }

  public abstract boolean allUplinksFound(Set<UserDeclType> classTypes);

  public void addModuleUplink(ClassGraph graph, ResolvedImportType importType, ImportUsage usage) {
    addModuleUplink(graph, importType.asModuleType(), usage);
  }
  
  public void addModuleUplink(ClassGraph graph, ModuleType moduleType, ImportUsage usage) {
    Nullable<Node> linkTo = graph.findNode(moduleType);
    if (linkTo.isNull()) {
      //create the node
      linkTo = Nullable.of(graph.addModuleNode(moduleType));
    }
    if (addUplink(new ClassUsageLink(linkTo.get(), usage))) {
      linkTo.get().addDownlink(new ClassUsageLink(this, ImportUsage.INVALID));
    }
  }

  public List<ClassDeclType> dfsAllChildClasses() {
    List<Node> dfsNodes = traverseDown();
    List<ClassDeclType> dfsClasses = new LinkedList<>();
    
    //remove our own node
    dfsNodes.remove(0);
    
    for (Node node : dfsNodes) {
      if (node instanceof ModuleNode) {
        continue;
      }
      ClassNode classNode = (ClassNode) node;
      if (classNode.classType instanceof ClassDeclType) {
        ClassDeclType cdeclType = (ClassDeclType) classNode.classType;
        dfsClasses.add(cdeclType);
      }
    }
    
    return dfsClasses;
  }

  public void updateDotGraph(DotGen dotGraph) {
    for (ClassUsageLink upNode : getUplinks()) {
      DotGen.Edge edge = new DotGen.Edge(getDeclType().toString(),
          upNode.node.getDeclType().toString());
      switch (upNode.usage) {
        case INVALID:
          edge.color = DotGen.Color.RED;
          break;
        case NONE:
          edge.color = DotGen.Color.BLUE;
          break;
        case SOFT:
          edge.color = DotGen.Color.YELLOW;
          break;
        case HARD:
          edge.color = DotGen.Color.GREEN;
          break;
        default:
          throw new IllegalArgumentException();
      }
      
      dotGraph.addEdge(edge);
    }
    
    for (ClassUsageLink downNode : getDownlinks()) {
      DotGen.Edge edge = new DotGen.Edge(getDeclType().toString(),
          downNode.node.getDeclType().toString());
      edge.color = DotGen.Color.BLACK;
      dotGraph.addEdge(edge);
    }
  }
}
