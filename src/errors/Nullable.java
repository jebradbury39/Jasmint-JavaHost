package errors;

import java.util.NoSuchElementException;
import java.util.Objects;

/*
 * Mainly a clone of Optional class, but we want to differentiate from Optional
 * Optional should be used for errors: Union[err==null, value]
 * Nullable should be used for Union[null, other]
 */

public final class Nullable<T> {
  
  private static final Nullable<?> NULL = new Nullable<>();
  
  private final T obj;
  
  private Nullable() {
    this.obj = null;
  }
  
  @Override
  public String toString() {
    if (isNull()) {
      return "null";
    }
    return obj.toString();
  }
  
  public static<T> Nullable<T> empty() {
    @SuppressWarnings("unchecked")
    Nullable<T> t = (Nullable<T>) NULL;
    return t;
  }
  
  private Nullable(T obj) {
    this.obj = Objects.requireNonNull(obj);
  }
  
  public static <T> Nullable<T> of(T obj) {
    return new Nullable<>(obj);
  }
  
  public T get() {
    if (obj == null) {
        throw new NoSuchElementException("No value present");
    }
    return obj;
  }
  
  public boolean isNull() {
    return obj == null;
  }
  
  public boolean isNotNull() {
    return obj != null;
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Nullable)) {
      return false;
    }
    @SuppressWarnings("rawtypes")
    Nullable tmp = (Nullable) other;
    if (isNull()) {
      return tmp.isNull();
    }
    if (tmp.isNull()) {
      return false;
    }
    return get().equals(tmp.get());
  }
  
  /*
   * If true, this also means that obj is not null
   */
  public boolean instanceOf(@SuppressWarnings("rawtypes") Class other) {
    return other.isInstance(obj);
  }
  
  @Override
  public int hashCode() {
    if (isNull()) {
      return 0;
    }
    return obj.hashCode();
  }

}
