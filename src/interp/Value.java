package interp;

import java.util.Map;

import errors.FatalMessageException;
import interp.AbstractValue.MetaValue;
import typecheck.IntType;
import typecheck.Type;

public interface Value {
  ValueBox getBox();
  
  void setBox(ValueBox box);

  MetaValue getMetaValue();
  
  Value setMetaValue(MetaValue metaValue);
  
  Type getType();
  
  void setType(Type type);
  
  //EnvBox<Value> getMemoryLocation();
  //Value setMemoryLocation(EnvBox<Value> memLoc);
  
  Value add(Value rightValue) throws InterpError;
  
  Value add(long leftValue, IntType leftType) throws InterpError;
  
  Value add(double leftValue) throws InterpError;
  
  Value add(String leftValue) throws InterpError;

  Value sub(Value rightValue) throws InterpError;
  
  Value sub(long leftValue, IntType leftType) throws InterpError;
  
  Value sub(double leftValue) throws InterpError;
  
  boolean toBool() throws InterpError;
  
  long toInt() throws InterpError;
  
  double toDouble() throws InterpError;
  
  ValueBox lookup(String id, Environment env) throws InterpError, FatalMessageException;
  
  Value mul(Value rightValue) throws InterpError;
  
  Value mul(long leftValue, IntType leftType) throws InterpError;
  
  Value mul(double leftValue) throws InterpError;
  
  //just for functions in generic classes
  Value replaceType(Map<Type, Type> mapFromTo, String moduleAppend) throws InterpError;
  
  Value div(Value rightValue) throws InterpError;
  
  Value div(long leftValue, IntType leftType) throws InterpError;
  
  Value div(double leftValue) throws InterpError;
  
  Value neg() throws InterpError;
  
  /** (deep) value copy 
   * @throws InterpError If we attempt to copy an invalid value
   */
  Value copy(Environment copyEnv) throws InterpError;
  
  /**
   * Check equality by value (like c++). Does compare memory, no worries about circular
   * @param other The value to compare against
   * @return
   */
  boolean equalsValue(Value other);
  
  boolean lessThan(Value rightValue) throws InterpError;
  
  boolean lessThan(double leftValue) throws InterpError;
  
  boolean lessThan(String leftValue) throws InterpError;
}
