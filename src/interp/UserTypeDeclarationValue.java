package interp;

import ast.ClassDeclaration;
import errors.FatalMessageException;
import type_env.TypeEnvironment;
import typecheck.Type;

public class UserTypeDeclarationValue extends AbstractValue {

  /* enclosing env with statics */
  private Environment staticEnv;  //GLOBAL -> STATIC (A) -> STATIC (B)
  public final ClassDeclaration declaration;
  private UserTypeDeclarationValue parent;
  
  public UserTypeDeclarationValue(Type type, Environment staticEnv,
      ClassDeclaration declaration, UserTypeDeclarationValue parent) {
    super(MetaValue.NORMAL, type);
    this.staticEnv = staticEnv;
    this.declaration = declaration;
    this.parent = parent;
  }
  
  public UserTypeDeclarationValue(MetaValue metaValue, Type type, Environment staticEnv,
      ClassDeclaration declaration, UserTypeDeclarationValue parent) {
    super(metaValue, type);
    this.staticEnv = staticEnv;
    this.declaration = declaration;
    this.parent = parent;
  }
  
  public Environment getStaticEnv(boolean withGlobal) {
    if (withGlobal) {
      return staticEnv;
    }
    return staticEnv.detachScope(TypeEnvironment.ScopeType.GLOBAL);
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue("<class declaration>");
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new UserTypeDeclarationValue(metaValue, type,
        staticEnv, declaration, parent); //this is immutable, but new value for new box
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return this == other;
  }
  
  @Override
  public ValueBox lookup(String id, Environment env) throws InterpError, FatalMessageException {
    return getStaticEnv(false).lookup(id); //no GLOBAL access
  }

}
