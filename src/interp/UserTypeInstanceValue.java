package interp;

import ast.FunctionCallExpression;
import errors.FatalMessageException;
import type_env.TypeEnvironment;
import typecheck.Type;

public class UserTypeInstanceValue extends AbstractValue {

  /* holds fields and functions */
  private Environment env; //GLOBAL -> STATIC (A) -> INSTANCE (A) -> STATIC(B) -> INSTANCE (B)
  
  public UserTypeInstanceValue(Type type, Environment env) {
    super(MetaValue.NORMAL, type);
    this.env = env;
  }
  
  public UserTypeInstanceValue(MetaValue metaValue, Type type, Environment env) {
    super(metaValue, type);
    this.env = env;
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue(leftValue + "<class instance>");
  }

  public Environment getEnv() {
    return env;
  }
  
  @Override
  public ValueBox lookup(String id, Environment env) throws InterpError, FatalMessageException {
    return this.env.lookup(id);
  }

  public void callConstructor(Environment argsEnv, FunctionCallExpression constructorCall)
      throws InterpError, FatalMessageException {
    constructorCall.interp(argsEnv, env); //could check return value, should be void
  }

  @Override
  public Value copy(Environment copyEnv) throws InterpError {
    //copy the INSTANCE
    Environment copy = env.fullCopy();
    Environment cc = copy;
    while (cc.getParent() != null) {
      if (cc.getParent().envType == TypeEnvironment.ScopeType.INSTANCE) {
        cc.setParent(cc.getParent().fullCopy());
      }
      cc = cc.getParent();
    }
    
    return new UserTypeInstanceValue(metaValue, type, copy);
  }

  @Override
  public boolean equalsValue(Value other) {
    if (!other.getType().equals(type)) {
      return false;
    }
    return this == other;
  }
}
