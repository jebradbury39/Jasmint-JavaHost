package interp;

import java.util.List;

@FunctionalInterface
public interface HostFunctionLambda {
  public Value interp(List<Value> paramValues) throws InterpError;
}
