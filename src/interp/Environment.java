package interp;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import multifile.Project;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import type_env.TypeEnvironment.ScopeType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import typegraph.TypeGraph;
import util.Pair;

public class Environment {

  public final ScopeType envType;
  public final UserInstanceType ofClass; /* may be null. Is set to class name */
  public final Project project;

  private Environment parent = null; /* this is the parent scope, may be null */
  private Map<String, IdToValue> identifiers = new HashMap<String, IdToValue>();

  public final TypeGraph typeGraph;
  public TypeEnvironment globalTenv; // needed for class resolution, but nothing else

  public Environment(ScopeType envType, UserInstanceType ofClass, TypeGraph typeGraph, Project project) {
    this.envType = envType;
    this.ofClass = ofClass;
    this.typeGraph = typeGraph;
    this.project = project;
  }

  public Environment(Map<String, IdToValue> identifiers, ScopeType envType, UserInstanceType ofClass,
      TypeGraph typeGraph, Project project) {
    this.envType = envType;
    this.ofClass = ofClass;
    this.identifiers = identifiers;
    this.typeGraph = typeGraph;
    this.project = project;
  }

  @Override
  public String toString() {
    String result = "[" + envType + " ofClass " + ofClass + "]\n";
    for (Entry<String, IdToValue> entry : identifiers.entrySet()) {
      result += entry.getKey() + " = " + entry.getValue() + "\n";
    }

    if (parent != null) {
      result += "\n(extends)->\n" + parent;
    }

    return result;
  }

  /* parent manipulation */
  public Environment setParent(Environment parent) {
    this.parent = parent;
    return this;
  }

  public Environment getParent() {
    return parent;
  }

  public static Environment createTopLevelEnv(TypeGraph typeGraph, Project project)
      throws InterpError {
    Map<String, IdToValue> topIds = new HashMap<String, IdToValue>();

    // add 'print' as a system function
    List<Type> printParam = new ArrayList<Type>();
    printParam.add(StringType.Create());
    topIds.put("print", new MapFunctionToValue().write("print",
        new ValueBox(HostFunctionValue.createHostFunctionValue(new FunctionType(Nullable.empty(),
            VoidType.Create(), printParam, LambdaStatus.NOT_LAMBDA), (List<Value> paramValues) -> {
              System.out.print(paramValues.get(0));
              return new VoidValue();
            }), VariableScoping.GLOBAL)));

    return new Environment(topIds, ScopeType.GLOBAL, null, typeGraph, project);
  }

  public static Environment createEmptyTopLevelTypeEnv(TypeGraph typeGraph, Project project) {
    Map<String, IdToValue> topIds = new HashMap<String, IdToValue>();
    return new Environment(topIds, ScopeType.GLOBAL, null, typeGraph, project);
  }

  public Environment extend() {
    Environment newEnv = new Environment(ScopeType.NORMAL, null, typeGraph, project);
    newEnv.setParent(this);
    return newEnv;
  }

  public Environment lightCopy() {
    Environment copy = new Environment(identifiers, envType, ofClass, typeGraph, project);
    copy.setParent(parent);
    return copy;
  }

  public Environment detachScope(ScopeType scopeType) {
    if (envType == scopeType) {
      return null;
    }
    Environment copy = lightCopy();
    if (parent != null) {
      copy.setParent(parent.detachScope(scopeType));
    }
    return copy;
  }

  public Environment fullCopy() throws InterpError {
    Map<String, IdToValue> copyIds = new HashMap<String, IdToValue>();
    Environment copy = new Environment(copyIds, envType, ofClass, typeGraph, project);

    for (Entry<String, IdToValue> identifier : identifiers.entrySet()) {
      IdToValue copyVal = identifier.getValue().fullCopy(identifier.getKey(), copy);
      copyIds.put(identifier.getKey(), copyVal);
    }

    copy.setParent(parent);
    return copy;
  }

  public Environment makeClosureCopy() throws InterpError {
    Environment cloEnv = null;
    Environment cEnv = this;
    Environment trackCloEnv = null;
    while (cEnv != null) {
      if (cEnv.envType != TypeEnvironment.ScopeType.NORMAL) {
        if (cloEnv == null) {
          cloEnv = cEnv;
          trackCloEnv = cloEnv;
        } else {
          trackCloEnv.setParent(cEnv);
          trackCloEnv = trackCloEnv.getParent();
        }
      } else {
        if (cloEnv == null) {
          cloEnv = cEnv.fullCopy();
          trackCloEnv = cEnv;
        } else {
          trackCloEnv.setParent(cEnv.fullCopy());
          trackCloEnv = trackCloEnv.getParent();
        }
      }
      cEnv = cEnv.getParent();
    }

    return cloEnv;
  }

  public ValueBox defineVar(String name, VariableScoping scoping, Value value, boolean asFunction)
      throws InterpError {

    ValueBox vBox = new ValueBox(value, scoping);

    // only search our identifiers (not parents')
    IdToValue existing = identifiers.get(name);
    if (existing == null) {
      if (asFunction) {
        existing = new MapFunctionToValue();
      } else {
        existing = new MapIdToValue();
      }
      identifiers.put(name, existing);
    }

    existing.write(name, vBox);

    return vBox;
  }

  public ValueBox lookup(String name) throws InterpError, FatalMessageException {
    return lookup(name, null);
  }

  public ValueBox lookup(String name, FunctionType functionType)
      throws InterpError, FatalMessageException {
    return ourLookup(name, functionType).a;
  }

  // only for functions, and really only for constructors after name-mangling
  // must provide a list of names (candidate functions)
  public ValueBox lookup(List<String> names, FunctionType functionType)
      throws InterpError, FatalMessageException {
    Pair<ValueBox, Integer> match = new Pair<ValueBox, Integer>(null, -1);
    for (String name : names) {
      Pair<ValueBox, Integer> tmp = ourLookup(name, functionType);
      if (match.b < tmp.b) {
        match = tmp;
      }
    }
    return match.a;
  }

  private Pair<ValueBox, Integer> ourLookup(String name, FunctionType functionType)
      throws InterpError, FatalMessageException {
    Pair<ValueBox, Integer> result = new Pair<ValueBox, Integer>(null, -1);
    if (identifiers.containsKey(name)) {
      try {
        result = identifiers.get(name).read(this, name, functionType);
      } catch (InterpError e) {
        // attach the location of the error
        throw new InterpError(e.coreMsg, -1, -1);
      }
    }
    if (result.a != null) {
      if (result.b == -1) {
        return result; // matched an id, not a function. Do not check parent
      }
    }

    if (parent != null) {
      Pair<ValueBox, Integer> parentResult = parent.ourLookup(name, functionType);
      if (result.a == null) {
        result = parentResult;
      } else if (parentResult != null) {
        if (parentResult.b > result.b) {
          result = parentResult;
        }
      }
    }
    return result;
  }

  public Environment replaceType(Map<Type, Type> mapFromTo) throws InterpError {
    Map<String, IdToValue> newIds = new HashMap<String, IdToValue>();

    for (Entry<String, IdToValue> entry : identifiers.entrySet()) {
      newIds.put(entry.getKey(), entry.getValue().replaceType(entry.getKey(), mapFromTo, ""));
    }

    Environment copy = new Environment(newIds, envType, ofClass, typeGraph, project);
    copy.setParent(parent);

    return copy;
  }

  public ValueBox lookupNestedClassname(UserInstanceType name) throws InterpError {
    // lookup farthest out first, then work back in
    /*
     * if (name.getParent() == null) { return lookup(name.getName()); } ValueBox
     * outerEnv = lookupNestedClassname(name.getParent().get()); if (outerEnv ==
     * null) { throw new InterpError("lookup failed on outerType: " +
     * name.getParent(), -1, -1); } if (outerEnv.getValue() == null) { throw new
     * InterpError("outerType value is null", -1, -1); }
     * 
     * if (outerEnv.getValue() instanceof UserTypeDeclarationValue) {
     * UserTypeDeclarationValue ute = (UserTypeDeclarationValue)
     * outerEnv.getValue(); return ute.getStaticEnv(false).lookup(name.getName());
     * //no GLOBAL access } else if (outerEnv.getValue() instanceof
     * ModuleValueEntry) { return ((ModuleValueEntry)
     * outerEnv.getValue()).getEnv(project, -1, -1).lookup(name.getName()); } else {
     * throw new InterpError("outerType is not a class declaration: " +
     * name.getParent(), -1, -1); }
     */
    return null;
  }

  /*
   * There must be an expectedEnvType above this environment (only check first
   * one), else return null The INSTANCE type must match expected, else return
   * null
   */
  public Environment lookupParent(ScopeType expectedEnvType, UserInstanceType expected) {
    Environment track = parent;
    while (track != null) {
      if (track.envType == expectedEnvType) {
        if (track.ofClass == expected) {
          return track;
        }
        if (track.ofClass != null) {
          if (track.ofClass.equals(expected)) {
            return track;
          }
        }
      }
      track = track.parent;
    }
    return null;
  }
}
