package interp;

import java.util.Map;

import errors.FatalMessageException;
import typecheck.IntType;
import typecheck.Type;

public abstract class AbstractValue implements Value {

  public static enum MetaValue {
    NORMAL, RETURN, BREAK, CONTINUE
  }
  
  protected MetaValue metaValue;
  protected Type type;

  private ValueBox box; //this must not persist too long. Only used by reference expression
  
  public AbstractValue(MetaValue metaValue, Type type) {
    this.metaValue = metaValue;
    this.type = type;
    this.box = null;
  }
  
  @Override
  public boolean equals(Object other) {
    if (other instanceof Value) {
      return equalsValue((Value)other);
    }
    return false;
  }
  
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public ValueBox getBox() {
    return box;
  }

  @Override
  public void setBox(ValueBox box) {
    this.box = box;
  }
  
  @Override
  public MetaValue getMetaValue() {
    return metaValue;
  }
  
  @Override
  public Value setMetaValue(MetaValue metaValue) {
    this.metaValue = metaValue;
    return this;
  }
  
  @Override
  public Type getType() {
    return type;
  }
  
  @Override
  public void setType(Type type) {
    this.type = type;
  }
  
  @Override
  public Value add(Value rightValue) throws InterpError {
    if (rightValue instanceof StringValue) {
      return new StringValue(((StringValue)add("")).value + ((StringValue)rightValue).value);
    }
    throw new InterpError("cannot add " + type + " to a non-string", -1, -1);
  }
  
  @Override
  public Value add(long leftValue, IntType leftType) throws InterpError {
    throw new InterpError("internal error: add int not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value add(double leftValue) throws InterpError {
    throw new InterpError("internal error: add double not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value sub(Value rightValue) throws InterpError {
    throw new InterpError("internal error: sub not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value sub(long leftValue, IntType leftType) throws InterpError {
    throw new InterpError("internal error: sub int not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value sub(double leftValue) throws InterpError {
    throw new InterpError("internal error: sub double not supported for type " + type, -1, -1);
  }
  
  @Override
  public String toString() {
    try {
      return ((StringValue)add("")).value;
    } catch (InterpError e) {
      throw new IllegalArgumentException("internal error on value toString: " + e);
    }
  }
  
  @Override
  public long toInt() throws InterpError {
    throw new InterpError("Cannot convert " + type + " to int", -1, -1);
  }
  
  @Override
  public boolean toBool() throws InterpError {
    throw new InterpError("Cannot convert " + type + " to bool", -1, -1);
  }
  
  @Override
  public ValueBox lookup(String id, Environment env) throws InterpError, FatalMessageException {
    throw new InterpError("Cannot perform id .[" + id + "] lookup on type " + type, -1, -1);
  }
  
  @Override
  public Value mul(Value rightValue) throws InterpError {
    throw new InterpError("internal error: mul not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value mul(long leftValue, IntType leftType) throws InterpError {
    throw new InterpError("internal error: mul int not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value mul(double leftValue) throws InterpError {
    throw new InterpError("internal error: mul double not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value replaceType(Map<Type, Type> mapFromTo, String moduleAppend) throws InterpError {
    Value copy = copy(null);
    copy.setType(type.replaceType(mapFromTo));
    return copy;
  }
  
  @Override
  public Value div(Value rightValue) throws InterpError {
    throw new InterpError("internal error: div not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value div(long leftValue, IntType leftType) throws InterpError {
    throw new InterpError("internal error: div int not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value div(double leftValue) throws InterpError {
    throw new InterpError("internal error: div double not supported for type " + type, -1, -1);
  }
  
  @Override
  public Value neg() throws InterpError {
    throw new InterpError("internal error: neg not supported for type " + type, -1, -1);
  }
  
  @Override
  public double toDouble() throws InterpError {
    return (double)toInt();
  }

  @Override
  public boolean lessThan(Value rightValue) throws InterpError {
    return rightValue.lessThan(toDouble());
  } 
  
  @Override
  public boolean lessThan(double leftValue) throws InterpError {
    return leftValue < toDouble();
  }
  
  @Override
  public boolean lessThan(String leftValue) throws InterpError {
    throw new InterpError("internal error: Cannot interp " + type + " < string", -1, -1);
  }
}
