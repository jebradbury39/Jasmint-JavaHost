package interp;

import typecheck.NullType;

public class NullValue extends AbstractValue {

  public NullValue() {
    super(MetaValue.NORMAL, NullType.Create());
  }
  
  public NullValue(MetaValue metaValue) {
    super(metaValue, NullType.Create());
  }

  @Override
  public Value add(String leftValue) throws InterpError {
    return new StringValue(leftValue + "<null>");
  }

  @Override
  public Value copy(Environment copyEnv) {
    return new NullValue();
  }

  @Override
  public boolean equalsValue(Value other) {
    return other instanceof NullValue;
  }

}
