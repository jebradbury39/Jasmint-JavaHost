package header;

import ast.DotExpression;
import ast.EnumDeclaration;
import ast.Expression;
import ast.IdentifierExpression;
import ast.Program.DestructorRenamings;
import ast.SerializationError;
import ast.StaticClassIdentifierExpression;
import astproto.AstProto;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImports;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import mbo.Renamings;
import mbo.Renamings.RenamingIdKey;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import type_env.BasicTypeBox;
import type_env.EnumTypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.ScopeType;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.ModuleType;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typegraph.TypeGraph;
import util.ScopedIdentifiers;

public class EnumHeader extends PsuedoClassHeader {

  public final EnumDeclType absName; // computed, mainly for convenience
  public final EnumDeclType originalName; // abs module path as well here. Before renaming

  public final List<String> enumIds;

  public EnumHeader(int lineNum, int columnNum, long label,
      ModuleType moduleType, String name, EnumDeclType originalName, boolean export,
      List<String> enumIds) {
    super(lineNum, columnNum, label, moduleType, export, name);
    this.enumIds = enumIds;
    
    if (enumIds.isEmpty()) {
      throw new IllegalArgumentException("enum cannot be empty");
    }
    
    this.absName = new EnumDeclType(Nullable.of(moduleType), name);
    this.originalName = originalName;
  }

  public AstProto.EnumHeader.Builder serialize() {
    AstProto.EnumHeader.Builder document = AstProto.EnumHeader.newBuilder();
    document.setLineNum(lineNum);
    document.setColumnNum(columnNum);
    document.setLabel(label);
    document.setExport(export);
    document.setModuleType(moduleType.serialize());
    document.setName(name);
    document.setOriginalName(originalName.serialize());
    document.addAllEnumIds(enumIds);
    return document;
  }

  public static EnumHeader deserialize(AstProto.EnumHeader document) throws SerializationError {
    return new EnumHeader(document.getLineNum(), document.getColumnNum(), document.getLabel(),
        (ModuleType) AbstractType.deserialize(document.getModuleType()),
        document.getName(), (EnumDeclType) AbstractType.deserialize(document.getOriginalName()),
        document.getExport(), new LinkedList<>(document.getEnumIdsList()));
  }

  public EnumHeader renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    List<String> newEnumIds = new LinkedList<>();
    for (String id : enumIds) {
      Nullable<String> newId = renamings.lookup(id, Nullable.empty(), Nullable.of(absName));
      if (newId.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
            "Enum header failed renamings: " + id);
      }
      newEnumIds.add(newId.get());
    }
    return new EnumHeader(lineNum, columnNum, label, moduleType, name, originalName, export,
        newEnumIds);
  }

  public EnumHeader replaceTypes(Map<Type, Type> typeRenamings) {
    EnumDeclType newAbsName = (EnumDeclType) absName.replaceType(typeRenamings);
    return new EnumHeader(lineNum, columnNum, label, newAbsName.outerType.get(), newAbsName.name,
        originalName, export, enumIds);
  }

  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, boolean isStaticTenv,
      UserInstanceType realType) throws FatalMessageException {
    // find the proper module endpoint
    Nullable<ModuleEndpoint> modEp = tenv.project.lookupModule(moduleType,
        tenv.project.getAuditTrail());
    if (modEp.isNull()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Unable to find module in project: " + moduleType);
      return Optional.empty();
    }
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    
    EffectiveImports effectiveImp = moduleHandle.moduleBuildObj.effectiveImports.applyTransforms(
        tenv.project.getAuditTrail(), tenv.msgState);
    
    TypeEnvironment newTenv = TypeEnvironment.createEmptyUserTypeEnv(tenv.project, absName,
        tenv.typeGraph, effectiveImp, tenv.msgState);
    newTenv.setTypeTenv(tenv.copyOnlyTypes());
    if (isStaticTenv) {
      newTenv.envType = ScopeType.STATIC;
    } else {
      newTenv.envType = ScopeType.INSTANCE;
    }

    for (String id : enumIds) {
      newTenv.defineVar(id, new BasicTypeBox(new EnumType(absName.outerType, absName.name),
          true, true, VariableScoping.CLASS_INSTANCE));
    }
    return Optional.of(newTenv);
  }

  @Override
  public List<Type> getInnerTypes() {
    return new LinkedList<>();
  }

  @Override
  public Optional<PsuedoClassHeader> replaceGenerics(AuditTrailLite projectAuditTrailSoFar,
      Project project, Nullable<TypeEnvironment> tenv, List<Type> realInnerTypes,
      boolean noRegister, Map<Type, Type> genericToRealMapping, MsgState msgState, int lineNum,
      int columnNum) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  protected void populateTypeGraph(TypeGraph typeGraph) throws FatalMessageException {
    typeGraph.addUserNode(new EnumType(Nullable.of(moduleType), name),
        Nullable.empty());
  }

  public void populateRenameIds(Renamings renamings) {
    // rename everything except NONE_ID
    for (String id : enumIds) {
      if (id.equals(EnumDeclaration.NONE_ID)) {
        renamings.renamings.put(new RenamingIdKey(Nullable.of(absName), Nullable.empty(), id), id);
        continue;
      }
      renamings.renamings.put(new RenamingIdKey(Nullable.of(absName), Nullable.empty(), id),
          id + "_" + label);
    }
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, boolean isStatic, int lineNum, int columnNum)
      throws FatalMessageException {
    // enum fields take 4 bytes (int32 underlying type)
    return 4;
  }

  @Override
  public void insertDestructorCalls(ScopedIdentifiers ourScope,
      DestructorRenamings destructorRenamings) {
  }

  @Override
  public Expression getDummyExpression(UserInstanceType self, MsgState msgState)
      throws FatalMessageException {
    if (!(self instanceof EnumType)) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "expected enum type, but got: " + self);
    }
    
    // just use the first enum id as the default
    StaticClassIdentifierExpression enumType = new StaticClassIdentifierExpression(absName);
    String id = enumIds.get(0);
    return new DotExpression(enumType, new IdentifierExpression(id));
  }
  
  @Override
  public UserDeclType getAbsName() {
    return absName;
  }

  @Override
  public void defineInTenv(TypeEnvironment tenv) {
    tenv.defineVar(name, new EnumTypeBox(this, true, VariableScoping.GLOBAL));
  }

  @Override
  public ResolvedImportType getImportType() {
    return (ResolvedImportType) absName.asImportType();
  }

  @Override
  public String getName() {
    return absName.name;
  }
}
