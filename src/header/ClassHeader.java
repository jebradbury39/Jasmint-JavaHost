package header;

import ast.AbstractAst;
import ast.AbstractAst.AstType;
import ast.Ast;
import ast.ClassDeclarationSection.Visibility;
import ast.DeclarationStatement;
import ast.Expression;
import ast.Function;
import ast.FunctionCallExpression;
import ast.IdentifierExpression;
import ast.NewClassInstanceExpression;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntry;
import audit.AuditRenameIds;
import audit.AuditResolveExceptions;
import audit.AuditResolveExceptions.RenamingEntry;
import audit.AuditTrailLite;
import callgraph.CallGraph;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.RegisteredGenerics.Registration;
import import_mgmt.EffectiveImport.ImportUsage;
import import_mgmt.EffectiveImports;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.ModuleBuildObj;
import mbo.Renamings;
import mbo.Renamings.RenamingIdKey;
import mbo.Vtables.ClassVtable;
import mbo.Vtables.VtableEntry;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import type_env.BasicTypeBox;
import type_env.ClassTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.ScopeType;
import type_env.TypeEnvironment.TypeStatus;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.GenericType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.ResolvedImportType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import typegraph.TypeGraph;
import util.Pair;
import util.ScopedIdentifiers;

/*
 * Used by TypeEnvironment and ProgHeader
 * Documents all class member info that an importing external module would need to know
 * for typechecking.
 * No RAM relationships to other classes, this is intended to be serialized/deserialized for a
 * partial build
 */
public class ClassHeader extends PsuedoClassHeader {
  
  public final ClassDeclType absName; // computed, mainly for convenience
  public final ClassDeclType originalName; // abs module path as well here. Before renaming

  private Nullable<ClassType> parent; // abs module path, with inner types

  // if this list is not empty, that makes this a template class
  // as new template usages are registered, we need to typecheck the method bodies
  // to ensure
  // all template variations work
  public final List<Type> innerTypes; // e.g. T, U, V. May appear in class members and parent
  // isTemplate if innerTypes is not empty

  public final boolean isFfi;
  public final boolean isStatic;

  // in case of overloaded functions, need to have a list of matches
  // after linking with parent, will add parent(s) members in here too with, as
  // long as
  // our members do not supersede them. By not linking via 'extend', we allow
  // ourselves to exit RAM
  // easily and be saved to disk, then loaded back later with no re-linking
  // note that no ffi stuff here (that is still stored in the AST with the class
  // implementation
  // details
  private Map<String, List<ClassMember>> members = new HashMap<>();
  
  // All the types we must have in order for this class to work. For example, if we have a field
  // of type a.X, then even if the current user has not imported module a, we still need to have
  // a.X in our classTable. All types here must have abs module path.
  private Set<UserDeclType> requiredTypes = new HashSet<>();

  public boolean isLinked = false;
  public boolean isTypechecked = false;

  public boolean isGeneric() {
    boolean foundGen = false;
    boolean foundNonGen = false;
    for (Type innerTy : innerTypes) {
      if (innerTy instanceof GenericType) {
        foundGen = true;
      } else {
        foundNonGen = true;
      }
    }
    if (foundGen && foundNonGen) {
      throw new IllegalArgumentException("Mixed generic and non-generic types not allowed");
    }
    return foundGen;
  }
  
  public Nullable<ClassType> getParent() {
    return parent;
  }

  public static class ClassMember {
    // purely for debug info
    public final int lineNum;
    public final int columnNum;
    
    public final long label; //from the ast

    // the class this member is actually defined in (e.g. parent)
    public final boolean isNotGenerated; //if true, then this member was written by the user
    public final ClassDeclType classOfOrigin; // abs module path
    private boolean isFfi; // same as classOfOrigin.header.isFfi, except when parent class matters
    public final String id;
    public final String originalId; // before any renaming
    private Type type; //abs module paths only
    public final Visibility visibility; // public, private, ...
    public final AbstractAst.AstType astType; // so we can know decls with lambdas vs functions
    public final boolean isStatic;
    public final boolean isOverride; // not stored, just tells us to look for isOverrideOf
    // only functions have this - this is the OG defining class
    public Nullable<ClassType> isOverrideOf;
    public boolean isOverridden; // cannot be true if isOverride is true. na if not fn
    public final boolean isTemplate; // true if classOfOrigin has generics
    // only include this if function/decl_value contains value with template type
    // (e.g. T)
    // only non-null if isTemplate and function decl
    public final Nullable<Ast> templateFunctionOrDecl;

    public boolean isInit;

    public final boolean isRenamed; // can only rename from starting point

    public ClassMember(int lineNum, int columnNum, long label,
        boolean isNotGenerated, ClassDeclType classOfOrigin, boolean isFfi, String id,
        String originalId, Type type,
        Visibility visibility, AbstractAst.AstType astType, boolean isStatic,
        boolean isOverride, Nullable<ClassType> isOverrideOf,
        boolean isOverridden, boolean isInit, boolean isTemplate, boolean isRenamed,
        Nullable<Ast> templateFunctionOrDecl) {
      this.lineNum = lineNum;
      this.columnNum = columnNum;
      this.label = label;
      this.isNotGenerated = isNotGenerated;
      this.classOfOrigin = classOfOrigin;
      this.isFfi = (isNotGenerated && !originalId.equals("constructor") && isFfi);
      this.id = id;
      this.originalId = originalId;
      this.type = type;
      this.astType = astType;
      this.visibility = visibility;
      this.isStatic = isStatic;
      this.isOverride = isOverride;
      this.isOverrideOf = isOverrideOf;
      this.isOverridden = isOverridden;
      this.isInit = isInit;
      this.isTemplate = isTemplate;
      this.isRenamed = isRenamed;
      this.templateFunctionOrDecl = templateFunctionOrDecl;

      if (originalId.isEmpty()) {
        throw new IllegalArgumentException();
      }
    }
    
    public boolean getIsFfi() {
      return isFfi;
    }
    
    public Type getType() {
      return type;
    }

    @Override
    public String toString() {
      return type.toString();
    }

    @Override
    public boolean equals(Object other) {
      if (!(other instanceof ClassMember)) {
        return false;
      }
      ClassMember otherMember = (ClassMember) other;

      return otherMember.id.equals(id) && otherMember.type.equals(type);
    }

    @Override
    public int hashCode() {
      return id.hashCode() + type.hashCode();
    }

    public boolean typecheck(UserDeclType classType, TypeEnvironment classTenv, boolean forceIsInit,
        int lineNum, int columnNum) throws FatalMessageException {

      // this only runs for members defined in this specific class, not the parents
      if (classOfOrigin.equals(classType)) {
        // verify that our type exists
        Optional<TypeStatus> tyStatusOpt = classTenv.lookupType(type, lineNum, columnNum);
        if (!tyStatusOpt.isPresent()) {
          return false;
        }
        TypeStatus tyStatus = tyStatusOpt.get();

        String errPrefix = "Type '" + type + "' of field '" + id + "' in class '" + classOfOrigin
            + "'";
        switch (tyStatus) {
          case UNDEFINED:
            classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
                errPrefix + " is undefined", lineNum, columnNum);
            return false;
          case COMPLETE:
            break;
          case GENERIC:
            break;
          case INCOMPLETE:
          case INCOMPLETE_AND_GENERIC:
            classTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
                errPrefix + " is incomplete", lineNum, columnNum);
            return false;
          default:
            break;
        }
      }

      // type is complete or generic. Add the field to the class tenv now. (Even
      // fields from parents)
      classTenv.defineVar(id, new BasicTypeBox(type, true, isInit || forceIsInit,
          VariableScoping.CLASS_INSTANCE, classOfOrigin, visibility, isStatic));
      return true;
    }

    public AstProto.ClassMember.Builder serialize() {
      AstProto.ClassMember.Builder document = AstProto.ClassMember.newBuilder();

      document.setLineNum(lineNum);
      document.setColumnNum(columnNum);
      document.setLabel(label);
      document.setIsNotGenerated(isNotGenerated);
      document.setClassOfOrigin(classOfOrigin.serialize());
      document.setIsFfi(isFfi);
      document.setId(id);
      document.setOriginalId(originalId);
      document.setType(type.serialize());
      document.setVisibility(visibility.ordinal());
      document.setAstType(astType.ordinal());
      document.setIsStatic(isStatic);
      if (isOverrideOf.isNotNull()) {
        document.addIsOverrideOf(isOverrideOf.get().serialize());
      }
      document.setIsOverridden(isOverridden);
      document.setIsInit(isInit);
      document.setIsTemplate(isTemplate);
      document.setIsRenamed(isRenamed);

      return document;
    }

    public static ClassMember deserialize(AstProto.ClassMember document) throws SerializationError {
      Nullable<ClassType> isOverrideOf = Nullable.empty();
      if (document.getIsOverrideOfCount() > 0) {
        ClassType tmp = (ClassType) AbstractType.deserialize(document.getIsOverrideOf(0));
        isOverrideOf = Nullable.of(tmp);
      }
      return new ClassMember(document.getLineNum(), document.getColumnNum(), document.getLabel(),
          document.getIsNotGenerated(),
          (ClassDeclType) AbstractType.deserialize(document.getClassOfOrigin()),
          document.getIsFfi(),
          document.getId(), document.getOriginalId(), AbstractType.deserialize(document.getType()),
          Visibility.values()[document.getVisibility()], AstType.values()[document.getAstType()],
          document.getIsStatic(), isOverrideOf.isNotNull(), isOverrideOf,
          document.getIsOverridden(),
          document.getIsInit(), document.getIsTemplate(), document.getIsRenamed(),
          Nullable.empty());
    }

    public void populateRenameIds(ClassDeclType withinClass,
        List<Type> withinClassGenerics,
        Project project, Renamings renamings, RegisteredGenerics registeredGenerics,
        MsgState msgState) throws FatalMessageException {

      Nullable<FunctionType> fnType = Nullable.empty();
      if (type instanceof FunctionType) {
        FunctionType tmpFnTy = (FunctionType) type;
        if (isOverrideOf.isNotNull()) {
          // use the class type which originally defined this function
          fnType = Nullable.of(new FunctionType(isOverrideOf.get(), tmpFnTy.returnType,
              tmpFnTy.argTypes, tmpFnTy.lambdaStatus));
        } else {
          fnType = Nullable.of(tmpFnTy);
        }
      }
      Nullable<Renamings> existingRenamings = Nullable.empty();
      RenamingIdKey tmp = new RenamingIdKey(Nullable.of(classOfOrigin), fnType, id);
      if (isOverrideOf.isNotNull()) {
        tmp = new RenamingIdKey(Nullable.of(isOverrideOf.get().asDeclType()), fnType, id);
      }
      Nullable<String> newId = Nullable.empty();
      
      if (!classOfOrigin.equals(withinClass) || isOverrideOf.isNotNull()) {
        ModuleType originModule = classOfOrigin.getModuleType();
        if (isOverrideOf.isNotNull()) {
          originModule = isOverrideOf.get().getModuleType();
        }
        
        // make sure to retrieve all keys which map to this renaming
        // lookup in class of origin first
        if (originModule.equals(withinClass.getModuleType())) {
          // same module
          newId = renamings.lookup(tmp);
          if (newId.isNull()) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS, "Failed to locate renaming");
          }
          existingRenamings = Nullable.of(renamings);
        } else {
          // different module, get that modEp and check renamings
          Nullable<ModuleEndpoint> modEp = project.lookupModule(originModule,
              project.getAuditTrail());
          if (modEp.isNull()) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS, "Failed to locate mod");
          }
          modEp.get().load();
          LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
          newId = ((AuditRenameIds) moduleHandle.moduleBuildObj.auditTrail.getLast()).renamings
              .lookup(tmp);
          if (newId.isNull()) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS, "Failed to locate renaming");
          }
          existingRenamings = Nullable
              .of(((AuditRenameIds) moduleHandle.moduleBuildObj.auditTrail.getLast()).renamings);
        }
      } else {
        if (!isFfi) {
          // come up with the renaming normally
          newId = Nullable.of(id + "_" + label);
        } else {
          // rename to ourselves
          newId = Nullable.of(id);
        }
      }

      if (isOverrideOf.isNotNull()) {
        // Put in the same renaming for our class
        FunctionType tmpFnTy = (FunctionType) type;
        renamings.renamings.put(new RenamingIdKey(
            Nullable.of(classOfOrigin), Nullable.of(tmpFnTy), id), newId.get());
        renamings.renamings.put(new RenamingIdKey(
            Nullable.of(withinClass), Nullable.of(tmpFnTy), id), newId.get());
      }
      renamings.renamings.put(new RenamingIdKey(Nullable.of(classOfOrigin), fnType, id),
          newId.get());
      renamings.renamings.put(new RenamingIdKey(Nullable.of(withinClass), fnType, id), newId.get());

      if (fnType.isNotNull()) {
        // pull in parent class generic variant renamings for this function
        if (existingRenamings.isNotNull()) {
          Map<RenamingIdKey, String> newRenamings = new HashMap<>();
          Set<Entry<RenamingIdKey, String>> tmpRenamings = existingRenamings.get().renamings.entrySet();
          for (Entry<RenamingIdKey, String> entry : tmpRenamings) {
            if (entry.getKey().fnType.isNull()) {
              continue;
            }
            if (entry.getValue().equals(newId.get())) {
              newRenamings.put(
                  new RenamingIdKey(Nullable.of(withinClass), entry.getKey().fnType, id),
                  entry.getValue());
            }
          }
          renamings.renamings.putAll(newRenamings);
        }

        // if we have not resolved generics, then create variants
        if (!withinClassGenerics.isEmpty()) {
          // now go through all registered generics and create variants
          // inherited functions already done
          // only vary on the generic types
          // so fn(U, int) and fn(U, string) stay separate
          for (Registration reg : registeredGenerics.getRegistrations()) {
            Map<Type, Type> mapGenericsToReal = AbstractType.zip(withinClassGenerics,
                reg.realType.innerTypes);
            FunctionType replFnType = (FunctionType) fnType.get().replaceType(mapGenericsToReal);
            renamings.renamings.put(
                new RenamingIdKey(Nullable.of(withinClass), Nullable.of(replFnType), id),
                newId.get());
          }
        }
      }
    }

    public ClassMember renameIds(Renamings renamings, MsgState msgState, boolean noError)
        throws FatalMessageException {
      /*
       * if this member is a function and is an override, then we must keep the same name
       * as the function that we are overriding.
       * 
       * This is done via populating the renamings.
       */

      Nullable<String> newId = renamings.lookup(id, type, Nullable.of(classOfOrigin));
      if (newId.isNull()) {
        if (noError) {
          newId = Nullable.of(id);
        } else {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
              "Class member failed renaming: " + id);
        }
      }
      Nullable<Ast> newFnDecl = templateFunctionOrDecl;
      if (newFnDecl.isNotNull()) {
        Ast tmp = newFnDecl.get();
        if (tmp instanceof DeclarationStatement) {
          newFnDecl = Nullable.of(((DeclarationStatement) tmp).renameIds(renamings, msgState));
        } else if (tmp instanceof Function) {
          newFnDecl = Nullable.of(((Function) tmp).renameIds(renamings, msgState));
        } else {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
              "templateFunctionOrDecl was neither: " + tmp);
        }
      }

      ClassMember newMember = new ClassMember(lineNum, columnNum, label, isNotGenerated,
          classOfOrigin, isFfi, newId.get(), originalId, type,
          visibility, astType, isStatic, isOverride, isOverrideOf, isOverridden, isInit, isTemplate,
          !newId.get().equals(id), newFnDecl);
      return newMember;
    }

    public ClassMember replaceTypes(Map<Type, Type> typeRenamings) {
      Nullable<Ast> newFnDecl = templateFunctionOrDecl;
      if (newFnDecl.isNotNull()) {
        if (templateFunctionOrDecl.instanceOf(Function.class)) {
          newFnDecl = Nullable
              .of(((Function) templateFunctionOrDecl.get()).replaceType(typeRenamings));
        } else if (templateFunctionOrDecl.instanceOf(DeclarationStatement.class)) {
          newFnDecl = Nullable
              .of(((DeclarationStatement) templateFunctionOrDecl.get()).replaceType(typeRenamings));
        }
      }
      Nullable<ClassType> newIsOverrideOf = isOverrideOf;
      if (isOverrideOf.isNotNull()) {
        newIsOverrideOf = Nullable.of((ClassType) isOverrideOf.get().replaceType(typeRenamings));
      }
      ClassMember newMember = new ClassMember(lineNum, columnNum, label, isNotGenerated,
          (ClassDeclType) classOfOrigin.replaceType(typeRenamings), isFfi, id, originalId,
          type.replaceType(typeRenamings), visibility, astType, isStatic,
          isOverride, newIsOverrideOf, isOverridden,
          isInit, isTemplate, true, newFnDecl);
      return newMember;
    }

    public boolean templateTypecheck(TypeEnvironment tenv, DotAccessType withinClass)
        throws FatalMessageException {
      if (templateFunctionOrDecl.isNull()) {
        // not a template
        return true;
      }

      Ast tmp = templateFunctionOrDecl.get();
      boolean typeOptIsPresent = true;
      if (tmp instanceof Function) {
        Function fn = (Function) tmp;
        fn.setScoping(VariableScoping.CLASS_INSTANCE);
        Optional<TypeBox> typeBoxOpt = fn.typecheckAsField(tenv, withinClass,
            Nullable.of(visibility), false);
        if (typeOptIsPresent) {
          typeOptIsPresent = typeBoxOpt.isPresent();
        }
      } else if (tmp instanceof DeclarationStatement) {
        DeclarationStatement decl = (DeclarationStatement) tmp;
        decl.setScoping(VariableScoping.CLASS_INSTANCE);
        Optional<TypecheckRet> typeBoxOpt = decl.typecheck(tenv, Nullable.empty());
        if (typeOptIsPresent) {
          typeOptIsPresent = typeBoxOpt.isPresent();
        }
      } else {
        throw new IllegalArgumentException();
      }

      return typeOptIsPresent;
    }

    public Pair<Nullable<ClassMember>, ClassMember> resolveExceptions(Project project,
        ModuleType currentModule, AuditResolveExceptions auditEntry,
        RenamesOfInterest jsmntGlobalRenamings, boolean enableResolution, String hitExcId,
        MsgState msgState) throws FatalMessageException {
      
      Nullable<Ast> newFnDecl = templateFunctionOrDecl;
      if (newFnDecl.isNotNull()) {
        newFnDecl = Nullable
            .of(templateFunctionOrDecl.get().resolveExceptions(project, currentModule,
                auditEntry, jsmntGlobalRenamings, Nullable.empty(), false, enableResolution, hitExcId,
                0, msgState).get().ast.get());
      }
      
      // if this is a function that is called by sandbox, the return sandboxed copy as well
      Nullable<ClassMember> copyFn = Nullable.empty();
      if (type instanceof FunctionType) {
        Nullable<RenamingEntry> entry = auditEntry.findFunction(id, (FunctionType) type);
        if (entry.isNotNull()) {
          copyFn = Nullable.of(new ClassMember(lineNum, columnNum, AbstractAst.getNextLabel(),
              false, classOfOrigin, isFfi, entry.get().name, originalId,
              entry.get().functionType, visibility, astType, isStatic,
              isOverride, isOverrideOf, isOverridden, isInit, isTemplate, true, newFnDecl));
        }
      }
      
      
      ClassMember newMember = new ClassMember(lineNum, columnNum, label,
          isNotGenerated, classOfOrigin, isFfi, id, originalId,
          type, visibility, astType, isStatic,
          isOverride, isOverrideOf, isOverridden, isInit, isTemplate, true, newFnDecl);
      return new Pair<>(copyFn, newMember);
    }

    public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
        throws FatalMessageException {
      return type.calculateSize(tenv, lineNum, columnNum);
    }

    public Set<UserDeclType> getExposedClassTypes(Set<Visibility> filter) {
      Set<UserDeclType> exposed = new HashSet<>();
      
      if (!filter.contains(visibility)) {
        return exposed;
      }
      exposed.addAll(type.getUserDeclTypes(false));
      return exposed;
    }

    public void addToCallgraphAsSandboxed(CallGraph callGraph)
        throws FatalMessageException {
      // only add if this is a non-lambda function
      if (!(type instanceof FunctionType)) {
        return;
      }
      FunctionType fnType = (FunctionType) type;
      if (fnType.lambdaStatus.isLambda()) {
        return;
      }
      callGraph.addNode(id, fnType, true);
    }

    public void determineIsOverrideOf(Project project, TypeEnvironment tenv, ClassHeader header,
        MsgState msgState)
        throws FatalMessageException {
      if (!isOverride) {
        return;
      }
      if (isOverrideOf.isNotNull()) {
        return;
      }
      if (header.parent.isNull()) {
        msgState.addMessage(MsgType.ERROR, MsgClass.AST, "class with override must have parent");
        return;
      }
      ClassType definingClass = header.parent.get();
      while (true) {
        // find the class header
        PsuedoClassHeader pch = project.lookupClassHeader(definingClass.asDeclType(),
            Nullable.empty(), true,
            project.getAuditTrail());
        if (!(pch instanceof ClassHeader)) {
          msgState.addMessage(MsgType.ERROR, MsgClass.AST, "class can only extend another class");
          return;
        }
        
        ClassHeader ch = (ClassHeader) pch;
        // find this member in the header
        Nullable<ClassMember> memberOpt = ch.lookupFn(id, (FunctionType) type, tenv.typeGraph,
            tenv);
        if (memberOpt.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
              "Class header must contain this function, even if not directly defined");
          return;
        }
        ClassMember member = memberOpt.get();
        
        // check if this member is also an override
        if (!member.isOverride) {
          // this is the class which initially defines the function
          isOverrideOf = Nullable.of(definingClass);
          // whether or not this override is ffi is based on the original defining class
          isFfi = ch.isFfi;
          break;
        }
        
        // not the class we are looking for
        if (ch.parent.isNull()) {
          msgState.addMessage(MsgType.ERROR, MsgClass.AST, "class with override must have parent");
          return;
        }
        definingClass = ch.parent.get();
      }
    }

    public void resolveUserTypes(TypeEnvironment tenv) throws FatalMessageException {
      Optional<Type> tmpTy = type.normalize(tenv, lineNum, columnNum);
      if (!tmpTy.isPresent()) {
        return;
      }
      type = tmpTy.get();
    }

  }

  public ClassHeader(int lineNum, int columnNum, long label,
      ModuleType moduleType, String name, ClassDeclType originalName,
      Nullable<ClassType> parent, List<Type> innerTypes, boolean export, boolean isFfi,
      boolean isStatic) {
    super(lineNum, columnNum, label, moduleType, export, name);
    this.parent = parent;
    this.innerTypes = new LinkedList<>(innerTypes);
    this.isFfi = isFfi;
    this.isStatic = isStatic;
    
    this.absName = new ClassDeclType(Nullable.of(moduleType), name);
    this.originalName = originalName;
  }

  @Override
  public String toString() {
    String res = "";

    res += "Abs Name: " + absName + "\n";
    res += "Parent: " + parent + "\n";
    res += "InnerTypes: " + innerTypes + "\n";
    res += "Members: " + members + "\n";

    return res;
  }

  public void addMember(ClassMember member, MsgState msgState) throws FatalMessageException {
    addMember(members, member, msgState);
  }

  private void addMember(Map<String, List<ClassMember>> memberMap, ClassMember member,
      MsgState msgState) throws FatalMessageException {
    List<ClassMember> tmp = memberMap.get(member.id);
    if (tmp == null) {
      tmp = new LinkedList<>();
    }
    // ensure no exact duplicate types unless it is from different origin class
    for (ClassMember other : tmp) {
      if (other.type.equals(member.type) && other.classOfOrigin.equals(member.classOfOrigin)) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "While adding class member " + member.id
                + " to header, hit collision with member of same name and type in class " + name);
        return;
      }
    }
    tmp.add(member);
    memberMap.put(member.id, tmp);
  }

  // The ClassHeader will be used by the Tenv of the module it is declared in as
  // well as in other
  // modules which import it
  public Nullable<List<ClassMember>> lookup(String id) {
    List<ClassMember> tmp = members.get(id);
    if (tmp == null) {
      return Nullable.empty();
    }
    return Nullable.of(tmp);
  }

  public Nullable<ClassMember> lookupFn(String id, FunctionType fnType, TypeGraph typeGraph,
      TypeEnvironment tenv) throws FatalMessageException {
    List<ClassMember> fnList = members.get(id);
    if (fnList.size() == 0) {
      return Nullable.empty();
    }
    if (fnList.size() == 1) {
      return Nullable.of(fnList.get(0));
    }
    // find best match with auto-casting
    int bestScore = -1;
    boolean bestMatchedWithin = false;
    Nullable<ClassMember> bestMatch = Nullable.empty();
    
    for (ClassMember member : fnList) {
      if (!(member.type instanceof FunctionType)) {
        continue;
      }
      Pair<Boolean, Integer> cmpResult = fnType.scoredCompare((FunctionType) member.type, tenv);
      boolean matchedWithin = cmpResult.a;
      int score = cmpResult.b;
      
      if (score > bestScore) {
        bestMatch = Nullable.of(member);
        bestScore = score;
        bestMatchedWithin = matchedWithin;
      } else if (score == bestScore) {
        if (matchedWithin && !bestMatchedWithin) {
          // use matchedWithin as a tie-breaker
          bestMatch = Nullable.of(member);
          bestScore = score;
          bestMatchedWithin = matchedWithin;
        }
      }
    }
    return bestMatch;
  }

  public List<ClassMember> getMemberList() {
    List<ClassMember> allMembers = new LinkedList<>();
    for (List<ClassMember> member : members.values()) {
      allMembers.addAll(member);
    }
    return allMembers;
  }

  /*
   * Find parent class if any and update our members
   */
  public void linkImports(Map<ModuleType, ProgHeader> importedMods,
      Map<String, ClassHeader> importedClasses, ProgHeader ourProgHeader, MsgState msgState)
      throws FatalMessageException {
    if (isLinked) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Attempted to link ClassHeader twice: " + originalName);
      return;
    }

    // update typegraph

    if (parent.isNull()) {
      isLinked = true;
      return;
    }

    // If the parent type has no mod path, then we must have imported the class
    // itself
    // otherwise, find the module, then locate the class within that prog header
    UserInstanceType parentType = parent.get();
    ClassHeader parentClassHeader = null;
    if (parentType.outerType.isNotNull()) {
      ProgHeader parentClassMod = importedMods.get(parentType.outerType.get());
      if (parentClassMod == null) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Unable to locate parent class module " + parentType.outerType.get()
                + " while searching for ClassHeader for parent class " + parentType
                + ". Known modules were: " + importedMods.keySet());
        return;
      }
      // found the module, now attempt to locate the class header
      Nullable<PsuedoClassHeader> tmp = parentClassMod.getClassHeader(parentType.name);
      if (tmp.isNull()) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Unable to locate parent class " + parentType + ". Known classes (from module "
                + parentClassMod.moduleType + ") are: " + parentClassMod.getClassNames());
        return;
      }
      parentClassHeader = (ClassHeader) tmp.get();
    } else {
      // first try sibling classes
      Nullable<PsuedoClassHeader> tmp = ourProgHeader.getClassHeader(parentType.name);
      if (tmp.isNull()) {
        // imported the class directly
        parentClassHeader = importedClasses.get(parentType.name);
      } else {
        parentClassHeader = (ClassHeader) tmp.get();
      }

      if (parentClassHeader == null) {
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Unable to locate parent class " + parentType + ". Known classes (siblings from module "
                + moduleType + ") are: " + ourProgHeader.getClassNames() + " and (direct imports "
                + importedClasses.keySet());
        return;
      }
    }
    
    /*
    if (parentClassHeader.isFfi != isFfi) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "ffi class cannot inherit from non-ffi class (or vice-versa): "
              + absName + " extends " + parent);
      return;
    }
    */

    // we found the parent ClassHeader (which is already done linking) so now link
    // it in
    for (Entry<String, List<ClassMember>> parentMember : parentClassHeader.members.entrySet()) {
      // if the exact member is already in our list, then ignore it as we have
      // overridden the parent
      List<ClassMember> matchList = members.get(parentMember.getKey());
      if (matchList == null || matchList.isEmpty()) {
        // no conflicts, so just add in parent member(s)
        members.put(parentMember.getKey(), parentMember.getValue());
        continue;
      }
      if (matchList.size() == 1 && !(matchList.get(0).type instanceof FunctionType)) {
        // conflicting newer member, don't add the parent member(s)
        continue;
      }

      for (ClassMember pm : parentMember.getValue()) {
        if (!(pm.type instanceof FunctionType)) {
          throw new IllegalArgumentException(
              "Expected parent member to be function but it was not!!");
        }
        // add only the variations of the fn which are not already in the child
        FunctionType parentFn = (FunctionType) pm.type;

        boolean found = false;
        for (ClassMember member : matchList) {
          if (!(member.type instanceof FunctionType)) {
            throw new IllegalArgumentException("Expected member to be function but it was not!!");
          }
          FunctionType memberFn = (FunctionType) member.type;
          // we want exact overloads, not "overloads by casting" here
          if (memberFn.equals(parentFn)) {
            found = true;
            break;
          }
        }
        if (!found) {
          if (!members.containsKey(parentMember.getKey())) {
            members.put(parentMember.getKey(), new LinkedList<>());
          }
          members.get(parentMember.getKey()).add(pm);
        }
      }
    }

    isLinked = true;
  }

  /*
   * Can only replace our own members, not any inherited members
   */
  @Override
  public Optional<PsuedoClassHeader> replaceGenerics(AuditTrailLite projectAuditTrailSoFar,
      Project project, Nullable<TypeEnvironment> tenv, List<Type> realInnerTypes,
      boolean noRegister, Map<Type, Type> genericToRealMapping, MsgState msgState, int lineNum,
      int columnNum) throws FatalMessageException {
    if (innerTypes.size() != realInnerTypes.size()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Wrong number of inner types", lineNum,
          columnNum);
      return Optional.empty();
    }
    if (innerTypes.isEmpty()) {
      return Optional.of(this); // no change
    }
    
    ClassType tmp = new ClassType(Nullable.of(moduleType), name, realInnerTypes);
    if (!genericToRealMapping.isEmpty()) {
      tmp = (ClassType) tmp.replaceType(genericToRealMapping);
    }

    // construct initial generics mapping
    Map<Type, Type> genericsMapping = AbstractType.zip(innerTypes, realInnerTypes);
    
    Map<String, List<ClassMember>> newMembers = new HashMap<>();
    if (!replaceGenerics(projectAuditTrailSoFar, project, tenv, genericsMapping, newMembers,
        noRegister, genericToRealMapping, msgState, lineNum, columnNum)) {
      return Optional.empty();
    }

    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull()) {
      newParent = Nullable.of((ClassType) parent.get().replaceType(genericsMapping));
    }
    
    ClassHeader newClassHeader = new ClassHeader(lineNum, columnNum, AbstractAst.getNextLabel(),
        tmp.outerType.get(), tmp.name,
        (ClassDeclType) originalName,
        newParent, tmp.innerTypes, export, isFfi, isStatic);
    newClassHeader.members = newMembers;
    newClassHeader.isLinked = isLinked;
    newClassHeader.isTypechecked = isTypechecked;
    /*
    if (newClassHeader.isGeneric()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "ClassHeader cannot be generic at this point: " + absName + ": " + realInnerTypes,
          lineNum, columnNum);
      return Optional.empty();
    }
    */
    return Optional.of(newClassHeader);
  }

  /*
   * Recursively update newMembers, starting with the top parent. Returns false on
   * error
   * 
   * genericsMapping is our new inner types: T -> string
   * genericsToRealMapping is our new class names: A<string> -> A_string
   */
  private boolean replaceGenerics(AuditTrailLite projectAuditTrailSoFar, Project project,
      Nullable<TypeEnvironment> tenv, Map<Type, Type> genericsMapping,
      Map<String, List<ClassMember>> newMembers, boolean noRegister,
      Map<Type, Type> genericToRealMapping, MsgState msgState, int lineNum, int columnNum)
      throws FatalMessageException {

    ClassType realClassInstanceType = (ClassType) new ClassType(Nullable.of(moduleType), name,
        innerTypes).replaceType(genericsMapping);
    if (!noRegister && tenv.isNotNull()) {
      tenv.get().registerNonGeneric(realClassInstanceType, lineNum, columnNum);
    }
    if (!genericToRealMapping.isEmpty()) {
      realClassInstanceType = (ClassType) realClassInstanceType.replaceType(genericToRealMapping);
    }

    // first the parent
    if (parent.isNotNull()) {
      // find the parent class header (parent is abs path, so no need for tenv)
      ClassHeader parentClassHeader = (ClassHeader) project.lookupClassHeader(
          new ClassDeclType(parent.get().outerType, parent.get().name), Nullable.empty(), false,
          projectAuditTrailSoFar);

      // create the generics mapping for the parent
      Map<Type, Type> parentGenericsMapping = new HashMap<>();
      if (parentClassHeader.innerTypes.size() != parent.get().innerTypes.size()) {
        // TODO should catch this sooner
        msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK, "Parent class " + parent + " of "
            + absName + " has the wrong number of parameterized types", lineNum, columnNum);
        return false;
      }
      Iterator<Type> keyIter = parentClassHeader.innerTypes.iterator();
      Iterator<Type> valIter = parent.get().innerTypes.iterator();
      while (keyIter.hasNext() && valIter.hasNext()) {
        Type tmpVal = valIter.next();
        // B<T> extends A<T, int> where A<X, Y>
        // for B<string> repl: T -> string, for A repl: X -> string, Y -> int
        Type tmpTy = genericsMapping.get(tmpVal);
        if (tmpTy != null) {
          tmpVal = tmpTy;
        }
        parentGenericsMapping.put(keyIter.next(), tmpVal);
      }

      if (!parentClassHeader.replaceGenerics(projectAuditTrailSoFar, project, tenv,
          parentGenericsMapping, newMembers, noRegister, genericToRealMapping, msgState, lineNum,
          columnNum)) {
        return false;
      }
    }
    
    ClassDeclType newClassDecl = (ClassDeclType) realClassInstanceType.asDeclType();
    Map<Type, Type> newWithinMapping = new HashMap<>();
    newWithinMapping.put(absName, newClassDecl);

    // now do our replacements
    for (Entry<String, List<ClassMember>> memberList : members.entrySet()) {
      for (ClassMember member : memberList.getValue()) {
        if (!member.classOfOrigin.equals(absName)) {
          // skip members that are inherited
          continue;
        }
        // replace T with string
        Type replTy1 = member.type.replaceType(genericsMapping);
        
        // replace A<string> with A_string
        Type replTy2 = replTy1.replaceType(genericToRealMapping);
        
        // replace A with A_string (update Function.within types)
        Type replTy3 = replTy2.replaceType(newWithinMapping);
        
        Nullable<Ast> newFnDecl = member.templateFunctionOrDecl;
        if (newFnDecl.isNotNull()) {
          Ast tmp = newFnDecl.get();
          if (tmp instanceof DeclarationStatement) {
            DeclarationStatement tmp1 = (DeclarationStatement) ((DeclarationStatement) tmp).replaceType(genericsMapping);
            DeclarationStatement tmp2 = (DeclarationStatement) tmp1.replaceType(genericToRealMapping);
            DeclarationStatement tmp3 = (DeclarationStatement) tmp2.replaceType(newWithinMapping);
            newFnDecl = Nullable.of(tmp3);
          } else if (tmp instanceof Function) {
            Function tmp1 = ((Function) tmp).replaceType(genericsMapping);
            Function tmp2 = tmp1.replaceType(genericToRealMapping);
            Function tmp3 = tmp2.replaceType(newWithinMapping);
            newFnDecl = Nullable.of(tmp3);
          } else {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
                "templateFunctionOrDecl was neither: " + tmp);
          }
        }

        Nullable<ClassType> newIsOverrideOf = Nullable.empty();
        if (member.isOverrideOf.isNotNull()) {
          ClassType tmp = (ClassType) member.isOverrideOf.get().replaceType(genericsMapping);
          tmp = (ClassType) tmp.replaceType(genericToRealMapping);
          newIsOverrideOf = Nullable.of(tmp);
        }
        
        ClassMember newMember = new ClassMember(member.lineNum, member.columnNum,
            AbstractAst.getNextLabel(),
            member.isNotGenerated, newClassDecl, member.isFfi,
            member.id, member.originalId, replTy3, member.visibility, member.astType,
            member.isStatic, member.isOverride, newIsOverrideOf, member.isOverridden, member.isInit,
            member.isTemplate, member.isRenamed, newFnDecl);
        addMember(newMembers, newMember, msgState);
      }
    }

    return true;
  }

  /*
   * Generate a type environment representing this class header, instance or
   * static. A class can never lose access to a type, even if you did not import that module.
   */
  @Override
  public Optional<TypeEnvironment> getTenv(TypeEnvironment tenv, boolean isStaticTenv,
      UserInstanceType realType) throws FatalMessageException {
    // find the proper module endpoint
    Nullable<ModuleEndpoint> modEp = tenv.project.lookupModule(moduleType,
        tenv.project.getAuditTrail());
    if (modEp.isNull()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Unable to find module in project: " + moduleType);
      return Optional.empty();
    }
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    
    EffectiveImports effectiveImp = moduleHandle.moduleBuildObj.effectiveImports.applyTransforms(
        tenv.project.getAuditTrail(), tenv.msgState);
    
    TypeEnvironment newTenv = TypeEnvironment.createEmptyUserTypeEnv(tenv.project, absName,
        tenv.typeGraph, effectiveImp, tenv.msgState);
    newTenv.setTypeTenv(tenv.copyOnlyTypes());
    if (isStaticTenv) {
      newTenv.envType = ScopeType.STATIC;
    } else {
      newTenv.envType = ScopeType.INSTANCE;
    }
    
    // add all of the types we use to the classTable
    /*
    for (UserDeclType requiredType : requiredTypes) {
      PsuedoClassHeader pch = tenv.project.lookupClassHeader(requiredType, Nullable.empty(), true,
          tenv.project.getAuditTrail());
      newTenv.classTable.get().addClassType(pch);
    }
    */

    for (Entry<String, List<ClassMember>> memberList : members.entrySet()) {
      for (ClassMember member : memberList.getValue()) {
        if (isStaticTenv == member.isStatic) {
          boolean isInit = member.isInit || tenv.project.lookupClassMemberIsInit(
              member.classOfOrigin, member.id);
          newTenv.defineVar(member.id, new BasicTypeBox(member.type, true, isInit,
              VariableScoping.CLASS_INSTANCE, member.classOfOrigin, member.visibility,
              member.isStatic));
        }
      }
    }

    if (!isStaticTenv) {
      // add 'this'
      newTenv.defineVar("this", new BasicTypeBox(new ReferenceType((ClassType) realType),
          true, true, VariableScoping.CLASS_INSTANCE, absName, Visibility.PRIVATE, false));
    }

    return Optional.of(newTenv);
  }

  public AstProto.ClassHeader.Builder serialize() {
    AstProto.ClassHeader.Builder document = AstProto.ClassHeader.newBuilder();

    document.setLineNum(lineNum);
    document.setColumnNum(columnNum);
    document.setLabel(label);
    document.setModuleType(moduleType.serialize());
    document.setName(name);
    document.setAbsName(absName.serialize());
    document.setOriginalName(originalName.serialize());
    if (parent.isNotNull()) {
      document.addParent(parent.get().serialize());
    }
    for (Type innerTy : innerTypes) {
      document.addInnerTypes(innerTy.serialize());
    }
    document.setExport(export);
    document.setIsFfi(isFfi);
    document.setIsStatic(isStatic);
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        document.addMembers(member.serialize());
      }
    }

    return document;
  }

  public static ClassHeader deserialize(AstProto.ClassHeader document, MsgState msgState)
      throws SerializationError, FatalMessageException {
    Nullable<ClassType> parent = Nullable.empty();
    if (document.getParentCount() != 0) {
      parent = Nullable.of((ClassType) AbstractType.deserialize(document.getParent(0)));
    }

    List<Type> innerTypes = new LinkedList<>();
    for (AstProto.Type bvType : document.getInnerTypesList()) {
      innerTypes.add(AbstractType.deserialize(bvType));
    }

    ClassHeader ch = new ClassHeader(document.getLineNum(), document.getColumnNum(),
        document.getLabel(),
        (ModuleType) AbstractType.deserialize(document.getModuleType()), document.getName(),
        (ClassDeclType) AbstractType.deserialize(document.getAbsName()), parent, innerTypes,
        document.getExport(), document.getIsFfi(), document.getIsStatic());

    for (AstProto.ClassMember bvMember : document.getMembersList()) {
      ClassMember member = ClassMember.deserialize(bvMember);
      ch.addMember(member, msgState);
    }
    
    ch.determineRequiredTypes();

    return ch;
  }

  public void populateRenameIds(Project project, Renamings renamings,
      RegisteredGenerics registeredGenerics, MsgState msgState) throws FatalMessageException {
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        member.populateRenameIds((ClassDeclType) absName, innerTypes, project, renamings,
            registeredGenerics,
            msgState);
      }
    }
  }

  public ClassHeader renameIds(Renamings renamings, MsgState msgState, boolean noError)
      throws FatalMessageException {
    ClassHeader newHeader = new ClassHeader(lineNum, columnNum, label,
        moduleType, name, (ClassDeclType) originalName, parent,
        innerTypes,
        export, isFfi, isStatic);
    newHeader.isLinked = isLinked;
    newHeader.isTypechecked = isTypechecked;
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        newHeader.addMember(member.renameIds(renamings, msgState, noError), msgState);
      }
    }
    return newHeader;
  }

  public ClassHeader replaceTypes(Map<Type, Type> typeRenamings, boolean strict, MsgState msgState)
      throws FatalMessageException {
    ClassDeclType newName = (ClassDeclType) typeRenamings.get(absName);
    if (newName == null) {
      if (!strict) {
        newName = (ClassDeclType) absName;
      } else {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAME_TYPES,
            "can't find renaming for: " + absName);
      }
    }

    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull()) {
      newParent = Nullable.of((ClassType) parent.get().replaceType(typeRenamings));
    }

    ClassHeader newHeader = new ClassHeader(lineNum, columnNum, label,
        (ModuleType) moduleType.replaceType(typeRenamings),
        newName.name, (ClassDeclType) originalName, newParent, innerTypes, export, isFfi, isStatic);
    newHeader.isLinked = isLinked;
    newHeader.isTypechecked = isTypechecked;
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        newHeader.addMember(member.replaceTypes(typeRenamings), msgState);
      }
    }
    return newHeader;
  }
  
  private static void helpMoveInitsToConstructors(Project project, AuditTrailLite projectAuditTrailSoFar,
      ClassHeader destinationHeader, ClassType currentClassType, MsgState msgState)
          throws FatalMessageException {
    // lookup the currentClassType header
    PsuedoClassHeader pch = project.lookupClassHeader(currentClassType.asDeclType(),
        Nullable.empty(), true,
        projectAuditTrailSoFar);
    if (!(pch instanceof ClassHeader)) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
          "invalid class type: " + currentClassType);
      return;
    }
    ClassHeader ch = (ClassHeader) pch;
    
    // if this header has a parent, call this recursively for the parent
    if (ch.parent.isNotNull()) {
      helpMoveInitsToConstructors(project, projectAuditTrailSoFar, destinationHeader,
          ch.parent.get(), msgState);
    }
    
    // determine if this header needs constructor/destructor
    boolean foundDefaultConstructor = false;
    boolean foundDestructor = false;
    
    for (List<ClassMember> memberList : ch.members.values()) {
      for (ClassMember member : memberList) {
        if (member.classOfOrigin.equals(ch.absName)) {
          if (member.originalId.equals("constructor")) {
            if (((FunctionType) member.type).argTypes.isEmpty()) {
              foundDefaultConstructor = true;
            }
          } else if (member.originalId.equals("destructor")) {
            foundDestructor = true;
          }
        }
      }
    }
    
    if (!foundDefaultConstructor) {
      destinationHeader.addMember(new ClassMember(ch.lineNum, ch.columnNum,
          AbstractAst.getNextLabel(),
          false, (ClassDeclType) ch.absName, false, "constructor", "constructor",
          new FunctionType(ch.absName, VoidType.Create(), new LinkedList<>(), LambdaStatus.NOT_LAMBDA),
          Visibility.PUBLIC, AstType.FUNCTION, false, false, Nullable.empty(), false, true, false,
          false,
          Nullable.empty()),
          msgState);
    }
    
    if (!foundDestructor) {
      destinationHeader.addMember(new ClassMember(ch.lineNum, ch.columnNum,
          AbstractAst.getNextLabel(),
          false, (ClassDeclType) ch.absName, false, "destructor", "destructor",
          new FunctionType(ch.absName, VoidType.Create(), new LinkedList<>(), LambdaStatus.NOT_LAMBDA),
          Visibility.PUBLIC, AstType.FUNCTION, false, false, Nullable.empty(), false, true, false,
          false, Nullable.empty()),
          msgState);
    }
  }
  
  ClassHeader moveInitsToConstructors(Project project, AuditTrailLite projectAuditTrailSoFar,
      MsgState msgState)
      throws FatalMessageException {
    //check if this header has default constructor and destructor. If not, then add
    //do this recursively for all parents of this class header, collecting the resulting newMembers
    //and adding them to this class header
    
    ClassHeader newHeader = new ClassHeader(lineNum, columnNum, label, moduleType,
        name, (ClassDeclType) originalName, parent, innerTypes, export, isFfi, isStatic);
    newHeader.isLinked = isLinked;
    newHeader.isTypechecked = isTypechecked;
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        newHeader.addMember(member, msgState);
      }
    }
    
    helpMoveInitsToConstructors(project, projectAuditTrailSoFar, newHeader,
        (ClassType) absName.asInstanceType(), msgState);
    
    return newHeader;
  }

  public Map<String, List<ClassMember>> getMembers() {
    return members;
  }

  // typecheck the ast of the declarations/functions with template types that are
  // now
  // replaced to be real
  public boolean templateTypecheck(TypeEnvironment globalTenv, int lineNum, int columnNum)
      throws FatalMessageException {
    
    ClassType absRealName = new ClassType(absName.outerType, absName.name, innerTypes);

    TypeEnvironment classTenv = globalTenv.extend(absRealName);

    // init class members as normal e.g. ClassDeclaration.typecheck
    boolean hitErr = false;
    for (ClassMember member : getMemberList()) {
      if (!member.typecheck(absName, classTenv, true, lineNum, columnNum)) {
        hitErr = true;
      }
    }
    if (hitErr) {
      return false;
    }

    // typecheck non-generic bodies now (used to be generic)
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        if (member.classOfOrigin.equals(absName)) {
          if (!member.templateTypecheck(classTenv, absRealName)) {
            hitErr = true;
          }
        }
      }
    }

    return !hitErr;
  }

  public static enum StaticFilter {
    NO_FILTER, NO_STATICS, ONLY_STATICS
  }

  /*
   * Only used by resolveGenerics
   */
  protected ClassHeader copy(boolean noInnerTypes, StaticFilter filterOnlyStatic, MsgState msgState)
      throws FatalMessageException {
    Nullable<ClassType> newParent = parent;
    if (parent.isNotNull() && filterOnlyStatic == StaticFilter.ONLY_STATICS) {
      newParent = Nullable
          .of(new ClassType(parent.get().outerType, parent.get().name, new LinkedList<>()));
    }
    ClassHeader newHeader = new ClassHeader(lineNum, columnNum, AbstractAst.getNextLabel(),
        moduleType, name, (ClassDeclType) originalName, newParent,
        noInnerTypes ? new LinkedList<>() : innerTypes, export, isFfi, isStatic);
    newHeader.isLinked = isLinked;
    newHeader.isTypechecked = isTypechecked;
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        if (filterOnlyStatic == StaticFilter.NO_STATICS && member.isStatic) {
          continue;
        }
        if (filterOnlyStatic == StaticFilter.ONLY_STATICS && !member.isStatic) {
          continue;
        }
        newHeader.addMember(member, msgState);
      }
    }
    return newHeader;
  }

  public void populateTypeGraph(TypeGraph typeGraph) throws FatalMessageException {
    typeGraph.addUserNode(new ClassType(Nullable.of(moduleType), name, innerTypes),
        parent.isNull() ? Nullable.empty() : Nullable.of(parent.get()));
  }

  /////////////////////////////////////////////////////////////
  // Vtable Functions
  /////////////////////////////////////////////////////////////

  static class VtableFunction {

    public final String name;
    public final FunctionType type;

    VtableFunction(String name, FunctionType type) {
      this.name = name;
      this.type = type;
    }

    @Override
    public boolean equals(Object other) {
      if (!(other instanceof VtableFunction)) {
        return false;
      }
      VtableFunction vfOther = (VtableFunction) other;
      return vfOther.name.equals(name) && vfOther.type.equals(type);
    }

    @Override
    public int hashCode() {
      return name.hashCode() + type.hashCode();
    }
  }

  public void populateVtable(Project project, ClassGraph classGraph, Node node,
      ModuleBuildObj mbo, MsgState msgState) throws FatalMessageException {

    // find/create our vtable
    ClassVtable vtable = mbo.vtables.get(absName);
    if (vtable == null) {
      vtable = new ClassVtable();
      mbo.vtables.put(absName, vtable);
    }

    // prepend our parent class's vtable
    if (parent.isNotNull()) {
      Nullable<ModuleEndpoint> parentModEp = project.lookupModule(parent.get().outerType.get(),
          project.getAuditTrail());
      if (parentModEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
            "unable to find parent class module: " + parent);
      }
      parentModEp.get().load();
      LoadedModuleHandle parentHandle = (LoadedModuleHandle) parentModEp.get().getHandle();
      ClassHeader parentCH = (ClassHeader) project.lookupClassHeader(
          new ClassDeclType(parent.get().outerType, parent.get().name),
          Nullable.of(parentHandle.moduleBuildObj), true, project.getAuditTrail());

      // get our list of functions that are overrides (for renaming parent vtable)
      Map<VtableFunction, ClassMember> fnOverrides = new HashMap<>();
      for (ClassMember member : getMemberList()) {
        if (member.classOfOrigin.equals(absName) && member.astType == AstType.FUNCTION
            && !member.isStatic && member.isOverrideOf.isNotNull()) {
          if (member.isOverridden) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
                "member cannot be both override and overridden");
          }
          fnOverrides.put(new VtableFunction(member.originalId, (FunctionType) member.type),
              member);
        }
      }

      vtable.vtableEntries = parentCH.copyVtableEntries(parentHandle.moduleBuildObj, msgState,
          fnOverrides);
    }

    // recursively check all child classes to determine which of our own functions
    // are overridden
    // stop when no more children, or all functions already overridden/no functions
    // of our own
    List<ClassDeclType> childClasses = classGraph.dfsAllChildClasses(node);

    if (childClasses.isEmpty()) {
      // no children, so no vtables
      return;
    }
    Map<VtableFunction, ClassMember> fnOverridden = new HashMap<>();
    int totalFns = 0; // only decrement when a function is newly found to be overridden
    for (ClassMember member : getMemberList()) {
      if (member.classOfOrigin.equals(absName) && member.astType == AstType.FUNCTION
          && !member.isStatic && !member.isOverride) {
        if (member.isOverridden) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
              "member cannot be both override and overridden");
        }
        // make sure to substitute the function.within type with the original defining class
        // ... which is just this class
        FunctionType fnType = (FunctionType) member.type;
        fnOverridden.put(new VtableFunction(member.originalId, fnType), member);
        totalFns += 1;
      }
    }

    if (totalFns == 0) {
      // no functions to override
      return;
    }

    for (ClassDeclType childClassTy : childClasses) {
      // check for stop conditions
      if (totalFns == 0) {
        // all our immediate functions overridden
        for (ClassMember ourFn : fnOverridden.values()) {
          if (!ourFn.isOverridden) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
                "fn override count mismatch: " + fnOverridden);
          }
        }
        break;
      }

      // find child class header
      ClassHeader childCH = (ClassHeader) project.lookupClassHeader(childClassTy, Nullable.of(mbo),
          true, project.getAuditTrail());

      // check its own functions to see if they name+type match any of our own
      // functions that are
      // not already overridden
      for (ClassMember childMember : childCH.getMemberList()) {
        if (!childMember.classOfOrigin.equals(absName) && childMember.astType == AstType.FUNCTION
            && !childMember.isStatic && childMember.isOverrideOf.isNotNull()) {
          // this member is a viable candidate, find the corresponding of our members
          // make sure to substitute the function.within type with the original defining class
          FunctionType tmpFnType = (FunctionType) childMember.type;
          FunctionType fnType = new FunctionType(childMember.isOverrideOf.get(), tmpFnType.returnType,
              tmpFnType.argTypes, tmpFnType.lambdaStatus);
          ClassMember ourMember = fnOverridden.get(
              new VtableFunction(childMember.originalId, fnType));
          if (ourMember == null) {
            // this function does not exist in our class, so cannot add it to our vtable
            // however, the function may exist further down the class chain
            // Obj -> A -> B, where A.sum() and B.sum(), for example
            continue;
            /*
            // override specified in vain
            msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
                "Function '" + childMember.id + "' is an override that overrides nothing. "
                    + "Should have been found during typecheck.");
                    */
          }
          if (!ourMember.isOverridden) {
            totalFns -= 1;
          }
          ourMember.isOverridden = true;
          // if so, then add it to mbo.vtable
          Nullable<Pair<VtableEntry, Integer>> fnd = mbo.vtables.searchVtable(absName, msgState,
              ourMember.id, (FunctionType) ourMember.type, true);
          if (fnd.isNull()) {
            vtable.vtableEntries.add(
                new VtableEntry(ourMember.id, ourMember.originalId, (FunctionType) ourMember.type,
                    true));
          }
        }
      }
    }
  }

  // copy parent vtable while also performing any name replacements
  private List<VtableEntry> copyVtableEntries(ModuleBuildObj mbo, MsgState msgState,
      Map<VtableFunction, ClassMember> fnOverrides) throws FatalMessageException {
    // find our vtable
    ClassVtable vtable = mbo.vtables.get(absName);
    if (vtable == null) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
          "Did not find vtable to copy for: " + absName);
    }

    List<VtableEntry> copy = new ArrayList<>();
    for (VtableEntry e : vtable.vtableEntries) {
      ClassMember childFn = fnOverrides.get(new VtableFunction(e.originalName, e.type));
      if (childFn == null) {
        // no override for this, just use inherited fn
        copy.add(new VtableEntry(e.name, e.originalName, e.type, false));
      } else {
        // use child override
        copy.add(new VtableEntry(childFn.id, e.originalName, e.type, false));
      }
    }
    return copy;
  }

  public void populateClassGraph(ClassGraph classGraph, EffectiveImports effectiveImports,
      MsgState msgState) throws FatalMessageException {
    Node node = classGraph.addClassNode(absName);
    if (parent.isNotNull()) {
      node.populateUplinks(classGraph, effectiveImports, parent.get(), ImportUsage.HARD);
    }
  }

  public ClassHeader resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, boolean enableResolution, String hitExcId,
      MsgState msgState) throws FatalMessageException {
    
    ClassHeader newHeader = new ClassHeader(lineNum, columnNum, label,
        moduleType, name, originalName, parent, innerTypes,
        export, isFfi, isStatic);
    newHeader.isLinked = isLinked;
    newHeader.isTypechecked = isTypechecked;
    
    for (List<ClassMember> memberList : members.values()) {
      for (ClassMember member : memberList) {
        Pair<Nullable<ClassMember>, ClassMember> tmp = member.resolveExceptions(project,
            currentModule, auditEntry,
            jsmntGlobalRenamings,
            enableResolution, hitExcId, msgState);
        if (tmp.a.isNotNull()) {
          newHeader.addMember(tmp.a.get(), msgState);
        }
        newHeader.addMember(tmp.b, msgState);
      }
    }
    return newHeader;
  }

  @Override
  public List<Type> getInnerTypes() {
    return innerTypes;
  }

  /*
   * all generics have already been replaced
   */
  @Override
  public int calculateSize(TypeEnvironment tenv, boolean isStatic, int lineNum, int columnNum)
      throws FatalMessageException {
    
    if (isStatic) {
      if (isGeneric()) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot calculate size of generic class declaration: "
                + new ClassDeclType(Nullable.of(moduleType), name), lineNum, columnNum);
        return -1;
      }
    }
    
    int size = 0;

    for (ClassMember member : getMemberList()) {
      int memberSize = member.calculateSize(tenv, lineNum, columnNum);
      if (memberSize == -1) {
        return -1;
      }
      size += memberSize;
    }

    return size;
  }

  @Override
  public void insertDestructorCalls(ScopedIdentifiers ourScope,
      DestructorRenamings destructorRenamings) {
    for (ClassMember member : getMemberList()) {
      if (member.classOfOrigin.equals(absName)) {
        ourScope.add(member.id, member.type);
      }
    }
  }
  
  public Set<UserDeclType> getExposedClassTypes(Set<Visibility> filter) {
    Set<UserDeclType> exposed = new HashSet<>();
    
    exposed.add(absName);
    for (ClassMember member : getMemberList()) {
      exposed.addAll(member.getExposedClassTypes(filter));
    }
    
    return exposed;
  }

  public void addToCallgraphAsSandboxed(CallGraph callGraph)
      throws FatalMessageException {
    // iterate through the members and add all functions to the callgraph. No recursion of types
    // here
    for (ClassMember member : getMemberList()) {
      member.addToCallgraphAsSandboxed(callGraph);
    }
  }

  // we need module/import info just to be able to normalize all type names
  public void determineRequiredTypes() {
    // parent type is already accounted for, so find typenames from members
    Set<Visibility> filter = new HashSet<>();
    filter.add(Visibility.PRIVATE);
    filter.add(Visibility.PROTECTED);
    filter.add(Visibility.PUBLIC);
    for (ClassMember member : getMemberList()) {
      // get ALL types used, not just exposed (they must be abs)
      // add them to our required types set
      requiredTypes.addAll(member.getExposedClassTypes(filter));
    }
  }

  public void determineIsOverrideOf(Project project, TypeEnvironment tenv, MsgState msgState)
      throws FatalMessageException {
    for (ClassMember member : getMemberList()) {
      member.determineIsOverrideOf(project, tenv, this, msgState);
    }
  }

  @Override
  public Expression getDummyExpression(UserInstanceType self, MsgState msgState)
      throws FatalMessageException {
    if (!(self instanceof ClassType)) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "expected class type, but got: " + self);
    }
    ClassType selfClass = (ClassType) self;
    
    String defaultConstructorId = "";
    
    // lookup our default constructor
    for (ClassMember member : getMemberList()) {
      if (!member.classOfOrigin.equals(absName)) {
        continue;
      }
      if (!(member.type instanceof FunctionType)) {
        continue;
      }
      FunctionType fnTy = (FunctionType) member.type;
      if (member.originalId.equals("constructor") && fnTy.argTypes.isEmpty()) {
        defaultConstructorId = member.id;
        break;
      }
    }
    if (defaultConstructorId.isEmpty()) {
      // then we have not created it yet, so default name will still just be "constructor"
      defaultConstructorId = "constructor";
    }
    
    IdentifierExpression fnName = new IdentifierExpression(defaultConstructorId);
    
    FunctionCallExpression constructorCall = new FunctionCallExpression(Nullable.empty(), fnName,
        new LinkedList<>(), "constructor");
    return new NewClassInstanceExpression(selfClass, selfClass.toString(), constructorCall);
  }

  @Override
  public UserDeclType getAbsName() {
    return absName;
  }

  @Override
  public void defineInTenv(TypeEnvironment tenv) {
    tenv.defineVar(name, new ClassTypeBox(this, true, VariableScoping.GLOBAL));
  }

  public boolean typecheck(ClassDeclType ofClassDecl,
      TypeEnvironment classTenv,
      int lineNum, int columnNum) throws FatalMessageException {
    
    boolean hitErr = false;
    for (ClassMember member : getMemberList()) {
      if (!member.typecheck(ofClassDecl, classTenv, false, lineNum, columnNum)) {
        hitErr = true;
      }
    }
    return !hitErr;
  }

  public void resolveUserTypes(TypeEnvironment tenv) throws FatalMessageException {
    if (parent.isNotNull()) {
      Optional<Type> tmpTy = parent.get().normalize(tenv, lineNum, columnNum);
      if (!tmpTy.isPresent()) {
        return;
      }
      parent = Nullable.of((ClassType) tmpTy.get());
    }
    
    for (ClassMember member : getMemberList()) {
      member.resolveUserTypes(tenv);
    }
  }

  @Override
  public ResolvedImportType getImportType() {
    return (ResolvedImportType) absName.asImportType();
  }

  @Override
  public String getName() {
    return absName.name;
  }

  // return true on success
  public boolean linkResolvedExceptionsAuditEntries(Project project,
      AuditResolveExceptions auditEntry, MsgState msgState) throws FatalMessageException {
    ClassHeader ch = this;
    Set<ModuleType> visitedModules = new HashSet<>();
    visitedModules.add(moduleType);
    while (ch.parent.isNotNull()) {
      // lookup the parent header
      PsuedoClassHeader pch = project.lookupClassHeader(ch.parent.get().asDeclType(),
          Nullable.empty(), true,
          project.getAuditTrail());
      if (!(pch instanceof ClassHeader)) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
            "failed to find parent class: " + ch.parent.get());
        return false;
      }
      ClassHeader parentHeader = (ClassHeader) pch;
      
      // audit entry is per module, so if we have not seen this module yet, merge the entry
      // into our entry
      if (!visitedModules.contains(parentHeader.moduleType)) {
        visitedModules.add(parentHeader.moduleType);
        
        // lookup the parent module to find the audit entry
        Nullable<ModuleEndpoint> modEp = project.lookupModule(parentHeader.moduleType,
            project.getAuditTrail());
        if (modEp.isNull()) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
              "failed to find parent module: " + parentHeader.moduleType);
          return false;
        }
        modEp.get().load();
        LoadedModuleHandle parentModHandle = (LoadedModuleHandle) modEp.get().getHandle();
        AuditEntry entry = parentModHandle.moduleBuildObj.auditTrail.getLast();
        if (!(entry instanceof AuditResolveExceptions)) {
          msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
              "last audit entry of parent is not correct: " + entry.getType());
          return false;
        }
        AuditResolveExceptions parentEntry = (AuditResolveExceptions) entry;
        
        auditEntry.linkWith(parentEntry);
      }
      
      ch = parentHeader;
    }
    return true;
  }

}
