package audit;

import astproto.AstProto;

/*
 * No associated data
 */

public class AuditBasic implements AuditEntry, AuditEntryLite {

  private final AuditEntryType type;
  private final StackTraceElement[] traceback;
  
  public AuditBasic(AuditEntryType type) {
    this.type = type;
    this.traceback = Thread.currentThread().getStackTrace();
  }
  
  @Override
  public String toString() {
    String res = getType().toString();
    
    res += " traceback: ";
    int i = -1;
    boolean start = false;
    for (StackTraceElement s : traceback) {
      i += 1;
      if (!start) {
        if (i < 1) {
          continue;
        }
        if ((s.getClassName() + "." + s.getMethodName()).equals("errors.Message.<init>")) {
          continue;
        }
        if (s.getMethodName().equals("addMessage")) {
          continue;
        }
      }
      start = true;
      res += "\n\tat " + s.getClassName() + "." + s.getMethodName()
        + " (" + s.getFileName() + ", " + s.getLineNumber() + ")";
    }
    return res + "\n";
  }
  
  @Override
  public AuditEntryType getType() {
    return type;
  }

  @Override
  public AstProto.AuditEntry.Builder serialize() {
    AstProto.AuditEntry.Builder document = AstProto.AuditEntry.newBuilder();
    document.setAuditEntryType(getType().ordinal());
    
    AstProto.AuditBasic.Builder subDoc = AstProto.AuditBasic.newBuilder();
    
    document.setAuditBasic(subDoc);
    return document;
  }
  
  @Override
  public AstProto.AuditEntryLite.Builder serializeLite() {
    AstProto.AuditEntryLite.Builder document = AstProto.AuditEntryLite.newBuilder();
    document.setAuditEntryType(getType().ordinal());
    
    AstProto.AuditBasic.Builder subDoc = AstProto.AuditBasic.newBuilder();
    
    document.setAuditBasic(subDoc);
    return document;
  }

  @Override
  public AuditEntry combineWith(AuditEntry other) {
    if (other.getType() != getType()) {
      throw new IllegalArgumentException();
    }
    return this;
  }

  @Override
  public AuditEntry withRelativeEntries() {
    return this;
  }

}
