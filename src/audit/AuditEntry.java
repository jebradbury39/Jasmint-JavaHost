package audit;

import astproto.AstProto;

public interface AuditEntry {
  public static enum AuditEntryType {
    RENAME_IDS, //fields, functions, ids
    RENAME_TYPES, //modules and classes
    RESOLVE_GENERICS,
    LINK_HEADERS,
    TYPECHECK,
    ANF,
    LINK_PROVIDERS,
    RESOLVE_EXCEPTIONS,
    INSERT_START_FUNCTION,
    INSERT_DESTRUCTOR_CALLS,
    MOVE_INITS_TO_CONSTRUCTORS,
    LIFT_CLASSES,
    REMOVE_NON_MODULE_IMPORTS,
  }

  public AuditEntryType getType();

  public AstProto.AuditEntry.Builder serialize();

  public AuditEntry combineWith(AuditEntry other);

  AuditEntry withRelativeEntries();

}
