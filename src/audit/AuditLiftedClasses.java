package audit;

import astproto.AstProto.AuditEntry.Builder;
import header.ClassHeader;
import java.util.LinkedList;
import java.util.List;

public class AuditLiftedClasses implements AuditEntry {
  public final List<ClassHeader> liftedClasses;
  
  public AuditLiftedClasses(List<ClassHeader> liftedClasses) {
    this.liftedClasses = liftedClasses;
  }
  
  public AuditLiftedClasses() {
    this.liftedClasses = new LinkedList<>();
  }

  @Override
  public AuditEntryType getType() {
    return AuditEntryType.LIFT_CLASSES;
  }

  @Override
  public Builder serialize() {
    throw new UnsupportedOperationException();
  }

  @Override
  public AuditEntry combineWith(AuditEntry other) {
    if (other.getType() != getType()) {
      throw new IllegalArgumentException();
    }
    throw new UnsupportedOperationException();
  }

  @Override
  public AuditEntry withRelativeEntries() {
    return this;
  }

}
