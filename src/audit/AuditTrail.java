package audit;

import ast.SerializationError;
import astproto.AstProto;
import audit.AuditEntry.AuditEntryType;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImports;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import typecheck.ImportType;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;
import util.Pair;

public class AuditTrail {
  public final ModuleType moduleType; //abs path
  private List<AuditEntry> entries;
  private ReentrantReadWriteLock rwlock = new ReentrantReadWriteLock();
  
  public AuditTrail(ModuleType moduleType) {
    this.moduleType = moduleType;
    this.entries = new LinkedList<>();
  }
  
  @Override
  public String toString() {
    rwlock.readLock().lock();
    String tmp = entries.toString();
    rwlock.readLock().unlock();
    return tmp;
  }

  public List<AuditEntry> getEntries() {
    return entries;
  }

  public Iterator<AuditEntry> iterator() {
    return entries.iterator();
  }

  public boolean contains(AuditEntryType type) {
    rwlock.readLock().lock();
    boolean found = false;
    for (AuditEntry entry : entries) {
      if (entry.getType() == type) {
        found = true;
        break;
      }
    }
    rwlock.readLock().unlock();
    return found;
  }

  public void add(AuditEntry entry) {
    rwlock.writeLock().lock();
    entries.add(entry);
    rwlock.writeLock().unlock();
  }

  public AuditEntry getLast() {
    return entries.get(entries.size() - 1);
  }

  public static AuditTrail deserialize(Project project, ModuleType moduleType,
      AstProto.AuditTrail document)
      throws SerializationError {
    AuditTrail auditTrail = new AuditTrail(moduleType);
    
    for (AstProto.AuditEntry entry : document.getEntriesList()) {
      AuditEntryType type = AuditEntryType.values()[entry.getAuditEntryType()];
      AuditEntry newEntry = null;
      switch (type) {
        case RENAME_IDS:
          newEntry = AuditRenameIds.deserialize(project, moduleType, entry.getAuditRenameIds());
          break;
        case RENAME_TYPES:
          newEntry = AuditRenameTypes.deserialize(entry.getAuditRenameTypes());
          break;
        default:
          newEntry = new AuditBasic(type);
          break;
      }
      auditTrail.add(newEntry);
    }
    
    return auditTrail;
  }

  public AstProto.AuditTrail.Builder serialize() {
    AstProto.AuditTrail.Builder document = AstProto.AuditTrail.newBuilder();
    
    for (AuditEntry entry : entries) {
      document.addEntries(entry.serialize());
    }
    
    return document;
  }

  public int size() {
    rwlock.readLock().lock();
    int tmp = entries.size();
    rwlock.readLock().unlock();
    return tmp;
  }
  
  int findPositionOf(AuditEntry entry) {
    AuditEntryType entryType = entry.getType();
    int nth = 0;
    
    rwlock.readLock().lock();
    for (AuditEntry tmp : entries) {
      if (tmp.getType() == entryType) {
        if (tmp == entry) { //check if this is the same entry
          break;
        }
        nth += 1;
      }
    }
    rwlock.readLock().unlock();
    return nth;
  }
  
  Nullable<AuditEntry> findAtPosition(AuditEntryType entryType, int nthAt) {
    int nth = 0;
    Nullable<AuditEntry> found = Nullable.empty();
    
    rwlock.readLock().lock();
    for (AuditEntry tmp : entries) {
      if (tmp.getType() == entryType) {
        if (nth == nthAt) { //check if this is the same entry
          found = Nullable.of(tmp);
          break;
        }
        nth += 1;
      }
    }
    rwlock.readLock().unlock();
    
    return found;
  }

  public AuditEntry combineWithImported(ModuleType currentModule, AuditEntry entry, Project project,
      EffectiveImports effectiveImports, AuditTrailLite projectAuditTrailSoFar, MsgState msgState)
          throws FatalMessageException {
    //find out the nth position of this entry
    int nth = findPositionOf(entry);
    
    //now iterate over effective imports, find audit trail from each module, and find
    //the corresponding audit entry. Then combine the entries
    AuditEntry combined = entry.withRelativeEntries();
    final Set<EffectiveImport.EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImport.EffectiveImportType.LITERAL_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.IMPLIED_IMPORT);
    filterImpTypes.add(EffectiveImport.EffectiveImportType.INHERITED_IMPORT);
    
    for (Pair<ImportType, Nullable<ProjectNameVersion>> impModType
        : effectiveImports.getImportList(true, filterImpTypes))
    {
      Nullable<ModuleEndpoint> impModEp = project.lookupModule(impModType.a, projectAuditTrailSoFar);
      if (impModEp.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Unable to find module: " + impModType.a);
      }
      impModEp.get().load();
      LoadedModuleHandle impModHandle = (LoadedModuleHandle) impModEp.get().getHandle();
      Nullable<AuditEntry> impEntry = impModHandle.moduleBuildObj.auditTrail.findAtPosition(
          entry.getType(), nth);
      if (impEntry.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "Unable to find (" + nth + ") nth entry: " + entry.getType());
      }
      combined = combined.combineWith(impEntry.get());
    }
    
    return combined;
  }

  public Nullable<AuditEntry> getCombined(AuditEntryType filterType) {
    //in theory, if multiple renamings we combine so we find the final mapping
    //in practice, assume renameIds was only called once and pull that first one
    for (AuditEntry entry : entries) {
      if (entry.getType() == filterType) {
        return Nullable.of(entry);
      }
    }
    return Nullable.empty();
  }

  /*
   * Travel along with auditTrail (from Project) and add entries of desired type together
   * 
   * This does not really work, since entries of other types can come in between, e.g.
   * 
   * if we have (X, a) -> a_1, and other has (X_1, a_1) -> a_1_1, then we want (X, a) -> a_1_1
   */
  /*
  public Nullable<AuditEntry> getCombined(AuditEntryType type, AuditTrailLite auditTrail,
      MsgState msgState)
      throws FatalMessageException {
    Iterator<AuditEntry> iterEntries = entries.iterator();
    Iterator<AuditEntryLite> iterTrail = auditTrail.iterator();
    
    Nullable<AuditEntry> finalEntry = Nullable.empty();
    while (iterTrail.hasNext()) {
      AuditEntryLite trailType = iterTrail.next();
      AuditEntry entry = iterEntries.next();
      if (trailType.getType() != entry.getType()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "audit trail mismatch: "
            + trailType + " vs " + entry);
      }
      
      if (finalEntry.isNull()) {
        if (trailType.getType() != type) {
          continue;
        }
        //the first entry of this type, so nothing to combine with yet
        finalEntry = Nullable.of(entry);
        continue;
      }
      finalEntry = Nullable.of(entry.applyTransform(entry));
    }
    return finalEntry;
  }
  */
  
}
