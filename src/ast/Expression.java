package ast;

import ast.Program.DestructorRenamings;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import mbo.Renamings;
import mbo.TypeReplacements;
import typecheck.ModuleType;
import typecheck.SizeofTarget;
import typecheck.Type;
import util.ScopedIdentifiers;

public interface Expression extends Ast, SizeofTarget {
  /* This ensures unique names and also takes care of shadowing.
   * This is a utility for the users of the ast
   */
  Expression renameIds(Renamings renamings,
      MsgState msgState)
          throws FatalMessageException;
  
  public Expression replaceType(Map<Type, Type> mapFromTo);

  Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException;
  
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException;
  
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException;
  
  // this MUST run exactly ONCE right AFTER the FIRST typecheck
  // must be after typecheck, because we need to know all the determinedTypes
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException;
  
  public Expression addThisExpression(ClassHeader classHeader);
}
