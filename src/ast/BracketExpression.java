package ast;

import ast.BinaryExpression.BinOp;
import ast.LeftUnaryOpExpression.LeftUnaryOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.ArrayValue;
import interp.CharValue;
import interp.Environment;
import interp.InterpError;
import interp.MapValue;
import interp.StringValue;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.ArrayType;
import typecheck.CharType;
import typecheck.FunctionType;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typegraph.CastFilter;
import util.Pair;
import util.ScopedIdentifiers;

public class BracketExpression extends AbstractExpression {

  public final Expression left;
  public final Expression index;

  public BracketExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression left, Expression index) {
    super(lineNum, columnNum, AstType.BRACKET_EXPRESSION, preComments, postComments);
    this.left = left;
    this.index = index;

    left.setParentAst(this);
    index.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + left.toString(indent) + "[" + index.toString(indent) + "]"
        + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> optLeftType = left.typecheck(tenv, Nullable.empty());
    Optional<TypecheckRet> optIndexType = index.typecheck(tenv, Nullable.empty());

    if (!optLeftType.isPresent() || !optIndexType.isPresent()) {
      return Optional.empty();
    }
    Type leftType = optLeftType.get().type;
    Type indexType = optIndexType.get().type;

    if (leftType instanceof ReferenceType) {
      leftType = ((ReferenceType) leftType).innerType;
    }

    if (leftType instanceof ArrayType) {
      if (!tenv.typeGraph.canCastTo(indexType, CastFilter.UP_OR_DOWN, IntType.Create(false, 64))) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "array index type " + indexType + " cannot be converted to an int", lineNum, columnNum);
        return Optional.empty();
      }
      determinedType = ((ArrayType) leftType).getInnerTypes().get(0);
    } else if (leftType instanceof MapType) {
      MapType mapTy = (MapType) leftType;

      if (!tenv.typeGraph.canCastTo(indexType, CastFilter.UP_OR_DOWN, mapTy.keyType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "map key type " + indexType + " cannot be converted to expected type " + mapTy.keyType,
            lineNum, columnNum);
        return Optional.empty();
      }
      determinedType = mapTy.valueType;
    } else if (leftType instanceof StringType) {
      determinedType = CharType.Create();
    } else {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "bracket notation not defined for expression type " + leftType, lineNum, columnNum);
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value leftValue = left.interp(env);
    Value idxValue = index.interp(env);

    if (leftValue instanceof ArrayValue) {
      ArrayValue laVal = (ArrayValue) leftValue;
      return laVal.getElementByIndex((int) idxValue.toInt()).getValue();
    }
    if (leftValue instanceof MapValue) {
      MapValue lmVal = (MapValue) leftValue;
      return lmVal.getValueByKey(idxValue).getValue();
    }
    if (leftValue instanceof StringValue) {
      StringValue strVal = (StringValue) leftValue;
      int charIdx = (int) idxValue.toInt();
      if (charIdx < 0 || charIdx >= strVal.value.length()) {
        throw new InterpError(
            "string index [" + charIdx + "] is out of range [0, " + strVal.value.length() + ")",
            lineNum, columnNum);
      }
      return new CharValue(strVal.value.charAt(charIdx));
    }
    throw new InterpError("bracket notation not defined for expression value " + leftValue, lineNum,
        columnNum);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.BracketExpression.Builder subDoc = AstProto.BracketExpression.newBuilder();

    subDoc.setLeft(left.serialize().build());
    subDoc.setIdx(index.serialize().build());

    document.setBracketExpression(subDoc);
    return document;
  }

  public static BracketExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.BracketExpression document) throws SerializationError {
    return new BracketExpression(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getLeft()),
        AbstractExpression.deserialize(document.getIdx()));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        left.renameIds(renamings, msgState), index.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = left.findFreeVariables();
    freeVars.putAll(index.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof BracketExpression)) {
      return false;
    }
    BracketExpression ourOther = (BracketExpression) other;
    return left.compare(ourOther.left) && index.compare(ourOther.index);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        (Expression) left.replace(target, with), (Expression) index.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = left.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    return index.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> leftPair = left.toAnf(tuning);
    insert.addAll(leftPair.a);
    Pair<List<DeclarationStatement>, Ast> indexPair = index.toAnf(tuning);
    insert.addAll(indexPair.a);

    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        (Expression) leftPair.b, (Expression) indexPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        left.replaceType(mapFromTo), index.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmpLeft = left.replaceVtableAccess(project, msgState);
    Optional<Ast> tmpIndex = index.replaceVtableAccess(project, msgState);
    if (!tmpLeft.isPresent() || !tmpIndex.isPresent()) {
      return Optional.empty();
    }

    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        (Expression) tmpLeft.get(), (Expression) tmpIndex.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Expression>> tmpLeft = left.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    Optional<Nullable<Expression>> tmpIndex = index.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpLeft.isPresent() || !tmpIndex.isPresent()) {
      return Optional.empty();
    }

    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        tmpLeft.get().get(), tmpIndex.get().get());
    Optional<Nullable<Type>> tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    value.determinedType = tmp.get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    left.addDependency(modDepGraph, ourModNode, tenv);
    index.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return left.containsType(types) || index.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return left.containsAst(asts) || index.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize, MsgState msgState)
      throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = left.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newLeft = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      // when doing these checks, it is essential that we have stored result in temp
      // variable (anf)
      if (left.getDeterminedType() instanceof ReferenceType) {
        // insert null check
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
            new BinaryExpression(BinOp.EQUAL, newLeft, new NullExpression()), false, msgState));
      }
    }

    tmp = index.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newIndex = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      // insert bounds check
      Type leftType = left.getDeterminedType();
      if (leftType instanceof ReferenceType) {
        leftType = ((ReferenceType) leftType).innerType;
      }
      if (leftType instanceof ArrayType) {
        // newIndex must be int value: (newIndex < 0 || newIndex >= newLeft.size())
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.outOfBoundsExc,
            new BinaryExpression(BinOp.OR,
                new BinaryExpression(BinOp.LT, newIndex, new IntegerExpression("0")),
                new BinaryExpression(BinOp.GTE, newIndex,
                    new FunctionCallExpression(Nullable.of(newLeft),
                        new IdentifierExpression("size"), new LinkedList<>(), "size"))), false,
            msgState));
      } else if (leftType instanceof MapType) {
        // newIndex must be a key value: (!newLeft.contains(newIndex))
        List<Expression> mapContainsArgs = new LinkedList<>();
        mapContainsArgs.add(newIndex);
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.outOfBoundsExc,
            new LeftUnaryOpExpression(LeftUnaryOp.NOT,
                new FunctionCallExpression(Nullable.of(newLeft),
                    new IdentifierExpression("contains"), mapContainsArgs, "contains")), false,
            msgState));
      } else {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
            "Left type of bracket must be either array or map (or reference to such)", lineNum,
            columnNum);
      }
    }

    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        newLeft, newIndex);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, value));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        left.insertDestructorCalls(scopedIds, destructorRenamings),
        index.insertDestructorCalls(scopedIds, destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        left.liftClassDeclarations(liftedClasses, replacements).get(),
        index.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        left.resolveUserTypes(msgState).get(),
        index.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    BracketExpression value = new BracketExpression(lineNum, columnNum, preComments, postComments,
        left.addThisExpression(classHeader),
        index.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
