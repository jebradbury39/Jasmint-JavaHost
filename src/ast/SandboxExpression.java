package ast;

import ast.ClassDeclarationSection.Visibility;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.EnumHeader;
import header.PsuedoClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import sandbox.AbstractSandboxParam;
import sandbox.AbstractSandboxParam.ParamType;
import sandbox.SandboxParam;
import transform.ResolveExcRet;
import type_env.ClassTypeBox;
import type_env.EnumTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.UserDeclTypeBox;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class SandboxExpression extends AbstractExpression {

  public final Type returnType;
  public final List<Type> importTypes; // while we implicitly pass in returnType and arg types
  public final List<DeclarationStatement> args; // int newId = expr LIKE expr as int newId
  public final List<Expression> importFunctions; // will stay as non-lambda
  public final List<SandboxParam> params; // timeout, recursion, memlimit
  public final BlockStatement body;
  public final CatchExcSection catchSection; // this is not in the sandbox

  private List<Type> determinedImportTypes = new LinkedList<>();
  
  public SandboxExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type returnType, List<Type> importTypes,
      List<DeclarationStatement> args, List<Expression> importFunctions, List<SandboxParam> params,
      BlockStatement body, CatchExcSection catchSection) {
    super(lineNum, columnNum, AstType.SANDBOX_EXPRESSION, preComments, postComments);
    this.returnType = returnType;
    this.importTypes = importTypes;
    this.args = args;
    this.params = params;
    this.importFunctions = importFunctions;
    this.body = body;
    this.catchSection = catchSection;

    for (Expression fname : importFunctions) {
      if (!(fname instanceof IdentifierExpression || fname instanceof DotExpression)) {
        throw new IllegalArgumentException();
      }
    }
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Renamings scopedRenamings = renamings.extend();

    List<Type> newImportTypes = new LinkedList<>();
    for (Type ty : importTypes) {
      newImportTypes.add(ty.renameIds(scopedRenamings, msgState));
    }

    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      newArgs.add((DeclarationStatement) arg.renameIds(scopedRenamings, msgState));
    }

    List<Expression> newImportFunctions = new LinkedList<>();
    for (Expression fname : importFunctions) {
      newImportFunctions.add(fname.renameIds(scopedRenamings, msgState));
    }

    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      newParams.add(param.renameIds(renamings, msgState));
    }

    BlockStatement newBody = (BlockStatement) body.renameIds(scopedRenamings, msgState);

    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType.renameIds(scopedRenamings, msgState), newImportTypes, newArgs,
        newImportFunctions, newParams, newBody,
        (CatchExcSection) catchSection.renameIds(scopedRenamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      Optional<Statement> tmp = arg.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newArgs.add((DeclarationStatement) tmp.get());
    }
    Optional<Nullable<Type>> returnTypeOpt = returnType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!returnTypeOpt.isPresent()) {
      return Optional.empty();
    }

    List<Type> newImportTypes = new LinkedList<>();
    for (Type ty : importTypes) {
      newImportTypes.add(ty
          .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get());
    }

    List<Expression> newImportFunctions = new LinkedList<>();
    for (Expression fname : importFunctions) {
      newImportFunctions.add(fname
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get());
    }

    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      Optional<SandboxParam> tmp = param.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      newParams.add(tmp.get());
    }

    Optional<Statement> bodyOpt = body.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!bodyOpt.isPresent()) {
      return Optional.empty();
    }

    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnTypeOpt.get().get(), newImportTypes, newArgs, newImportFunctions, newParams,
        (BlockStatement) bodyOpt.get(),
        (CatchExcSection) catchSection
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    Optional<Nullable<Type>> detTyOpt = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!detTyOpt.isPresent()) {
      return Optional.empty();
    }
    value.determinedType = detTyOpt.get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public String toString(String indent) {
    String result = "sandbox (";
    boolean first = true;
    for (DeclarationStatement arg : args) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += arg;
    }
    return result + ") " + body.toString(indent) + " " + catchSection.toString(indent);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {

    /*
     * Ensure no illegal import types
     */
    List<Type> normalizedImportTypes = new LinkedList<>();
    boolean hitErr = false;
    for (Type type : importTypes) {
      if (!(type instanceof UserDeclType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Can only import modules and class declaration types into sandbox, not: " + type,
            lineNum, columnNum);
        hitErr = true;
      }
      normalizedImportTypes.add(type.normalize(tenv, lineNum, columnNum).get());
    }
    determinedImportTypes = normalizedImportTypes;

    /*
     * Ensure no duplicate params (e.g. can't specify timeout twice)
     */
    Set<ParamType> foundParamTypes = new HashSet<>();
    for (SandboxParam param : params) {
      if (foundParamTypes.contains(param.getParamType())) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot have duplicate sandbox params for: " + param.getParamType(), lineNum,
            columnNum);
        hitErr = true;
        continue;
      }
      foundParamTypes.add(param.getParamType());
    }

    /*
     * Typecheck params
     */
    for (SandboxParam param : params) {
      Optional<Type> tmp = param.typecheck(tenv);
      if (!tmp.isPresent()) {
        hitErr = true;
      }
    }

    /*
     * Install new top environment (no imports, globals, or locals. Only the sandbox
     * args)
     */
    TypeEnvironment newTenv = TypeEnvironment.createEmptyUserTypeEnv(tenv.project, tenv.ofDeclType,
        tenv.typeGraph, tenv.effectiveImports, tenv.msgState);

    // check args for duplicate names
    for (DeclarationStatement pname : args) {
      for (DeclarationStatement cmp : args) {
        if (pname == cmp) {
          continue;
        }
        if (pname.name.equals(cmp.name)) {
          tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
              "duplicate parameter names [" + pname.name + "] in sandbox", lineNum, columnNum);
          hitErr = true;
          break;
        }
      }
    }

    /*
     * Add the types to the tenv
     * 
     * Also add all the functions within these class types to the callgraph as being sandboxed
     * 
     * Class types may contain other classes, so involve those as well if they are exposed
     * publicly via field or function return
     */
    Set<UserDeclType> sandboxedClassTypes = new HashSet<>();
    for (Type type : importTypes) {
      sandboxedClassTypes.addAll(type.getUserDeclTypes(false));
    }
    for (DeclarationStatement arg : args) {
      sandboxedClassTypes.addAll(arg.type.getUserDeclTypes(false));
    }
    sandboxedClassTypes.addAll(returnType.getUserDeclTypes(false));
    
    // now check class graph to get all exposed types recursively
    
    Set<UserDeclType> allSandboxedClassTypes = new HashSet<>();
    Set<Visibility> filter = new HashSet<>();
    filter.add(Visibility.PUBLIC);
    
    Queue<UserDeclType> classTypeQueue = new LinkedList<>();
    classTypeQueue.addAll(sandboxedClassTypes);
    Set<UserDeclType> visitedClassTypes = new HashSet<>();
    while (!classTypeQueue.isEmpty()) {
      // if queue is empty, stop
      // get next class type from the queue
      UserDeclType type = classTypeQueue.poll();
      
      // get all exposed class types from the class and push them onto the queue
      // add this class type to the callgraph as being sandboxed
      // add this class type to set of sandboxed class types
      Optional<UserDeclTypeBox> userTyBox = tenv.lookupUserDeclType(type, lineNum, columnNum);
      if (!userTyBox.isPresent()) {
        return Optional.empty();
      }
      PsuedoClassHeader pch = null;
      if (userTyBox.get() instanceof ClassTypeBox) {
        pch = ((ClassTypeBox) userTyBox.get()).classHeader;
      } else if (userTyBox.get() instanceof EnumTypeBox) {
        pch = ((EnumTypeBox) userTyBox.get()).enumHeader;
      } else {
        return Optional.empty();
      }
      
      // make sure the typename is absolute
      type = pch.getAbsName();
      
      // if class type already visited, skip it
      if (visitedClassTypes.contains(type)) {
        continue;
      }
      visitedClassTypes.add(type);
      
      if (pch instanceof ClassHeader) {
        ClassHeader ch = (ClassHeader) pch;
        //add all the functions in this class to the callgraph.
        //since every function adds itself to the callgraph anyway, this should just be like
        //marking said function as sandboxed (it may already be in the graph)
        ch.addToCallgraphAsSandboxed(tenv.callGraph);
        Set<UserDeclType> exposedTypes = ch.getExposedClassTypes(filter);
        classTypeQueue.addAll(exposedTypes);
        allSandboxedClassTypes.addAll(exposedTypes);
      } else {
        EnumHeader eh = (EnumHeader) pch;
        allSandboxedClassTypes.add(type);
      }
    }
    
    ModuleType withinModule = tenv.ofDeclType.asUserDeclType().getModuleType();
    for (UserDeclType type : allSandboxedClassTypes) {
      type.copyFromTenvIntoTenv(tenv, newTenv);
      if (type.getModuleType().equals(withinModule)) {
        // we also want the relative name
        UserDeclType relativeName = (UserDeclType) type.basicNormalize(true, withinModule,
            tenv.effectiveImports, tenv.msgState).get().get();
        relativeName.copyFromTenvIntoTenv(tenv, newTenv);
      }
    }

    /*
     * Add the sandbox args, but ensure they can't be used in sibling expressions
     */
    for (DeclarationStatement arg : args) {
      if (arg.getScoping() == VariableScoping.UNKNOWN) {
        arg.setScoping(VariableScoping.NORMAL);
      }
      Optional<TypeBox> optBox = arg.typecheckAsField(newTenv, tenv, expectedReturnType,
          Nullable.empty(), Nullable.empty());
      if (!optBox.isPresent()) {
        return Optional.empty();
      }
      TypeBox box = optBox.get();
      newTenv.defineVar(arg.name, box);
    }
    if (hitErr) {
      return Optional.empty();
    }
    
    /*
     * Add non-lambda functions to newTenv
     */
    for (Expression fname : importFunctions) {
      tenv.copyNonLambdaFromTenvIntoTenv(lineNum, columnNum, newTenv, fname);
    }

    /*
     * Now typecheck the body in sandbox mode. This means that all function calls
     * are followed and ensured that those functions are statically defined and
     * sandbox-safe. FFI is not allowed.
     */
    newTenv.activateSandboxMode(lineNum, columnNum);

    if (!body.typecheckAsFunctionBody(newTenv, returnType).isPresent()) {
      return Optional.empty();
    }

    determinedType = returnType.normalize(tenv, lineNum, columnNum).get();
    
    // Now typecheck the catch section, which is not in the sandbox, but does need access
    // to the resulting exception. This exception will be added to the tenv
    // in the catchSection.typecheck
    if (!catchSection.typecheck(tenv, expectedReturnType).isPresent()) {
      return Optional.empty();
    }
    
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.SandboxExpression.Builder subDoc = AstProto.SandboxExpression.newBuilder();

    for (Type ty : importTypes) {
      subDoc.addImportTypes(ty.serialize());
    }

    for (DeclarationStatement arg : args) {
      subDoc.addArgs(arg.serialize());
    }
    
    for (Expression fname : importFunctions) {
      subDoc.addImportFunctions(fname.serialize());
    }
    
    for (SandboxParam param : params) {
      subDoc.addParams(param.serialize());
    }

    subDoc.setReturnType(returnType.serialize());
    subDoc.setBody(body.serialize());
    subDoc.setCatchSection(catchSection.serialize());

    AstProto.Ast.Builder document = preSerialize();
    document.setSandboxExpression(subDoc);
    return document;
  }

  public static SandboxExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.SandboxExpression document) throws SerializationError {

    List<Type> importTypes = new LinkedList<>();
    for (AstProto.Type type : document.getImportTypesList()) {
      importTypes.add(AbstractType.deserialize(type));
    }

    List<DeclarationStatement> args = new LinkedList<>();
    for (AstProto.Ast arg : document.getArgsList()) {
      args.add((DeclarationStatement) AbstractStatement.deserialize(arg));
    }
    
    List<Expression> importFunctions = new LinkedList<>();
    for (AstProto.Ast fname : document.getImportFunctionsList()) {
      importFunctions.add(AbstractExpression.deserialize(fname));
    }

    List<SandboxParam> params = new LinkedList<>();
    for (AstProto.SandboxParam param : document.getParamsList()) {
      params.add(AbstractSandboxParam.deserialize(param));
    }

    return new SandboxExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getReturnType()), importTypes, args, importFunctions,
        params,
        (BlockStatement) AbstractStatement.deserialize(document.getBody()),
        (CatchExcSection) AbstractExpression.deserialize(document.getCatchSection()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = catchSection.findFreeVariables();
    freeVars.putAll(body.findFreeVariables());
    for (DeclarationStatement arg : args) {
      freeVars.remove(arg.name);
    }
    for (DeclarationStatement arg : args) {
      freeVars.putAll(arg.findFreeVariables());
    }
    for (SandboxParam param : params) {
      freeVars.putAll(param.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof SandboxExpression)) {
      return false;
    }
    SandboxExpression ourOther = (SandboxExpression) other;

    if (ourOther.args.size() != args.size()
        || ourOther.params.size() != params.size()
        || ourOther.importFunctions.size() != importFunctions.size()) {
      return false;
    }
    Iterator<DeclarationStatement> iterOtherA = ourOther.args.iterator();
    Iterator<DeclarationStatement> iterUsA = args.iterator();
    while (iterOtherA.hasNext() && iterUsA.hasNext()) {
      if (!iterOtherA.next().compare(iterUsA.next())) {
        return false;
      }
    }
    
    Iterator<Expression> iterOtherF = ourOther.importFunctions.iterator();
    Iterator<Expression> iterUsF = importFunctions.iterator();
    while (iterOtherF.hasNext() && iterUsF.hasNext()) {
      if (!iterOtherF.next().compare(iterUsF.next())) {
        return false;
      }
    }

    Iterator<SandboxParam> iterOtherP = ourOther.params.iterator();
    Iterator<SandboxParam> iterUsP = params.iterator();
    while (iterOtherP.hasNext() && iterUsP.hasNext()) {
      if (!iterOtherP.next().compare(iterUsP.next())) {
        return false;
      }
    }

    return ourOther.body.compare(body) && ourOther.catchSection.compare(catchSection);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      newArgs.add((DeclarationStatement) arg.replace(target, with));
    }
    
    List<Expression> newImportFunctions = new LinkedList<>();
    for (Expression fname : importFunctions) {
      newImportFunctions.add((Expression) fname.replace(target, with));
    }

    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      newParams.add(param.replace(target, with));
    }

    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType, importTypes, newArgs, newImportFunctions, newParams,
        (BlockStatement) body.replace(target, with),
        (CatchExcSection) catchSection.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (DeclarationStatement arg : args) {
      Nullable<Ast> tmp = arg.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    
    for (Expression fname : importFunctions) {
      Nullable<Ast> tmp = fname.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }

    for (SandboxParam param : params) {
      Nullable<Ast> tmp = param.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }

    Nullable<Ast> tmp = body.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }

    return catchSection.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      Pair<List<DeclarationStatement>, Ast> tmp = arg.toAnf(tuning);
      insert.addAll(tmp.a);
      newArgs.add((DeclarationStatement) tmp.b);
    }

    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      Pair<List<DeclarationStatement>, SandboxParam> tmp = param.toAnf(tuning);
      insert.addAll(tmp.a);
      newParams.add(tmp.b);
    }

    Pair<List<DeclarationStatement>, Ast> tmpPair = body.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final BlockStatement newBody = (BlockStatement) tmpPair.b;

    tmpPair = catchSection.toAnf(tuning);
    insert.addAll(tmpPair.a);
    final CatchExcSection newCatchSection = (CatchExcSection) tmpPair.b;

    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType, importTypes, newArgs, importFunctions, newParams, newBody, newCatchSection);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());

    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    List<Type> newImportTypes = new LinkedList<>();
    for (Type ty : importTypes) {
      newImportTypes.add(ty.replaceType(mapFromTo));
    }

    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      newArgs.add((DeclarationStatement) arg.replaceType(mapFromTo));
    }
    
    List<Expression> newImportFunctions = new LinkedList<>();
    for (Expression fname : importFunctions) {
      newImportFunctions.add(fname.replaceType(mapFromTo));
    }
    
    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      newParams.add(param.replaceType(mapFromTo));
    }
    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType.replaceType(mapFromTo), newImportTypes, newArgs, newImportFunctions, newParams,
        (BlockStatement) body.replaceType(mapFromTo),
        (CatchExcSection) catchSection.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      Optional<Ast> tmp = arg.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newArgs.add((DeclarationStatement) tmp.get());
    }

    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      Optional<SandboxParam> tmp = param.replaceVtableAccess(project, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newParams.add(tmp.get());
    }

    Optional<Ast> tmpBody = body.replaceVtableAccess(project, msgState);
    if (!tmpBody.isPresent()) {
      return Optional.empty();
    }

    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType, importTypes, newArgs, importFunctions, newParams,
        (BlockStatement) tmpBody.get(),
        (CatchExcSection) catchSection.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (returnType.containsType(types)) {
      return true;
    }

    for (Type type : importTypes) {
      if (type.containsType(types)) {
        return true;
      }
    }

    for (DeclarationStatement arg : args) {
      if (arg.containsType(types)) {
        return true;
      }
    }
    for (Expression fname : importFunctions) {
      if (fname.containsType(types)) {
        return true;
      }
    }
    for (SandboxParam param : params) {
      if (param.containsType(types)) {
        return true;
      }
    }
    return body.containsType(types) || catchSection.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (DeclarationStatement arg : args) {
      if (arg.containsAst(asts)) {
        return true;
      }
    }
    for (Expression fname : importFunctions) {
      if (fname.containsAst(asts)) {
        return true;
      }
    }
    for (SandboxParam param : params) {
      if (param.containsAst(asts)) {
        return true;
      }
    }
    return body.containsAst(asts) || catchSection.containsAst(asts);
  }

  /*
   * will return single-use lambda
   * 
   * sandbox int (int x = 10, int y = 11) { //foo }
   * 
   * Becomes (any exceptions must be bubbled up)
   * 
   * FunctionCtx tmp_fn_ctx_0 = *fn_ctx; fun int (FunctionContext* fn_ctx, int x,
   * int y) { //foo }(&tmp_fn_ctx, 10, 11);
   */
  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();
    boolean alreadyInSandboxMode = enableResolution;
    
    if (alreadyInSandboxMode) {
      msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
          "Cannot have sandbox within sandboxed function/expression", lineNum, columnNum);
      return Optional.empty();
    }
    enableResolution = true; // we are now in sandbox mode for sure

    final String sandboxLambdaFnCtx = "tmp_" + JsmntGlobal.fnCtxId + "_" + getTemp();
    List<Expression> sandboxLambdaCallArgs = new LinkedList<>();
    sandboxLambdaCallArgs
        .add(new ReferenceExpression(new IdentifierExpression(sandboxLambdaFnCtx)));

    List<DeclarationStatement> sandboxLambdaParams = new LinkedList<>();

    sandboxLambdaParams.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, JsmntGlobal.fnCtxId,
        Nullable.empty(), false, false, 8));
    for (DeclarationStatement arg : args) {
      Optional<ResolveExcRet> tmp = arg.resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, false, enableResolution, "", stackSize,
          msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      DeclarationStatement newArg = (DeclarationStatement) tmp.get().ast.get();
      sandboxLambdaCallArgs.add(newArg.getValue().get());
      sandboxLambdaParams.add(
          new DeclarationStatement(newArg.type, newArg.name, Nullable.empty(), false, false, 0));
    }
    Optional<ResolveExcRet> tmpBody = body.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, false, enableResolution, "", stackSize,
        msgState);
    if (!tmpBody.isPresent()) {
      return Optional.empty();
    }
    final BlockStatement newBody = (BlockStatement) tmpBody.get().ast.get();

    // FunctionContext tmp_fn_ctx = FunctionContext();
    // tmp_fn_ctx.memtracker = new Memtracker(20);
    List<Statement> initFnCtxStatements = new LinkedList<>();
    // add the declaration line
    FunctionCallExpression tmpConstructorCall = new FunctionCallExpression(Nullable.empty(),
        new IdentifierExpression(jsmntGlobalRenamings.defaultCtxConstructor),
        new LinkedList<>(), "constructor");
    NewClassInstanceExpression newFnCtx = new NewClassInstanceExpression(
        JsmntGlobal.fnCtxClassType, JsmntGlobal.fnCtxClassType.toString(), tmpConstructorCall);
    initFnCtxStatements.add(new DeclarationStatement(JsmntGlobal.fnCtxClassType, sandboxLambdaFnCtx,
        Nullable.of(newFnCtx), false, false, JsmntGlobal.fnCtxClassTypeSizeof));
    
    // iterate through sandbox params, adding field assignments
    for (SandboxParam param : params) {
      initFnCtxStatements.addAll(param.createAssignmentLines(sandboxLambdaFnCtx,
          jsmntGlobalRenamings));
    }
    preStatements.addAll(initFnCtxStatements);

    LambdaExpression sandboxLambda = new LambdaExpression(returnType, sandboxLambdaParams, newBody);

    FunctionCallExpression sandboxLambdaCall = new FunctionCallExpression(Nullable.empty(),
        sandboxLambda, sandboxLambdaCallArgs, "");
    String tmpSandboxVal = "tmp_sandbox_val_" + label;
    DeclarationStatement saveSandboxValue = new DeclarationStatement(
        returnType, tmpSandboxVal,
        Nullable.of(sandboxLambdaCall), false, false,
        stackSize);
    preStatements.add(saveSandboxValue);
    
    //now handle the catchSection and add to preStatements. The saved tmpSandboxVal is returned
    //after this:
    // if (tmp_fn_ctx.exc != null) {
    //   jsmnt_global.Exception* e = tmp_fn_ctx.exc;
    //   <catch body>
    // }
    Optional<ResolveExcRet> resolvedCatchSection = catchSection.resolveExceptions(project,
        currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, sandboxLambdaFnCtx, stackSize, msgState);
    if (!resolvedCatchSection.isPresent()) {
      return Optional.empty();
    }
    if (!resolvedCatchSection.get().preStatements.isEmpty()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.SANDBOX,
          "catch section of sandbox should only return one if-statement");
    }
    preStatements.add((ConditionalStatement) resolvedCatchSection.get().ast.get());
    
    return Optional.of(new ResolveExcRet(preStatements, new IdentifierExpression(tmpSandboxVal)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    for (Type type : importTypes) {
      type.addDependency(modDepGraph, ourModNode, tenv, lineNum, columnNum);
    }

    for (DeclarationStatement arg : args) {
      arg.addDependency(modDepGraph, ourModNode, tenv);
    }
    body.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.FUNCTION);

    SandboxExpression val = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType, importTypes, args, importFunctions, params,
        (BlockStatement) body.insertDestructorCalls(ourScope, destructorRenamings).statements
            .get(0),
        (CatchExcSection) catchSection.insertDestructorCalls(ourScope, destructorRenamings));
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    // sandbox needs to know what new classes to import
    List<ClassDeclaration> scopedLiftedClasses = new LinkedList<>();
    
    Nullable<Statement> newBody = body.liftClassDeclarations(scopedLiftedClasses, replacements);
    if (newBody.isNull()) {
      replacements.msgState.addMessage(MsgType.INTERNAL, MsgClass.LIFT_CLASS_DECLARATIONS,
          "new body is null", lineNum, columnNum);
    }
    
    List<Type> newImportTypes = new LinkedList<>(importTypes);
    for (ClassDeclaration cdecl : scopedLiftedClasses) {
      newImportTypes.add(cdecl.fullType.asClassDeclType());
    }
    
    liftedClasses.addAll(scopedLiftedClasses);
    
    SandboxExpression val = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType, newImportTypes, args, importFunctions, params,
        (BlockStatement) newBody.get(),
        catchSection);
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      newArgs.add((DeclarationStatement) arg.resolveUserTypes(msgState).get());
    }
    
    List<Expression> newImportFunctions = new LinkedList<>();
    for (Expression fname : importFunctions) {
      newImportFunctions.add(fname.resolveUserTypes(msgState).get());
    }
    
    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      newParams.add(param.resolveUserType(msgState).get());
    }
    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        determinedType, determinedImportTypes, newArgs, newImportFunctions, newParams,
        (BlockStatement) body.resolveUserTypes(msgState).get(),
        (CatchExcSection) catchSection.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    List<DeclarationStatement> newArgs = new LinkedList<>();
    for (DeclarationStatement arg : args) {
      newArgs.add((DeclarationStatement) arg.addThisExpression(classHeader));
    }
    
    List<Expression> newImportFunctions = new LinkedList<>();
    for (Expression fname : importFunctions) {
      newImportFunctions.add(fname.addThisExpression(classHeader));
    }
    
    List<SandboxParam> newParams = new LinkedList<>();
    for (SandboxParam param : params) {
      newParams.add(param.addThisExpression(classHeader));
    }
    SandboxExpression value = new SandboxExpression(lineNum, columnNum, preComments, postComments,
        returnType, importTypes, newArgs, newImportFunctions, newParams,
        (BlockStatement) body.addThisExpression(classHeader),
        (CatchExcSection) catchSection.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
