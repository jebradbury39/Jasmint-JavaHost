package ast;

import ast.ClassDeclarationSection.Visibility;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.TypeStatus;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.ClassDeclType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.GenericType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UndeterminedType;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class DeclarationStatement extends AbstractStatement implements Declaration {
  public final Type type;
  private Type determinedTypeType = UndeterminedType.Create(); // abs path for this.type

  public final String name;
  private Nullable<Expression> value;
  private boolean isStatic; /* only used within class declarations */
  public final boolean required; // if true, 'value' will be null until linked

  /* only determined by typechecker (set before typecheck() call) */
  private VariableScoping scoping = VariableScoping.UNKNOWN;
  private int calculatedSizeof = -1; // determine during typecheck and store. Then use for memcheck

  public Type getAbsType() {
    return determinedTypeType;
  }

  public int getCalculatedSizeof() {
    return calculatedSizeof;
  }

  public Nullable<Expression> getValue() {
    return value;
  }

  public boolean getIsStatic() {
    return isStatic;
  }

  public void setIsStatic(boolean s) {
    isStatic = s;
  }

  public DeclarationStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type type, String name, Nullable<Expression> value,
      boolean isStatic, boolean required) {
    super(lineNum, columnNum, AstType.DECLARATION_STATEMENT, preComments, postComments);

    this.type = type;
    this.name = name;
    this.value = value;
    this.isStatic = isStatic;
    this.required = required;
    if (type == null || type instanceof UndeterminedType) {
      throw new IllegalArgumentException();
    }
    if (value.isNotNull()) {
      value.get().setParentAst(this);
    }
    determinedType = VoidType.Create();
  }

  public DeclarationStatement(int lineNum, int columnNum, Type type, String name,
      Nullable<Expression> value, boolean isStatic, boolean required) {
    super(lineNum, columnNum, AstType.DECLARATION_STATEMENT);

    this.type = type;
    this.name = name;
    this.value = value;
    this.isStatic = isStatic;
    this.required = required;
    if (type == null || type instanceof UndeterminedType) {
      throw new IllegalArgumentException();
    }
    if (value.isNotNull()) {
      value.get().setParentAst(this);
    }
    determinedType = VoidType.Create();
  }

  public DeclarationStatement(Type type, String name, Nullable<Expression> value, boolean isStatic,
      boolean required, int calculatedSizeof) {
    super(AstType.DECLARATION_STATEMENT);

    this.type = type;
    this.name = name;
    this.value = value;
    this.isStatic = isStatic;
    this.required = required;
    this.calculatedSizeof = calculatedSizeof;
    if (type == null || type instanceof UndeterminedType) {
      throw new IllegalArgumentException();
    }
    if (value.isNotNull()) {
      value.get().setParentAst(this);
    }
    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = preToString();

    if (required) {
      result += "req ";
    }

    if (isStatic) {
      result += "static ";
    }

    result += type + " " + name;

    if (value.isNotNull()) {
      result += " = " + value.get().toString(indent);
    }

    return result + postToString();
  }

  @Override
  public void setScoping(VariableScoping scoping) {
    this.scoping = scoping;
  }

  @Override
  public VariableScoping getScoping() {
    return scoping;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    if (!validateName(tenv)) {
      return Optional.empty();
    }
    Optional<TypeBox> optBox = typecheckAsField(tenv, tenv, expectedReturnType,
        Nullable.empty(), Nullable.empty());
    if (!optBox.isPresent()) {
      return Optional.empty();
    }
    TypeBox box = optBox.get();
    tenv.defineVar(name, box);
    return Optional.of(new TypecheckRet(determinedType));
  }

  public boolean validateName(TypeEnvironment tenv) throws FatalMessageException {
    // We cannot create a variable with the same name as a type (just bad coding,
    // since one instance
    // does not represent the entire scope of the type)
    Nullable<TypeBox> tyBox = tenv.lookup(name, lineNum, columnNum);
    if (tyBox.isNull()) {
      return true;
    }
    if (tyBox.get().getType() instanceof UserDeclType) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot shadow user declaration: " + name, lineNum, columnNum);
      return false;
    }
    /*
     * We cannot shadow a function declaration either (important we can look at an id and know if
     * it refers to a non-lambda)
     */
    if (tyBox.get().getType() instanceof FunctionType) {
      FunctionType fnType = (FunctionType) tyBox.get().getType();
      if (fnType.lambdaStatus == LambdaStatus.NOT_LAMBDA) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot shadow function declaration: " + name, lineNum, columnNum);
        return false;
      }
    }
    
    return true;
  }

  public Optional<TypeBox> typecheckAsField(TypeEnvironment tenv, TypeEnvironment valueTenv,
      Nullable<Type> expectedReturnType,
      Nullable<DotAccessType> definedInClass, Nullable<Visibility> visibility)
      throws FatalMessageException {

    if (scoping == VariableScoping.UNKNOWN) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "scoping undetermined for Declaration", lineNum, columnNum);
      return Optional.empty();
    }

    // first lookup the type (outer type and name) to see if it is complete and/or
    // real
    Optional<TypeStatus> optTyStatus = valueTenv.lookupType(type, lineNum, columnNum);
    if (!optTyStatus.isPresent()) {
      return Optional.empty();
    }
    TypeStatus tyStatus = optTyStatus.get();
    if (tyStatus == TypeStatus.UNDEFINED) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot declare [" + name + "] of type [" + type + "] (this type is undefined)", lineNum,
          columnNum);
      return Optional.empty();
    }
    if (isStatic && tyStatus.isGeneric()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "The static variable [" + name + "] cannot have a generic type: " + type, lineNum,
          columnNum);
      return Optional.empty();
    }

    // determine abs type
    Optional<Type> tmpAbsType = type.normalize(valueTenv, lineNum, columnNum);
    if (!tmpAbsType.isPresent()) {
      return Optional.empty();
    }
    determinedTypeType = tmpAbsType.get();

    if (value.isNotNull()) {
      Optional<TypecheckRet> valueType = value.get().typecheck(valueTenv, expectedReturnType);
      if (!valueType.isPresent()) {
        return Optional.empty();
      }
      if (!tenv.canSafeCast(valueType.get().type, determinedTypeType)) {
        tenv.msgState.addMessage(
            MsgType.ERROR, MsgClass.TYPECHECK, "declaration assignment value "
                + valueType.get().type + " cannot cast up to declared type "
                + determinedTypeType + "",
            lineNum, columnNum);
        return Optional.empty();
      }
    }

    if (type instanceof UserInstanceType) {
      // ((UserType) type).checkIfNonGeneric(tenv, lineNum, columnNum);
    }

    // calculate and store sizeof type
    if (!(type instanceof GenericType)) {
      calculatedSizeof = type.calculateSize(tenv, lineNum, columnNum);
      if (calculatedSizeof == -1) {
        tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "Failed to calculate size of decl type: " + type, lineNum, columnNum);
      }
    }

    boolean isInit = (value != null || required);
    return Optional.of(new BasicTypeBox(determinedTypeType, true, isInit, scoping,
        definedInClass, visibility, isStatic));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value initValue = null;
    if (value.isNotNull()) {
      initValue = value.get().interp(env).copy(null);
    }
    env.defineVar(name, scoping, initValue, false);
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.DeclarationStatement.Builder subDoc = AstProto.DeclarationStatement.newBuilder();

    subDoc.setType(type.serialize().build());
    subDoc.setName(name);
    if (value.isNotNull()) {
      subDoc.addValue(value.get().serialize().build());
    }
    
    subDoc.setScoping(scoping.serialize());
    subDoc.setIsStatic(isStatic);
    subDoc.setRequired(required);

    document.setDeclarationStatement(subDoc);
    return document;
  }

  public static DeclarationStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.DeclarationStatement document) throws SerializationError {
    Nullable<Expression> value = Nullable.empty();
    if (document.getValueCount() > 0) {
      value = Nullable.of(AbstractExpression.deserialize(document.getValue(0)));
    }
    DeclarationStatement result = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, AbstractType.deserialize(document.getType()), document.getName(), value,
        document.getIsStatic(), document.getRequired());

    result.setScoping(VariableScoping.deserialize(document.getScoping()));

    return result;
  }

  public static DeclarationStatement deserialize(AstProto.Ast document) throws SerializationError {
    int dLineNum = document.getLineNum();
    int dColumnNum = document.getColNum();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    AbstractAst.preDeserialize(document, preComments, postComments);
    Type dDeterminedType = AbstractType.deserialize(document.getDeterminedType());

    DeclarationStatement value = deserialize(dLineNum, dColumnNum, preComments, postComments,
        document.getDeclarationStatement());

    value.determinedType = dDeterminedType;
    return value;
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    return renameIdsCustom(renamings, Nullable.empty(), msgState);
  }

  public Statement renameIdsCustom(Renamings renamings, Nullable<ClassDeclType> withinClass,
      MsgState msgState) throws FatalMessageException {
    Nullable<Expression> newVal = value;
    if (value.isNotNull()) {
      newVal = Nullable.of(value.get().renameIds(renamings, msgState));
    }

    // do an automatic rename to get rid of shadowing
    Nullable<String> newId = Nullable.empty();
    if (withinClass.isNotNull()) {
      // class already did the renaming
      newId = renamings.lookup(name, type, Nullable.of((UserDeclType) withinClass.get()));
      if (newId.isNull()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.AST,
            "Unable to find: " + name + " in renamings: " + renamings);
      }
    } else {
      newId = Nullable.of(renamings.add(name, determinedTypeType, Nullable.empty(), label));
    }

    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type.renameIds(renamings, msgState), newId.get(), newVal, isStatic, required);
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    val.calculatedSizeof = calculatedSizeof;
    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<>();
    if (value.isNotNull()) {
      freeVars.putAll(value.get().findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof DeclarationStatement)) {
      return false;
    }
    DeclarationStatement ourOther = (DeclarationStatement) other;
    if (!ourOther.type.equals(type)) {
      return false;
    }
    if (!ourOther.name.equals(name)) {
      return false;
    }
    if ((ourOther.value.isNull() && value.isNotNull())
        || (ourOther.value.isNotNull() && value.isNull())) {
      return false;
    }
    if (value.isNotNull()) {
      return ourOther.value.get().compare(value.get());
    }
    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type, name, this.value.isNull() ? Nullable.empty()
            : Nullable.of((Expression) this.value.get().replace(target, with)),
        isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    if (value.isNotNull()) {
      return value.get().findByOriginalLabel(findLabel);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    if (value.isNotNull()) {
      Pair<List<DeclarationStatement>, Ast> valuePair = value.get().toAnf(tuning);
      insert.addAll(valuePair.a);
      DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
          postComments, type, name, Nullable.of((Expression) valuePair.b), isStatic, required);
      val.determinedType = determinedType;
      val.setOriginalLabel(getOriginalLabel());
      val.calculatedSizeof = calculatedSizeof;
      return new Pair<List<DeclarationStatement>, Ast>(insert, val);
    }
    return new Pair<List<DeclarationStatement>, Ast>(insert, this);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    Nullable<Expression> newExpr = Nullable.empty();
    if (value.isNotNull()) {
      newExpr = Nullable.of((Expression) this.value.get().replaceType(mapFromTo));
    }
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type.replaceType(mapFromTo), name, newExpr, isStatic, required);
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return val;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    Nullable<Expression> newValue = this.value;
    if (this.value.isNotNull()) {
      Optional<Nullable<Expression>> tmp = this.value.get().normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newValue = tmp.get();
    }

    Optional<Nullable<Type>> tmpType = type.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpType.isPresent()) {
      return Optional.empty();
    }
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, tmpType.get().get(), name,
        newValue,
        isStatic, required);
    Optional<Nullable<Type>> tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    val.determinedType = tmp.get().get();
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return Optional.of(val);
  }

  public DeclarationStatement removeValue() {
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type, name, Nullable.empty(), isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Nullable<Optional<Ast>> tmpVal = Nullable.empty();
    if (this.value.isNotNull()) {
      tmpVal = Nullable.of(this.value.get().replaceVtableAccess(project, msgState));
    }
    if (tmpVal.isNotNull() && !tmpVal.get().isPresent()) {
      return Optional.empty();
    }
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type, name,
        this.value.isNull() ? Nullable.empty() : Nullable.of((Expression) tmpVal.get().get()),
        isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    type.addDependency(modDepGraph, ourModNode, tenv, false, lineNum, columnNum);
    if (value.isNotNull()) {
      value.get().addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  // Only a decl from a provider module calls this
  public boolean linkRequiredStaticFields(ClassDeclarationSection section,
      ClassDeclaration reqClassAst, MsgState msgState) throws FatalMessageException {
    Nullable<DeclarationStatement> reqDeclAst = reqClassAst.lookupDeclaration(name);
    if (reqDeclAst.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Unable to find required declaration:" + this, lineNum, columnNum);
      return false;
    }

    DeclarationStatement reqDecl = reqDeclAst.get();
    if (!reqDecl.required) {
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Cannot provide ffi for a non-req declaration: " + this, lineNum, columnNum);
      return false;
    }
    // finish linking
    reqDecl.value = value;
    reqDecl.calculatedSizeof = calculatedSizeof;
    return true;
  }

  public boolean findUnlinkedReqStatic(MsgState msgState) throws FatalMessageException {
    if (required && value.isNull()) {
      msgState.addMessage(MsgType.ERROR, MsgClass.LINK_REQ_PROV_FN,
          "Failed to link required static field: " + this, lineNum, columnNum);
      return true;
    }
    return false;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (type.containsType(types)) {
      return true;
    }
    if (value.isNotNull()) {
      if (value.get().containsType(types)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    if (value.isNotNull()) {
      if (value.get().containsAst(asts)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    List<Statement> preStatements = new LinkedList<>();

    Nullable<Expression> newValue = value;
    if (value.isNotNull()) {
      Optional<ResolveExcRet> tmp = value.get().resolveExceptions(project,
          currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
          enableResolution,
          hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newValue = Nullable.of((Expression) tmp.get().ast.get());
    } else {
      if (enableResolution) {
        newValue = Nullable.of(type.getDummyExpression(project, msgState));
      }
    }

    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type, name, newValue, isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    
    Nullable<Expression> newValue = value;
    if (value.isNull()) {
      // insert default expression
      newValue = Nullable.of(type.getDummyExpression(destructorRenamings.project,
          destructorRenamings.msgState));
    } else {
      newValue = Nullable.of(value.get().insertDestructorCalls(scopedIds, destructorRenamings));
    }
    
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type, name,
        newValue,
        isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    scopedIds.add(name, type);
    return new InsertDestructorsRet(val);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments, type, name,
        value.isNull() ? value
            : Nullable.of(value.get().liftClassDeclarations(liftedClasses, replacements).get()),
        isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return Nullable.of(val);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    Nullable<Expression> newValue = value;
    if (value.isNotNull()) {
      newValue = Nullable.of(value.get().resolveUserTypes(msgState).get());
    }
    
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments,
        determinedTypeType instanceof UndeterminedType ? type : determinedTypeType, name,
        newValue,
        isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return Optional.of(val);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    if (isStatic) {
      return this;
    }
    Nullable<Expression> newValue = value;
    if (value.isNotNull()) {
      newValue = Nullable.of(value.get().addThisExpression(classHeader));
    }
    
    DeclarationStatement val = new DeclarationStatement(lineNum, columnNum, preComments,
        postComments,
        type, name,
        newValue,
        isStatic, required);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    val.scoping = scoping;
    val.calculatedSizeof = calculatedSizeof;
    return val;
  }
}
