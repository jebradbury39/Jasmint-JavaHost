package ast;

import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import typecheck.AnyType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class LvalueFfi extends AbstractLvalue {

  public final StringExpression literal;

  public LvalueFfi(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, StringExpression literal) {
    super(lineNum, columnNum, AbstractAst.AstType.FFI_LVALUE, preComments, postComments);
    this.literal = literal;

    determinedType = AnyType.Create();
  }

  @Override
  public Expression toExpression() {
    return new FfiExpression(lineNum, columnNum, preComments, postComments, literal);
  }

  @Override
  public Lvalue renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Lvalue normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) {
    return this;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "@" + literal + "@" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Nullable<SandboxModeInfo> sandboxModeInfo = tenv.inSandboxMode();
    if (sandboxModeInfo.isNotNull()) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
          "FFI lvalues are not allowed in sandbox mode (" + sandboxModeInfo + ")", lineNum,
          columnNum);
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    throw new InterpError("Cannot interp ffi", lineNum, columnNum);
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LvalueFfi.Builder subDoc = AstProto.LvalueFfi.newBuilder();

    subDoc.setLiteral(literal.serialize());

    document.setLvalueFfi(subDoc);
    return document;
  }

  public static LvalueFfi deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.LvalueFfi document) throws SerializationError {
    return new LvalueFfi(lineNum, columnNum, preComments, postComments,
        (StringExpression) AbstractExpression.deserialize(document.getLiteral()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<>();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LvalueFfi)) {
      return false;
    }
    return ((LvalueFfi) other).literal.compare(literal);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<>(new LinkedList<>(), this);
  }

  @Override
  public Lvalue replaceType(Map<Type, Type> mapFromTo) {
    return this;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Lvalue addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
