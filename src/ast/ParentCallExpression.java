package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditRenameIds;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.AbstractValue.MetaValue;
import interp.ClosureValue;
import interp.Environment;
import interp.InterpError;
import interp.UserTypeDeclarationValue;
import interp.Value;
import interp.ValueBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.ClassType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.Pair;
import util.ScopedIdentifiers;

public class ParentCallExpression extends AbstractExpression {

  public static enum IdType {
    CONSTRUCTOR("constructor"), DESTRUCTOR("destructor");

    public final String idTypeName;

    private IdType(String name) {
      this.idTypeName = name;
    }
  }

  private static final Map<String, IdType> STR_TO_IDTYPE = new HashMap<String, IdType>();

  static {
    for (IdType b : IdType.values()) {
      STR_TO_IDTYPE.put(b.idTypeName, b);
    }
  }

  // basically just the "parent" id, but avoid special cases in
  // IdentifierExpression
  private Nullable<ClassType> parentType; // must be abs path
  public final IdType id;

  public ParentCallExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Nullable<ClassType> parentType, IdType id) {
    super(lineNum, columnNum, AstType.PARENT_CALL_EXPRESSION, preComments, postComments);
    this.parentType = parentType;
    this.id = id;
  }

  public ParentCallExpression(Nullable<ClassType> parentType, IdType id) {
    super(AstType.PARENT_CALL_EXPRESSION);
    this.parentType = parentType;
    this.id = id;
  }
  
  public Nullable<ClassType> getParentType() {
    return parentType;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    // lookup parentType renamings
    Nullable<ModuleEndpoint> parentModEp = renamings.project
        .lookupModule(parentType.get().outerType.get(), renamings.project.getAuditTrail());
    if (parentModEp.isNull()) {

    }
    parentModEp.get().load();
    LoadedModuleHandle parentModHandle = (LoadedModuleHandle) parentModEp.get().getHandle();
    Renamings parentModRenamings = ((AuditRenameIds) parentModHandle.moduleBuildObj.auditTrail
        .getLast()).renamings;

    // convert from 'constructor/destructor' to the right name to match our
    // determinedType
    Nullable<String> newId = parentModRenamings.lookup(id.toString(), (FunctionType) determinedType,
        Nullable.of((UserInstanceType) parentType.get()));
    if (newId.isNotNull()) {
      // if a class field, or left is class
      // (so renaming in a class context) find out what class and check the vtable

      IdentifierExpression value = new IdentifierExpression(lineNum, columnNum, preComments,
          postComments, newId.get());
      value.determinedType = determinedType.renameIds(renamings, msgState);
      value.setOriginalLabel(getOriginalLabel());
      return value;
    }

    return this;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "parent" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
        "internal: Cannot call this function (do not directly typecheck 'parent')", lineNum,
        columnNum);
    return Optional.empty();
  }

  // should not run after renaming (should have been replaced by
  public TypeBox typecheckAsFunction(TypeEnvironment argsTenv, TypeEnvironment bodyTenv,
      List<Type> argTypes) throws FatalMessageException {
    if (parentType.isNull()) {
      argsTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Cannot call parent constructor since there is no parent class", lineNum, columnNum);
    }
    
    parentType = Nullable.of((ClassType) parentType.get().normalize(argsTenv, lineNum, columnNum).get());

    Nullable<TypeBox> match = Nullable.empty();
    FunctionType queryFnTy = new FunctionType(parentType.get(), VoidType.Create(),
        argTypes, LambdaStatus.NOT_LAMBDA);
    switch (id) {
      case CONSTRUCTOR:
        match = bodyTenv.lookup(id.idTypeName, queryFnTy, lineNum, columnNum);
        break;
      case DESTRUCTOR:
        // find destructor (must at least be default)
        match = bodyTenv.lookup(id.idTypeName, queryFnTy, lineNum, columnNum);
        break;
      default:
        argsTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, "id cannot be: " + id,
            lineNum, columnNum);
    }

    // if none found, throw error
    if (match.isNull()) {
      argsTenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Unable to find matching " + id + " in class [" + parentType + "] with argument types: "
              + argTypes,
          lineNum, columnNum);
    }

    // otherwise, make that fn type determined and return it
    determinedType = match.get().getType();
    return match.get();
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    throw new InterpError("internal: Do not call parent interp directly", lineNum, columnNum);
  }

  public Value interpAsFunction(Environment bodyEnv, List<Value> argValues)
      throws InterpError, FatalMessageException {
    // Find parent declaration and INSTANCE
    ValueBox declBox = bodyEnv.lookupNestedClassname(parentType.get());
    if (declBox == null) {
      throw new InterpError(
          "parent class [" + parentType + "] is not defined. Cannot call parent constructor",
          lineNum, columnNum);
    }
    if (!(declBox.getValue() instanceof UserTypeDeclarationValue)) {
      throw new InterpError("parent constructor class [" + parentType
          + "] is not a class, but is actually: " + declBox.getValue(), lineNum, columnNum);
    }
    ClassDeclaration cdecl = ((UserTypeDeclarationValue) declBox.getValue()).declaration;

    List<Type> argTypes = new ArrayList<Type>();
    for (Value argVal : argValues) {
      argTypes.add(argVal.getType());
    }

    ValueBox match = null;

    // we need the environment for the declBox level, otherwise we will keep going
    // in circles
    Environment declEnv = bodyEnv.lookupParent(TypeEnvironment.ScopeType.INSTANCE,
        parentType.get());
    switch (id) {
      case CONSTRUCTOR:
        // iterate through constructors to find the matching one
        List<String> constructorNames = new LinkedList<String>();
        for (ClassDeclarationSection section : cdecl.sections) {
          for (Function fn : section.constructors) {
            constructorNames.add(fn.name);
          }

          match = declEnv.lookup(constructorNames,
              new FunctionType(Nullable.empty(), null, argTypes, LambdaStatus.NOT_LAMBDA));
        }
        break;
      case DESTRUCTOR:
        // find destructor (must have default)
        break;
      default:
        throw new InterpError("id must be either constructor or destructor, not: " + id, lineNum,
            columnNum);
    }

    // if none found, throw error
    if (match == null) {
      throw new InterpError("Unable to find matching constructor in class [" + parentType
          + "] with argument values: " + argValues, lineNum, columnNum);
    }

    // otherwise, execute and return
    Value exprValue = match.getValue();
    if (!(exprValue instanceof ClosureValue)) {
      throw new InterpError("expression did not evaluate to closure value", lineNum, columnNum);
    }

    ClosureValue fnVal = (ClosureValue) exprValue;
    return fnVal.interp(argValues).setMetaValue(MetaValue.NORMAL);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ParentCallExpression.Builder subDoc = AstProto.ParentCallExpression.newBuilder();

    if (parentType.isNotNull()) {
      subDoc.addParentType(parentType.get().serialize());
    }

    subDoc.setId(id.idTypeName);

    document.setParentCallExpression(subDoc);
    return document;
  }

  public static ParentCallExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.ParentCallExpression document) throws SerializationError {
    Nullable<ClassType> parentType = Nullable.empty();
    if (document.getParentTypeCount() > 0) {
      parentType = Nullable.of((ClassType) AbstractType.deserialize(document.getParentType(0)));
    }

    ParentCallExpression result = new ParentCallExpression(lineNum, columnNum, preComments,
        postComments, parentType, STR_TO_IDTYPE.get(document.getId()));
    return result;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    return other instanceof ParentCallExpression;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    ParentCallExpression val = new ParentCallExpression(lineNum, columnNum, preComments,
        postComments, Nullable.of((ClassType) parentType.get().replaceType(mapFromTo)), id);
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    ParentCallExpression val = new ParentCallExpression(lineNum, columnNum, preComments,
        postComments,
        Nullable.of((ClassType) parentType.get()
            .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get()),
        id);
    val.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    return this;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    return this;
  }

}
