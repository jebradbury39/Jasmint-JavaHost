package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.BoolValue;
import interp.Environment;
import interp.IntValue;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.AbstractType.TypeType;
import typecheck.AnyType;
import typecheck.BoolType;
import typecheck.Float64Type;
import typecheck.FunctionType;
import typecheck.IntType;
import typecheck.ModuleType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import typegraph.TypeGraph.CastDirection;
import util.Pair;
import util.ScopedIdentifiers;

public class BinaryExpression extends AbstractExpression {

  public static enum BinOp {
    ADD("+"), SUB("-"), MUL("*"), DIV("/"), LT("<"), GT(">"), LTE("<="), GTE(">="), EQUAL("=="),
    NOT_EQUAL("!="), AND("&&"), OR("||"), MODULO("%"), FFI_CONCAT("#");

    public final String binOpName;

    private BinOp(String name) {
      this.binOpName = name;
    }
  }

  private static final Map<String, BinOp> STR_TO_BINOP = new HashMap<String, BinOp>();

  static {
    for (BinOp b : BinOp.values()) {
      STR_TO_BINOP.put(b.binOpName, b);
    }
  }

  public final BinOp operator;
  public final Expression left;
  public final Expression right;

  public BinaryExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, BinOp operator, Expression left, Expression right) {
    super(lineNum, columnNum, AstType.BINARY_EXPRESSION, preComments, postComments);
    this.operator = operator;
    this.left = left;
    this.right = right;

    left.setParentAst(this);
    right.setParentAst(this);
  }

  public BinaryExpression(int lineNum, int columnNum, BinOp operator, Expression left, Expression right) {
    super(lineNum, columnNum, AstType.BINARY_EXPRESSION);
    this.operator = operator;
    this.left = left;
    this.right = right;

    left.setParentAst(this);
    right.setParentAst(this);
  }

  public BinaryExpression(BinOp operator, Expression left, Expression right) {
    super(AstType.BINARY_EXPRESSION);
    this.operator = operator;
    this.left = left;
    this.right = right;

    left.setParentAst(this);
    right.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + left.toString(indent) + " " + operator.binOpName + " "
        + right.toString(indent) + postToString();
  }

  public static BinOp stringToBinOp(String str) {
    BinOp op = STR_TO_BINOP.get(str);
    if (op == null) {
      throw new IllegalArgumentException("invalid binary op: " + str);
    }
    return op;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> optLeftType = left.typecheck(tenv, Nullable.empty());
    Optional<TypecheckRet> optRightType = right.typecheck(tenv, Nullable.empty());
    boolean error = false;

    if (!optLeftType.isPresent() || !optRightType.isPresent()) {
      return Optional.empty();
    }
    Type leftType = optLeftType.get().type;
    Type rightType = optRightType.get().type;

    if (leftType instanceof VoidType || rightType instanceof VoidType) {
      error = true;
    }

    switch (operator) {
      case ADD:
        if (tenv.typeGraph.canCastTo(leftType, CastDirection.DOWN, Float64Type.Create())
            && tenv.typeGraph.canCastTo(rightType, CastDirection.DOWN, Float64Type.Create())) {
          determinedType = AbstractType.makePromotion(leftType, rightType);
          break;
        }
        if (leftType.equals(StringType.Create()) || rightType.equals(StringType.Create())) {
          determinedType = StringType.Create();
          break;
        }
        error = true;
        break;
      case SUB:
      case MUL:
      case DIV:
        if (tenv.typeGraph.canCastTo(leftType, CastDirection.DOWN, Float64Type.Create())
            && tenv.typeGraph.canCastTo(rightType, CastDirection.DOWN, Float64Type.Create())) {
          determinedType = AbstractType.makePromotion(leftType, rightType);
          break;
        }
        error = true;
        break;
      case LT:
      case GT:
      case LTE:
      case GTE:
        if (leftType instanceof StringType && rightType instanceof StringType) {
          determinedType = BoolType.Create();
          break;
        }
        if (tenv.typeGraph.canCastTo(leftType, CastDirection.DOWN, Float64Type.Create())
            && tenv.typeGraph.canCastTo(rightType, CastDirection.DOWN, Float64Type.Create())) {
          determinedType = BoolType.Create();
          break;
        }
        error = true;
        break;
      case EQUAL:
      case NOT_EQUAL:
        if (tenv.typeGraph.canCastTo(leftType, CastDirection.DOWN, Float64Type.Create())
            && tenv.typeGraph.canCastTo(rightType, CastDirection.DOWN, Float64Type.Create())) {
          determinedType = BoolType.Create();
          break;
        }
        if (leftType.refEquals(rightType)) {
          determinedType = BoolType.Create();
          break;
        }
        error = true;
        break;
      case AND:
      case OR:
        if (tenv.typeGraph.canCastTo(leftType, CastDirection.UP, BoolType.Create())
            && tenv.typeGraph.canCastTo(rightType, CastDirection.UP, BoolType.Create())) {
          determinedType = BoolType.Create();
          break;
        }
        error = true;
        break;
      case MODULO:
        if (leftType.getTypeType() == TypeType.INT_TYPE
            && rightType.getTypeType() == TypeType.INT_TYPE) {
          determinedType = AbstractType.makePromotion(leftType, rightType);
          break;
        }
        error = true;
        break;
      case FFI_CONCAT:
        // skip typechecking this
        determinedType = AnyType.Create();
        break;
      default:
        error = true;
        break;
    }

    if (error) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Binary " + operator.binOpName + " failed on types " + leftType + " and " + rightType,
          lineNum, columnNum);
      return Optional.empty();
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value leftValue = left.interp(env);
    Value rightValue = right.interp(env);
    Value result = null;
    boolean error = false;

    if (leftValue instanceof VoidValue || rightValue instanceof VoidValue) {
      error = true;
    }

    switch (operator) {
      case ADD:
        result = leftValue.add(rightValue);
        break;
      case SUB:
        result = leftValue.sub(rightValue);
        break;
      case MUL:
        result = leftValue.mul(rightValue);
        break;
      case DIV:
        result = leftValue.div(rightValue);
        break;
      case EQUAL:
        result = new BoolValue(leftValue.equalsValue(rightValue));
        break;
      case NOT_EQUAL:
        result = new BoolValue(!leftValue.equalsValue(rightValue));
        break;
      case LT:
        result = new BoolValue(leftValue.lessThan(rightValue));
        break;
      case GT:
        result = new BoolValue(!leftValue.lessThan(rightValue) && !leftValue.equals(rightValue));
        break;
      case LTE:
        result = new BoolValue(leftValue.lessThan(rightValue) || leftValue.equals(rightValue));
        break;
      case GTE:
        result = new BoolValue(!leftValue.lessThan(rightValue));
        break;
      case AND:
        result = new BoolValue(leftValue.toBool() && rightValue.toBool());
        break;
      case OR:
        result = new BoolValue(leftValue.toBool() || rightValue.toBool());
        break;
      case MODULO:
        result = new IntValue(leftValue.toInt() % rightValue.toInt(), IntType.Create(false, 64));
        break;
      default:
        error = true;
        break;
    }

    if (error) {
      throw new InterpError("Binary " + operator.binOpName + " failed", lineNum, columnNum);
    }

    return result;
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.BinaryExpression.Builder subDoc = AstProto.BinaryExpression.newBuilder();
    subDoc.setOp(operator.ordinal());
    subDoc.setLeft(left.serialize().build());
    subDoc.setRight(right.serialize().build());

    AstProto.Ast.Builder document = preSerialize();
    document.setBinaryExpression(subDoc);
    return document;
  }

  public static BinaryExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.BinaryExpression document) throws SerializationError {
    return new BinaryExpression(lineNum, columnNum, preComments, postComments,
        BinOp.values()[document.getOp()],
        AbstractExpression.deserialize(document.getLeft()),
        AbstractExpression.deserialize(document.getRight()));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, left.renameIds(renamings, msgState), right.renameIds(renamings, msgState));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = left.findFreeVariables();
    freeVars.putAll(right.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof BinaryExpression)) {
      return false;
    }
    BinaryExpression ourOther = (BinaryExpression) other;
    if (operator != ourOther.operator) {
      return false;
    }
    return left.compare(ourOther.left) && right.compare(ourOther.right);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, (Expression) left.replace(target, with),
        (Expression) right.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = left.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    return right.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> leftPair = left.toAnf(tuning);
    insert.addAll(leftPair.a);
    Pair<List<DeclarationStatement>, Ast> rightPair = right.toAnf(tuning);
    insert.addAll(rightPair.a);

    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, (Expression) leftPair.b, (Expression) rightPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, left.replaceType(mapFromTo),
        right.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmpLeft = left.replaceVtableAccess(project, msgState);
    Optional<Ast> tmpRight = right.replaceVtableAccess(project, msgState);
    if (!tmpLeft.isPresent() || !tmpRight.isPresent()) {
      return Optional.empty();
    }
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, (Expression) tmpLeft.get(), (Expression) tmpRight.get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Expression>> tmpLeft = left.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    Optional<Nullable<Expression>> tmpRight = right.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpLeft.isPresent() || !tmpRight.isPresent()) {
      return Optional.empty();
    }
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, tmpLeft.get().get(), tmpRight.get().get());
    Optional<Nullable<Type>> tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    value.determinedType = tmp.get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    left.addDependency(modDepGraph, ourModNode, tenv);
    right.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return left.containsType(types) || right.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return left.containsAst(asts) || right.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = left.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newLeft = (Expression) tmp.get().ast.get();

    tmp = right.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newRight = (Expression) tmp.get().ast.get();
    
    if (enableResolution) {
      if (operator == BinOp.DIV) {
        // insert div by zero check
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.divByZeroExc,
            new BinaryExpression(BinOp.EQUAL, newRight, new IntegerExpression("0")), false,
            msgState));
      }
    }

    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, newLeft, newRight);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, left.insertDestructorCalls(scopedIds, destructorRenamings),
        right.insertDestructorCalls(scopedIds, destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, left.liftClassDeclarations(liftedClasses, replacements).get(),
        right.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, left.resolveUserTypes(msgState).get(),
        right.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    BinaryExpression value = new BinaryExpression(lineNum, columnNum, preComments, postComments,
        operator, left.addThisExpression(classHeader),
        right.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
