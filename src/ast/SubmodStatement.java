package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import astproto.AstProto.Ast.Builder;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.ModuleEndpoint;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserInstanceType;
import util.GenericTree;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class SubmodStatement extends AbstractStatement {

  // this is a child module of the current module.
  // The child must also list this module as their parent
  // For convenience, use abs module path here, but we really only count the
  // 'name'
  public final ModuleType absSubmodPath;

  public SubmodStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, ModuleType absSubmodPath) {
    super(lineNum, columnNum, AstType.SUBMOD_STATEMENT, preComments, postComments);
    this.absSubmodPath = absSubmodPath;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "submod " + absSubmodPath.name + postToString();
  }

  public void populateTypeGraph(ModuleType ourModuleType, Nullable<UserInstanceType> shortImportModType,
      TypeEnvironment tenv) throws FatalMessageException {
    Nullable<ModuleEndpoint> submodule = tenv.project.lookupModule(absSubmodPath,
        tenv.project.getAuditTrail());
    if (submodule.isNull()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Failed to find submodule: " + absSubmodPath, -1, -1);
    }

    // submodule.get().program.populateTypeGraph(shortImportModType, tenv);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType) {
    // TODO Auto-generated method stub
    return Optional.empty();
  }

  @Override
  public Value interp(Environment env) throws InterpError {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.SubmodStatement.Builder subDoc = AstProto.SubmodStatement.newBuilder();

    subDoc.setAbsSubmodPath(absSubmodPath.serialize());

    document.setSubmodStatement(subDoc);
    return document;
  }

  public static SubmodStatement deserialize(int deLineNum, int deColumnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.SubmodStatement document) throws SerializationError {
    return new SubmodStatement(deLineNum, deColumnNum, preComments, postComments,
        (ModuleType) AbstractType.deserialize(document.getAbsSubmodPath()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return new HashMap<String, Type>();
  }

  @Override
  public boolean compare(Ast other) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    // TODO Auto-generated method stub
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    return new SubmodStatement(lineNum, columnNum, preComments, postComments,
        (ModuleType) absSubmodPath.replaceType(mapFromTo));
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) {
    return this;
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) {
    return Optional.of(this);
  }

  public void linkRenamings(Project project, GenericTree<String, String> globalRenamings,
      ModuleType ourModuleType) throws FatalMessageException {
    ModuleEndpoint mod = project.lookupModule(absSubmodPath, project.getAuditTrail()).get();
    // globalRenamings.putEdge(submodName,
    // mod.getLiveInfo().submodsLinkedRenamings.get());
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(new LinkedList<>(), Nullable.of(this)));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) {
    return new InsertDestructorsRet(this);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    return this;
  }
}
