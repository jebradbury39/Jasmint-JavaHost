package ast;

import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ClassHeader.ClassMember;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MemValue;
import interp.Value;
import interp.ValueBox;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class LvalueId extends AbstractLvalue {

  public final String id;

  public LvalueId(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, String id) {
    super(lineNum, columnNum, AstType.ID_LVALUE, preComments, postComments);
    this.id = id;
  }

  public LvalueId(int lineNum, int columnNum, String id) {
    super(lineNum, columnNum, AstType.ID_LVALUE);
    this.id = id;
  }

  public LvalueId(String id) {
    super(AstType.ID_LVALUE);
    this.id = id;
  }

  @Override
  public String toString(String indent) {
    return preToString() + id + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    if (id.equals("this")) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "Cannot use 'this' as an Lvalue", lineNum, columnNum);
    }

    Nullable<TypeBox> tyBox = tenv.lookup(id, lineNum, columnNum);
    if (tyBox.isNull()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK, id + " is not defined",
          lineNum, columnNum);
    }
    if (tyBox.get().getType().getIsConst()) {
      if (!(!tyBox.get().getIsInit() && withinConstructor())) {
        tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            id + " is const and cannot be an lvalue", lineNum, columnNum);
      }
    }
    if (tyBox.instanceOf(BasicTypeBox.class)) {
      checkVisibilityLevel(tenv, id, (BasicTypeBox) tyBox.get(), tenv.msgState);
    }

    tyBox.get().setIsInit(true);
    determinedType = tyBox.get().getType();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    ValueBox box = env.lookup(id);
    if (box == null) {
      throw new InterpError(id + " is not defined", lineNum, columnNum);
    }
    return new MemValue(determinedType, box);
  }

  @Override
  public AstProto.Ast.Builder serialize() {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LvalueId.Builder subDoc = AstProto.LvalueId.newBuilder();

    subDoc.setId(id);

    document.setLvalueId(subDoc);
    return document;
  }

  public static LvalueId deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.LvalueId document) {
    return new LvalueId(lineNum, columnNum, preComments, postComments, document.getId());
  }

  @Override
  public Expression toExpression() {
    return new IdentifierExpression(lineNum, columnNum, preComments, postComments, id);
  }

  @Override
  public Lvalue renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Nullable<String> newId = renamings.lookup(id, Nullable.of(determinedType), Nullable.empty());
    if (newId.isNotNull()) {
      LvalueId value = new LvalueId(lineNum, columnNum, preComments, postComments, newId.get());
      value.determinedType = determinedType.renameIds(renamings, msgState);
      value.setOriginalLabel(getOriginalLabel());
      return value;
    }
    return this;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<String, Type>();
    freeVars.put(id, determinedType);
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LvalueId)) {
      return false;
    }
    LvalueId ourOther = (LvalueId) other;
    return id.equals(ourOther.id);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return Nullable.empty();
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    return new Pair<List<DeclarationStatement>, Ast>(new LinkedList<DeclarationStatement>(), this);
  }

  @Override
  public Lvalue replaceType(Map<Type, Type> mapFromTo) {
    LvalueId val = new LvalueId(lineNum, columnNum, preComments, postComments, id);
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState) {
    return Optional.of(this);
  }

  @Override
  public Lvalue normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    LvalueId val = new LvalueId(lineNum, columnNum, preComments, postComments, id);
    val.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return false;
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    return asts.contains(this);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return Optional.of(new ResolveExcRet(this));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
  }

  @Override
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    return Nullable.of(this);
  }

  @Override
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    return Optional.of(this);
  }

  @Override
  public Lvalue addThisExpression(ClassHeader classHeader) {
    Nullable<List<ClassMember>> members = classHeader.lookup(id);
    if (members.isNotNull()) {
      // make sure these members are not static
      for (ClassMember member : members.get()) {
        if (member.isStatic) {
          return this;
        }
      }
      // we are within the class as a member, so prepend with 'this.'
      ThisExpression thisExpr = new ThisExpression();
      thisExpr.setOriginalLabel(thisExpr.label);
      
      LvalueDot dotExpr = new LvalueDot(lineNum, columnNum, preComments, postComments,
          thisExpr, id);
      dotExpr.determinedType = determinedType;
      dotExpr.setOriginalLabel(dotExpr.label);
      return dotExpr;
    }
    return this;
  }
}
