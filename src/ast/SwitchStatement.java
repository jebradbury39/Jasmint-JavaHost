package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;

public class SwitchStatement extends AbstractStatement {

  private final Expression value;
  private final List<SwitchCaseStatement> cases;
  private final Nullable<SwitchCaseStatement> defaultCase; /* may be null */

  public SwitchStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression value, List<SwitchCaseStatement> cases,
      Nullable<SwitchCaseStatement> defaultCase) {
    super(lineNum, columnNum, AstType.SWITCH_STATEMENT, preComments, postComments);
    this.value = value;
    this.cases = cases;
    this.defaultCase = defaultCase;

    value.setParentAst(this);
    if (defaultCase.isNotNull()) {
      defaultCase.get().setParentAst(this);
    }
    for (SwitchCaseStatement scs : this.cases) {
      scs.setParentAst(this);
    }

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + "switch (" + value.toString(indent) + ") {";

    for (SwitchCaseStatement scs : cases) {
      result += "\n" + indent + scs.toString(indent);
    }

    if (defaultCase.isNotNull()) {
      result += "\n" + indent + defaultCase.get().toString(indent);
    }

    return result + "\n" + indent + "}" + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> tyOpt = value.typecheck(tenv, Nullable.empty());
    if (!tyOpt.isPresent()) {
      return Optional.empty();
    }
    Type valueType = tyOpt.get().type;
    TypeEnvironment newTenv = tenv.extend();
    // check if all cases will return (like ConditionalStatement)
    Nullable<Type> retType = Nullable.empty();
    boolean foundNonRetCase = false;

    boolean hitErr = false;
    Optional<Pair<Nullable<Type>, TypecheckRet>> scsOpt = Optional.empty();
    Nullable<Type> scsRetTy = Nullable.empty();
    for (SwitchCaseStatement scs : cases) {
      scsOpt = scs.customTypecheck(newTenv, expectedReturnType);
      if (!scsOpt.isPresent()) {
        hitErr = true;
        continue;
      }
      if (scsOpt.get().a.isNull()) {
        tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "SCS value type is null for non-default case", lineNum, columnNum);
      }

      Type scsValueTy = scsOpt.get().a.get();
      if (!tenv.canSafeCast(valueType, scsValueTy)) {
        tenv.msgState
            .addMessage(
                MsgType.ERROR, MsgClass.TYPECHECK, "Switch case type [" + scsValueTy
                    + "] is not compatable with switched type [" + valueType + "]",
                lineNum, columnNum);
        hitErr = true;
      }

      if (!foundNonRetCase) {
        scsRetTy = scsOpt.get().b.returnType;
        if (scsRetTy.isNull()) {
          // this case does not return, which means the whole switch statement is kaput
          foundNonRetCase = true;
          retType = Nullable.empty();
        } else {
          // this case returns, so if we have no actual return type yet, set it
          if (retType.isNull()) {
            retType = scsRetTy;
          }
        }
      }
    }

    if (defaultCase.isNotNull()) {
      scsOpt = defaultCase.get().customTypecheck(newTenv, expectedReturnType);
      if (!scsOpt.isPresent()) {
        return Optional.empty();
      }
      if (!foundNonRetCase) {
        scsRetTy = scsOpt.get().b.returnType;
        if (scsRetTy.isNull()) {
          // this case does not return, which means the whole switch statement is kaput
          foundNonRetCase = true;
          retType = Nullable.empty();
        } else {
          // this case returns, so if we have no actual return type yet, set it
          if (retType.isNull()) {
            retType = scsRetTy;
          }
        }
      }
    } else {
      // no default case, so we don't know if this is a definite return
      retType = Nullable.empty();
    }

    if (hitErr) {
      return Optional.empty();
    }

    return Optional.of(new TypecheckRet(retType, determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value keyValue = value.interp(env);
    Environment newEnv = env.extend();

    boolean foundCase = false;
    for (SwitchCaseStatement scs : cases) {
      if (!foundCase) {
        if (scs.value.get().interp(newEnv).equals(keyValue)) {
          foundCase = true;
          Value caseBodyVal = scs.interp(newEnv);
          boolean breakswitch = false;
          switch (caseBodyVal.getMetaValue()) {
            case BREAK:
              breakswitch = true;
              break;
            case RETURN:
              return caseBodyVal;
            default:
              break;
          }
          if (breakswitch) {
            break;
          }
        }
        continue;
      }

      /* keep executing until we are stopped */
      Value caseBodyVal = scs.interp(newEnv);
      boolean breakswitch = false;
      switch (caseBodyVal.getMetaValue()) {
        case BREAK:
          breakswitch = true;
          break;
        case RETURN:
          return caseBodyVal;
        default:
          break;
      }
      if (breakswitch) {
        break;
      }
    }

    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.SwitchStatement.Builder subDoc = AstProto.SwitchStatement.newBuilder();

    subDoc.setValue(value.serialize());

    for (SwitchCaseStatement scs : cases) {
      subDoc.addCases(scs.serialize());
    }

    if (defaultCase.isNotNull()) {
      subDoc.addDefaultCase(defaultCase.get().serialize());
    }

    document.setSwitchStatement(subDoc);
    return document;
  }

  public static SwitchStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.SwitchStatement document) throws SerializationError {
    List<SwitchCaseStatement> cases = new LinkedList<SwitchCaseStatement>();
    for (AstProto.Ast bvScs : document.getCasesList()) {
      cases.add(SwitchCaseStatement.deserialize(bvScs));
    }

    Nullable<SwitchCaseStatement> defaultCase = Nullable.empty();
    if (document.getDefaultCaseCount() > 0) {
      defaultCase = Nullable.of(SwitchCaseStatement.deserialize(document.getDefaultCase(0)));
    }

    return new SwitchStatement(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getValue()), cases, defaultCase);
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    List<SwitchCaseStatement> newCases = new LinkedList<SwitchCaseStatement>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.renameIds(renamings, msgState));
    }

    Nullable<SwitchCaseStatement> newDefault = defaultCase;
    if (defaultCase.isNotNull()) {
      newDefault = Nullable
          .of((SwitchCaseStatement) defaultCase.get().renameIds(renamings, msgState));
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        value.renameIds(renamings, msgState), newCases, newDefault);
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = new HashMap<String, Type>();
    if (defaultCase.isNotNull()) {
      freeVars.putAll(defaultCase.get().findFreeVariables());
    }
    for (SwitchCaseStatement scs : cases) {
      freeVars.putAll(scs.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof SwitchStatement)) {
      return false;
    }
    SwitchStatement ourOther = (SwitchStatement) other;
    if ((ourOther.defaultCase.isNull() && defaultCase.isNotNull())
        || (ourOther.defaultCase.isNotNull() && defaultCase.isNull())) {
      return false;
    }
    if (defaultCase.isNotNull()) {
      if (!ourOther.defaultCase.get().compare(defaultCase.get())) {
        return false;
      }
    }

    if (ourOther.cases.size() != cases.size()) {
      return false;
    }
    Iterator<SwitchCaseStatement> iterOther = ourOther.cases.iterator();
    Iterator<SwitchCaseStatement> iterUs = cases.iterator();
    while (iterOther.hasNext() && iterUs.hasNext()) {
      if (!iterOther.next().compare(iterUs.next())) {
        return false;
      }
    }

    return ourOther.value.compare(value);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<SwitchCaseStatement> newCases = new LinkedList<SwitchCaseStatement>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.replace(target, with));
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        (Expression) this.value.replace(target, with), newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get().replace(target, with))));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = Nullable.empty();
    if (defaultCase.isNotNull()) {
      tmp = defaultCase.get().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    for (SwitchCaseStatement scs : cases) {
      tmp = scs.findByOriginalLabel(findLabel);
      if (tmp != null) {
        return tmp;
      }
    }
    return value.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    List<SwitchCaseStatement> newCases = new LinkedList<>();
    for (SwitchCaseStatement scs : cases) {
      Pair<List<DeclarationStatement>, Ast> tmp = scs.toAnf(tuning);
      insert.addAll(tmp.a);
      newCases.add((SwitchCaseStatement) tmp.b);
    }

    Pair<List<DeclarationStatement>, Ast> valuePair = value.toAnf(tuning);
    insert.addAll(valuePair.a);
    Pair<List<DeclarationStatement>, Ast> defaultPair = null;
    if (defaultCase.isNotNull()) {
      defaultPair = defaultCase.get().toAnf(tuning);
      insert.addAll(defaultPair.a);
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        (Expression) valuePair.b, newCases, (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultPair.b)));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    List<SwitchCaseStatement> newCases = new LinkedList<>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.replaceType(mapFromTo));
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        (Expression) value.replaceType(mapFromTo), newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get().replaceType(mapFromTo))));
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    List<SwitchCaseStatement> newCases = new LinkedList<SwitchCaseStatement>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.replaceVtableAccess(project, msgState).get());
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        (Expression) value.replaceVtableAccess(project, msgState).get(), newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get()
                .replaceVtableAccess(project, msgState).get())));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    List<SwitchCaseStatement> newCases = new LinkedList<SwitchCaseStatement>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs
          .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get());
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        value
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get()
                .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
                .get())));
    val.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    value.addDependency(modDepGraph, ourModNode, tenv);
    for (SwitchCaseStatement scs : cases) {
      scs.addDependency(modDepGraph, ourModNode, tenv);
    }
    if (defaultCase.isNotNull()) {
      defaultCase.get().addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    for (SwitchCaseStatement scs : cases) {
      if (scs.containsType(types)) {
        return true;
      }
    }
    if (defaultCase.isNotNull()) {
      if (defaultCase.get().containsType(types)) {
        return true;
      }
    }
    return value.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (SwitchCaseStatement scs : cases) {
      if (scs.containsAst(asts)) {
        return true;
      }
    }
    if (defaultCase.isNotNull()) {
      if (defaultCase.get().containsAst(asts)) {
        return true;
      }
    }
    return value.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();
    List<SwitchCaseStatement> newCases = new LinkedList<>();
    Optional<ResolveExcRet> tmp = Optional.empty();

    tmp = value.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newValue = (Expression) tmp.get().ast.get();

    for (SwitchCaseStatement scs : cases) {
      tmp = scs.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
          withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newCases.add((SwitchCaseStatement) tmp.get().ast.get());
    }

    Nullable<SwitchCaseStatement> newDefault = defaultCase;
    if (defaultCase.isNotNull()) {
      tmp = defaultCase.get().resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
          stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newDefault = Nullable.of((SwitchCaseStatement) tmp.get().ast.get());
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        newValue, newCases, newDefault);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(val)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {

    boolean mustReturn = false;
    boolean foundNoReturn = false;
    List<SwitchCaseStatement> newCases = new LinkedList<>();
    for (SwitchCaseStatement scs : cases) {
      InsertDestructorsRet tmp = scs.insertDestructorCalls(scopedIds, destructorRenamings);
      newCases.add((SwitchCaseStatement) tmp.statements.get(0));
      if (!foundNoReturn) {
        if (tmp.mustReturn) {
          mustReturn = true;
        } else {
          mustReturn = false;
          foundNoReturn = true;
        }
      }
    }

    Nullable<SwitchCaseStatement> newDefault = defaultCase;
    if (defaultCase.isNotNull()) {
      InsertDestructorsRet tmp = defaultCase.get().insertDestructorCalls(scopedIds,
          destructorRenamings);
      newDefault = Nullable.of((SwitchCaseStatement) tmp.statements.get(0));
      if (!foundNoReturn) {
        if (tmp.mustReturn) {
          mustReturn = true;
        } else {
          mustReturn = false;
          foundNoReturn = true;
        }
      }
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments, value,
        newCases, newDefault);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());

    return new InsertDestructorsRet(val, mustReturn);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    List<SwitchCaseStatement> newCases = new LinkedList<SwitchCaseStatement>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.liftClassDeclarations(liftedClasses,
          replacements).get());
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        value.liftClassDeclarations(liftedClasses, replacements).get(), newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get()
                .liftClassDeclarations(liftedClasses, replacements).get())));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<SwitchCaseStatement> newCases = new LinkedList<>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.resolveUserTypes(msgState).get());
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        value.resolveUserTypes(msgState).get(), newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get()
                .resolveUserTypes(msgState).get())));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    List<SwitchCaseStatement> newCases = new LinkedList<>();
    for (SwitchCaseStatement scs : cases) {
      newCases.add((SwitchCaseStatement) scs.addThisExpression(classHeader));
    }

    SwitchStatement val = new SwitchStatement(lineNum, columnNum, preComments, postComments,
        value.addThisExpression(classHeader), newCases,
        (defaultCase.isNull() ? Nullable.empty()
            : Nullable.of((SwitchCaseStatement) defaultCase.get()
                .addThisExpression(classHeader))));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
