package ast;

import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import mbo.Renamings;
import mbo.TypeReplacements;
import typecheck.ModuleType;
import typecheck.Type;

public interface Lvalue extends Ast {

  /* for desugaring */
  Expression toExpression();

  /* This ensures unique names and also takes care of shadowing.
   * This is a utility for the users of the ast
   */
  Lvalue renameIds(Renamings renamings,
      MsgState msgState)
          throws FatalMessageException;
  
  public Lvalue replaceType(Map<Type, Type> mapFromTo);

  Lvalue normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
          throws FatalMessageException;
  
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException;
  
  //this MUST run exactly ONCE right after the FIRST typecheck
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException;
  
  public Lvalue addThisExpression(ClassHeader classHeader);
}
