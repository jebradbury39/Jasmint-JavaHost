package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.BoolValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.BoolType;
import typecheck.Float64Type;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typegraph.TypeGraph.CastDirection;
import util.Pair;
import util.ScopedIdentifiers;

public class LeftUnaryOpExpression extends AbstractExpression {

  public static enum LeftUnaryOp {
    NEGATE("-"), NOT("!");

    public final String leftUnaryOpName;

    private LeftUnaryOp(String name) {
      this.leftUnaryOpName = name;
    }
  }

  private static final Map<String, LeftUnaryOp> STR_TO_LUOP = new HashMap<String, LeftUnaryOp>();

  static {
    for (LeftUnaryOp o : LeftUnaryOp.values()) {
      STR_TO_LUOP.put(o.leftUnaryOpName, o);
    }
  }

  public final LeftUnaryOp operator;
  public final Expression right;

  private LeftUnaryOpExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, LeftUnaryOp operator, Expression right) {
    super(lineNum, columnNum, AstType.LEFT_UNARY_OP_EXPRESSION, preComments, postComments);
    this.operator = operator;
    this.right = right;

    right.setParentAst(this);
  }

  public LeftUnaryOpExpression(int lineNum, int columnNum, LeftUnaryOp operator, Expression right) {
    super(lineNum, columnNum, AstType.LEFT_UNARY_OP_EXPRESSION);
    this.operator = operator;
    this.right = right;

    right.setParentAst(this);
  }

  public LeftUnaryOpExpression(LeftUnaryOp operator, Expression right) {
    super(AstType.LEFT_UNARY_OP_EXPRESSION);
    this.operator = operator;
    this.right = right;

    right.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + operator.leftUnaryOpName + right.toString(indent) + postToString();
  }

  public static LeftUnaryOpExpression createLeftUnaryOpExpression(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments, String opStr,
      Expression right) {
    return new LeftUnaryOpExpression(lineNum, columnNum, preComments, postComments,
        STR_TO_LUOP.get(opStr), right);
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> rightTypeOpt = right.typecheck(tenv, Nullable.empty());
    if (!rightTypeOpt.isPresent()) {
      return Optional.empty();
    }
    Type rightType = rightTypeOpt.get().type;
    boolean error = false;

    switch (operator) {
      case NEGATE:
        if (!tenv.typeGraph.canCastTo(rightType, CastDirection.DOWN, Float64Type.Create())) {
          error = true;
          break;
        }
        determinedType = rightType;
        break;
      case NOT:
        if (!tenv.typeGraph.canCastTo(rightType, CastDirection.UP, BoolType.Create())) {
          error = true;
          break;
        }
        determinedType = BoolType.Create();
        break;
      default:
        error = true;
        break;
    }

    if (error) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "LeftUnary " + operator.leftUnaryOpName + " failed on type " + rightType, lineNum,
          columnNum);
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value rightValue = right.interp(env);
    switch (operator) {
      case NEGATE:
        return rightValue.neg();
      case NOT:
        return new BoolValue(!rightValue.toBool());
      default:
        break;
    }

    throw new InterpError(
        "LeftUnary " + operator.leftUnaryOpName + " failed on value " + rightValue, lineNum,
        columnNum);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LeftUnaryOpExpression.Builder subDoc = AstProto.LeftUnaryOpExpression.newBuilder();

    subDoc.setOp(operator.leftUnaryOpName);
    subDoc.setRight(right.serialize());

    document.setLeftUnaryOpExpression(subDoc);
    return document;
  }

  public static LeftUnaryOpExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.LeftUnaryOpExpression document) throws SerializationError {
    return new LeftUnaryOpExpression(lineNum, columnNum, preComments, postComments,
        STR_TO_LUOP.get(document.getOp()), AbstractExpression.deserialize(document.getRight()));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, right.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return right.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LeftUnaryOpExpression)) {
      return false;
    }
    LeftUnaryOpExpression ourOther = (LeftUnaryOpExpression) other;
    if (ourOther.operator != operator) {
      return false;
    }
    return right.compare(ourOther.right);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, (Expression) right.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return right.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> rightPair = null;
    rightPair = right.toAnf(tuning);
    insert.addAll(rightPair.a);

    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, (Expression) rightPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, (Expression) right.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, (Expression) right.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator,
        right.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    right.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return right.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return right.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize,
      MsgState msgState)
      throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = right.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newRight = (Expression) tmp.get().ast.get();

    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, newRight);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, right.insertDestructorCalls(scopedIds, destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, right.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, right.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    LeftUnaryOpExpression value = new LeftUnaryOpExpression(lineNum, columnNum, preComments,
        postComments, operator, right.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
