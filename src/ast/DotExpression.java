package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.ArrayValue;
import interp.Environment;
import interp.InterpError;
import interp.MapValue;
import interp.ModuleValueEntry;
import interp.ReferenceValue;
import interp.UserTypeDeclarationValue;
import interp.UserTypeInstanceValue;
import interp.Value;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.ArrayType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import util.Pair;
import util.ScopedIdentifiers;

public class DotExpression extends AbstractExpression {

  public final Expression left;
  public final IdentifierExpression right;

  public DotExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression left, IdentifierExpression right) {
    super(lineNum, columnNum, AstType.DOT_EXPRESSION, preComments, postComments);
    this.left = left;
    this.right = right;

    left.setParentAst(this);
    right.setParentAst(this);
  }

  public DotExpression(int lineNum, int columnNum, Expression left, IdentifierExpression right) {
    super(lineNum, columnNum, AstType.DOT_EXPRESSION);
    this.left = left;
    this.right = right;

    left.setParentAst(this);
    right.setParentAst(this);
  }

  public DotExpression(Expression left, IdentifierExpression right) {
    super(AstType.DOT_EXPRESSION);
    this.left = left;
    this.right = right;

    left.setParentAst(this);
    right.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + left.toString(indent) + "." + right.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> leftTypeOpt = left.typecheck(tenv, Nullable.empty());

    if (!leftTypeOpt.isPresent()) {
      return Optional.empty();
    }

    Type leftType = leftTypeOpt.get().type;
    if (leftType instanceof ReferenceType) {
      leftType = ((ReferenceType) leftType).innerType;
    }

    // check for builtin array .method()
    if (leftType instanceof ArrayType) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "DotExpression right side of array must be a function call, found: " + right, lineNum,
          columnNum);
      return Optional.empty();
    }

    // check for builtin map .method()
    if (leftType instanceof MapType) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "DotExpression right side of map must be a function call", lineNum, columnNum);
      return Optional.empty();
    }

    // typical case of class instance .id
    Optional<TypeEnvironment> newTenv = Optional.empty();
    if (leftType instanceof UserInstanceType) {
      newTenv = ((UserInstanceType) leftType).getTenv(tenv, lineNum, columnNum);
    } else if (leftType instanceof UserDeclType) {
      newTenv = ((UserDeclType) leftType).getTenv(tenv, lineNum, columnNum);
    } else {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          ". expression left side is not a class: " + leftType, lineNum, columnNum);
    }
    if (!newTenv.isPresent()) {
      return Optional.empty();
    }

    Optional<Type> rightType = Optional.empty();
    if (right instanceof IdentifierExpression) {
      Optional<TypecheckRet> rightTypeOpt = right.typecheck(newTenv.get(), Nullable.empty());
      if (!rightTypeOpt.isPresent()) {
        return Optional.empty();
      }
      rightType = Optional.of(rightTypeOpt.get().type);
    } else {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          ". expression right side is not an id", lineNum, columnNum);
    }

    if (!rightType.isPresent()) {
      return Optional.empty();
    }
    determinedType = rightType.get();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value leftValue = left.interp(env);

    if (leftValue instanceof ReferenceValue) {
      leftValue = ((ReferenceValue) leftValue).referenced.getValue();
    }

    // check for builtin array .method()
    if (leftValue instanceof ArrayValue) {
      throw new InterpError(
          "DotExpression right side of array must be a function call, found: " + right, lineNum,
          columnNum);
    }

    // check for builtin map .method()
    if (leftValue instanceof MapValue) {
      throw new InterpError(
          "DotExpression right side of map must be a function call, found: " + right, lineNum,
          columnNum);
    }

    Environment newEnv = null;
    if (leftValue instanceof UserTypeDeclarationValue) {
      newEnv = ((UserTypeDeclarationValue) leftValue).getStaticEnv(false); // no GLOBAL access
    } else if (leftValue instanceof UserTypeInstanceValue) {
      newEnv = ((UserTypeInstanceValue) leftValue).getEnv();
    } else if (leftValue instanceof ModuleValueEntry) {
      newEnv = ((ModuleValueEntry) leftValue).getEnv(env.project, lineNum, columnNum);
    } else {
      throw new InterpError(". expression left side is not a class: " + leftValue, lineNum,
          columnNum);
    }

    Value resultValue = null;
    if (right instanceof IdentifierExpression) {
      resultValue = ((IdentifierExpression) right).interp(newEnv);
    } else {
      throw new InterpError(". expression right side is not an id or function call", lineNum,
          columnNum);
    }
    if (resultValue == null) {
      throw new InterpError(
          "unknown internal interp error on dot expression. Interp result is null", lineNum,
          columnNum);
    }

    return resultValue;
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.DotExpression.Builder subDoc = AstProto.DotExpression.newBuilder();

    subDoc.setLeft(left.serialize().build());
    subDoc.setRight(right.serialize().build());

    document.setDotExpression(subDoc);
    return document;
  }

  public static DotExpression deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.DotExpression document) throws SerializationError {
    return new DotExpression(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getLeft()),
        (IdentifierExpression) AbstractExpression.deserialize(document.getRight()));
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {

    Type leftTy = left.getDeterminedType();
    if (leftTy instanceof ReferenceType) {
      leftTy = ((ReferenceType) leftTy).innerType;
    }
    Nullable<DotAccessType> leftUserTy = Nullable.empty();
    if (leftTy instanceof DotAccessType) {
      leftUserTy = Nullable.of((DotAccessType) leftTy);
    }

    Expression newRight = null;
    if (right instanceof IdentifierExpression) {
      newRight = ((IdentifierExpression) right).renameIdsCustom(renamings, leftUserTy, msgState);
    } else {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.AST, "Dot right can only be id", lineNum,
          columnNum);
    }

    // check the left's type to see what renamings are in place (for fn name/id)
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        left.renameIds(renamings, msgState), (IdentifierExpression) newRight);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    // ex: G.x then G is a free var but x is not
    return left.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof DotExpression)) {
      return false;
    }
    DotExpression ourOther = (DotExpression) other;
    return ourOther.left.compare(left) && ourOther.right.compare(right);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        (Expression) left.replace(target, with),
        (IdentifierExpression) right.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = left.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    return right.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> leftPair = left.toAnf(tuning);
    insert.addAll(leftPair.a);
    Pair<List<DeclarationStatement>, Ast> rightPair = right.toAnf(tuning);
    insert.addAll(rightPair.a);

    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        (Expression) leftPair.b, (IdentifierExpression) rightPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  Optional<List<String>> toIdList() {
    List<String> idList = new LinkedList<>();
    if (left instanceof IdentifierExpression) {
      idList.add(((IdentifierExpression) left).id);
    } else if (left instanceof DotExpression) {
      Optional<List<String>> tmp = ((DotExpression) left).toIdList();
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      idList.addAll(tmp.get());
    } else {
      return Optional.empty();
    }

    if (right instanceof IdentifierExpression) {
      idList.add(((IdentifierExpression) right).id);
    } else {
      return Optional.empty();
    }

    return Optional.of(idList);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    // This expression at this point will NOT handle static class access such as std.io.Constants
    
    // remove all the single names from the mapping for the right side since
    // it depends on the left context e.g. std.io.Constants and Constants ... remove
    // Constants
    Map<Type, Type> rightMapFromTo = new HashMap<>();
    for (Entry<Type, Type> mapping : mapFromTo.entrySet()) {
      if (mapping.getKey() instanceof UserInstanceType) {
        if (((UserInstanceType) mapping.getKey()).outerType.isNotNull()) {
          // must have at least one dot in it
          rightMapFromTo.put(mapping.getKey(), mapping.getValue());
        }
      } else {
        rightMapFromTo.put(mapping.getKey(), mapping.getValue());
      }
    }

    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        (Expression) left.replaceType(mapFromTo),
        (IdentifierExpression) right.replaceType(rightMapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  // only for replaceType with static dot access
  /*
  public static Expression fromStaticList(List<String> list, int lineNum, int columnNum) {
    if (list.isEmpty()) {
      throw new IllegalArgumentException("list may never be empty!");
    }
    if (list.size() == 1) {
      IdentifierExpression val = new IdentifierExpression(lineNum, columnNum, new LinkedList<>(),
          new LinkedList<>(), list.get(0));
      val.determinedType = UserDeclType.fromList(list);
      val.setOriginalLabel(val.getOriginalLabel());
      return val;
    }
    // list.size >= 2, so split e.g. ['std', 'io', 'Constants']
    Expression left = fromStaticList(list.subList(0, list.size() - 1), lineNum, columnNum);
    Expression right = fromStaticList(list.subList(list.size() - 1, list.size()), lineNum,
        columnNum);
    UserDeclType dotTy = UserDeclType.fromList(list);
    right.setDeterminedType(dotTy);

    DotExpression val = new DotExpression(lineNum, columnNum, new LinkedList<>(),
        new LinkedList<>(), left, (IdentifierExpression) right);
    val.determinedType = dotTy;
    val.setOriginalLabel(val.getOriginalLabel());
    return val;
  }
  */

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        (Expression) left.replaceVtableAccess(project, msgState).get(),
        (IdentifierExpression) right.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        left.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        (IdentifierExpression) right
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    left.addDependency(modDepGraph, ourModNode, tenv);
    right.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return left.containsType(types) || right.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return left.containsAst(asts) || right.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = left.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newLeft = (Expression) tmp.get().ast.get();

    tmp = right.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newRight = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      if (left.getDeterminedType() instanceof ReferenceType) {
        // insert null check
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
            new BinaryExpression(BinOp.EQUAL, newLeft, new NullExpression()), false, msgState));
      }
    }

    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments, newLeft,
        (IdentifierExpression) newRight);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        left.insertDestructorCalls(scopedIds, destructorRenamings),
        (IdentifierExpression) right.insertDestructorCalls(scopedIds, destructorRenamings));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        left.liftClassDeclarations(liftedClasses, replacements).get(),
        (IdentifierExpression) right.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    // check if this is the leaf of a static class access
    if (!(left instanceof StaticClassIdentifierExpression)) {
      if (left.getDeterminedType() instanceof UserDeclType && !(determinedType instanceof UserDeclType)) {
        UserDeclType leftType = (UserDeclType) left.getDeterminedType();
        
        // now ensure that this is all a string of identifiers
        Optional<List<String>> idList = toIdList();
        if (!idList.isPresent()) {
          return Optional.empty();
        }
        // remove last id (which would be this.id)
        idList.get().remove(idList.get().size() - 1);
        UserDeclType asType = leftType.fromList(idList.get());
        
        StaticClassIdentifierExpression staticLeft = new StaticClassIdentifierExpression(lineNum,
            columnNum,
            preComments, postComments, asType);
        staticLeft.determinedType = asType;
        staticLeft.setOriginalLabel(staticLeft.label);
        
        DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
            staticLeft,
            (IdentifierExpression) right.resolveUserTypes(msgState).get());
        value.determinedType = determinedType;
        value.setOriginalLabel(getOriginalLabel());
        return Optional.of(value);
      }
    }
    
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        left.resolveUserTypes(msgState).get(),
        (IdentifierExpression) right.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    DotExpression value = new DotExpression(lineNum, columnNum, preComments, postComments,
        left.addThisExpression(classHeader), right);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
