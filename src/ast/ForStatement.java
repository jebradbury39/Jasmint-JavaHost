package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import type_env.VariableScoping;
import typecheck.BoolType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import typegraph.TypeGraph.CastDirection;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class ForStatement extends AbstractStatement {

  public final Nullable<Statement> init; /* may be null */
  public final Expression guard;
  public final Nullable<Statement> action; /* may be null */
  public final BlockStatement body;

  public ForStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Nullable<Statement> init, Expression guard,
      Nullable<Statement> action, BlockStatement body) {
    super(lineNum, columnNum, AstType.FOR_STATEMENT, preComments, postComments);
    this.init = init;
    this.guard = guard;
    this.action = action;
    this.body = body;

    if (init.isNotNull()) {
      init.get().setParentAst(this);
    }
    guard.setParentAst(this);
    if (action.isNotNull()) {
      action.get().setParentAst(this);
    }
    body.setParentAst(this);

    determinedType = VoidType.Create();
  }

  @Override
  public String toString(String indent) {
    String result = preToString() + "for (";
    if (init.isNotNull()) {
      result += init.get().toString(indent) + "; ";
    }
    result += guard.toString(indent) + "; ";
    if (action.isNotNull()) {
      result += action.get().toString(indent);
    }
    result += ") " + body.toString(indent);
    return result + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    TypeEnvironment newTenv = tenv.extend();
    boolean hitErr = false;

    if (init.isNotNull()) {
      if (init.instanceOf(DeclarationStatement.class)) {
        ((DeclarationStatement) init.get()).setScoping(VariableScoping.NORMAL);
      }
      Optional<TypecheckRet> initType = init.get().typecheck(newTenv, Nullable.empty());
      if (!initType.isPresent()) {
        hitErr = true;
      }
    }

    Optional<TypecheckRet> guardType = guard.typecheck(newTenv.extend(), Nullable.empty());
    if (!guardType.isPresent()) {
      hitErr = true;
    } else {
      if (!tenv.typeGraph.canCastTo(guardType.get().type, CastDirection.UP, BoolType.Create())) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "for loop guard type [" + guardType + "] cannot be converted to bool", lineNum,
            columnNum);
        hitErr = true;
      }
    }

    Nullable<Type> bodyRetType = Nullable.empty();
    Optional<TypecheckRet> bodyType = body.typecheck(newTenv.extend(), expectedReturnType);
    if (!bodyType.isPresent()) {
      hitErr = true;
    } else {
      bodyRetType = bodyType.get().returnType;
    }

    if (action.isNotNull()) {
      Optional<TypecheckRet> actionType = action.get().typecheck(newTenv, Nullable.empty());
      if (!actionType.isPresent()) {
        hitErr = true;
      }
    }

    if (hitErr) {
      return Optional.empty();
    }

    //if guard does not enter loop, then no return
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Environment newEnv = env.extend();

    if (init.isNotNull()) {
      init.get().interp(newEnv);
    }

    while (true) {
      Value guardValue = guard.interp(newEnv);
      if (!guardValue.toBool()) {
        break;
      }

      Value bodyVal = body.interp(newEnv.extend());
      boolean breakloop = false;
      switch (bodyVal.getMetaValue()) {
        case BREAK:
          breakloop = true;
          break;
        case RETURN:
          return bodyVal;
        default:
          break;
      }
      if (breakloop) {
        break;
      }

      if (action.isNotNull()) {
        action.get().interp(newEnv);
      }
    }
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.ForStatement.Builder subDoc = AstProto.ForStatement.newBuilder();

    if (init.isNotNull()) {
      subDoc.addInit(init.get().serialize());
    }

    subDoc.setGuard(guard.serialize());

    if (action.isNotNull()) {
      subDoc.addAction(action.get().serialize());
    }

    subDoc.setBody(body.serialize());

    document.setForStatement(subDoc);
    return document;
  }

  public static ForStatement deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.ForStatement document) throws SerializationError {
    Nullable<Statement> init = Nullable.empty();
    if (document.getInitCount() > 0) {
      init = Nullable.of(AbstractStatement.deserialize(document.getInit(0)));
    }

    Nullable<Statement> action = Nullable.empty();
    if (document.getActionCount() > 0) {
      action = Nullable.of(AbstractStatement.deserialize(document.getAction(0)));
    }
    return new ForStatement(lineNum, columnNum, preComments, postComments, init,
        AbstractExpression.deserialize(document.getGuard()), action,
        (BlockStatement) AbstractStatement.deserialize(document.getBody()));
  }

  @Override
  public Statement renameIds(mbo.Renamings renamings, MsgState msgState)
      throws FatalMessageException {
    Renamings scopedRenamings = renamings.extend();

    Nullable<Statement> newInit = init;
    if (init.isNotNull()) {
      newInit = Nullable.of(init.get().renameIds(scopedRenamings, msgState));
    }

    Nullable<Statement> newAction = action;
    if (action.isNotNull()) {
      newAction = Nullable.of(action.get().renameIds(scopedRenamings, msgState));
    }

    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments, newInit,
        guard.renameIds(scopedRenamings, msgState), newAction,
        (BlockStatement) body.renameIds(scopedRenamings, msgState));
    value.determinedType = determinedType.renameIds(scopedRenamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = body.findFreeVariables();
    if (init.isNotNull()) {
      if (init.get() instanceof DeclarationStatement) {
        freeVars.remove(((DeclarationStatement) init.get()).name);
      }
      freeVars.putAll(init.get().findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof ForStatement)) {
      return false;
    }
    ForStatement ourOther = (ForStatement) other;

    if ((ourOther.init.isNull() && init.isNotNull())
        || (ourOther.init.isNotNull() && init.isNull())) {
      return false;
    }
    if (init.isNotNull()) {
      if (!init.get().compare(ourOther.init.get())) {
        return false;
      }
    }

    if ((ourOther.action.isNull() && action.isNotNull())
        || (ourOther.action.isNotNull() && action.isNull())) {
      return false;
    }
    if (action.isNotNull()) {
      if (!(action.get().compare(ourOther.action.get()))) {
        return false;
      }
    }

    if (!guard.compare(ourOther.guard)) {
      return false;
    }
    if (!body.compare(ourOther.body)) {
      return false;
    }

    return true;
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init.isNull() ? Nullable.empty()
            : Nullable.of((Statement) init.get().replace(target, with))),
        (Expression) guard.replace(target, with),
        (action.isNull() ? Nullable.empty()
            : Nullable.of((Statement) action.get().replace(target, with))),
        (BlockStatement) body.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = Nullable.empty();
    if (init.isNotNull()) {
      tmp = init.get().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    tmp = guard.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    if (action.isNotNull()) {
      tmp = action.get().findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return body.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> initPair = null;
    if (init.isNotNull()) {
      initPair = init.get().toAnf(tuning);
      insert.addAll(initPair.a);
    }
    Pair<List<DeclarationStatement>, Ast> guardPair = guard.toAnf(tuning);
    insert.addAll(guardPair.a);
    Pair<List<DeclarationStatement>, Ast> actionPair = null;
    if (action.isNotNull()) {
      actionPair = action.get().toAnf(tuning);
      insert.addAll(actionPair.a);
    }
    Pair<List<DeclarationStatement>, Ast> bodyPair = body.toAnf(tuning);
    insert.addAll(bodyPair.a);

    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init.isNull() ? Nullable.empty() : Nullable.of((Statement) initPair.b)),
        (Expression) guardPair.b,
        (action.isNull() ? Nullable.empty() : Nullable.of((Statement) actionPair.b)),
        (BlockStatement) bodyPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init.isNull() ? Nullable.empty()
            : Nullable.of((Statement) init.get().replaceType(mapFromTo))),
        (Expression) guard.replaceType(mapFromTo),
        (action.isNull() ? Nullable.empty()
            : Nullable.of((Statement) action.get().replaceType(mapFromTo))),
        (BlockStatement) body.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init.isNull() ? Nullable.empty()
            : Nullable.of((Statement) init.get().replaceVtableAccess(project, msgState).get())),
        (Expression) guard.replaceVtableAccess(project, msgState).get(),
        (action.isNull() ? Nullable.empty()
            : Nullable.of((Statement) action.get().replaceVtableAccess(project, msgState).get())),
        (BlockStatement) body.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init
            .isNull()
                ? Nullable.empty()
                : Nullable.of(init.get()
                    .normalizeType(keepCurrentModRelative, currentModule, effectiveImports,
                        msgState)
                    .get())),
        guard.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        (action
            .isNull()
                ? Nullable.empty()
                : Nullable.of(action.get()
                    .normalizeType(keepCurrentModRelative, currentModule, effectiveImports,
                        msgState)
                    .get())),
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    if (init.isNotNull()) {
      init.get().addDependency(modDepGraph, ourModNode, tenv);
    }
    guard.addDependency(modDepGraph, ourModNode, tenv);
    if (action.isNotNull()) {
      action.get().addDependency(modDepGraph, ourModNode, tenv);
    }

    body.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    if (init.isNotNull()) {
      if (init.get().containsType(types)) {
        return true;
      }
    }
    if (guard.containsType(types)) {
      return true;
    }
    if (action.isNotNull()) {
      if (action.get().containsType(types)) {
        return true;
      }
    }
    return body.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    if (init.isNotNull()) {
      if (init.get().containsAst(asts)) {
        return true;
      }
    }
    if (guard.containsAst(asts)) {
      return true;
    }
    if (action.isNotNull()) {
      if (action.get().containsAst(asts)) {
        return true;
      }
    }
    return body.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();
    Optional<ResolveExcRet> tmp = Optional.empty();

    Nullable<Statement> newInit = init;
    if (init.isNotNull()) {
      tmp = init.get().resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
          stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newInit = Nullable.of((Statement) tmp.get().ast.get());
    }

    tmp = guard.resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings,
        withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    Expression newGuard = (Expression) tmp.get().ast.get();
    if (!hitExcId.isEmpty()) {
      newGuard = new BinaryExpression(BinOp.AND, newGuard, new BinaryExpression(BinOp.EQUAL,
          new IdentifierExpression(hitExcId), new NullExpression()));
    }

    Nullable<Statement> newAction = action;
    if (action.isNotNull()) {
      tmp = action.get().resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
          stackSize, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newAction = Nullable.of((Statement) tmp.get().ast.get());
    }

    List<Statement> statements = new LinkedList<>();
    if (enableResolution) {
      List<Type> sanityChecksTypes = new LinkedList<>();
      sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
      List<Expression> sanityChecksArgs = new LinkedList<>();
      FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
          Nullable.of(new IdentifierExpression(JsmntGlobal.modType.name)),
          new IdentifierExpression(Program.sanityCheckTimeoutFnName), sanityChecksArgs,
          Program.sanityCheckTimeoutFnName);
      sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
          VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

      statements.add(new FunctionCallStatement(sanityChecksCall));
    }
    statements.addAll(body.statements);

    tmp = new BlockStatement(statements).resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    BlockStatement newBody = (BlockStatement) tmp.get().ast.get();

    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments, newInit,
        newGuard, newAction, newBody);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.BLOCK);

    List<Statement> ourStatements = new LinkedList<>();

    Nullable<Statement> newInit = init;
    if (init.isNotNull()) {
      InsertDestructorsRet tmp = init.get().insertDestructorCalls(ourScope, destructorRenamings);
      newInit = Nullable.of(tmp.statements.remove(tmp.statements.size() - 1));
      ourStatements.addAll(tmp.statements);
    }

    ScopedIdentifiers ourBodyScope = ourScope.extend(ScopeFilter.LOOP);
    InsertDestructorsRet bodyTmp = body.insertDestructorCalls(ourBodyScope, destructorRenamings);
    BlockStatement newBody = (BlockStatement) bodyTmp.statements.get(0);

    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments, newInit,
        guard, action, newBody);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    ourStatements.add(value);

    ourStatements.addAll(ourScope.getDestructors(ScopeFilter.BLOCK, destructorRenamings));

    //if guard never enters loop, then can't say we must return
    return new InsertDestructorsRet(ourStatements);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init
            .isNull()
                ? Nullable.empty()
                : Nullable.of(init.get()
                    .liftClassDeclarations(liftedClasses, replacements)
                    .get())),
        guard.liftClassDeclarations(liftedClasses, replacements)
            .get(),
        (action
            .isNull()
                ? Nullable.empty()
                : Nullable.of(action.get()
                    .liftClassDeclarations(liftedClasses, replacements)
                    .get())),
        (BlockStatement) body
            .liftClassDeclarations(liftedClasses, replacements)
            .get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init
            .isNull()
                ? Nullable.empty()
                : Nullable.of(init.get()
                    .resolveUserTypes(msgState)
                    .get())),
        guard.resolveUserTypes(msgState)
            .get(),
        (action
            .isNull()
                ? Nullable.empty()
                : Nullable.of(action.get()
                    .resolveUserTypes(msgState)
                    .get())),
        (BlockStatement) body
            .resolveUserTypes(msgState)
            .get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    ForStatement value = new ForStatement(lineNum, columnNum, preComments, postComments,
        (init
            .isNull()
                ? Nullable.empty()
                : Nullable.of(init.get()
                    .addThisExpression(classHeader))),
        guard.addThisExpression(classHeader),
        (action
            .isNull()
                ? Nullable.empty()
                : Nullable.of(action.get()
                    .addThisExpression(classHeader))),
        (BlockStatement) body
            .addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
