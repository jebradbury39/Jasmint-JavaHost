package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.AbstractValue.MetaValue;
import interp.ClosureValue;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import mbo.Vtables;
import mbo.Vtables.VtableEntry;
import multifile.JsmntGlobal;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import type_env.TypeEnvironment.SandboxModeInfo;
import typecheck.AbstractType;
import typecheck.ArrayType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.DotAccessType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.Pair;
import util.ScopedIdentifiers;

public class FunctionCallExpression extends AbstractExpression {
  // null, id, fnCall, or dot, eg: a.fn(), a.b.fn(), a.b().fn()
  public final Nullable<Expression> prefix;

  public final Expression expression;
  public final List<Expression> arguments;

  // figure this out during typechecking, then store
  // ModuleType, ClassDeclType, ArrayType, MapType
  Nullable<FunctionType> calledType = Nullable.empty(); // will contain within info
  // cannot allow destructor to be called directly (until inserted)
  boolean isDestructorCall = false; // we insert these calls right before deletion/out-of-scope

  public final String originalName; // only non-null if expression is Identifier

  public FunctionCallExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Nullable<Expression> prefix, Expression expression,
      List<Expression> arguments, String originalName) {
    super(lineNum, columnNum, AstType.FUNCTION_CALL_EXPRESSION, preComments, postComments);
    this.prefix = prefix;
    this.expression = expression;
    this.arguments = arguments;

    if (originalName != null || !(expression instanceof IdentifierExpression)) {
      this.originalName = originalName;
    } else {
      this.originalName = ((IdentifierExpression) expression).id;
    }

    expression.setParentAst(this);
    for (Expression arg : this.arguments) {
      arg.setParentAst(this);
    }
  }

  public FunctionCallExpression(int lineNum, int columnNum, Nullable<Expression> prefix,
      Expression expression, List<Expression> arguments, String originalName) {
    super(lineNum, columnNum, AstType.FUNCTION_CALL_EXPRESSION);
    this.prefix = prefix;
    this.expression = expression;
    this.arguments = arguments;

    if (originalName != null || !(expression instanceof IdentifierExpression)) {
      this.originalName = originalName;
    } else {
      this.originalName = ((IdentifierExpression) expression).id;
    }

    expression.setParentAst(this);
    for (Expression arg : this.arguments) {
      arg.setParentAst(this);
    }
  }

  public FunctionCallExpression(Nullable<Expression> prefix, Expression expression,
      List<Expression> arguments, String originalName) {
    super(AstType.FUNCTION_CALL_EXPRESSION);
    this.prefix = prefix;
    this.expression = expression;
    this.arguments = arguments;

    if (originalName != null || !(expression instanceof IdentifierExpression)) {
      this.originalName = originalName;
    } else {
      this.originalName = ((IdentifierExpression) expression).id;
    }

    expression.setParentAst(this);
    for (Expression arg : this.arguments) {
      arg.setParentAst(this);
    }
  }

  public void setCalledType(FunctionType type) {
    calledType = Nullable.of(type);
  }

  public void setIsDestructorCall(boolean val) {
    isDestructorCall = val;
  }

  /**
   * Transform "proper" form into the form we actually want and need. Call this
   * during initial parsing phase
   * 
   * ({(<a.fn0>).b.fn1}) ({(<a><fn0>).b}{fn1})
   * 
   * @return
   */
  public FunctionCallExpression flattenDot() {
    if (expression instanceof DotExpression) {

      Expression newLeft = ((DotExpression) expression).left;
      if (newLeft instanceof FunctionCallExpression) {
        newLeft = ((FunctionCallExpression) newLeft).flattenDot();
      }

      FunctionCallExpression fnCall = new FunctionCallExpression(lineNum, columnNum, preComments,
          postComments, Nullable.of(newLeft), ((DotExpression) expression).right, arguments,
          originalName);
      fnCall.setOriginalLabel(fnCall.label);
      return fnCall;
    } else {
      return this;
    }
  }

  @Override
  public String toString(String indent) {
    String result = preToString();

    if (prefix.isNotNull()) {
      result += prefix.get().toString(indent) + ".";
    }

    result += expression.toString(indent) + "(";

    boolean first = true;
    for (Expression arg : arguments) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += arg;
    }

    return result + ")" + postToString();
  }

  private static class TypecheckPrefixRet {
    final Type leftType;
    final TypeEnvironment argsTenv;
    final TypeEnvironment bodyTenv;

    TypecheckPrefixRet(Type leftType, TypeEnvironment argsTenv, TypeEnvironment bodyTenv) {
      this.leftType = leftType;
      this.argsTenv = argsTenv;
      this.bodyTenv = bodyTenv;
    }
  }

  private Optional<TypecheckPrefixRet> typecheckPrefix(TypeEnvironment tenv)
      throws FatalMessageException {
    if (prefix.isNull()) {
      return Optional.of(new TypecheckPrefixRet(tenv.ofDeclType, tenv, tenv));
    }
    Expression prefixExpr = prefix.get();
    Optional<TypecheckRet> typeOpt = prefixExpr.typecheck(tenv, Nullable.empty());
    if (!typeOpt.isPresent()) {
      return Optional.empty();
    }
    Type prefixType = typeOpt.get().type;
    if (prefixType instanceof ReferenceType) {
      prefixType = ((ReferenceType) prefixType).innerType;
    }

    if (prefixType instanceof ArrayType) {
      return Optional
          .of(new TypecheckPrefixRet(prefixType, tenv, ((ArrayType) prefixType).getTenv(tenv,
              lineNum, columnNum).get()));
    }

    if (prefixType instanceof MapType) {
      return Optional
          .of(new TypecheckPrefixRet(prefixType, tenv, ((MapType) prefixType).getTenv(tenv,
              lineNum, columnNum).get()));
    }

    if (!(prefixType instanceof DotAccessType)) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          ". expression left side is not a dot-access type: " + prefixType, lineNum, columnNum);
      return Optional.empty();
    }
    Optional<TypeEnvironment> newTenv = ((DotAccessType) prefixType).getTenv(tenv, lineNum, columnNum);
    if (!newTenv.isPresent()) {
      return Optional.empty();
    }

    return Optional.of(new TypecheckPrefixRet(prefixType, tenv, newTenv.get()));
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckPrefixRet> leftTy = typecheckPrefix(tenv);
    if (!leftTy.isPresent()) {
      return Optional.empty();
    }

    Optional<Type> optTy = typecheck(leftTy.get().argsTenv, leftTy.get().bodyTenv,
        leftTy.get().leftType);
    if (!optTy.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(new TypecheckRet(optTy.get()));
  }

  public Optional<Type> typecheck(TypeEnvironment argsTenv, TypeEnvironment bodyTenv, Type leftTy)
      throws FatalMessageException {

    if (!(leftTy instanceof DotAccessType)) {
      argsTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Left side of function call cannot be: " + leftTy, lineNum, columnNum);
      return Optional.empty();
    }
    DotAccessType tmpWithin = (DotAccessType) leftTy;

    List<Type> argTypes = new ArrayList<>();
    boolean hitErr = false;
    for (Expression arg : arguments) {
      Optional<TypecheckRet> tmp = arg.typecheck(argsTenv, Nullable.empty());
      if (tmp.isPresent()) {
        argTypes.add(tmp.get().type);
      } else {
        hitErr = true;
      }
    }
    if (hitErr) {
      return Optional.empty();
    }

    // sanbox check it is id
    Nullable<SandboxModeInfo> sandboxModeInfo = argsTenv.inSandboxMode();
    if (sandboxModeInfo.isNotNull()) {
      if (!(expression instanceof IdentifierExpression)) {
        argsTenv.msgState.addMessage(
            MsgType.ERROR, MsgClass.SANDBOX, "in sandbox mode (" + sandboxModeInfo
                + "), function calls can only be done with" + "identifiers, not: " + expression,
            lineNum, columnNum);
        return Optional.empty();
      }
    }

    Type exprType = null;
    String fnName = "";
    // only identifiers are allowed to have overloads
    if (expression instanceof IdentifierExpression) {
      IdentifierExpression idExpr = (IdentifierExpression) expression;
      fnName = idExpr.id;
      
      Optional<TypeBox> tyBoxOpt = idExpr.typecheckAsFunction(bodyTenv, tmpWithin, argTypes);
      if (!tyBoxOpt.isPresent()) {
        return Optional.empty();
      }
      TypeBox tyBox = tyBoxOpt.get();
      exprType = tyBox.getType();
      
      // sandbox check is not lambda
      if (sandboxModeInfo.isNotNull()) {
        if (exprType instanceof FunctionType) {
          FunctionType fnType = (FunctionType) exprType;
          if (!fnType.lambdaStatus.isNonLambda()) {
            argsTenv.msgState.addMessage(MsgType.ERROR, MsgClass.SANDBOX,
                "Cannot call lambda function in sandbox mode (" + sandboxModeInfo + ")", lineNum,
                columnNum);
            return Optional.empty();
          }
        }
        // if not a function type, then will error out below
      }

    } else if (expression instanceof ParentCallExpression) {
      ParentCallExpression parentExpr = (ParentCallExpression) expression;
      TypeBox tyBox = parentExpr.typecheckAsFunction(argsTenv, bodyTenv, argTypes);
      exprType = tyBox.getType();

    } else {
      Optional<TypecheckRet> exprTypeOpt = expression.typecheck(bodyTenv, Nullable.empty());
      if (!exprTypeOpt.isPresent()) {
        return Optional.empty();
      }
      exprType = exprTypeOpt.get().type;
    }
    
    // check if this is a constructor (will only happen on first typecheck, before resolveUserTypes)
    if (exprType instanceof ClassDeclType) {
      ClassDeclType declType = (ClassDeclType) exprType;
      ClassType classType = (ClassType) declType.asInstanceType();
      // this is a constructor
      IdentifierExpression constructorName = new IdentifierExpression(lineNum, columnNum,
          new LinkedList<>(),
          new LinkedList<>(), "constructor");
      constructorName.setOriginalLabel(constructorName.label);
      
      FunctionCallExpression fnCall = new FunctionCallExpression(lineNum, columnNum,
          new LinkedList<>(),
          new LinkedList<>(), Nullable.empty(), constructorName, arguments, originalName);
      fnCall.setOriginalLabel(getOriginalLabel());
      
      NewClassInstanceExpression constructorCall = new NewClassInstanceExpression(
          lineNum, columnNum,
          preComments, postComments, classType, classType.toString(),
          fnCall);
      constructorCall.setOriginalLabel(getOriginalLabel());
      
      Optional<TypecheckRet> typeRetOpt = constructorCall.typecheck(argsTenv, Nullable.empty());
      if (!typeRetOpt.isPresent()) {
        return Optional.empty();
      }
      
      calledType = fnCall.calledType;
      determinedType = typeRetOpt.get().type; // ClassType
    } else if (exprType instanceof FunctionType) {
      calledType = Nullable.of((FunctionType) exprType);
      // add to call graph
      if (calledType.get().lambdaStatus.isNonLambda() && !fnName.isEmpty()) {
        // add to the call graph
        Nullable<Function> withinFn = withinFunction();
        // might be inside a lambda inside a function, in which case we can't be certain of the
        // call graph since the lambda can move around. In case of sandbox, this is not an
        // issue since lambdas are not allowed
        // after building the graph and finding all sandbox-related functions, a second pass
        // is needed to ensure none of those functions contain lambdas
        if (withinFn.isNotNull()) {
          argsTenv.callGraph.addLink(withinFn.get().name, withinFn.get().fullType,
              fnName, calledType.get(), sandboxModeInfo.isNotNull());
        }
      }
      determinedType = calledType.get().returnType;
    } else {
      argsTenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "expression did not evalute to closure: " + exprType, lineNum, columnNum);
      return Optional.empty();
    }
    
    return Optional.of(determinedType);
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    return interp(env, env);
  }

  public Value interp(Environment argsEnv, Environment bodyEnv)
      throws InterpError, FatalMessageException {
    List<Value> argValues = new ArrayList<Value>();
    for (Expression arg : arguments) {
      argValues.add(arg.interp(argsEnv).copy(null));
    }

    Value exprValue = null;
    // only identifiers are allowed to have overloads
    if (expression instanceof IdentifierExpression) {
      exprValue = ((IdentifierExpression) expression).interpAsFunction(bodyEnv, argValues);
    } else if (expression instanceof ParentCallExpression) {
      return ((ParentCallExpression) expression).interpAsFunction(bodyEnv, argValues);
    } else {
      exprValue = expression.interp(bodyEnv);
    }
    if (!(exprValue instanceof ClosureValue)) {
      throw new InterpError("expression did not evaluate to closure value: " + exprValue, lineNum,
          columnNum);
    }

    ClosureValue fnVal = (ClosureValue) exprValue;
    return fnVal.interp(argValues).setMetaValue(MetaValue.NORMAL);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {

    AstProto.FunctionCallExpression.Builder subDoc = AstProto.FunctionCallExpression.newBuilder();

    subDoc.setExpression(expression.serialize());

    for (Expression arg : arguments) {
      subDoc.addArguments(arg.serialize());
    }

    if (calledType.isNull()) {
      // for ffi, this is also replaced when linked and typechecked, just put dummy
      // info
      calledType = Nullable.of(new FunctionType(Nullable.empty(), VoidType.Create(),
          new LinkedList<>(), LambdaStatus.LAMBDA));
      // throw new SerializationError("calledType cannot be null", true);
    }
    subDoc.setCalledType(calledType.get().serialize());
    if (originalName != null) {
      subDoc.addOriginalName(originalName);
    }

    subDoc.setIsDestructorCall(isDestructorCall);

    if (prefix.isNotNull()) {
      subDoc.addPrefix(prefix.get().serialize());
    }

    AstProto.Ast.Builder document = preSerialize();
    document.setFunctionCallExpression(subDoc);
    return document;
  }

  public static FunctionCallExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.FunctionCallExpression document) throws SerializationError {
    List<Expression> arguments = new LinkedList<Expression>();
    for (AstProto.Ast bvArg : document.getArgumentsList()) {
      arguments.add(AbstractExpression.deserialize(bvArg));
    }

    String originalName = null;
    if (document.getOriginalNameCount() > 0) {
      originalName = document.getOriginalName(0);
    }

    Nullable<Expression> prefix = Nullable.empty();
    if (document.getPrefixCount() > 0) {
      prefix = Nullable.of(AbstractExpression.deserialize(document.getPrefix(0)));
    }

    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, prefix, AbstractExpression.deserialize(document.getExpression()), arguments,
        originalName);
    value.calledType = Nullable
        .of((FunctionType) AbstractType.deserialize(document.getCalledType()));
    value.isDestructorCall = document.getIsDestructorCall();

    return value;
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    // new id, but expression is not always an id. Can only overload with an id
    Nullable<String> overloadRename = Nullable.empty();
    
    if (calledType.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
          "calledType must be determined before trying to rename ids (run typecheck): " + this,
          lineNum, columnNum);
    }

    String expressionId = "";
    if (expression instanceof IdentifierExpression) {
      expressionId = ((IdentifierExpression) expression).id;
      overloadRename = renamings.lookup(expressionId, calledType.get(), calledType.get().within);
    } else if (expression instanceof ParentCallExpression) {
      expressionId = ((ParentCallExpression) expression).id.idTypeName;
      overloadRename = renamings.lookup(expressionId, calledType.get(), calledType.get().within);
    }

    Expression newExpression = null;
    if (overloadRename.isNull()) {
      // not overloaded, so just do the normal thing
      newExpression = expression.renameIds(renamings, msgState);
    } else {
      /*
       * It's fine to replace ParentCall with Identifier since a rename can only mean
       * you are going for the compile
       *
       * Searching for "constructor*" won't find anything, and it makes more sense to
       * track the unique constructor name at this point
       */
      newExpression = new IdentifierExpression(lineNum, columnNum, new LinkedList<>(),
          new LinkedList<>(), overloadRename.get());
      newExpression.setOriginalLabel(newExpression.getLabel());
      newExpression.setDeterminedType(calledType.get()); // this was the overload's type
    }

    List<Expression> newArgs = new LinkedList<Expression>();
    for (Expression argument : arguments) {
      newArgs.add(argument.renameIds(renamings, msgState));
    }

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      newPrefix = Nullable.of(prefix.get().renameIds(renamings, msgState));
    }

    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, newExpression, newArgs, originalName);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    value.isDestructorCall = isDestructorCall;
    return value;
  }

  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Expression newExpression = null;

    // if part of a class, get the class header
    if (calledType.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE, "typecheck must run first", lineNum,
          columnNum);
    }

    if (calledType.get().within.instanceOf(ClassDeclType.class)) {
      if (expression instanceof IdentifierExpression) {
        if (((IdentifierExpression) expression).determinedType instanceof FunctionType) {
          // get class header and check vtable
          ClassDeclType withinClassDecl = (ClassDeclType) calledType.get().within.get();
          FunctionType fnTy = (FunctionType) ((IdentifierExpression) expression).determinedType;

          // find the correct vtables for this module
          Nullable<ModuleEndpoint> definingModEp = project
              .lookupModule(withinClassDecl.outerType.get(), project.getAuditTrail());
          if (definingModEp.isNull()) {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.VTABLE,
                "did not find module: " + withinClassDecl, this.lineNum, this.columnNum);
          }
          definingModEp.get().load();
          LoadedModuleHandle definingModHandle = ((LoadedModuleHandle) definingModEp.get()
              .getHandle());
          Vtables vtables = definingModHandle.moduleBuildObj.vtables;

          // if our new name/type is in vtable, then return a vtable access instead
          Nullable<Pair<VtableEntry, Integer>> vtableRes = vtables.searchVtable(withinClassDecl,
              msgState, originalName, fnTy, true);
          // if not in vtable, then no problem. Must not be an overridden function (by
          // name)
          if (vtableRes.isNotNull()) {
            // if it is in vtable, then replace with vtable access
            VtableAccessExpression vvalue = new VtableAccessExpression(lineNum, columnNum,
                preComments, postComments, vtableRes.get().b,
                (FunctionType) ((IdentifierExpression) expression).determinedType);
            vvalue.setOriginalLabel(vvalue.label);

            newExpression = vvalue;
          }
        }
      }
    }

    if (newExpression == null) {
      newExpression = (Expression) expression.replaceVtableAccess(project, msgState).get();
    }

    List<Expression> newArgs = new LinkedList<Expression>();
    for (Expression argument : arguments) {
      newArgs.add((Expression) argument.replaceVtableAccess(project, msgState).get());
    }

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      newPrefix = Nullable
          .of((Expression) prefix.get().replaceVtableAccess(project, msgState).get());
    }

    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, newExpression, newArgs, originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType;
    value.isDestructorCall = isDestructorCall;
    return Optional.of(value);
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    // ex: G.x then G is a free var but x is not
    Map<String, Type> freeVars = new HashMap<>();
    if (prefix.isNull()) {
      freeVars.putAll(expression.findFreeVariables());
    } else {
      freeVars.putAll(prefix.get().findFreeVariables());
    }
    for (Expression argument : arguments) {
      freeVars.putAll(argument.findFreeVariables());
    }
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof FunctionCallExpression)) {
      return false;
    }
    FunctionCallExpression ourOther = (FunctionCallExpression) other;

    if (ourOther.arguments.size() != arguments.size()) {
      return false;
    }
    Iterator<Expression> iterOtherA = ourOther.arguments.iterator();
    Iterator<Expression> iterUsA = arguments.iterator();
    while (iterOtherA.hasNext() && iterUsA.hasNext()) {
      if (!iterOtherA.next().compare(iterUsA.next())) {
        return false;
      }
    }

    return ourOther.expression.compare(expression);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      newArgs.add((Expression) arg.replace(target, with));
    }

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      newPrefix = Nullable.of((Expression) prefix.get().replace(target, with));
    }

    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, (Expression) expression.replace(target, with), newArgs,
        originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType;
    value.isDestructorCall = isDestructorCall;
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    for (Expression arg : arguments) {
      Nullable<Ast> tmp = arg.findByOriginalLabel(findLabel);
      if (tmp.isNotNull()) {
        return tmp;
      }
    }
    return expression.findByOriginalLabel(findLabel);
  }

  private Nullable<Statement> argToAnf(Expression arg, Ast newAst, List<Expression> newArgs) {
    Type argTy = newAst.getDeterminedType();
    if ((argTy instanceof ArrayType || argTy instanceof MapType || argTy instanceof UserInstanceType)
        && !(newAst instanceof IdentifierExpression || newAst instanceof DotExpression
            || newAst instanceof BracketExpression)) {
      argTy = argTy.setConst(true); // make it constant
      DeclarationStatement dval = new DeclarationStatement(lineNum, columnNum, new LinkedList<>(),
          new LinkedList<>(), argTy, "t_arg_" + getTemp(), Nullable.of((Expression) newAst), false,
          false);

      IdentifierExpression ival = new IdentifierExpression(lineNum, columnNum, new LinkedList<>(),
          new LinkedList<>(), dval.name);
      ival.determinedType = argTy;
      newArgs.add(ival);
      return Nullable.of(dval);
    } else {
      newArgs.add((Expression) newAst);
      return Nullable.empty();
    }
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    // assign map/array/class args to temp declarations, newArgs will only be
    // identifiers
    List<DeclarationStatement> insert = new LinkedList<>();

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      Pair<List<DeclarationStatement>, Ast> prefixPair = prefix.get().toAnf(tuning);
      insert.addAll(prefixPair.a);
      newPrefix = Nullable.of((Expression) prefixPair.b);
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      Pair<List<DeclarationStatement>, Ast> tmp = arg.toAnf(tuning);
      insert.addAll(tmp.a);
      Nullable<Statement> dval = argToAnf(arg, tmp.b, newArgs);
      if (dval.isNotNull()) {
        insert.add((DeclarationStatement) dval.get());
      }
    }
    Pair<List<DeclarationStatement>, Ast> exprPair = expression.toAnf(tuning);
    insert.addAll(exprPair.a);

    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, (Expression) exprPair.b, newArgs, originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType;
    value.isDestructorCall = isDestructorCall;
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      newPrefix = Nullable.of((Expression) prefix.get().replaceType(mapFromTo));
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      newArgs.add((Expression) arg.replaceType(mapFromTo));
    }
    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, (Expression) expression.replaceType(mapFromTo), newArgs,
        originalName);
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType.isNull() ? Nullable.empty()
        : Nullable.of((FunctionType) calledType.get().replaceType(mapFromTo));
    value.isDestructorCall = isDestructorCall;
    return value;
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      Expression oldPrefix = prefix.get();
      Optional<Nullable<Expression>> tmp = oldPrefix.normalizeType(keepCurrentModRelative,
          currentModule, effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newPrefix = tmp.get();
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      Optional<Nullable<Expression>> tmp = arg.normalizeType(keepCurrentModRelative, currentModule,
          effectiveImports, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      newArgs.add(tmp.get().get());
    }
    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, expression
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        newArgs, originalName);
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType; // should already be abs
    value.isDestructorCall = isDestructorCall;
    return Optional.of(Nullable.of(value));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    expression.addDependency(modDepGraph, ourModNode, tenv);
    for (Expression arg : arguments) {
      arg.addDependency(modDepGraph, ourModNode, tenv);
    }
  }

  @Override
  public boolean containsType(Set<Type> types) {
    for (Expression arg : arguments) {
      if (arg.containsType(types)) {
        return true;
      }
    }
    return expression.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    for (Expression arg : arguments) {
      if (arg.containsAst(asts)) {
        return true;
      }
    }
    return expression.containsAst(asts);
  }

  /*
   * preStatements will contain decls for our arguments as well as statements for
   * our expression, then the actual function call + assignment to vars that we
   * declared, then the conditional check for exc success/failure Our new ast will
   * be an identifier or multi expression with identifiers (minus the exc ret)
   */
  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    return resolveExceptions(project, currentModule, auditEntry, jsmntGlobalRenamings, withinFn,
        withinFnStatement, Nullable.empty(), enableResolution, hitExcId, msgState);
  }

  /*
   * We've prepended "jsmnt_global.ExceptionRef* pre_exc" to every function. On
   * failure:
   * 
   * fun int myfun(jsmnt_global.ExceptionRef* pre_exc) { fnCall(null);
   * print(fn2(null)); return 13; }
   * 
   * TO
   * 
   * fun int myfun(jsmnt_global.ExceptionRef* pre_exc) { jsmnt_global.ExceptionRef
   * tmp_pre_exc_0(); fnCall(&tmp_pre_exc_0); if (tmp_pre_exc_0.exc != null) {
   * pre_exc.exc = tmp_pre_exc_0.exc; //pre_exc.exc.bt.push(43) ... just need to
   * know rename for bt return 0; //<dummy values> }
   * 
   * jsmnt_global.ExceptionRef tmp_pre_exc_1(); tmp_re_1 = fn2(&tmp_pre_exc_1); if
   * (tmp_pre_exc_1.exc != null) { pre_exc.exc = tmp_pre_exc_1;
   * //pre_exc.bt.push(43) ... just need to know rename for bt return 0; //<dummy
   * values> } print(tmp_re_1);
   * 
   * return 13; }
   * 
   * We happen to know that pre_exc is unique since this is after renameId
   */
  public Optional<ResolveExcRet> resolveExceptions(Project project, ModuleType currentModule,
      AuditResolveExceptions auditEntry, RenamesOfInterest jsmntGlobalRenamings,
      Nullable<FunctionType> withinFn, boolean withinFnStatement,
      Nullable<Expression> withinConstructor, boolean enableResolution, String hitExcId,
      MsgState msgState) throws FatalMessageException {
    
    List<Statement> preStatements = new LinkedList<>();

    if (calledType.isNull()) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS,
          "calledType is null (must call typecheck before resolveExceptions)", lineNum, columnNum);
    }
    Type returnType = calledType.get().returnType;
    
    // check if this function call is inside a sandbox or sandboxed function
    if (!enableResolution) {
      if (withinConstructor.isNotNull()) {
        return Optional.of(new ResolveExcRet(withinConstructor.get()));
      }
      return Optional.of(new ResolveExcRet(this));
    }

    if (withinConstructor.isNotNull()) {
      returnType = withinConstructor.get().getDeterminedType();
    }

    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      Optional<ResolveExcRet> tmp = prefix.get().resolveExceptions(project, currentModule,
          auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
          0, msgState);
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      preStatements.addAll(tmp.get().preStatements);
      newPrefix = Nullable.of((Expression) tmp.get().ast.get());
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      ResolveExcRet tmp = arg.resolveExceptions(project, currentModule, auditEntry,
          jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, 0, msgState).get();
      preStatements.addAll(tmp.preStatements);
      Nullable<Statement> dval = argToAnf(arg, tmp.ast.get(), newArgs);
      if (dval.isNotNull()) {
        preStatements.add((DeclarationStatement) dval.get());
      }
    }

    ResolveExcRet tmpExpr = expression.resolveExceptions(project, currentModule, auditEntry,
        jsmntGlobalRenamings, withinFn, false, enableResolution, hitExcId, 0, msgState).get();
    preStatements.addAll(tmpExpr.preStatements);
    final Expression newExpr = (Expression) tmpExpr.ast.get();

    /*
     * Add our temp exception ref and init *fn_ctx, but only if fn_ctx is not null
     * 
     * FunctionCtx tmp_fn_ctx_base_0; FunctionCtx* tmp_fn_ctx_0 = null; if (fn_ctx
     * != null) { tmp_fn_ctx_base_0 = *fn_ctx; tmp_fn_ctx_0 = &tmp_fn_ctx_base_0; }
     * fncall(tmp_fn_ctx_0);
     */

    final String ourFnCtxBaseId = "tmp_" + JsmntGlobal.fnCtxId + "_base_" + getTemp();
    final String ourFnCtxPtrId = "tmp_" + JsmntGlobal.fnCtxId + "_" + getTemp();
    if (true) {
      preStatements.add(new DeclarationStatement(JsmntGlobal.fnCtxClassType, ourFnCtxBaseId,
          Nullable.empty(), false, false, JsmntGlobal.fnCtxClassTypeSizeof));
      preStatements.add(new DeclarationStatement(JsmntGlobal.fnCtxRefType, ourFnCtxPtrId,
          Nullable.of(new NullExpression()), false, false, 8));
      List<Statement> initOurFnCtxStatements = new LinkedList<>();
      initOurFnCtxStatements.add(new AssignmentStatement(new LvalueId(ourFnCtxBaseId),
          new DereferenceExpression(new IdentifierExpression(JsmntGlobal.fnCtxId))));
      initOurFnCtxStatements.add(new AssignmentStatement(new LvalueId(ourFnCtxPtrId),
          new ReferenceExpression(new IdentifierExpression(ourFnCtxBaseId))));
      preStatements
          .add(new ConditionalStatement(
              new BinaryExpression(BinOp.NOT_EQUAL, new IdentifierExpression(JsmntGlobal.fnCtxId),
                  new NullExpression()),
              new BlockStatement(initOurFnCtxStatements), Nullable.empty()));
    }

    /*
     * Pass in a reference to our exception ptr to replace the old first null
     * argument to our function call
     */

    if (!expression.toString().equals("print")) {
      //newArgs.remove(0);
      newArgs.add(0, new IdentifierExpression(ourFnCtxPtrId));
    }

    /*
     * Create temp variables to store our return(s), if any
     */

    List<Type> retTypes = new LinkedList<>();
    if (!withinFnStatement) {
      if (returnType instanceof VoidType) {
        // pass
      } else if (returnType instanceof MultiType) {
        retTypes.addAll(((MultiType) returnType).types);
      } else {
        retTypes.add(returnType);
      }
    }

    List<String> retDeclIds = new LinkedList<>();
    for (Type retType : retTypes) {
      String newDeclId = "tmp_re_" + getTemp();
      DeclarationStatement retDecl = new DeclarationStatement(retType, newDeclId, Nullable.empty(),
          false, false, 0);
      retDeclIds.add(newDeclId);

      preStatements.add(retDecl);
    }

    /*
     * Add the function call
     */

    if (enableResolution && calledType.get().lambdaStatus == LambdaStatus.LAMBDA) {
      if (calledType.get().lambdaStatus == LambdaStatus.UNKNOWN) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_EXCEPTIONS, "must be a lambda");
      }
      // before dereferencing, must ensure that the target is not null
      // if it is, then throw an exception
      Expression tmpFnExpr = newExpr;
      if (newPrefix.isNotNull()) {
        tmpFnExpr = new DotExpression(newPrefix.get(), (IdentifierExpression) newExpr);
      }
      preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings,
          hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
          new BinaryExpression(BinOp.EQUAL, tmpFnExpr, new NullExpression()), false, msgState));
    }

    FunctionCallExpression fnCall = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, newExpr, newArgs, originalName);
    fnCall.setOriginalLabel(getOriginalLabel());
    fnCall.determinedType = determinedType;
    fnCall.calledType = calledType;
    fnCall.isDestructorCall = isDestructorCall;

    if (expression.toString().equals("print")) {
      //FunctionCallStatement fnCallState = new FunctionCallStatement(fnCall);
      return Optional.of(new ResolveExcRet(preStatements, Nullable.of(fnCall)));
    }

    Nullable<Ast> retAst = Nullable.empty(); // the return values
    if (retDeclIds.isEmpty()) {
      FunctionCallStatement fnCallStatement = new FunctionCallStatement(fnCall);
      preStatements.add(fnCallStatement);
    } else {
      Lvalue lvalue = AbstractLvalue.fromIdList(lineNum, columnNum, retDeclIds, msgState);

      Expression sourceExpr = fnCall;
      if (withinConstructor.isNotNull()) {
        if (withinConstructor.instanceOf(NewClassInstanceExpression.class)) {
          NewClassInstanceExpression withinNewClass = (NewClassInstanceExpression) withinConstructor
              .get();
          NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
              preComments, postComments, withinNewClass.getName(), withinNewClass.originalName,
              fnCall);
          value.determinedType = withinConstructor.get().getDeterminedType();
          value.setOriginalLabel(withinConstructor.get().getOriginalLabel());
          sourceExpr = value;
        } else if (withinConstructor.instanceOf(NewExpression.class)) {

          if (enableResolution) {
            if (withinFn.isNotNull()) {
              ReferenceType refType = (ReferenceType) withinConstructor.get().getDeterminedType();
              preStatements.addAll(
                  ResolveRaiseException.sanityMemcheck(project,
                      new SizeofExpression(refType.innerType),
                      false, jsmntGlobalRenamings, hitExcId, withinFn.get(), msgState));
            }
          }

          NewExpression withinNew = (NewExpression) withinConstructor.get();
          NewClassInstanceExpression withinNewClass = (NewClassInstanceExpression) withinNew.value;
          NewClassInstanceExpression newClassVal = new NewClassInstanceExpression(lineNum,
              columnNum, preComments, postComments, withinNewClass.getName(),
              withinNewClass.originalName, fnCall);
          newClassVal.determinedType = withinConstructor.get().getDeterminedType();
          newClassVal.setOriginalLabel(withinConstructor.get().getOriginalLabel());

          NewExpression value = new NewExpression(newClassVal);
          value.determinedType = withinNew.determinedType;
          value.setOriginalLabel(withinNew.getOriginalLabel());
          sourceExpr = value;
        } else {
          throw new IllegalArgumentException();
        }
      }

      AssignmentStatement assignVal = new AssignmentStatement(lvalue, sourceExpr);

      preStatements.add(assignVal);

      Expression retExpr = AbstractExpression.fromIdList(lineNum, columnNum, retDeclIds, msgState);
      retAst = Nullable.of(retExpr);
    }

    if (enableResolution) {
      /*
       * Add the conditional statement
       *
       * { *pre_exc = tmp_exc_ptr_0; tmp_exc_ptr_0.bt.push(<fn id int>); return exc,
       * <dummy>; }
       */
      final List<Statement> statements = new LinkedList<>();
      /*
       * IdentifierExpression excBtId = new IdentifierExpression("bt"); //bt
       * DotExpression excDotBt = new DotExpression(excPtrIdentifier, excBtId);
       * //exc.bt IdentifierExpression pushId = new IdentifierExpression("push");
       * List<Expression> pushArgs = new LinkedList<>(); pushArgs.add(new
       * IntegerExpression("0")); FunctionCallExpression excBtPushFnCall = new
       * FunctionCallExpression(Nullable.of(excDotBt), pushId, pushArgs, "push");
       * //exc.bt.push(0) FunctionCallStatement excPushBt = new
       * FunctionCallStatement(excBtPushFnCall); //exc.bt.push(0);
       */
      // TODO (discover new name of bt and push) statements.add(excPushBt);

      AssignmentStatement assignPreExc = new AssignmentStatement(
          new LvalueDot(new IdentifierExpression(JsmntGlobal.fnCtxId), jsmntGlobalRenamings.exc),
          new DotExpression(new IdentifierExpression(ourFnCtxPtrId),
              new IdentifierExpression(jsmntGlobalRenamings.exc)));
      statements.add(assignPreExc);

      if (!hitExcId.isEmpty()) {
        // instead of returning, just set this variable to our exception that we hit
        statements.add(new AssignmentStatement(new LvalueId(hitExcId),
            new DotExpression(new IdentifierExpression(ourFnCtxPtrId),
                new IdentifierExpression(jsmntGlobalRenamings.exc))));
      } else {
        Nullable<Expression> retExcExpr = Nullable.empty();
        Type withinFnReturnType = withinFn.get().returnType;
        if (!(withinFnReturnType instanceof VoidType)) {
          // add dummy/null values
          retExcExpr = Nullable.of(withinFnReturnType.getDummyExpression(project, msgState));
        }
        ReturnStatement retStatement = new ReturnStatement(retExcExpr);
        statements.add(retStatement);
      }

      BlockStatement thenBlock = new BlockStatement(statements);
      NullExpression nullExpr = new NullExpression();
      BinaryExpression excCmp = new BinaryExpression(BinaryExpression.BinOp.NOT_EQUAL,
          new DotExpression(new IdentifierExpression(ourFnCtxPtrId),
              new IdentifierExpression(jsmntGlobalRenamings.exc)),
          nullExpr);
      ConditionalStatement condVal = new ConditionalStatement(excCmp, thenBlock, Nullable.empty());

      preStatements.add(condVal);
    }

    return Optional.of(new ResolveExcRet(preStatements, retAst));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      newPrefix = Nullable.of(prefix.get().insertDestructorCalls(scopedIds, destructorRenamings));
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      newArgs.add(arg.insertDestructorCalls(scopedIds, destructorRenamings));
    }
    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, expression.insertDestructorCalls(scopedIds, destructorRenamings),
        newArgs, originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType; // should already be abs
    value.isDestructorCall = isDestructorCall;
    return value;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      newPrefix = Nullable.of(prefix.get()
          .liftClassDeclarations(liftedClasses, replacements).get());
    }

    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      newArgs.add(arg
          .liftClassDeclarations(liftedClasses, replacements).get());
    }
    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, expression
            .liftClassDeclarations(liftedClasses, replacements).get(),
        newArgs, originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType; // should already be abs
    value.isDestructorCall = isDestructorCall;
    return Nullable.of(value);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      newArgs.add(arg.resolveUserTypes(msgState).get());
    }
    
    if (expression.getDeterminedType() instanceof ClassDeclType) {
      ClassDeclType exprType = (ClassDeclType) expression.getDeterminedType();
      ClassType classType = (ClassType) exprType.asInstanceType();
      // this is a constructor
      IdentifierExpression constructorName = new IdentifierExpression(lineNum, columnNum,
          new LinkedList<>(),
          new LinkedList<>(), "constructor");
      constructorName.setOriginalLabel(constructorName.label);
      
      FunctionCallExpression fnCall = new FunctionCallExpression(lineNum, columnNum, preComments,
          postComments, Nullable.empty(), constructorName, newArgs, "");
      fnCall.determinedType = determinedType; // already the ClassType
      fnCall.setOriginalLabel(getOriginalLabel());
      fnCall.calledType = calledType; // should already be abs
      
      NewClassInstanceExpression value = new NewClassInstanceExpression(lineNum, columnNum,
          preComments, postComments, classType, classType.toString(),
          fnCall);
      value.determinedType = determinedType;
      value.setOriginalLabel(getOriginalLabel());
      return Optional.of(value);
    }
    
    // not a constructor..just a regular call
    Nullable<Expression> newPrefix = prefix;
    if (prefix.isNotNull()) {
      Expression prefixExpr = prefix.get();
      
      // check if prefix is a static access
      if (!(prefixExpr instanceof StaticClassIdentifierExpression)) {
        if (prefixExpr.getDeterminedType() instanceof UserDeclType) {
          UserDeclType prefixType = (UserDeclType) prefixExpr.getDeterminedType();
          
          List<String> idList = new LinkedList<>();
          if (prefixExpr instanceof IdentifierExpression) {
            idList.add(((IdentifierExpression) prefixExpr).id);
          } else if (prefixExpr instanceof DotExpression) {
            Optional<List<String>> tmpList = ((DotExpression) prefixExpr).toIdList();
            if (!tmpList.isPresent()) {
              msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_USER_TYPES,
                  "prefix is static type " + prefixType
                    + " dot expr but did not give id list", lineNum, columnNum);
              return Optional.empty();
            }
            idList = tmpList.get();
          } else {
            msgState.addMessage(MsgType.INTERNAL, MsgClass.RESOLVE_USER_TYPES,
                "prefix is static " + prefixType
                  + " but is not id or dot expression", lineNum, columnNum);
            return Optional.empty();
          }
          
          UserDeclType asType = prefixType.fromList(idList);
          
          StaticClassIdentifierExpression staticLeft = new StaticClassIdentifierExpression(
              lineNum, columnNum,
              new LinkedList<>(), new LinkedList<>(), asType);
          staticLeft.determinedType = asType;
          staticLeft.setOriginalLabel(staticLeft.label);
          
          newPrefix = Nullable.of(staticLeft);
        } else {
          newPrefix = Nullable.of(prefix.get().resolveUserTypes(msgState).get());
        }
      }
    }

    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, expression.resolveUserTypes(msgState).get(),
        newArgs, originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType; // should already be abs
    value.isDestructorCall = isDestructorCall;
    
    return Optional.of(value);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    List<Expression> newArgs = new LinkedList<>();
    for (Expression arg : arguments) {
      newArgs.add(arg.addThisExpression(classHeader));
    }
    
    Nullable<Expression> newPrefix = prefix;
    Expression newExpression = expression;
    if (prefix.isNotNull()) {
      newPrefix = Nullable.of(prefix.get().addThisExpression(classHeader));
    } else {
      newExpression = expression.addThisExpression(classHeader);
    }
    
    FunctionCallExpression value = new FunctionCallExpression(lineNum, columnNum, preComments,
        postComments, newPrefix, newExpression, newArgs, originalName);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    value.calledType = calledType; // should already be abs
    value.isDestructorCall = isDestructorCall;
    return value;
  }

}
