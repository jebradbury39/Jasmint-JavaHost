package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MemValue;
import interp.ReferenceValue;
import interp.Value;
import interp.ValueBox;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.BasicTypeBox;
import type_env.TypeBox;
import type_env.TypeEnvironment;
import typecheck.ClassDeclType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import util.Pair;

public class LvalueDot extends AbstractLvalue {

  public final String id;
  public final Expression left;

  public LvalueDot(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression left, String id) {
    super(lineNum, columnNum, AstType.DOT_LVALUE, preComments, postComments);
    this.id = id;
    this.left = left;

    left.setParentAst(this);
  }

  public LvalueDot(int lineNum, int columnNum, Expression left, String id) {
    super(lineNum, columnNum, AstType.DOT_LVALUE);
    this.id = id;
    this.left = left;

    left.setParentAst(this);
  }

  public LvalueDot(Expression left, String id) {
    super(AstType.DOT_LVALUE);
    this.id = id;
    this.left = left;

    left.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + left.toString(indent) + "." + id + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> opt = left.typecheck(tenv, Nullable.empty());
    if (!opt.isPresent()) {
      return opt;
    }
    Type leftType = opt.get().type;

    if (leftType.getIsConst()) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          "left side of lvalue.id is const", lineNum, columnNum);
    }

    if (leftType instanceof ReferenceType) {
      leftType = ((ReferenceType) leftType).innerType;
    }

    if (leftType instanceof UserInstanceType || leftType instanceof UserDeclType) {
      // class instance/static or module
      if (leftType instanceof ModuleType || leftType instanceof EnumDeclType || leftType instanceof EnumType) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            ". expression left side is not a class (" + leftType + "): " + left, lineNum,
            columnNum);
        return Optional.empty();
      }
    } else {
      // not allowed to have id assignment
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          ". expression left side is not a class/module (" + leftType + "): " + left, lineNum,
          columnNum);
      return Optional.empty();
    }

    // get the class tenv
    TypeEnvironment classTenv = null;
    if (leftType instanceof UserInstanceType) {
      UserInstanceType userLeftType = (UserInstanceType) leftType;
      Optional<TypeEnvironment> classTenvOpt = userLeftType.getTenv(tenv, lineNum, columnNum);
      if (!classTenvOpt.isPresent()) {
        return Optional.empty();
      }
      classTenv = classTenvOpt.get();
    } else {
      UserDeclType userLeftType = (UserDeclType) leftType;
      Optional<TypeEnvironment> classTenvOpt = userLeftType.getTenv(tenv, lineNum, columnNum);
      if (!classTenvOpt.isPresent()) {
        return Optional.empty();
      }
      classTenv = classTenvOpt.get();
    }

    Nullable<TypeBox> tyBox = classTenv.lookup(id, lineNum, columnNum);
    if (tyBox.instanceOf(BasicTypeBox.class)) {
      checkVisibilityLevel(classTenv, id, (BasicTypeBox) tyBox.get(), tenv.msgState);
    }

    // set init on the class header members
    tenv.project.addInitClassMember((ClassDeclType) classTenv.ofDeclType, id);

    if (tyBox.get().getType().getIsConst()) {
      if (!(!tyBox.get().getIsInit() && withinConstructor())) {
        tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
            "right side of lvalue.id is const", lineNum, columnNum);
      }
    }
    tyBox.get().setIsInit(true);
    determinedType = tyBox.get().getType();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value leftValue = left.interp(env);

    if (leftValue instanceof ReferenceValue) {
      leftValue = ((ReferenceValue) leftValue).referenced.getValue();
    }

    ValueBox valueBox = leftValue.lookup(id, env);

    if (valueBox == null) {
      throw new InterpError("field [" + id + "] was not found", lineNum, columnNum);
    }
    return new MemValue(determinedType, valueBox);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LvalueDot.Builder subDoc = AstProto.LvalueDot.newBuilder();

    subDoc.setId(id);
    subDoc.setLeft(left.serialize());

    document.setLvalueDot(subDoc);
    return document;
  }

  public static LvalueDot deserialize(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, AstProto.LvalueDot document) throws SerializationError {
    return new LvalueDot(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getLeft()), document.getId());
  }

  @Override
  public Expression toExpression() {
    return new DotExpression(lineNum, columnNum, preComments, postComments, left,
        new IdentifierExpression(lineNum, columnNum, preComments, postComments, id));
  }

  @Override
  public Lvalue renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {

    Type leftTy = left.getDeterminedType();
    if (leftTy instanceof ReferenceType) {
      leftTy = ((ReferenceType) leftTy).innerType;
    }
    
    if (!(leftTy instanceof UserInstanceType || leftTy instanceof UserDeclType)) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
          "Expected leftTy to be a UserType, not " + leftTy, lineNum, columnNum);
    }
    if (leftTy instanceof ModuleType) {
      msgState.addMessage(MsgType.INTERNAL, MsgClass.RENAMINGS,
          "Expected leftTy to be a class type, not " + leftTy, lineNum, columnNum);
    }
    
    UserDeclType declLeftTy = null;
    if (leftTy instanceof UserInstanceType) {
      declLeftTy = ((UserInstanceType) leftTy).asDeclType();
    } else if (leftTy instanceof UserDeclType) {
      declLeftTy = (UserDeclType) leftTy;
    }

    String newId = id;
    Nullable<String> tmpNewId = renamings.lookup(id, Nullable.of(determinedType),
        Nullable.of(declLeftTy));
    if (tmpNewId.isNotNull()) {
      newId = tmpNewId.get();
    }

    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        left.renameIds(renamings, msgState), newId);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return left.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LvalueDot)) {
      return false;
    }
    LvalueDot ourOther = (LvalueDot) other;
    return left.compare(ourOther.left) && id.equals(ourOther.id);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        (Expression) left.replace(target, with), id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return left.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    Pair<List<DeclarationStatement>, Ast> leftPair = left.toAnf(tuning);
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        (Expression) leftPair.b, id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(leftPair.a, value);
  }

  @Override
  public Lvalue replaceType(Map<Type, Type> mapFromTo) {

    Expression newExpr = (Expression) left.replaceType(mapFromTo);

    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments, newExpr, id);
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        (Expression) left.replaceVtableAccess(project, msgState).get(), id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Lvalue normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        left.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get(),
        id);
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    left.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return left.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return left.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = left.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    final Expression newLeft = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      if (left.getDeterminedType() instanceof ReferenceType) {
        // insert null check
        preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
            jsmntGlobalRenamings,
            hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
            new BinaryExpression(BinOp.EQUAL, newLeft, new NullExpression()), false,
            msgState));
      }
    }

    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments, newLeft, id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        left.liftClassDeclarations(liftedClasses, replacements).get(),
        id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  private Optional<List<String>> toIdList() {
    List<String> idList = new LinkedList<>();
    if (left instanceof IdentifierExpression) {
      idList.add(((IdentifierExpression) left).id);
    } else if (left instanceof DotExpression) {
      Optional<List<String>> tmp = ((DotExpression) left).toIdList();
      if (!tmp.isPresent()) {
        return Optional.empty();
      }
      idList.addAll(tmp.get());
    } else {
      return Optional.empty();
    }

    idList.add(id);

    return Optional.of(idList);
  }
  
  @Override
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    // check if this is the leaf of a static class access
    if (!(left instanceof StaticClassIdentifierExpression)) {
      if (left.getDeterminedType() instanceof UserDeclType && !(determinedType instanceof UserDeclType)) {
        UserDeclType leftType = (UserDeclType) left.getDeterminedType();
        
        // now ensure that this is all a string of identifiers
        Optional<List<String>> idList = toIdList();
        if (!idList.isPresent()) {
          return Optional.empty();
        }
        // remove last id (which would be this.id)
        idList.get().remove(idList.get().size() - 1);
        UserDeclType asType = leftType.fromList(idList.get());
        
        StaticClassIdentifierExpression staticLeft = new StaticClassIdentifierExpression(
            lineNum, columnNum,
            preComments, postComments, asType);
        staticLeft.determinedType = asType;
        staticLeft.setOriginalLabel(staticLeft.label);
        
        LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
            staticLeft,
            id);
        value.determinedType = determinedType;
        value.setOriginalLabel(getOriginalLabel());
        return Optional.of(value);
      }
    }
    
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        left.resolveUserTypes(msgState).get(),
        id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Lvalue addThisExpression(ClassHeader classHeader) {
    LvalueDot value = new LvalueDot(lineNum, columnNum, preComments, postComments,
        left.addThisExpression(classHeader), id);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }
}
