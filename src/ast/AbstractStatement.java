package ast;

import astproto.AstProto;
import java.util.LinkedList;
import java.util.List;
import typecheck.AbstractType;
import typecheck.Type;
import util.Pair;

public abstract class AbstractStatement extends AbstractAst implements Statement {

  public AbstractStatement(int lineNum, int columnNum, AstType astType,
      List<CommentAttr> preComments,
      List<CommentAttr> postComments) {
    super(lineNum, columnNum, astType, preComments, postComments);
  }
  
  public AbstractStatement(int lineNum, int columnNum, AstType astType) {
    super(lineNum, columnNum, astType);
  }
  
  public AbstractStatement(AstType astType) {
    super(astType);
  }

  public static Statement deserialize(AstProto.Ast document) throws SerializationError {
    final int deLineNum = document.getLineNum();
    final int deColumnNum = document.getColNum();
    final Type deDeterminedType = AbstractType.deserialize(document.getDeterminedType());
    final long originalLabel = document.getLabel();
    final List<CommentAttr> preComments = new LinkedList<>();
    final List<CommentAttr> postComments = new LinkedList<>();
    
    Statement value = null;
    Pair<AstType, Long> tmp = AbstractAst.preDeserialize(document, preComments, postComments);
    AstType astType = tmp.a;
    long ordering = tmp.b;
    switch (astType) {
      case ASSIGNMENT_STATEMENT:
        value = AssignmentStatement.deserialize(deLineNum, deColumnNum,
            preComments, postComments,
            document.getAssignmentStatement());
        break;
      case BLOCK_STATEMENT:
        value = BlockStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getBlockStatement());
        break;
      case BREAK_STATEMENT:
        value = BreakStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getBreakStatement());
        break;
      case CONDITIONAL_STATEMENT:
        value = ConditionalStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getConditionalStatement());
        break;
      case CONTINUE_STATEMENT:
        value = ContinueStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getContinueStatement());
        break;
      case FUNCTION:
        value = Function.deserialize(deLineNum, deColumnNum, preComments, postComments, ordering,
            document.getFunction());
        break;
      case DECLARATION_STATEMENT:
        value = DeclarationStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getDeclarationStatement());
        break;
      case DELETE_STATEMENT:
        value = DeleteStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getDeleteStatement());
        break;
      case FOR_STATEMENT:
        value = ForStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getForStatement());
        break;
      case FUNCTION_CALL_STATEMENT:
        value = FunctionCallStatement.deserialize(deLineNum, deColumnNum,
            preComments, postComments,
            document.getFunctionCallStatement());
        break;
      case RETURN_STATEMENT:
        value = ReturnStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getReturnStatement());
        break;
      case SWITCH_CASE_STATEMENT:
        value = SwitchCaseStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getSwitchCaseStatement());
        break;
      case SWITCH_STATEMENT:
        value = SwitchStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getSwitchStatement());
        break;
      case WHILE_STATEMENT:
        value = WhileStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getWhileStatement());
        break;
      case IMPORT_STATEMENT:
        value = ImportStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getImportStatement());
        break;
      case MODULE_STATEMENT:
        value = ModuleStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getModuleStatement());
        break;
      case SUBMOD_STATEMENT:
        value = SubmodStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getSubmodStatement());
        break;
      case FFI_STATEMENT:
        value = FfiStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getFfiStatement());
        break;
      case EXPRESSION_STATEMENT:
        value = ExpressionStatement.deserialize(deLineNum, deColumnNum,
            preComments, postComments,
            document.getExpressionStatement());
        break;
      case COMMENT_STATEMENT:
        value = CommentStatement.deserialize(deLineNum, deColumnNum, preComments, postComments,
            document.getCommentStatement());
        break;
      case CLASS_DECLARATION:
        value = ClassDeclaration.deserialize(deLineNum, deColumnNum, preComments, postComments,
            ordering, document.getClassDeclaration());
        break;
      default:
        break;
    }
    
    if (value == null) {
      throw new SerializationError(
          "Unable to deserialize statement astType[" + astType + "]", false);
    }
    
    ((AbstractAst) value).determinedType = deDeterminedType;
    ((AbstractAst) value).setOriginalLabel(originalLabel);
    return value;
  }
}
