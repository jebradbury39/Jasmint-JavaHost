package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import interp.VoidValue;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.JsmntGlobal;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.BoolType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ModuleType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typecheck.VoidType;
import typegraph.TypeGraph.CastDirection;
import util.InsertDestructorsRet;
import util.Pair;
import util.ScopedIdentifiers;
import util.ScopedIdentifiers.ScopeFilter;

public class WhileStatement extends AbstractStatement {

  public final Expression guard;
  public final BlockStatement body;

  public WhileStatement(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression guard, BlockStatement body) {
    super(lineNum, columnNum, AstType.WHILE_STATEMENT, preComments, postComments);
    this.guard = guard;
    this.body = body;

    guard.setParentAst(this);
    body.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + "while (" + guard.toString(indent) + ") " + body.toString(indent)
        + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> guardType = guard.typecheck(tenv, Nullable.empty());
    if (guardType.isPresent()) {
      if (!tenv.typeGraph.canCastTo(guardType.get().type, CastDirection.UP, BoolType.Create())) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "while statement guard type [" + guardType + "] is not bool", lineNum, columnNum);
      }
    }

    Optional<TypecheckRet> bodyType = body.typecheck(tenv.extend(), expectedReturnType);
    if (!bodyType.isPresent()) {
      return Optional.empty();
    }

    determinedType = VoidType.Create();
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    while (guard.interp(env).toBool()) {
      Value bodyVal = body.interp(env.extend());
      boolean breakloop = false;
      switch (bodyVal.getMetaValue()) {
        case BREAK:
          breakloop = true;
          break;
        case RETURN:
          return bodyVal;
        default:
          break;
      }
      if (breakloop) {
        break;
      }
    }
    return new VoidValue();
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.WhileStatement.Builder subDoc = AstProto.WhileStatement.newBuilder();

    subDoc.setGuard(guard.serialize());
    subDoc.setBody(body.serialize());

    document.setWhileStatement(subDoc);
    return document;
  }

  public static WhileStatement deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.WhileStatement document) throws SerializationError {
    return new WhileStatement(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getGuard()),
        (BlockStatement) AbstractStatement.deserialize(document.getBody()));
  }

  @Override
  public Statement renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    Expression newGuard = guard.renameIds(renamings, msgState);
    BlockStatement newBody = (BlockStatement) body.renameIds(renamings, msgState);
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        newGuard, newBody);
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    Map<String, Type> freeVars = body.findFreeVariables();
    freeVars.putAll(guard.findFreeVariables());
    return freeVars;
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof WhileStatement)) {
      return false;
    }
    WhileStatement ourOther = (WhileStatement) other;
    return ourOther.guard.compare(guard) && ourOther.body.compare(body);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }

    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        (Expression) guard.replace(target, with), (BlockStatement) body.replace(target, with));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    Nullable<Ast> tmp = guard.findByOriginalLabel(findLabel);
    if (tmp.isNotNull()) {
      return tmp;
    }
    return body.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> guardPair = guard.toAnf(tuning);
    insert.addAll(guardPair.a);
    Pair<List<DeclarationStatement>, Ast> bodyPair = body.toAnf(tuning);
    insert.addAll(bodyPair.a);

    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        (Expression) guardPair.b, (BlockStatement) bodyPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Statement replaceType(Map<Type, Type> mapFromTo) {
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        (Expression) guard.replaceType(mapFromTo), (BlockStatement) body.replaceType(mapFromTo));
    value.determinedType = determinedType.replaceType(mapFromTo);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        (Expression) guard.replaceVtableAccess(project, msgState).get(),
        (BlockStatement) body.replaceVtableAccess(project, msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Optional<Statement> normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        guard.normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get(),
        (BlockStatement) body
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get());
    value.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    guard.addDependency(modDepGraph, ourModNode, tenv);
    body.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return guard.containsType(types) || body.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return guard.containsAst(asts) || body.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution, String hitExcId, int stackSize,
      MsgState msgState)
      throws FatalMessageException {

    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = guard.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    Expression newGuard = (Expression) tmp.get().ast.get();
    if (!hitExcId.isEmpty()) {
      newGuard = new BinaryExpression(BinOp.AND, newGuard, new BinaryExpression(BinOp.EQUAL,
          new IdentifierExpression(hitExcId), new NullExpression()));
    }

    List<Statement> statements = new LinkedList<>();
    if (enableResolution) {
      List<Type> sanityChecksTypes = new LinkedList<>();
      sanityChecksTypes.add(JsmntGlobal.fnCtxRefType);
      List<Expression> sanityChecksArgs = new LinkedList<>();
      FunctionCallExpression sanityChecksCall = new FunctionCallExpression(
          Nullable.of(new IdentifierExpression(JsmntGlobal.modType.name)),
          new IdentifierExpression(Program.sanityCheckTimeoutFnName), sanityChecksArgs,
          Program.sanityCheckTimeoutFnName);
      sanityChecksCall.calledType = Nullable.of(new FunctionType(JsmntGlobal.modType,
          VoidType.Create(), sanityChecksTypes, LambdaStatus.NOT_LAMBDA));

      statements.add(new FunctionCallStatement(sanityChecksCall));
    }
    statements.addAll(body.statements);

    tmp = new BlockStatement(statements).resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId,
        stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    BlockStatement newBody = (BlockStatement) tmp.get().ast.get();

    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        newGuard, newBody);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public InsertDestructorsRet insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    ScopedIdentifiers ourScope = scopedIds.extend(ScopeFilter.LOOP);
    BlockStatement newBody = (BlockStatement) body
        .insertDestructorCalls(ourScope, destructorRenamings).statements.get(0);

    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments, guard,
        newBody);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());

    return new InsertDestructorsRet(value);
  }

  @Override
  public Nullable<Statement> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        guard.liftClassDeclarations(liftedClasses, replacements).get(),
        (BlockStatement) body.liftClassDeclarations(liftedClasses, replacements).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Nullable.of(value);
  }

  @Override
  public Optional<Statement> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        guard.resolveUserTypes(msgState).get(),
        (BlockStatement) body.resolveUserTypes(msgState).get());
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(value);
  }

  @Override
  public Statement addThisExpression(ClassHeader classHeader) {
    WhileStatement value = new WhileStatement(lineNum, columnNum, preComments, postComments,
        guard.addThisExpression(classHeader),
        (BlockStatement) body.addThisExpression(classHeader));
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

}
