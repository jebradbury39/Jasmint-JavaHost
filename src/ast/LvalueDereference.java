package ast;

import ast.BinaryExpression.BinOp;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.MemValue;
import interp.ReferenceValue;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import util.Pair;

public class LvalueDereference extends AbstractLvalue {
  public final Expression dereferenced;

  public LvalueDereference(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Expression dereferenced) {
    super(lineNum, columnNum, AstType.DEREFERENCE_LVALUE, preComments, postComments);
    this.dereferenced = dereferenced;
  }

  public LvalueDereference(int lineNum, int columnNum, Expression dereferenced) {
    super(lineNum, columnNum, AstType.DEREFERENCE_LVALUE);
    this.dereferenced = dereferenced;
  }

  public LvalueDereference(Expression dereferenced) {
    super(AstType.DEREFERENCE_LVALUE);
    this.dereferenced = dereferenced;
  }

  @Override
  public String toString(String indent) {
    return preToString() + "@" + dereferenced.toString(indent) + postToString();
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> derefTyOpt = dereferenced.typecheck(tenv, Nullable.empty());
    if (!derefTyOpt.isPresent()) {
      return Optional.empty();
    }
    Type derefTy = derefTyOpt.get().type;

    if (!(derefTy instanceof ReferenceType)) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          dereferenced + " is not a reference, so cannot dereference it", lineNum, columnNum);
    }

    if (derefTy.getIsConst()) {
      // if (!(!tyBox.getIsInit() && withinConstructor())) {
      tenv.msgState.addMessage(MsgType.INTERNAL, MsgClass.TYPECHECK,
          dereferenced + " is const and cannot be an lvalue", lineNum, columnNum);
      // }
    }
    // checkVisibilityLevel(id, tyBox);

    determinedType = ((ReferenceType) derefTy).innerType;
    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value derefVal = dereferenced.interp(env);

    if (!(derefVal instanceof ReferenceValue)) {
      throw new InterpError(
          dereferenced + " is not a reference, so cannot dereference: " + derefVal, lineNum,
          columnNum);
    }
    ReferenceValue rval = (ReferenceValue) derefVal;
    if (rval.referenced == null) {
      throw new InterpError(dereferenced + " references null. Cannot assign to null.", lineNum,
          columnNum);
    }

    return new MemValue(determinedType, rval.referenced);
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.LvalueDereference.Builder subDoc = AstProto.LvalueDereference.newBuilder();

    subDoc.setDereferenced(dereferenced.serialize());

    document.setLvalueDereference(subDoc);
    return document;
  }

  public static LvalueDereference deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.LvalueDereference document) throws SerializationError {
    return new LvalueDereference(lineNum, columnNum, preComments, postComments,
        AbstractExpression.deserialize(document.getDereferenced()));
  }

  @Override
  public Expression toExpression() {
    return new DereferenceExpression(lineNum, columnNum, preComments, postComments, dereferenced);
  }

  @Override
  public Lvalue renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    LvalueDereference value = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        dereferenced.renameIds(renamings, msgState));
    value.determinedType = determinedType.renameIds(renamings, msgState);
    value.setOriginalLabel(getOriginalLabel());
    return value;
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return dereferenced.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof LvalueDereference)) {
      return false;
    }
    return dereferenced.compare(((LvalueDereference) other).dereferenced);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    return this;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return dereferenced.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<DeclarationStatement>();

    Pair<List<DeclarationStatement>, Ast> dereferencedPair = dereferenced.toAnf(tuning);
    insert.addAll(dereferencedPair.a);

    LvalueDereference value = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        (Expression) dereferencedPair.b);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, value);
  }

  @Override
  public Lvalue replaceType(Map<Type, Type> mapFromTo) {
    LvalueDereference val = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        (Expression) dereferenced.replaceType(mapFromTo));
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    LvalueDereference val = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        (Expression) dereferenced.replaceVtableAccess(project, msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Lvalue normalizeType(boolean keepCurrentModRelative, ModuleType currentModule,
      EffectiveImports effectiveImports, MsgState msgState) throws FatalMessageException {
    LvalueDereference val = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        dereferenced
            .normalizeType(keepCurrentModRelative, currentModule, effectiveImports, msgState)
            .get().get());
    val.determinedType = determinedType
        .basicNormalize(keepCurrentModRelative, currentModule, effectiveImports, msgState).get().get();
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    dereferenced.addDependency(modDepGraph, ourModNode, tenv);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return dereferenced.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return dereferenced.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn,
      boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {
    List<Statement> preStatements = new LinkedList<>();

    Optional<ResolveExcRet> tmp = dereferenced.resolveExceptions(project,
        currentModule, auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement,
        enableResolution,
        hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    preStatements.addAll(tmp.get().preStatements);
    Expression newDereferenced = (Expression) tmp.get().ast.get();

    if (enableResolution) {
      // before dereferencing, must ensure that the target is not null
      // if it is, then throw an exception
      preStatements.add(ResolveRaiseException.raiseResolvedExcWithinCond(project,
          jsmntGlobalRenamings,
          hitExcId, withinFn.get(), jsmntGlobalRenamings.nullPtrExc,
          new BinaryExpression(BinOp.EQUAL, newDereferenced, new NullExpression()), false,
          msgState));
    }

    LvalueDereference value = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        newDereferenced);
    value.determinedType = determinedType;
    value.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(preStatements, Nullable.of(value)));
  }

  @Override
  public Nullable<Lvalue> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    LvalueDereference val = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        dereferenced.liftClassDeclarations(liftedClasses, replacements).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(this);
  }

  @Override
  public Optional<Lvalue> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    LvalueDereference val = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        dereferenced.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Lvalue addThisExpression(ClassHeader classHeader) {
    LvalueDereference val = new LvalueDereference(lineNum, columnNum, preComments, postComments,
        dereferenced.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }
}
