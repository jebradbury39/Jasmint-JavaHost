package ast;

import ast.Program.DestructorRenamings;
import ast.Program.RenamesOfInterest;
import astproto.AstProto;
import audit.AuditResolveExceptions;
import classgraph.ClassGraph;
import classgraph.Node;
import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import import_mgmt.EffectiveImports;
import interp.Environment;
import interp.InterpError;
import interp.Value;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import mbo.Renamings;
import mbo.TypeReplacements;
import multifile.Project;
import transform.ResolveExcRet;
import type_env.TypeEnvironment;
import typecheck.AbstractType;
import typecheck.FunctionType;
import typecheck.ModuleType;
import typecheck.NullType;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.TypecheckRet;
import typegraph.CastFilter;
import typegraph.TypeGraph.CastDirection;
import util.Pair;
import util.ScopedIdentifiers;

public class CastExpression extends AbstractExpression {

  public final Type target;
  public final Expression value;

  public CastExpression(int lineNum, int columnNum, List<CommentAttr> preComments,
      List<CommentAttr> postComments, Type target, Expression value) {
    super(lineNum, columnNum, AstType.CAST_EXPRESSION, preComments, postComments);
    this.target = target;
    this.value = value;

    value.setParentAst(this);
  }

  @Override
  public String toString(String indent) {
    return preToString() + "cast{" + target + "}(" + value.toString(indent) + ")" + postToString();
  }

  @Override
  public Expression renameIds(Renamings renamings, MsgState msgState) throws FatalMessageException {
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        target.renameIds(renamings, msgState), value.renameIds(renamings, msgState));
    val.determinedType = determinedType.renameIds(renamings, msgState);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<TypecheckRet> typecheck(TypeEnvironment tenv, Nullable<Type> expectedReturnType)
      throws FatalMessageException {
    Optional<TypecheckRet> optValueTy = value.typecheck(tenv, Nullable.empty());
    if (!optValueTy.isPresent()) {
      return Optional.empty();
    }
    Type valueTy = optValueTy.get().type;

    if (value instanceof NullType) {
      tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
          "Cannot cast null value to target type " + target, lineNum, columnNum);
      return Optional.empty();
    }
    
    determinedType = target.normalize(tenv, lineNum, columnNum).get();

    if (target instanceof ReferenceType) {
      // no point casting up, since every B is an A, but not every A is a B
      if (!tenv.typeGraph.canCastTo(valueTy, CastDirection.DOWN, determinedType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot cast from " + valueTy + " down to target type " + determinedType, lineNum, columnNum);
        return Optional.empty();
      }
    } else {
      if (!tenv.typeGraph.canCastTo(valueTy, CastFilter.UP_OR_DOWN_OR_SIBLING, determinedType)) {
        tenv.msgState.addMessage(MsgType.ERROR, MsgClass.TYPECHECK,
            "Cannot cast from " + valueTy + " to target type " + determinedType, lineNum, columnNum);
        return Optional.empty();
      }
    }

    return Optional.of(new TypecheckRet(determinedType));
  }

  @Override
  public Value interp(Environment env) throws InterpError, FatalMessageException {
    Value val = value.interp(env);
    Type valType = val.getType();

    if (!env.typeGraph.canCastTo(valType, CastDirection.DOWN, target)) {
      throw new InterpError("cast from value type " + value.getDeterminedType() + " to type "
          + target + " failed with actual value type " + valType, lineNum, columnNum);
    }
    return val;
  }

  @Override
  public AstProto.Ast.Builder serialize() throws SerializationError {
    AstProto.Ast.Builder document = preSerialize();

    AstProto.CastExpression.Builder subDoc = AstProto.CastExpression.newBuilder();

    subDoc.setTarget(target.serialize().build());
    subDoc.setValue(value.serialize().build());

    document.setCastExpression(subDoc);
    return document;
  }

  public static CastExpression deserialize(int lineNum, int columnNum,
      List<CommentAttr> preComments, List<CommentAttr> postComments,
      AstProto.CastExpression document) throws SerializationError {
    return new CastExpression(lineNum, columnNum, preComments, postComments,
        AbstractType.deserialize(document.getTarget()),
        AbstractExpression.deserialize(document.getValue()));
  }

  @Override
  public Map<String, Type> findFreeVariables() {
    return value.findFreeVariables();
  }

  @Override
  public boolean compare(Ast other) {
    if (!(other instanceof CastExpression)) {
      return false;
    }
    CastExpression ourOther = (CastExpression) other;
    return ourOther.target.equals(target) && ourOther.value.compare(value);
  }

  @Override
  public Ast replace(Ast target, Ast with) {
    if (compare(target)) {
      return with;
    }
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        this.target, (Expression) value.replace(target, with));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Ast> findByOriginalLabel(int findLabel) {
    if (getOriginalLabel() == findLabel) {
      return Nullable.of(this);
    }
    return value.findByOriginalLabel(findLabel);
  }

  @Override
  public Pair<List<DeclarationStatement>, Ast> toAnf(AnfTune tuning) {
    List<DeclarationStatement> insert = new LinkedList<>();

    Pair<List<DeclarationStatement>, Ast> valuePair = value.toAnf(tuning);
    insert.addAll(valuePair.a);

    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        this.target, (Expression) valuePair.b);
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return new Pair<List<DeclarationStatement>, Ast>(insert, val);
  }

  @Override
  public Expression replaceType(Map<Type, Type> mapFromTo) {
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        target.replaceType(mapFromTo), value.replaceType(mapFromTo));
    val.determinedType = determinedType.replaceType(mapFromTo);
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Optional<Ast> replaceVtableAccess(Project project, MsgState msgState)
      throws FatalMessageException {
    Optional<Ast> tmp = value.replaceVtableAccess(project, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments, target,
        (Expression) tmp.get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Optional<Nullable<Expression>> normalizeType(boolean keepCurrentModRelative,
      ModuleType currentModule, EffectiveImports effectiveImports, MsgState msgState)
      throws FatalMessageException {
    Optional<Nullable<Type>> tmpTarget = target.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    Optional<Nullable<Expression>> tmpValue = value.normalizeType(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmpTarget.isPresent() || !tmpValue.isPresent()) {
      return Optional.empty();
    }
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        tmpTarget.get().get(), tmpValue.get().get());
    Optional<Nullable<Type>> tmp = determinedType.basicNormalize(keepCurrentModRelative, currentModule,
        effectiveImports, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }
    val.determinedType = tmp.get().get();
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(Nullable.of(val));
  }

  @Override
  public void addDependency(ClassGraph modDepGraph, Node ourModNode, TypeEnvironment tenv)
      throws FatalMessageException {
    target.addDependency(modDepGraph, ourModNode, tenv, lineNum, columnNum);
    value.addDependency(modDepGraph, ourModNode, tenv, lineNum, columnNum);
  }

  @Override
  public boolean containsType(Set<Type> types) {
    return target.containsType(types) || value.containsType(types);
  }

  @Override
  public boolean containsAst(Set<Ast> asts) {
    if (asts.contains(this)) {
      return true;
    }
    return value.containsAst(asts);
  }

  @Override
  public Optional<ResolveExcRet> resolveExceptions(Project project,
      ModuleType currentModule, AuditResolveExceptions auditEntry,
      RenamesOfInterest jsmntGlobalRenamings, Nullable<FunctionType> withinFn, boolean withinFnStatement, boolean enableResolution,
      String hitExcId, int stackSize, MsgState msgState) throws FatalMessageException {

    Optional<ResolveExcRet> tmp = value.resolveExceptions(project, currentModule,
        auditEntry, jsmntGlobalRenamings, withinFn, withinFnStatement, enableResolution, hitExcId, stackSize, msgState);
    if (!tmp.isPresent()) {
      return Optional.empty();
    }

    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments, target,
        (Expression) tmp.get().ast.get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(new ResolveExcRet(tmp.get().preStatements, val));
  }

  @Override
  public int calculateSize(TypeEnvironment tenv, int lineNum, int columnNum)
      throws FatalMessageException {
    return determinedType.calculateSize(tenv, lineNum, columnNum);
  }

  @Override
  public Expression insertDestructorCalls(ScopedIdentifiers scopedIds,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments, target,
        value.insertDestructorCalls(scopedIds, destructorRenamings));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

  @Override
  public Nullable<Expression> liftClassDeclarations(List<ClassDeclaration> liftedClasses,
      TypeReplacements replacements) throws FatalMessageException {
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments, target,
        value.liftClassDeclarations(liftedClasses, replacements).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Nullable.of(val);
  }

  @Override
  public Optional<Expression> resolveUserTypes(MsgState msgState) throws FatalMessageException {
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        determinedType,
        value.resolveUserTypes(msgState).get());
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return Optional.of(val);
  }

  @Override
  public Expression addThisExpression(ClassHeader classHeader) {
    CastExpression val = new CastExpression(lineNum, columnNum, preComments, postComments,
        target,
        value.addThisExpression(classHeader));
    val.determinedType = determinedType;
    val.setOriginalLabel(getOriginalLabel());
    return val;
  }

}
