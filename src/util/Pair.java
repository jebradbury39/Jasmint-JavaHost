package util;

public class Pair<K,V> {
  public K a;
  public V b;
  
  public Pair(K a, V b) {
    this.a = a;
    this.b = b;
  }
  
  @Override
  public String toString() {
    return "(" + a + ", " + b + ")";
  }
  
  @Override
  public int hashCode() {
    return a.hashCode() + b.hashCode();
  }
  
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Pair)) {
      return false;
    }
    Pair otherPair = (Pair) other;
    
    return a.equals(otherPair.a) && b.equals(otherPair.b);
  }
}
