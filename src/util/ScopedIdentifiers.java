package util;

import ast.Program.DestructorRenamings;
import ast.Statement;
import errors.FatalMessageException;
import errors.Nullable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import typecheck.DotAccessType;
import typecheck.Type;

public class ScopedIdentifiers {
  public static enum ScopeFilter {
    BLOCK, //normal end of a block
    LOOP, //break and continue
    FUNCTION, //return
    GLOBAL //should only be one, with no identifiers
  }
  
  public final Nullable<ScopedIdentifiers> parent; //our parent block statement (up until fn decl)
  
  public final ScopeFilter scopeType;
  public Map<String, DotAccessType> identifiers = new HashMap<>(); //our block statement
  
  public ScopedIdentifiers(ScopeFilter scopeType) {
    this.parent = Nullable.empty();
    this.scopeType = scopeType;
  }
  
  public ScopedIdentifiers(ScopedIdentifiers parent, ScopeFilter scopeType) {
    this.parent = Nullable.of(parent);
    this.scopeType = scopeType;
  }
  
  public ScopedIdentifiers extend(ScopeFilter scopeType) {
    return new ScopedIdentifiers(this, scopeType);
  }

  public void add(String name, Type type) {
    if (type instanceof DotAccessType) {
      identifiers.put(name, (DotAccessType) type);
    }
  }

  public List<Statement> getDestructors(ScopeFilter filter,
      DestructorRenamings destructorRenamings) throws FatalMessageException {
    //first collect all relevant ids
    Map<String, DotAccessType> filterIds = new HashMap<>();
    
    Nullable<ScopedIdentifiers> track = Nullable.of(this);
    while (track.isNotNull()) {
      filterIds.putAll(track.get().identifiers);
      if (track.get().scopeType == filter) {
        break;
      }
      track = track.get().parent;
    }
    
    List<Statement> statements = new LinkedList<>();
    
    for (Entry<String, DotAccessType> entry : filterIds.entrySet()) {
      Optional<Statement> tmp = entry.getValue().createDestructorCall(entry.getKey(),
          destructorRenamings);
      if (tmp.isPresent()) {
        statements.add(tmp.get());
      }
    }
    
    return statements;
  }
}
