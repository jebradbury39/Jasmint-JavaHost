package util;

public class Triplet<X, Y, Z> {
  public X x;
  public Y y;
  public Z z;
  
  public Triplet(X x, Y y, Z z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  @Override
  public String toString() {
    return "(" + x + ", " + y + ", " + z + ")";
  }
}
