package util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreeNode<T> {
  public final T item;
  private TreeNode<T> parent = null;
  private List<TreeNode<T>> children = new ArrayList<TreeNode<T>>();
  
  public TreeNode(T item) {
    this.item = item;
  }
  
  public TreeNode<T> getParent() {
    return parent;
  }
  
  public TreeNode<T> addChild(T child) {
    TreeNode<T> x = new TreeNode<T>(child);
    x.parent = this;
    children.add(x);
    return x;
  }
  
  public TreeNode<T> addChild(TreeNode<T> child) {
    child.parent = this;
    children.add(child);
    return child;
  }
  
  public TreeNode<T> findChild(T child) {
    for (TreeNode<T> c : children) {
      if (c.item.equals(child)) {
        return c;
      }
    }
    return null;
  }
  
  //breadth-first ordering
  public List<T> bfo() {
    List<T> ordered = new ArrayList<T>();
    ordered.add(item);
    
    LinkedList<TreeNode<T>> queue = new LinkedList<TreeNode<T>>();
    queue.addAll(children);
    
    while (!queue.isEmpty()) {
      TreeNode<T> child = queue.poll();
      ordered.add(child.item);
      queue.addAll(child.children);
    }
    return ordered;
  }
}
