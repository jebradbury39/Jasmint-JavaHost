package transpile;

import errors.Nullable;
import java.io.IOException;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import typecheck.ModuleType;
import typecheck.ModuleType.ProjectNameVersion;

public abstract class BuiltinGenerator {
  
  public final String namespace;
  public final NamespaceName namespaceName;
  private BuiltinFunctionOverloads builtinOverloads;

  public BuiltinGenerator(String namespace) {
    this.namespace = namespace;
    this.namespaceName = new NamespaceName(
        new ProjectNameVersion(false, namespace, 0, 1),
        new ModuleType(Nullable.empty(), "builtin"));
  }
  
  void setBuiltinOverloads(BuiltinFunctionOverloads builtinOverloads) {
    this.builtinOverloads = builtinOverloads;
  }
  
  protected BuiltinFunctionOverloads getBuiltinOverloads() {
    return builtinOverloads;
  }
  
  public abstract void generate() throws IOException;
}
