package transpile;

import errors.FatalMessageException;
import errors.Nullable;
import header.ClassHeader;
import header.EnumHeader;
import header.ProgHeader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import typecheck.ModuleType.ProjectNameVersion;
import typecheck.ReferenceType;
import typecheck.Type;
import typecheck.UserDeclType;
import util.Pair;

public abstract class BuiltinFunctionOverloads {
  // a 'namespace' is projectVersionName.<module.path> This will ensure uniqueness, and
  // methods already exist to establish these namespaces and which are used by a Program
  
  // map project to all the builtin function names by arg types (must be exact match - no casting)
  public static class FunctionOverload {
    private final BuiltinFunctionOverloads withinOverloads;
    public final String overloadedName; // e.g. custom_to_string
    private String newName; // e.g. custom_to_string_int
    public final List<Type> argTypes;
    private final Nullable<BuiltinFnGen> builtinFnGen;
    private Map<List<Type>, String> generatedFor = new HashMap<>();
    
    public FunctionOverload(BuiltinFunctionOverloads withinOverloads,
        String overloadedName, String newName, List<Type> argTypes,
        Nullable<BuiltinFnGen> builtinFnGen) {
      this.withinOverloads = withinOverloads;
      this.overloadedName = overloadedName;
      this.newName = newName;
      this.argTypes = argTypes;
      this.builtinFnGen = builtinFnGen;
    }
    
    @Override
    public String toString() {
      return newName + "(" + argTypes + ")";
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof FunctionOverload)) {
        return false;
      }
      FunctionOverload fnOther = (FunctionOverload) other;
      return newName.equals(fnOther.newName);
    }
    
    @Override
    public int hashCode() {
      return newName.hashCode();
    }
    
    /*
     * Returns (newName, generated) - generated is needed for templates
     */
    public Pair<String, String> getNewName(Set<NamespaceName> usingNamespaces,
        List<Type> realArgTypes) {
      String generated = ""; // only fill it in the first time
      String name = newName;
      if (builtinFnGen.isNotNull()) {
        name = generatedFor.get(realArgTypes);
        if (name == null) {
          Pair<String, String> tmp = builtinFnGen.get().function(withinOverloads, this, usingNamespaces,
              realArgTypes);
          name = tmp.a;
          generated = tmp.b;
          generatedFor.put(realArgTypes, name);
        }
      }
      return new Pair<>(name, generated);
    }
  }
  
  public static class FunctionOverloadReturn {
    public final FunctionOverload overload;
    public final Set<NamespaceName> usingNamespaces;
    public final List<Type> realArgTypes;
    
    public FunctionOverloadReturn(FunctionOverload overload, Set<NamespaceName> usingNamespaces,
        List<Type> realArgTypes) {
      this.overload = overload;
      this.usingNamespaces = usingNamespaces;
      this.realArgTypes = realArgTypes;
    }
    
    public Pair<String, String> getNewName() {
      return overload.getNewName(usingNamespaces, realArgTypes);
    }
  }
  
  public static class NamespaceName {
    public final ProjectNameVersion project;
    public final UserDeclType moduleType; // abs path
    
    public NamespaceName(ProjectNameVersion project, UserDeclType moduleType) {
      this.project = project;
      this.moduleType = moduleType;
    }
    
    @Override
    public String toString() {
      return project + "." + moduleType;
    }
    
    @Override
    public boolean equals(Object other) {
      if (!(other instanceof NamespaceName)) {
        return false;
      }
      NamespaceName ourOther = (NamespaceName) other;
      return project.equals(ourOther.project) && moduleType.equals(ourOther.moduleType);
    }
    
    @Override
    public int hashCode() {
      return project.hashCode() + moduleType.hashCode();
    }
  }
  
  public static class Namespace {
    public final NamespaceName namespaceName; // e.g. myproject_0_1.module_a
    public final Map<String, Set<FunctionOverload>> overloadedFunctions;
    
    public Namespace(NamespaceName namespaceName, Map<String,
        Set<FunctionOverload>> overloadedFunctions) {
      this.namespaceName = namespaceName;
      this.overloadedFunctions = overloadedFunctions;
    }
    
    @Override
    public String toString() {
      return overloadedFunctions.toString();
    }
  }
  
  protected Nullable<BuiltinFunctionOverloads> parent = Nullable.empty();
  protected Map<NamespaceName, Namespace> namespaces = new HashMap<>();
  
  public final BuiltinGenerator builtinGenerator;
  
  public BuiltinFunctionOverloads(BuiltinGenerator generator) {
    this.builtinGenerator = generator;
    generator.setBuiltinOverloads(this);
  }
  
  public abstract BuiltinFunctionOverloads extend();
  
  // transpiler has to implement this
  public abstract void populate(Project project)
      throws FatalMessageException;
  
  protected void populateHelper(Project project, String overloadName)
      throws FatalMessageException {

    // traverse buildLibs as well (recursive)
    for (Project lib : project.getBuildLibs()) {
      populateHelper(lib, overloadName);
    }

    // traverse every module, add it to this via scanning the ProgHeader for class
    // types and creating custom_to_string
    for (ModuleEndpoint moduleEp : project.getSources()) {
      moduleEp.load();
      LoadedModuleHandle modHandle = (LoadedModuleHandle) moduleEp.getHandle();
      ProgHeader progHeader = modHandle.moduleBuildObj.progHeader.applyTransforms(project,
          project.getAuditTrail(), modHandle.moduleBuildObj, modHandle.registeredGenerics,
          moduleEp.msgState);

      processProgHeader(project, progHeader, overloadName);
    }
  }
  
  private void processProgHeader(Project project, ProgHeader progHeader, String overloadName) {
    NamespaceName namespaceName = new NamespaceName(project.getModProjectNameVersion(),
        progHeader.moduleType);
    Map<String, Set<FunctionOverload>> overloadedFunctions = new HashMap<>();
    Set<FunctionOverload> customToString = new HashSet<>();

    for (ClassHeader classHeader : progHeader.classes) {
      List<Type> argTypes = new LinkedList<>();

      // add stack type
      argTypes.add(classHeader.absName); // no inner types at this point
      FunctionOverload fn = new FunctionOverload(this, overloadName,
          overloadName + "_" + classHeader.name, argTypes,
          Nullable.empty());
      customToString.add(fn);

      // add ptr type
      argTypes = new LinkedList<>();
      argTypes.add(new ReferenceType(classHeader.absName.asInstanceType())); // no inner types at this point
      fn = new FunctionOverload(this, overloadName,
          overloadName + "_" + classHeader.name + "_ptr", argTypes,
          Nullable.empty());
      customToString.add(fn);
    }

    for (EnumHeader enumHeader : progHeader.enums) {
      List<Type> argTypes = new LinkedList<>();

      // add stack type
      argTypes.add(enumHeader.absName); // no inner types at this point
      FunctionOverload fn = new FunctionOverload(this, overloadName,
          overloadName + "_" + enumHeader.name, argTypes,
          Nullable.empty());
      customToString.add(fn);

      // add ptr type
      argTypes = new LinkedList<>();
      argTypes.add(new ReferenceType(enumHeader.absName.asInstanceType())); // no inner types at this point
      fn = new FunctionOverload(this, overloadName,
          overloadName + "_" + enumHeader.name + "_ptr", argTypes,
          Nullable.empty());
      customToString.add(fn);
    }

    overloadedFunctions.put(overloadName, customToString);

    Namespace namespace = new Namespace(namespaceName, overloadedFunctions);
    namespaces.put(namespaceName, namespace);
  }
  
  public Set<FunctionOverload> filterAvailableOverloads(Set<NamespaceName> usingNamespaces,
      String overloadedName) {
    Set<FunctionOverload> overloads = new HashSet<>();
    
    if (parent.isNotNull()) {
      overloads.addAll(parent.get().filterAvailableOverloads(usingNamespaces, overloadedName));
    }
    
    for (NamespaceName namespace : usingNamespaces) {
      Namespace tmp = namespaces.get(namespace);
      if (tmp == null) {
        if (parent.isNull()) {
          throw new IllegalArgumentException("unknown namespace: " + namespace);
        }
        continue;
      }
      
      Set<FunctionOverload> fns = tmp.overloadedFunctions.get(overloadedName);
      if (fns != null) {
        overloads.addAll(fns);
      }
    }
    
    return overloads;
  }
  
  public Nullable<FunctionOverloadReturn> lookupOverload(Set<NamespaceName> usingNamespaces,
      String overloadedName, Type argType) {
    
    List<Type> argTypes = new LinkedList<>();
    argTypes.add(argType);
    return lookupOverload(usingNamespaces, overloadedName, argTypes);
  }
  
  public Nullable<FunctionOverloadReturn> lookupOverload(Set<NamespaceName> usingNamespaces,
      String overloadedName, List<Type> argTypes) {
    
    List<Type> sanitizedArgTypes = new LinkedList<>();
    for (Type argType : argTypes) {
      sanitizedArgTypes.add(argType.asClassDeclType());
    }
    
    // filter out all possible options using the namespaces
    // selecting only those that match the overloadedName.
    Set<FunctionOverload> overloads = filterAvailableOverloads(usingNamespaces, overloadedName);
    
    // Of those, there must be a single match with the same argTypes.
    Nullable<FunctionOverloadReturn> match = Nullable.empty();
    for (FunctionOverload overload : overloads) {
      if (sanitizedArgTypes.equals(overload.argTypes)) {
        if (match.isNotNull()) {
          throw new IllegalArgumentException("multiple matched arg types for: " + overload.newName);
        }
        match = Nullable.of(new FunctionOverloadReturn(overload, usingNamespaces,
            sanitizedArgTypes));
      }
    }
    
    // return the matched newName, if found
    return match;
  }
  
  protected static String typeToIdentifier(Type ty) {
    String result = ty.toString();
    result = result.replace('.', '_');
    result = result.replace("*", "_ptr");
    result = result.replace("[", "S_");
    result = result.replace("]", "_E");
    result = result.replace("{", "SB_");
    result = result.replace("}", "_EB");

    final String specialCharsPattern = "[^a-zA-Z0-9_]";
    String tmp = result.replaceAll(specialCharsPattern, "");
    if (!tmp.equals(result)) {
      throw new IllegalArgumentException("Failed to remove all special chars from: " + result);
    }
    return result;
  }
}
