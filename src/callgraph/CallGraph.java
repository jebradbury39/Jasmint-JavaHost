package callgraph;

import errors.FatalMessageException;
import errors.Message.MsgClass;
import errors.Message.MsgType;
import errors.MsgState;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import typecheck.FunctionType;
import util.DotGen;

// shared across all modules
/*
 * Typecheck all the modules once. While doing this, add any sandboxed functions to the callgraph.
 * This includes any functions that are part of a class. If that class has a public function,
 * then that function must be sandboxed. Also, classes may expose other classes (as fields or
 * returns), so the classgraph must be recursively traversed here.
 * At the end of typechecking, determine the full list of sandboxed functions by traversing the graph.
 * If A() calls B() and B is sandboxed, then A must also be sandboxed.
 * Verify that none of the sandboxed functions contain any illegal ASTs (such as lambdas or FFI).
 * On a later step, use the full list of sandboxed functions to determine which functions to copy
 * and modify to have fn_ctx + checks. Since Jasmint supports overloading, we don't have to worry
 * about changing the function's name.
 * After this, all overloads will be resolved.
 */

public class CallGraph {
  private Map<CallNode, CallNode> nodes = new HashMap<>();
  private final MsgState msgState;
  
  public CallGraph(MsgState msgState) {
    this.msgState = msgState;
  }
  
  @Override
  public String toString() {
    return toDotFormat().toString();
  }
  
  public DotGen toDotFormat() {
    DotGen dotGraph = new DotGen();
    
    for (CallNode node : nodes.values()) {
      node.updateDotGraph(dotGraph);
    }
    
    return dotGraph;
  }
  
  public void clear() {
    nodes.clear();
  }
  
  // get node and create if it does not exist
  private CallNode getNode(String name, FunctionType fullType, boolean withinSandboxExpr)
      throws FatalMessageException {
    
    if (!fullType.lambdaStatus.isNonLambda()) {
        msgState.addMessage(MsgType.INTERNAL, MsgClass.SANDBOX,
            "internal: callgraph cannot add fun with lambda status: " + fullType.lambdaStatus);
    }
    
    CallNode tmpNode = new CallNode(name, fullType, withinSandboxExpr);
    CallNode node = nodes.get(tmpNode);
    if (node == null) {
      node = tmpNode;
      nodes.put(node, node);
    }
    if (withinSandboxExpr) {
      node.withinSandboxExpr = withinSandboxExpr;
    }
    
    return node;
  }
  
  // this is called by Function for non-lambdas and pseudo-lambdas
  public void addNode(String name, FunctionType fnDeclType, boolean withinSandboxExpr)
      throws FatalMessageException {
    getNode(name, fnDeclType, withinSandboxExpr);
  }
  
  // this is called by FunctionCallExpression only for non-lambdas and pseudo-lambdas
  // withinSandboxExpr is true if directly under sandbox
  public void addLink(String withinFnName, FunctionType withinFn,
      String fnName, FunctionType fnCallType, boolean withinSandboxExpr)
      throws FatalMessageException {
    /*
     * If we have not seen the parent fn decl yet, then it must NOT be within a sandbox expr
     */
    CallNode parentNode = getNode(withinFnName, withinFn, false);
    CallNode childNode = getNode(fnName, fnCallType, withinSandboxExpr);
    parentNode.addDownlink(childNode);
  }

  public void cloneFrom(CallGraph srcCallGraph) {
    nodes = srcCallGraph.nodes; // set to be the same so any modifications are shared
  }

  public Set<CallNode> determineSandboxedFns() {
    // ensure we visit every node, starting with those that are known sandbox functions
    Queue<CallNode> sandboxFunctions = new LinkedList<>();
    Queue<CallNode> nonSandboxFunctions = new LinkedList<>();
    Queue<CallNode> allFunctions = new LinkedList<>();
    
    for (CallNode node : nodes.values()) {
      if (node.withinSandboxExpr) {
        sandboxFunctions.add(node);
      } else {
        nonSandboxFunctions.add(node);
      }
    }
    allFunctions.addAll(sandboxFunctions);
    allFunctions.addAll(nonSandboxFunctions);
    
    // now loop through, marking nodes as visited. Use dfs, passing down sandboxed
    // if a sandboxed fn calls another fn, that child fn also becomes sandboxed
    Set<CallNode> sandboxedFunctions = new HashSet<>();
    Set<CallNode> visited = new HashSet<>();
    while (!allFunctions.isEmpty()) {
      CallNode node = allFunctions.poll();
      node.dfs(visited, sandboxedFunctions, false);
    }
    
    return sandboxedFunctions;
  }
}
