#include <iostream>

class A {
public:
	A* self;
	int x;
	
	A(int v) {
		self = this;
		x = v;
		std::cout << "#" << self << std::endl;
	}
	
	void print() {
		std::cout << "print: " << this->x << " vs " << x << std::endl;
	}
};

A createA() {
	return A(5);
}

int main() {
	A a = A(5);//createA();
	std::cout << a.self << ", " << a.x << std::endl;
	a.print();
	A b = a;
	std::cout << b.self << ", " << b.x << std::endl;
	b.print();
	b.self->x = 12;
	std::cout << a.self << ", " << a.x << std::endl;
	a.print();
	std::cout << b.self << ", " << b.x << std::endl;
	b.print();
	return 0;
}

/*
 * 'this' clearly only really works with heap-based objects. Anything on the stack, with copying,
 * causes odd problems that are clearly logical but again, they are just unexpected.
 */