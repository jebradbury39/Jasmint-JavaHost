package test_types;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
  TestTypeSystem.class,
  TestTypeGraph.class,
  TestTreeNode.class,
  TestRegressions.class
})
public class AllTests {

}
