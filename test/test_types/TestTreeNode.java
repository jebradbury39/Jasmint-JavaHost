package test_types;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import util.TreeNode;

public class TestTreeNode {

  @Test
  public void testBFO() {
    TreeNode<String> tree = new TreeNode<String>("a");
    tree.addChild("b");
    tree.addChild("c");
    tree.findChild("b").addChild("d");
    
    List<String> expect = new ArrayList<String>();
    expect.add("a");
    expect.add("b");
    expect.add("c");
    expect.add("d");
    assertEquals(expect, tree.bfo());
  }

}
