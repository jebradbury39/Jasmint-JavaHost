FROM frolvlad/alpine-glibc:latest

RUN apk upgrade --update
RUN apk add curl

RUN apk --no-cache add git
RUN git --version

RUN apk --no-cache add openjdk11
RUN java -version

RUN apk --no-cache add g++
RUN g++ --version

RUN apk --no-cache add python3
RUN python3 --version

ENV GRADLE_VERSION 5.1
ENV GRADLE_HOME /usr/local/gradle-$GRADLE_VERSION
ENV PATH=$GRADLE_HOME/bin:$PATH

RUN \
   cd /usr/local && \
   curl -L https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -o gradle-${GRADLE_VERSION}-bin.zip && \
   ls && \
   pwd && \
   unzip gradle-${GRADLE_VERSION}-bin.zip && \
   rm gradle-${GRADLE_VERSION}-bin.zip

